package com.ruoyi.project.system.hiddendanger.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.project.system.hiddendanger.mapper.HiddendangerMapper;
import com.ruoyi.project.system.hiddendanger.domain.Hiddendanger;
import com.ruoyi.project.system.hiddendanger.service.IHiddendangerService;
import com.ruoyi.common.utils.text.Convert;

/**
 * 安全隐患信息Service业务层处理
 * 
 * @author chenhe
 * @date 2020-01-06
 */
@Service
public class HiddendangerServiceImpl implements IHiddendangerService 
{
    @Autowired
    private HiddendangerMapper hiddendangerMapper;

    /**
     * 查询安全隐患信息
     * 
     * @param dangerid 安全隐患信息ID
     * @return 安全隐患信息
     */
    @Override
    public Hiddendanger selectHiddendangerById(Integer dangerid)
    {
        return hiddendangerMapper.selectHiddendangerById(dangerid);
    }

    /**
     * 查询安全隐患信息列表
     * 
     * @param hiddendanger 安全隐患信息
     * @return 安全隐患信息
     */
    @Override
    public List<Hiddendanger> selectHiddendangerList(Hiddendanger hiddendanger)
    {
        return hiddendangerMapper.selectHiddendangerList(hiddendanger);
    }

    /**
     * 新增安全隐患信息
     * 
     * @param hiddendanger 安全隐患信息
     * @return 结果
     */
    @Override
    public int insertHiddendanger(Hiddendanger hiddendanger)
    {
        return hiddendangerMapper.insertHiddendanger(hiddendanger);
    }

    /**
     * 修改安全隐患信息
     * 
     * @param hiddendanger 安全隐患信息
     * @return 结果
     */
    @Override
    public int updateHiddendanger(Hiddendanger hiddendanger)
    {
        return hiddendangerMapper.updateHiddendanger(hiddendanger);
    }

    /**
     * 删除安全隐患信息对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteHiddendangerByIds(String ids)
    {
        return hiddendangerMapper.deleteHiddendangerByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除安全隐患信息信息
     * 
     * @param dangerid 安全隐患信息ID
     * @return 结果
     */
    @Override
    public int deleteHiddendangerById(Integer dangerid)
    {
        return hiddendangerMapper.deleteHiddendangerById(dangerid);
    }
}
