package com.ruoyi.project.system.hiddendanger.controller;

import java.util.List;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.ruoyi.framework.aspectj.lang.annotation.Log;
import com.ruoyi.framework.aspectj.lang.enums.BusinessType;
import com.ruoyi.project.system.hiddendanger.domain.Hiddendanger;
import com.ruoyi.project.system.hiddendanger.service.IHiddendangerService;
import com.ruoyi.framework.web.controller.BaseController;
import com.ruoyi.framework.web.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.framework.web.page.TableDataInfo;

/**
 * 安全隐患信息Controller
 * 
 * @author chenhe
 * @date 2020-01-06
 */
@Controller
@RequestMapping("/system/hiddendanger")
public class HiddendangerController extends BaseController
{
    private String prefix = "system/hiddendanger";

    @Autowired
    private IHiddendangerService hiddendangerService;

    @RequiresPermissions("system:hiddendanger:view")
    @GetMapping()
    public String hiddendanger()
    {
        return prefix + "/hiddendanger";
    }

    /**
     * 查询安全隐患信息列表
     */
    @RequiresPermissions("system:hiddendanger:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(Hiddendanger hiddendanger)
    {
        startPage();
        List<Hiddendanger> list = hiddendangerService.selectHiddendangerList(hiddendanger);
        return getDataTable(list);
    }

    /**
     * 导出安全隐患信息列表
     */
    @RequiresPermissions("system:hiddendanger:export")
    @Log(title = "安全隐患信息", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(Hiddendanger hiddendanger)
    {
        List<Hiddendanger> list = hiddendangerService.selectHiddendangerList(hiddendanger);
        ExcelUtil<Hiddendanger> util = new ExcelUtil<Hiddendanger>(Hiddendanger.class);
        return util.exportExcel(list, "hiddendanger");
    }

    /**
     * 新增安全隐患信息
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存安全隐患信息
     */
    @RequiresPermissions("system:hiddendanger:add")
    @Log(title = "安全隐患信息", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(Hiddendanger hiddendanger)
    {
        return toAjax(hiddendangerService.insertHiddendanger(hiddendanger));
    }

    /**
     * 修改安全隐患信息
     */
    @GetMapping("/edit/{dangerid}")
    public String edit(@PathVariable("dangerid") Integer dangerid, ModelMap mmap)
    {
        Hiddendanger hiddendanger = hiddendangerService.selectHiddendangerById(dangerid);
        mmap.put("hiddendanger", hiddendanger);
        return prefix + "/edit";
    }

    /**
     * 修改保存安全隐患信息
     */
    @RequiresPermissions("system:hiddendanger:edit")
    @Log(title = "安全隐患信息", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(Hiddendanger hiddendanger)
    {
        return toAjax(hiddendangerService.updateHiddendanger(hiddendanger));
    }

    /**
     * 删除安全隐患信息
     */
    @RequiresPermissions("system:hiddendanger:remove")
    @Log(title = "安全隐患信息", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(hiddendangerService.deleteHiddendangerByIds(ids));
    }
}
