package com.ruoyi.project.system.hiddendanger.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.framework.aspectj.lang.annotation.Excel;
import com.ruoyi.framework.web.domain.BaseEntity;
import java.util.Date;

/**
 * 安全隐患信息对象 hiddendanger
 * 
 * @author chenhe
 * @date 2020-01-06
 */
public class Hiddendanger extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 安全隐患编号（主键） */
    private Integer dangerid;

    /** 名称 */
    @Excel(name = "名称")
    private String name;

    /** 隐患阐述 */
    @Excel(name = "隐患阐述")
    private String elaborate;

    /** 类型 */
    @Excel(name = "类型")
    private String type;

    /** 整改建议 */
    @Excel(name = "整改建议")
    private String suggestions;

    /** 期限 */
    @Excel(name = "期限", width = 30, dateFormat = "yyyy-MM-dd")
    private Date deadline;

    /** 检测人员 */
    @Excel(name = "检测人员")
    private String testPerson;

    /** 复查结果 */
    @Excel(name = "复查结果")
    private String reviewResults;

    /** 复查时间 */
    @Excel(name = "复查时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date reviewTime;

    /** 确认签字 */
    @Excel(name = "确认签字")
    private String confirmSign;

    public void setDangerid(Integer dangerid) 
    {
        this.dangerid = dangerid;
    }

    public Integer getDangerid() 
    {
        return dangerid;
    }
    public void setName(String name) 
    {
        this.name = name;
    }

    public String getName() 
    {
        return name;
    }
    public void setElaborate(String elaborate) 
    {
        this.elaborate = elaborate;
    }

    public String getElaborate() 
    {
        return elaborate;
    }
    public void setType(String type) 
    {
        this.type = type;
    }

    public String getType() 
    {
        return type;
    }
    public void setSuggestions(String suggestions) 
    {
        this.suggestions = suggestions;
    }

    public String getSuggestions() 
    {
        return suggestions;
    }
    public void setDeadline(Date deadline) 
    {
        this.deadline = deadline;
    }

    public Date getDeadline() 
    {
        return deadline;
    }
    public void setTestPerson(String testPerson) 
    {
        this.testPerson = testPerson;
    }

    public String getTestPerson() 
    {
        return testPerson;
    }
    public void setReviewResults(String reviewResults) 
    {
        this.reviewResults = reviewResults;
    }

    public String getReviewResults() 
    {
        return reviewResults;
    }
    public void setReviewTime(Date reviewTime) 
    {
        this.reviewTime = reviewTime;
    }

    public Date getReviewTime() 
    {
        return reviewTime;
    }
    public void setConfirmSign(String confirmSign) 
    {
        this.confirmSign = confirmSign;
    }

    public String getConfirmSign() 
    {
        return confirmSign;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("dangerid", getDangerid())
            .append("name", getName())
            .append("elaborate", getElaborate())
            .append("type", getType())
            .append("suggestions", getSuggestions())
            .append("deadline", getDeadline())
            .append("testPerson", getTestPerson())
            .append("reviewResults", getReviewResults())
            .append("reviewTime", getReviewTime())
            .append("confirmSign", getConfirmSign())
            .append("remark", getRemark())
            .toString();
    }
}
