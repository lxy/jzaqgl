package com.ruoyi.project.system.hiddendanger.mapper;

import com.ruoyi.project.system.hiddendanger.domain.Hiddendanger;
import java.util.List;

/**
 * 安全隐患信息Mapper接口
 * 
 * @author chenhe
 * @date 2020-01-06
 */
public interface HiddendangerMapper 
{
    /**
     * 查询安全隐患信息
     * 
     * @param dangerid 安全隐患信息ID
     * @return 安全隐患信息
     */
    public Hiddendanger selectHiddendangerById(Integer dangerid);

    /**
     * 查询安全隐患信息列表
     * 
     * @param hiddendanger 安全隐患信息
     * @return 安全隐患信息集合
     */
    public List<Hiddendanger> selectHiddendangerList(Hiddendanger hiddendanger);

    /**
     * 新增安全隐患信息
     * 
     * @param hiddendanger 安全隐患信息
     * @return 结果
     */
    public int insertHiddendanger(Hiddendanger hiddendanger);

    /**
     * 修改安全隐患信息
     * 
     * @param hiddendanger 安全隐患信息
     * @return 结果
     */
    public int updateHiddendanger(Hiddendanger hiddendanger);

    /**
     * 删除安全隐患信息
     * 
     * @param dangerid 安全隐患信息ID
     * @return 结果
     */
    public int deleteHiddendangerById(Integer dangerid);

    /**
     * 批量删除安全隐患信息
     * 
     * @param dangerids 需要删除的数据ID
     * @return 结果
     */
    public int deleteHiddendangerByIds(String[] dangerids);
}
