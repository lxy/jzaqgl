package com.ruoyi.project.system.riskidentification.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.project.system.riskidentification.mapper.RiskidentificationMapper;
import com.ruoyi.project.system.riskidentification.domain.Riskidentification;
import com.ruoyi.project.system.riskidentification.service.IRiskidentificationService;
import com.ruoyi.common.utils.text.Convert;

/**
 * 风险识别信息Service业务层处理
 * 
 * @author chenhe
 * @date 2020-01-06
 */
@Service
public class RiskidentificationServiceImpl implements IRiskidentificationService 
{
    @Autowired
    private RiskidentificationMapper riskidentificationMapper;

    /**
     * 查询风险识别信息
     * 
     * @param riskidentifyId 风险识别信息ID
     * @return 风险识别信息
     */
    @Override
    public Riskidentification selectRiskidentificationById(Integer riskidentifyId)
    {
        return riskidentificationMapper.selectRiskidentificationById(riskidentifyId);
    }

    /**
     * 查询风险识别信息列表
     * 
     * @param riskidentification 风险识别信息
     * @return 风险识别信息
     */
    @Override
    public List<Riskidentification> selectRiskidentificationList(Riskidentification riskidentification)
    {
        return riskidentificationMapper.selectRiskidentificationList(riskidentification);
    }

    /**
     * 新增风险识别信息
     * 
     * @param riskidentification 风险识别信息
     * @return 结果
     */
    @Override
    public int insertRiskidentification(Riskidentification riskidentification)
    {
        return riskidentificationMapper.insertRiskidentification(riskidentification);
    }

    /**
     * 修改风险识别信息
     * 
     * @param riskidentification 风险识别信息
     * @return 结果
     */
    @Override
    public int updateRiskidentification(Riskidentification riskidentification)
    {
        return riskidentificationMapper.updateRiskidentification(riskidentification);
    }

    /**
     * 删除风险识别信息对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteRiskidentificationByIds(String ids)
    {
        return riskidentificationMapper.deleteRiskidentificationByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除风险识别信息信息
     * 
     * @param riskidentifyId 风险识别信息ID
     * @return 结果
     */
    @Override
    public int deleteRiskidentificationById(Integer riskidentifyId)
    {
        return riskidentificationMapper.deleteRiskidentificationById(riskidentifyId);
    }
}
