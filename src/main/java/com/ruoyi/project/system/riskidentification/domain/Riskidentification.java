package com.ruoyi.project.system.riskidentification.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.framework.aspectj.lang.annotation.Excel;
import com.ruoyi.framework.web.domain.BaseEntity;
import java.util.Date;

/**
 * 风险识别信息对象 riskidentification
 * 
 * @author chenhe
 * @date 2020-01-06
 */
public class Riskidentification extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 风险识别编号（主键） */
    private Integer riskidentifyId;

    /** 名称 */
    @Excel(name = "名称")
    private String name;

    /** 类别 */
    @Excel(name = "类别")
    private String type;

    /** 发生地点 */
    @Excel(name = "发生地点")
    private String address;

    /** 状态 */
    @Excel(name = "状态")
    private String state;

    /** 隐患描述 */
    @Excel(name = "隐患描述")
    private String discription;

    /** 规避方法 */
    @Excel(name = "规避方法")
    private String avoidMethod;

    /** 风险等级 */
    @Excel(name = "风险等级")
    private String riskLevel;

    /** 危险因素 */
    @Excel(name = "危险因素")
    private String riskFactor;

    /** 时间 */
    @Excel(name = "时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date time;

    public void setRiskidentifyId(Integer riskidentifyId) 
    {
        this.riskidentifyId = riskidentifyId;
    }

    public Integer getRiskidentifyId() 
    {
        return riskidentifyId;
    }
    public void setName(String name) 
    {
        this.name = name;
    }

    public String getName() 
    {
        return name;
    }
    public void setType(String type) 
    {
        this.type = type;
    }

    public String getType() 
    {
        return type;
    }
    public void setAddress(String address) 
    {
        this.address = address;
    }

    public String getAddress() 
    {
        return address;
    }
    public void setState(String state) 
    {
        this.state = state;
    }

    public String getState() 
    {
        return state;
    }
    public void setDiscription(String discription) 
    {
        this.discription = discription;
    }

    public String getDiscription() 
    {
        return discription;
    }
    public void setAvoidMethod(String avoidMethod) 
    {
        this.avoidMethod = avoidMethod;
    }

    public String getAvoidMethod() 
    {
        return avoidMethod;
    }
    public void setRiskLevel(String riskLevel) 
    {
        this.riskLevel = riskLevel;
    }

    public String getRiskLevel() 
    {
        return riskLevel;
    }
    public void setRiskFactor(String riskFactor) 
    {
        this.riskFactor = riskFactor;
    }

    public String getRiskFactor() 
    {
        return riskFactor;
    }
    public void setTime(Date time) 
    {
        this.time = time;
    }

    public Date getTime() 
    {
        return time;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("riskidentifyId", getRiskidentifyId())
            .append("name", getName())
            .append("type", getType())
            .append("address", getAddress())
            .append("state", getState())
            .append("discription", getDiscription())
            .append("avoidMethod", getAvoidMethod())
            .append("riskLevel", getRiskLevel())
            .append("riskFactor", getRiskFactor())
            .append("time", getTime())
            .append("remark", getRemark())
            .toString();
    }
}
