package com.ruoyi.project.system.riskidentification.controller;

import java.util.List;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.ruoyi.framework.aspectj.lang.annotation.Log;
import com.ruoyi.framework.aspectj.lang.enums.BusinessType;
import com.ruoyi.project.system.riskidentification.domain.Riskidentification;
import com.ruoyi.project.system.riskidentification.service.IRiskidentificationService;
import com.ruoyi.framework.web.controller.BaseController;
import com.ruoyi.framework.web.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.framework.web.page.TableDataInfo;

/**
 * 风险识别信息Controller
 * 
 * @author chenhe
 * @date 2020-01-06
 */
@Controller
@RequestMapping("/system/riskidentification")
public class RiskidentificationController extends BaseController
{
    private String prefix = "system/riskidentification";

    @Autowired
    private IRiskidentificationService riskidentificationService;

    @RequiresPermissions("system:riskidentification:view")
    @GetMapping()
    public String riskidentification()
    {
        return prefix + "/riskidentification";
    }

    /**
     * 查询风险识别信息列表
     */
    @RequiresPermissions("system:riskidentification:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(Riskidentification riskidentification)
    {
        startPage();
        List<Riskidentification> list = riskidentificationService.selectRiskidentificationList(riskidentification);
        return getDataTable(list);
    }

    /**
     * 导出风险识别信息列表
     */
    @RequiresPermissions("system:riskidentification:export")
    @Log(title = "风险识别信息", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(Riskidentification riskidentification)
    {
        List<Riskidentification> list = riskidentificationService.selectRiskidentificationList(riskidentification);
        ExcelUtil<Riskidentification> util = new ExcelUtil<Riskidentification>(Riskidentification.class);
        return util.exportExcel(list, "riskidentification");
    }

    /**
     * 新增风险识别信息
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存风险识别信息
     */
    @RequiresPermissions("system:riskidentification:add")
    @Log(title = "风险识别信息", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(Riskidentification riskidentification)
    {
        return toAjax(riskidentificationService.insertRiskidentification(riskidentification));
    }

    /**
     * 修改风险识别信息
     */
    @GetMapping("/edit/{riskidentifyId}")
    public String edit(@PathVariable("riskidentifyId") Integer riskidentifyId, ModelMap mmap)
    {
        Riskidentification riskidentification = riskidentificationService.selectRiskidentificationById(riskidentifyId);
        mmap.put("riskidentification", riskidentification);
        return prefix + "/edit";
    }

    /**
     * 修改保存风险识别信息
     */
    @RequiresPermissions("system:riskidentification:edit")
    @Log(title = "风险识别信息", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(Riskidentification riskidentification)
    {
        return toAjax(riskidentificationService.updateRiskidentification(riskidentification));
    }

    /**
     * 删除风险识别信息
     */
    @RequiresPermissions("system:riskidentification:remove")
    @Log(title = "风险识别信息", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(riskidentificationService.deleteRiskidentificationByIds(ids));
    }
}
