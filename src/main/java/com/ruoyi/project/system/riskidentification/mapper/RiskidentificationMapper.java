package com.ruoyi.project.system.riskidentification.mapper;

import com.ruoyi.project.system.riskidentification.domain.Riskidentification;
import java.util.List;

/**
 * 风险识别信息Mapper接口
 * 
 * @author chenhe
 * @date 2020-01-06
 */
public interface RiskidentificationMapper 
{
    /**
     * 查询风险识别信息
     * 
     * @param riskidentifyId 风险识别信息ID
     * @return 风险识别信息
     */
    public Riskidentification selectRiskidentificationById(Integer riskidentifyId);

    /**
     * 查询风险识别信息列表
     * 
     * @param riskidentification 风险识别信息
     * @return 风险识别信息集合
     */
    public List<Riskidentification> selectRiskidentificationList(Riskidentification riskidentification);

    /**
     * 新增风险识别信息
     * 
     * @param riskidentification 风险识别信息
     * @return 结果
     */
    public int insertRiskidentification(Riskidentification riskidentification);

    /**
     * 修改风险识别信息
     * 
     * @param riskidentification 风险识别信息
     * @return 结果
     */
    public int updateRiskidentification(Riskidentification riskidentification);

    /**
     * 删除风险识别信息
     * 
     * @param riskidentifyId 风险识别信息ID
     * @return 结果
     */
    public int deleteRiskidentificationById(Integer riskidentifyId);

    /**
     * 批量删除风险识别信息
     * 
     * @param riskidentifyIds 需要删除的数据ID
     * @return 结果
     */
    public int deleteRiskidentificationByIds(String[] riskidentifyIds);
}
