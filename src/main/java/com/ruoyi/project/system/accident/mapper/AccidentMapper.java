package com.ruoyi.project.system.accident.mapper;

import com.ruoyi.project.system.accident.domain.Accident;
import java.util.List;

/**
 * 施工事故管理Mapper接口
 * 
 * @author 陈贺
 * @date 2020-01-03
 */
public interface AccidentMapper 
{
    /**
     * 查询施工事故管理
     * 
     * @param accidentId 施工事故管理ID
     * @return 施工事故管理
     */
    public Accident selectAccidentById(Integer accidentId);

    /**
     * 查询施工事故管理列表
     * 
     * @param accident 施工事故管理
     * @return 施工事故管理集合
     */
    public List<Accident> selectAccidentList(Accident accident);

    /**
     * 新增施工事故管理
     * 
     * @param accident 施工事故管理
     * @return 结果
     */
    public int insertAccident(Accident accident);

    /**
     * 修改施工事故管理
     * 
     * @param accident 施工事故管理
     * @return 结果
     */
    public int updateAccident(Accident accident);

    /**
     * 删除施工事故管理
     * 
     * @param accidentId 施工事故管理ID
     * @return 结果
     */
    public int deleteAccidentById(Integer accidentId);

    /**
     * 批量删除施工事故管理
     * 
     * @param accidentIds 需要删除的数据ID
     * @return 结果
     */
    public int deleteAccidentByIds(String[] accidentIds);
}
