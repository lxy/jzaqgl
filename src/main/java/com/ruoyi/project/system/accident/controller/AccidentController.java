package com.ruoyi.project.system.accident.controller;

import java.util.List;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.ruoyi.framework.aspectj.lang.annotation.Log;
import com.ruoyi.framework.aspectj.lang.enums.BusinessType;
import com.ruoyi.project.system.accident.domain.Accident;
import com.ruoyi.project.system.accident.service.IAccidentService;
import com.ruoyi.framework.web.controller.BaseController;
import com.ruoyi.framework.web.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.framework.web.page.TableDataInfo;

/**
 * 施工事故管理Controller
 * 
 * @author 陈贺
 * @date 2020-01-03
 */
@Controller
@RequestMapping("/system/accident")
public class AccidentController extends BaseController
{
    private String prefix = "system/accident";

    @Autowired
    private IAccidentService accidentService;

    @RequiresPermissions("system:accident:view")
    @GetMapping()
    public String accident()
    {
        return prefix + "/accident";
    }

    /**
     * 查询施工事故管理列表
     */
    @RequiresPermissions("system:accident:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(Accident accident)
    {
        startPage();
        List<Accident> list = accidentService.selectAccidentList(accident);
        return getDataTable(list);
    }

    /**
     * 导出施工事故管理列表
     */
    @RequiresPermissions("system:accident:export")
    @Log(title = "施工事故管理", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(Accident accident)
    {
        List<Accident> list = accidentService.selectAccidentList(accident);
        ExcelUtil<Accident> util = new ExcelUtil<Accident>(Accident.class);
        return util.exportExcel(list, "accident");
    }

    /**
     * 新增施工事故管理
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存施工事故管理
     */
    @RequiresPermissions("system:accident:add")
    @Log(title = "施工事故管理", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(Accident accident)
    {
        return toAjax(accidentService.insertAccident(accident));
    }

    /**
     * 修改施工事故管理
     */
    @GetMapping("/edit/{accidentId}")
    public String edit(@PathVariable("accidentId") Integer accidentId, ModelMap mmap)
    {
        Accident accident = accidentService.selectAccidentById(accidentId);
        mmap.put("accident", accident);
        return prefix + "/edit";
    }

    /**
     * 修改保存施工事故管理
     */
    @RequiresPermissions("system:accident:edit")
    @Log(title = "施工事故管理", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(Accident accident)
    {
        return toAjax(accidentService.updateAccident(accident));
    }

    /**
     * 删除施工事故管理
     */
    @RequiresPermissions("system:accident:remove")
    @Log(title = "施工事故管理", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(accidentService.deleteAccidentByIds(ids));
    }
}
