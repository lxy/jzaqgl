package com.ruoyi.project.system.accident.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.project.system.accident.mapper.AccidentMapper;
import com.ruoyi.project.system.accident.domain.Accident;
import com.ruoyi.project.system.accident.service.IAccidentService;
import com.ruoyi.common.utils.text.Convert;

/**
 * 施工事故管理Service业务层处理
 * 
 * @author 陈贺
 * @date 2020-01-03
 */
@Service
public class AccidentServiceImpl implements IAccidentService 
{
    @Autowired
    private AccidentMapper accidentMapper;

    /**
     * 查询施工事故管理
     * 
     * @param accidentId 施工事故管理ID
     * @return 施工事故管理
     */
    @Override
    public Accident selectAccidentById(Integer accidentId)
    {
        return accidentMapper.selectAccidentById(accidentId);
    }

    /**
     * 查询施工事故管理列表
     * 
     * @param accident 施工事故管理
     * @return 施工事故管理
     */
    @Override
    public List<Accident> selectAccidentList(Accident accident)
    {
        return accidentMapper.selectAccidentList(accident);
    }

    /**
     * 新增施工事故管理
     * 
     * @param accident 施工事故管理
     * @return 结果
     */
    @Override
    public int insertAccident(Accident accident)
    {
        return accidentMapper.insertAccident(accident);
    }

    /**
     * 修改施工事故管理
     * 
     * @param accident 施工事故管理
     * @return 结果
     */
    @Override
    public int updateAccident(Accident accident)
    {
        return accidentMapper.updateAccident(accident);
    }

    /**
     * 删除施工事故管理对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteAccidentByIds(String ids)
    {
        return accidentMapper.deleteAccidentByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除施工事故管理信息
     * 
     * @param accidentId 施工事故管理ID
     * @return 结果
     */
    @Override
    public int deleteAccidentById(Integer accidentId)
    {
        return accidentMapper.deleteAccidentById(accidentId);
    }
}
