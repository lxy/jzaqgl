package com.ruoyi.project.system.accident.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.framework.aspectj.lang.annotation.Excel;
import com.ruoyi.framework.web.domain.BaseEntity;
import java.util.Date;

/**
 * 施工事故管理对象 accident
 * 
 * @author 陈贺
 * @date 2020-01-03
 */
public class Accident extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 事故编号(主键) */
    private Integer accidentId;

    /** 发生时间 */
    @Excel(name = "发生时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date happenTime;

    /** 发生地点 */
    @Excel(name = "发生地点")
    private String happenAddress;

    /** 责任人 */
    @Excel(name = "责任人")
    private String blamePerson;

    /** 事故类型 */
    @Excel(name = "事故类型")
    private String accidentType;

    /** 损失程度 */
    @Excel(name = "损失程度")
    private String loseLevel;

    /** 事故描述 */
    @Excel(name = "事故描述")
    private String accidentDescrible;

    public void setAccidentId(Integer accidentId) 
    {
        this.accidentId = accidentId;
    }

    public Integer getAccidentId() 
    {
        return accidentId;
    }
    public void setHappenTime(Date happenTime) 
    {
        this.happenTime = happenTime;
    }

    public Date getHappenTime() 
    {
        return happenTime;
    }
    public void setHappenAddress(String happenAddress) 
    {
        this.happenAddress = happenAddress;
    }

    public String getHappenAddress() 
    {
        return happenAddress;
    }
    public void setBlamePerson(String blamePerson) 
    {
        this.blamePerson = blamePerson;
    }

    public String getBlamePerson() 
    {
        return blamePerson;
    }
    public void setAccidentType(String accidentType) 
    {
        this.accidentType = accidentType;
    }

    public String getAccidentType() 
    {
        return accidentType;
    }
    public void setLoseLevel(String loseLevel) 
    {
        this.loseLevel = loseLevel;
    }

    public String getLoseLevel() 
    {
        return loseLevel;
    }
    public void setAccidentDescrible(String accidentDescrible) 
    {
        this.accidentDescrible = accidentDescrible;
    }

    public String getAccidentDescrible() 
    {
        return accidentDescrible;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("accidentId", getAccidentId())
            .append("happenTime", getHappenTime())
            .append("happenAddress", getHappenAddress())
            .append("blamePerson", getBlamePerson())
            .append("accidentType", getAccidentType())
            .append("loseLevel", getLoseLevel())
            .append("accidentDescrible", getAccidentDescrible())
            .append("remark", getRemark())
            .toString();
    }
}
