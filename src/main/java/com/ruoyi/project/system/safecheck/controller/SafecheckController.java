package com.ruoyi.project.system.safecheck.controller;

import java.util.List;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.ruoyi.framework.aspectj.lang.annotation.Log;
import com.ruoyi.framework.aspectj.lang.enums.BusinessType;
import com.ruoyi.project.system.safecheck.domain.Safecheck;
import com.ruoyi.project.system.safecheck.service.ISafecheckService;
import com.ruoyi.framework.web.controller.BaseController;
import com.ruoyi.framework.web.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.framework.web.page.TableDataInfo;

/**
 * 安全检查信息Controller
 * 
 * @author chenhe
 * @date 2020-01-06
 */
@Controller
@RequestMapping("/system/safecheck")
public class SafecheckController extends BaseController
{
    private String prefix = "system/safecheck";

    @Autowired
    private ISafecheckService safecheckService;

    @RequiresPermissions("system:safecheck:view")
    @GetMapping()
    public String safecheck()
    {
        return prefix + "/safecheck";
    }

    /**
     * 查询安全检查信息列表
     */
    @RequiresPermissions("system:safecheck:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(Safecheck safecheck)
    {
        startPage();
        List<Safecheck> list = safecheckService.selectSafecheckList(safecheck);
        return getDataTable(list);
    }

    /**
     * 导出安全检查信息列表
     */
    @RequiresPermissions("system:safecheck:export")
    @Log(title = "安全检查信息", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(Safecheck safecheck)
    {
        List<Safecheck> list = safecheckService.selectSafecheckList(safecheck);
        ExcelUtil<Safecheck> util = new ExcelUtil<Safecheck>(Safecheck.class);
        return util.exportExcel(list, "safecheck");
    }

    /**
     * 新增安全检查信息
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存安全检查信息
     */
    @RequiresPermissions("system:safecheck:add")
    @Log(title = "安全检查信息", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(Safecheck safecheck)
    {
        return toAjax(safecheckService.insertSafecheck(safecheck));
    }

    /**
     * 修改安全检查信息
     */
    @GetMapping("/edit/{securitycheckid}")
    public String edit(@PathVariable("securitycheckid") Integer securitycheckid, ModelMap mmap)
    {
        Safecheck safecheck = safecheckService.selectSafecheckById(securitycheckid);
        mmap.put("safecheck", safecheck);
        return prefix + "/edit";
    }

    /**
     * 修改保存安全检查信息
     */
    @RequiresPermissions("system:safecheck:edit")
    @Log(title = "安全检查信息", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(Safecheck safecheck)
    {
        return toAjax(safecheckService.updateSafecheck(safecheck));
    }

    /**
     * 删除安全检查信息
     */
    @RequiresPermissions("system:safecheck:remove")
    @Log(title = "安全检查信息", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(safecheckService.deleteSafecheckByIds(ids));
    }
}
