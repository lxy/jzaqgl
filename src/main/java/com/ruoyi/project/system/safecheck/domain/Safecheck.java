package com.ruoyi.project.system.safecheck.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.framework.aspectj.lang.annotation.Excel;
import com.ruoyi.framework.web.domain.BaseEntity;
import java.util.Date;

/**
 * 安全检查信息对象 safecheck
 * 
 * @author chenhe
 * @date 2020-01-06
 */
public class Safecheck extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 安全检查编号(主键) */
    private Integer securitycheckid;

    /** 名称 */
    @Excel(name = "名称")
    private String name;

    /** 检查类型 */
    @Excel(name = "检查类型")
    private String checkType;

    /** 检查人 */
    @Excel(name = "检查人")
    private String checkPerson;

    /** 检查部门 */
    @Excel(name = "检查部门")
    private String checkDepartment;

    /** 说明 */
    @Excel(name = "说明")
    private String checkExplain;

    /** 检查结果 */
    @Excel(name = "检查结果")
    private String checkResult;

    /** 解决方案 */
    @Excel(name = "解决方案")
    private String solution;

    /** 检查时间 */
    @Excel(name = "检查时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date checkTime;

    /** 负责人签字 */
    @Excel(name = "负责人签字")
    private String sign;

    public void setSecuritycheckid(Integer securitycheckid) 
    {
        this.securitycheckid = securitycheckid;
    }

    public Integer getSecuritycheckid() 
    {
        return securitycheckid;
    }
    public void setName(String name) 
    {
        this.name = name;
    }

    public String getName() 
    {
        return name;
    }
    public void setCheckType(String checkType) 
    {
        this.checkType = checkType;
    }

    public String getCheckType() 
    {
        return checkType;
    }
    public void setCheckPerson(String checkPerson) 
    {
        this.checkPerson = checkPerson;
    }

    public String getCheckPerson() 
    {
        return checkPerson;
    }
    public void setCheckDepartment(String checkDepartment) 
    {
        this.checkDepartment = checkDepartment;
    }

    public String getCheckDepartment() 
    {
        return checkDepartment;
    }
    public void setCheckExplain(String checkExplain) 
    {
        this.checkExplain = checkExplain;
    }

    public String getCheckExplain() 
    {
        return checkExplain;
    }
    public void setCheckResult(String checkResult) 
    {
        this.checkResult = checkResult;
    }

    public String getCheckResult() 
    {
        return checkResult;
    }
    public void setSolution(String solution) 
    {
        this.solution = solution;
    }

    public String getSolution() 
    {
        return solution;
    }
    public void setCheckTime(Date checkTime) 
    {
        this.checkTime = checkTime;
    }

    public Date getCheckTime() 
    {
        return checkTime;
    }
    public void setSign(String sign) 
    {
        this.sign = sign;
    }

    public String getSign() 
    {
        return sign;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("securitycheckid", getSecuritycheckid())
            .append("name", getName())
            .append("checkType", getCheckType())
            .append("checkPerson", getCheckPerson())
            .append("checkDepartment", getCheckDepartment())
            .append("checkExplain", getCheckExplain())
            .append("checkResult", getCheckResult())
            .append("solution", getSolution())
            .append("checkTime", getCheckTime())
            .append("sign", getSign())
            .append("remark", getRemark())
            .toString();
    }
}
