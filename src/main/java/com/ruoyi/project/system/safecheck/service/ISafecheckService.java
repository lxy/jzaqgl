package com.ruoyi.project.system.safecheck.service;

import com.ruoyi.project.system.safecheck.domain.Safecheck;
import java.util.List;

/**
 * 安全检查信息Service接口
 * 
 * @author chenhe
 * @date 2020-01-06
 */
public interface ISafecheckService 
{
    /**
     * 查询安全检查信息
     * 
     * @param securitycheckid 安全检查信息ID
     * @return 安全检查信息
     */
    public Safecheck selectSafecheckById(Integer securitycheckid);

    /**
     * 查询安全检查信息列表
     * 
     * @param safecheck 安全检查信息
     * @return 安全检查信息集合
     */
    public List<Safecheck> selectSafecheckList(Safecheck safecheck);

    /**
     * 新增安全检查信息
     * 
     * @param safecheck 安全检查信息
     * @return 结果
     */
    public int insertSafecheck(Safecheck safecheck);

    /**
     * 修改安全检查信息
     * 
     * @param safecheck 安全检查信息
     * @return 结果
     */
    public int updateSafecheck(Safecheck safecheck);

    /**
     * 批量删除安全检查信息
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteSafecheckByIds(String ids);

    /**
     * 删除安全检查信息信息
     * 
     * @param securitycheckid 安全检查信息ID
     * @return 结果
     */
    public int deleteSafecheckById(Integer securitycheckid);
}
