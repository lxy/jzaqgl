package com.ruoyi.project.system.safecheck.mapper;

import com.ruoyi.project.system.safecheck.domain.Safecheck;
import java.util.List;

/**
 * 安全检查信息Mapper接口
 * 
 * @author chenhe
 * @date 2020-01-06
 */
public interface SafecheckMapper 
{
    /**
     * 查询安全检查信息
     * 
     * @param securitycheckid 安全检查信息ID
     * @return 安全检查信息
     */
    public Safecheck selectSafecheckById(Integer securitycheckid);

    /**
     * 查询安全检查信息列表
     * 
     * @param safecheck 安全检查信息
     * @return 安全检查信息集合
     */
    public List<Safecheck> selectSafecheckList(Safecheck safecheck);

    /**
     * 新增安全检查信息
     * 
     * @param safecheck 安全检查信息
     * @return 结果
     */
    public int insertSafecheck(Safecheck safecheck);

    /**
     * 修改安全检查信息
     * 
     * @param safecheck 安全检查信息
     * @return 结果
     */
    public int updateSafecheck(Safecheck safecheck);

    /**
     * 删除安全检查信息
     * 
     * @param securitycheckid 安全检查信息ID
     * @return 结果
     */
    public int deleteSafecheckById(Integer securitycheckid);

    /**
     * 批量删除安全检查信息
     * 
     * @param securitycheckids 需要删除的数据ID
     * @return 结果
     */
    public int deleteSafecheckByIds(String[] securitycheckids);
}
