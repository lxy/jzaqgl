package com.ruoyi.project.system.safecheck.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.project.system.safecheck.mapper.SafecheckMapper;
import com.ruoyi.project.system.safecheck.domain.Safecheck;
import com.ruoyi.project.system.safecheck.service.ISafecheckService;
import com.ruoyi.common.utils.text.Convert;

/**
 * 安全检查信息Service业务层处理
 * 
 * @author chenhe
 * @date 2020-01-06
 */
@Service
public class SafecheckServiceImpl implements ISafecheckService 
{
    @Autowired
    private SafecheckMapper safecheckMapper;

    /**
     * 查询安全检查信息
     * 
     * @param securitycheckid 安全检查信息ID
     * @return 安全检查信息
     */
    @Override
    public Safecheck selectSafecheckById(Integer securitycheckid)
    {
        return safecheckMapper.selectSafecheckById(securitycheckid);
    }

    /**
     * 查询安全检查信息列表
     * 
     * @param safecheck 安全检查信息
     * @return 安全检查信息
     */
    @Override
    public List<Safecheck> selectSafecheckList(Safecheck safecheck)
    {
        return safecheckMapper.selectSafecheckList(safecheck);
    }

    /**
     * 新增安全检查信息
     * 
     * @param safecheck 安全检查信息
     * @return 结果
     */
    @Override
    public int insertSafecheck(Safecheck safecheck)
    {
        return safecheckMapper.insertSafecheck(safecheck);
    }

    /**
     * 修改安全检查信息
     * 
     * @param safecheck 安全检查信息
     * @return 结果
     */
    @Override
    public int updateSafecheck(Safecheck safecheck)
    {
        return safecheckMapper.updateSafecheck(safecheck);
    }

    /**
     * 删除安全检查信息对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteSafecheckByIds(String ids)
    {
        return safecheckMapper.deleteSafecheckByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除安全检查信息信息
     * 
     * @param securitycheckid 安全检查信息ID
     * @return 结果
     */
    @Override
    public int deleteSafecheckById(Integer securitycheckid)
    {
        return safecheckMapper.deleteSafecheckById(securitycheckid);
    }
}
