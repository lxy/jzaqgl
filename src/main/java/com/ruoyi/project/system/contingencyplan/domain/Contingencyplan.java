package com.ruoyi.project.system.contingencyplan.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.framework.aspectj.lang.annotation.Excel;
import com.ruoyi.framework.web.domain.BaseEntity;
import java.util.Date;

/**
 * 应急预案信息 
对象 contingencyplan
 * 
 * @author chenhe
 * @date 2020-01-06
 */
public class Contingencyplan extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 预案编号(主键) */
    private Integer planId;

    /** 编制人 */
    @Excel(name = "编制人")
    private String preparedPerson;

    /** 编制时间 */
    @Excel(name = "编制时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date preparedTime;

    /** 事故类型 */
    @Excel(name = "事故类型")
    private String riskType;

    /** 事故处理 */
    @Excel(name = "事故处理")
    private String riskHandle;

    /** 操作规范 */
    @Excel(name = "操作规范")
    private String operateStandard;

    /** 注意事项 */
    @Excel(name = "注意事项")
    private String notice;

    public void setPlanId(Integer planId) 
    {
        this.planId = planId;
    }

    public Integer getPlanId() 
    {
        return planId;
    }
    public void setPreparedPerson(String preparedPerson) 
    {
        this.preparedPerson = preparedPerson;
    }

    public String getPreparedPerson() 
    {
        return preparedPerson;
    }
    public void setPreparedTime(Date preparedTime) 
    {
        this.preparedTime = preparedTime;
    }

    public Date getPreparedTime() 
    {
        return preparedTime;
    }
    public void setRiskType(String riskType) 
    {
        this.riskType = riskType;
    }

    public String getRiskType() 
    {
        return riskType;
    }
    public void setRiskHandle(String riskHandle) 
    {
        this.riskHandle = riskHandle;
    }

    public String getRiskHandle() 
    {
        return riskHandle;
    }
    public void setOperateStandard(String operateStandard) 
    {
        this.operateStandard = operateStandard;
    }

    public String getOperateStandard() 
    {
        return operateStandard;
    }
    public void setNotice(String notice) 
    {
        this.notice = notice;
    }

    public String getNotice() 
    {
        return notice;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("planId", getPlanId())
            .append("preparedPerson", getPreparedPerson())
            .append("preparedTime", getPreparedTime())
            .append("riskType", getRiskType())
            .append("riskHandle", getRiskHandle())
            .append("operateStandard", getOperateStandard())
            .append("notice", getNotice())
            .append("remark", getRemark())
            .toString();
    }
}
