package com.ruoyi.project.system.contingencyplan.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.project.system.contingencyplan.mapper.ContingencyplanMapper;
import com.ruoyi.project.system.contingencyplan.domain.Contingencyplan;
import com.ruoyi.project.system.contingencyplan.service.IContingencyplanService;
import com.ruoyi.common.utils.text.Convert;

/**
 * 应急预案信息 
Service业务层处理
 * 
 * @author chenhe
 * @date 2020-01-06
 */
@Service
public class ContingencyplanServiceImpl implements IContingencyplanService 
{
    @Autowired
    private ContingencyplanMapper contingencyplanMapper;

    /**
     * 查询应急预案信息 

     * 
     * @param planId 应急预案信息 
ID
     * @return 应急预案信息 

     */
    @Override
    public Contingencyplan selectContingencyplanById(Integer planId)
    {
        return contingencyplanMapper.selectContingencyplanById(planId);
    }

    /**
     * 查询应急预案信息 
列表
     * 
     * @param contingencyplan 应急预案信息 

     * @return 应急预案信息 

     */
    @Override
    public List<Contingencyplan> selectContingencyplanList(Contingencyplan contingencyplan)
    {
        return contingencyplanMapper.selectContingencyplanList(contingencyplan);
    }

    /**
     * 新增应急预案信息 

     * 
     * @param contingencyplan 应急预案信息 

     * @return 结果
     */
    @Override
    public int insertContingencyplan(Contingencyplan contingencyplan)
    {
        return contingencyplanMapper.insertContingencyplan(contingencyplan);
    }

    /**
     * 修改应急预案信息 

     * 
     * @param contingencyplan 应急预案信息 

     * @return 结果
     */
    @Override
    public int updateContingencyplan(Contingencyplan contingencyplan)
    {
        return contingencyplanMapper.updateContingencyplan(contingencyplan);
    }

    /**
     * 删除应急预案信息 
对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteContingencyplanByIds(String ids)
    {
        return contingencyplanMapper.deleteContingencyplanByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除应急预案信息 
信息
     * 
     * @param planId 应急预案信息 
ID
     * @return 结果
     */
    @Override
    public int deleteContingencyplanById(Integer planId)
    {
        return contingencyplanMapper.deleteContingencyplanById(planId);
    }
}
