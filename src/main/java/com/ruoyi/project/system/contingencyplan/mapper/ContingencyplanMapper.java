package com.ruoyi.project.system.contingencyplan.mapper;

import com.ruoyi.project.system.contingencyplan.domain.Contingencyplan;
import java.util.List;

/**
 * 应急预案信息 
Mapper接口
 * 
 * @author chenhe
 * @date 2020-01-06
 */
public interface ContingencyplanMapper 
{
    /**
     * 查询应急预案信息 

     * 
     * @param planId 应急预案信息 
ID
     * @return 应急预案信息 

     */
    public Contingencyplan selectContingencyplanById(Integer planId);

    /**
     * 查询应急预案信息 
列表
     * 
     * @param contingencyplan 应急预案信息 

     * @return 应急预案信息 
集合
     */
    public List<Contingencyplan> selectContingencyplanList(Contingencyplan contingencyplan);

    /**
     * 新增应急预案信息 

     * 
     * @param contingencyplan 应急预案信息 

     * @return 结果
     */
    public int insertContingencyplan(Contingencyplan contingencyplan);

    /**
     * 修改应急预案信息 

     * 
     * @param contingencyplan 应急预案信息 

     * @return 结果
     */
    public int updateContingencyplan(Contingencyplan contingencyplan);

    /**
     * 删除应急预案信息 

     * 
     * @param planId 应急预案信息 
ID
     * @return 结果
     */
    public int deleteContingencyplanById(Integer planId);

    /**
     * 批量删除应急预案信息 

     * 
     * @param planIds 需要删除的数据ID
     * @return 结果
     */
    public int deleteContingencyplanByIds(String[] planIds);
}
