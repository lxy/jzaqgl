package com.ruoyi.project.system.contingencyplan.controller;

import java.util.List;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.ruoyi.framework.aspectj.lang.annotation.Log;
import com.ruoyi.framework.aspectj.lang.enums.BusinessType;
import com.ruoyi.project.system.contingencyplan.domain.Contingencyplan;
import com.ruoyi.project.system.contingencyplan.service.IContingencyplanService;
import com.ruoyi.framework.web.controller.BaseController;
import com.ruoyi.framework.web.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.framework.web.page.TableDataInfo;

/**
 * 应急预案信息 
Controller
 * 
 * @author chenhe
 * @date 2020-01-06
 */
@Controller
@RequestMapping("/system/contingencyplan")
public class ContingencyplanController extends BaseController
{
    private String prefix = "system/contingencyplan";

    @Autowired
    private IContingencyplanService contingencyplanService;

    @RequiresPermissions("system:contingencyplan:view")
    @GetMapping()
    public String contingencyplan()
    {
        return prefix + "/contingencyplan";
    }

    /**
     * 查询应急预案信息 
列表
     */
    @RequiresPermissions("system:contingencyplan:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(Contingencyplan contingencyplan)
    {
        startPage();
        List<Contingencyplan> list = contingencyplanService.selectContingencyplanList(contingencyplan);
        return getDataTable(list);
    }

    /**
     * 导出应急预案信息 
列表
     */
    @RequiresPermissions("system:contingencyplan:export")
    @Log(title = "应急预案信息 ", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(Contingencyplan contingencyplan)
    {
        List<Contingencyplan> list = contingencyplanService.selectContingencyplanList(contingencyplan);
        ExcelUtil<Contingencyplan> util = new ExcelUtil<Contingencyplan>(Contingencyplan.class);
        return util.exportExcel(list, "contingencyplan");
    }

    /**
     * 新增应急预案信息 

     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存应急预案信息 

     */
    @RequiresPermissions("system:contingencyplan:add")
    @Log(title = "应急预案信息 ", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(Contingencyplan contingencyplan)
    {
        return toAjax(contingencyplanService.insertContingencyplan(contingencyplan));
    }

    /**
     * 修改应急预案信息 

     */
    @GetMapping("/edit/{planId}")
    public String edit(@PathVariable("planId") Integer planId, ModelMap mmap)
    {
        Contingencyplan contingencyplan = contingencyplanService.selectContingencyplanById(planId);
        mmap.put("contingencyplan", contingencyplan);
        return prefix + "/edit";
    }

    /**
     * 修改保存应急预案信息 

     */
    @RequiresPermissions("system:contingencyplan:edit")
    @Log(title = "应急预案信息 ", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(Contingencyplan contingencyplan)
    {
        return toAjax(contingencyplanService.updateContingencyplan(contingencyplan));
    }

    /**
     * 删除应急预案信息 

     */
    @RequiresPermissions("system:contingencyplan:remove")
    @Log(title = "应急预案信息 ", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(contingencyplanService.deleteContingencyplanByIds(ids));
    }
}
