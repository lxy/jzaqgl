/*
 Navicat Premium Data Transfer

 Source Server         : MySQL57
 Source Server Type    : MySQL
 Source Server Version : 50716
 Source Host           : localhost:3306
 Source Schema         : jzaqgl

 Target Server Type    : MySQL
 Target Server Version : 50716
 File Encoding         : 65001

 Date: 07/01/2020 01:57:38
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for accident
-- ----------------------------
DROP TABLE IF EXISTS `accident`;
CREATE TABLE `accident`  (
  `Accident_ID` int(4) NOT NULL AUTO_INCREMENT COMMENT '事故编号(主键)',
  `Happen_time` date NULL DEFAULT NULL COMMENT '发生时间',
  `Happen_address` char(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '发生地点',
  `Blame_person` char(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '责任人',
  `Accident_type` char(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '事故类型',
  `Lose_level` char(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '损失程度',
  `Accident_describle` char(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '事故描述',
  `Remark` char(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`Accident_ID`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 9 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '事故信息表 \r\n' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of accident
-- ----------------------------
INSERT INTO `accident` VALUES (2, '2019-12-01', '徐州', '陈贺', '1', '高', '材料不合格引起的', NULL);
INSERT INTO `accident` VALUES (3, '2019-12-31', '徐州', '陈贺', '2', '中', '质量不合格', NULL);
INSERT INTO `accident` VALUES (4, '2020-01-01', '北京', '贺', '3', '中', '人员分配不合理引起的', NULL);
INSERT INTO `accident` VALUES (5, '2020-01-02', '上海', '陈', '2', '高', '缺少人员', NULL);
INSERT INTO `accident` VALUES (6, '2020-01-15', '上海', '陈', '1', '高', '缺少人员缺少人员缺少人员', NULL);
INSERT INTO `accident` VALUES (7, '2020-01-04', '北京', '贺', '2', '中', '人员分配不合理引起的', NULL);
INSERT INTO `accident` VALUES (8, '2020-01-02', '上海', '陈', '2', '高', '人员分配不合理引起的', NULL);

-- ----------------------------
-- Table structure for contingencyplan
-- ----------------------------
DROP TABLE IF EXISTS `contingencyplan`;
CREATE TABLE `contingencyplan`  (
  `Plan_ID` int(4) NOT NULL AUTO_INCREMENT COMMENT '预案编号(主键)',
  `Prepared_Person` char(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '编制人',
  `Prepared_time` date NULL DEFAULT NULL COMMENT '编制时间',
  `Risk_type` char(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '事故类型',
  `Risk_handle` char(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '事故处理',
  `Operate_standard` char(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '操作规范',
  `Notice` char(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '注意事项',
  `Remark` char(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`Plan_ID`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 8 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '应急预案信息表 \r\n' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of contingencyplan
-- ----------------------------
INSERT INTO `contingencyplan` VALUES (1, '董娟娟1', '2019-12-30', NULL, '完善设施', '设备就绪', '无', NULL);
INSERT INTO `contingencyplan` VALUES (2, '郭宙', '2019-12-31', NULL, '钉子扎脚', '伤口包扎处理', '注意地面', NULL);
INSERT INTO `contingencyplan` VALUES (3, '陈贺', '2020-01-01', NULL, '电死电伤', '及时关闭电源', '无', NULL);
INSERT INTO `contingencyplan` VALUES (4, '陈贺1', '2020-01-03', NULL, '吊车坠落处理', '使用前检查吊车', '注意绳缆', NULL);
INSERT INTO `contingencyplan` VALUES (5, '郭宙2', '2020-01-04', NULL, '雾天塔吊', '更改塔吊日期', '避免雾天塔吊', NULL);
INSERT INTO `contingencyplan` VALUES (6, '董娟娟2', '2020-01-05', NULL, '坍塌', '根据土质机械设备重量堆放', '无', NULL);
INSERT INTO `contingencyplan` VALUES (7, '郭宙3', '2020-01-06', NULL, '机械伤害', '及时止血', '及时拨打120', NULL);

-- ----------------------------
-- Table structure for departmentalposts
-- ----------------------------
DROP TABLE IF EXISTS `departmentalposts`;
CREATE TABLE `departmentalposts`  (
  `DeptID` int(4) NOT NULL AUTO_INCREMENT COMMENT '部门编号(主键)',
  `UserID` int(4) NULL DEFAULT NULL COMMENT '用户编号(外键)',
  `DeptName` char(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '部门名称',
  `JobName` char(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '岗位名称',
  `Name` char(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '姓名',
  `DeptLevel` int(4) NULL DEFAULT NULL COMMENT '部门级别',
  `Compiler` char(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '编制人',
  `Time` date NULL DEFAULT NULL COMMENT '编制日期',
  `Remark` char(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`DeptID`) USING BTREE,
  INDEX `UserID`(`UserID`) USING BTREE,
  CONSTRAINT `departmentalposts_ibfk_1` FOREIGN KEY (`UserID`) REFERENCES `useraccount` (`UserID`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '部门岗位信息表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for gen_table
-- ----------------------------
DROP TABLE IF EXISTS `gen_table`;
CREATE TABLE `gen_table`  (
  `table_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '编号',
  `table_name` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '表名称',
  `table_comment` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '表描述',
  `class_name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '实体类名称',
  `tpl_category` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT 'crud' COMMENT '使用的模板（crud单表操作 tree树表操作）',
  `package_name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '生成包路径',
  `module_name` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '生成模块名',
  `business_name` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '生成业务名',
  `function_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '生成功能名',
  `function_author` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '生成功能作者',
  `options` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '其它生成选项',
  `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`table_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 8 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '代码生成业务表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of gen_table
-- ----------------------------
INSERT INTO `gen_table` VALUES (7, 'safecheck', '安全检查信息表', 'Safecheck', 'crud', 'com.ruoyi.project.system', 'system', 'safecheck', '安全检查信息', 'chenhe', '{\"treeName\":\"\",\"treeParentCode\":\"\",\"treeCode\":\"\"}', 'admin', '2020-01-06 15:35:44', '', '2020-01-06 15:35:55', '');

-- ----------------------------
-- Table structure for gen_table_column
-- ----------------------------
DROP TABLE IF EXISTS `gen_table_column`;
CREATE TABLE `gen_table_column`  (
  `column_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '编号',
  `table_id` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '归属表编号',
  `column_name` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '列名称',
  `column_comment` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '列描述',
  `column_type` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '列类型',
  `java_type` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'JAVA类型',
  `java_field` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'JAVA字段名',
  `is_pk` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '是否主键（1是）',
  `is_increment` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '是否自增（1是）',
  `is_required` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '是否必填（1是）',
  `is_insert` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '是否为插入字段（1是）',
  `is_edit` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '是否编辑字段（1是）',
  `is_list` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '是否列表字段（1是）',
  `is_query` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '是否查询字段（1是）',
  `query_type` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT 'EQ' COMMENT '查询方式（等于、不等于、大于、小于、范围）',
  `html_type` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '显示类型（文本框、文本域、下拉框、复选框、单选框、日期控件）',
  `dict_type` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '字典类型',
  `sort` int(11) NULL DEFAULT NULL COMMENT '排序',
  `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`column_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 69 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '代码生成业务表字段' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of gen_table_column
-- ----------------------------
INSERT INTO `gen_table_column` VALUES (58, '7', 'SecuritycheckID', '安全检查编号(主键)', 'int(4)', 'Integer', 'securitycheckid', '1', '1', NULL, '1', NULL, NULL, NULL, 'EQ', 'input', '', 1, 'admin', '2020-01-06 15:35:44', NULL, '2020-01-06 15:35:55');
INSERT INTO `gen_table_column` VALUES (59, '7', 'Name', '名称', 'char(20)', 'String', 'name', '0', '0', NULL, '1', '1', '1', '1', 'LIKE', 'input', '', 2, 'admin', '2020-01-06 15:35:44', NULL, '2020-01-06 15:35:55');
INSERT INTO `gen_table_column` VALUES (60, '7', 'check_type', '检查类型', 'char(20)', 'String', 'checkType', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 3, 'admin', '2020-01-06 15:35:44', NULL, '2020-01-06 15:35:55');
INSERT INTO `gen_table_column` VALUES (61, '7', 'Check_person', '检查人', 'char(20)', 'String', 'checkPerson', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 4, 'admin', '2020-01-06 15:35:44', NULL, '2020-01-06 15:35:55');
INSERT INTO `gen_table_column` VALUES (62, '7', 'Check_department', '检查部门', 'char(20)', 'String', 'checkDepartment', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 5, 'admin', '2020-01-06 15:35:44', NULL, '2020-01-06 15:35:55');
INSERT INTO `gen_table_column` VALUES (63, '7', 'Check_explain', '说明', 'char(20)', 'String', 'checkExplain', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 6, 'admin', '2020-01-06 15:35:44', NULL, '2020-01-06 15:35:55');
INSERT INTO `gen_table_column` VALUES (64, '7', 'Check_result', '检查结果', 'char(20)', 'String', 'checkResult', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 7, 'admin', '2020-01-06 15:35:44', NULL, '2020-01-06 15:35:55');
INSERT INTO `gen_table_column` VALUES (65, '7', 'Solution', '解决方案', 'char(20)', 'String', 'solution', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 8, 'admin', '2020-01-06 15:35:44', NULL, '2020-01-06 15:35:55');
INSERT INTO `gen_table_column` VALUES (66, '7', 'Check_time', '检查时间', 'date', 'Date', 'checkTime', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'datetime', '', 9, 'admin', '2020-01-06 15:35:44', NULL, '2020-01-06 15:35:55');
INSERT INTO `gen_table_column` VALUES (67, '7', 'Sign', '负责人签字', 'char(20)', 'String', 'sign', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 10, 'admin', '2020-01-06 15:35:44', NULL, '2020-01-06 15:35:55');
INSERT INTO `gen_table_column` VALUES (68, '7', 'Remark', '备注', 'char(20)', 'String', 'remark', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 11, 'admin', '2020-01-06 15:35:44', NULL, '2020-01-06 15:35:55');

-- ----------------------------
-- Table structure for hiddendanger
-- ----------------------------
DROP TABLE IF EXISTS `hiddendanger`;
CREATE TABLE `hiddendanger`  (
  `DangerID` int(4) NOT NULL AUTO_INCREMENT COMMENT '安全隐患编号（主键）',
  `Name` char(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '名称',
  `Elaborate` char(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '隐患阐述',
  `Type` char(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '类型',
  `Suggestions` char(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '整改建议',
  `Deadline` date NULL DEFAULT NULL COMMENT '期限',
  `Test_person` char(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '检测人员',
  `Review_results` char(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '复查结果',
  `Review_time` date NULL DEFAULT NULL COMMENT '复查时间',
  `Confirm_sign` char(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '确认签字',
  `Remark` char(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`DangerID`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 8 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '安全隐患信息表\r\n' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of hiddendanger
-- ----------------------------
INSERT INTO `hiddendanger` VALUES (3, '1', '设备故障', NULL, '维修', '2020-01-01', '秦佳', '合格', '2020-01-31', '秦佳', NULL);
INSERT INTO `hiddendanger` VALUES (4, '2', '用电', NULL, '及时保修', '2020-01-08', '秦佳', '合格', '2020-01-24', '秦佳', NULL);
INSERT INTO `hiddendanger` VALUES (5, '3', '电源开关', NULL, '按时断电', '2020-01-22', '秦佳', '合格', '2020-01-31', '秦佳', NULL);
INSERT INTO `hiddendanger` VALUES (6, '4', '灯管损坏', NULL, '修整', '2020-02-01', '秦佳', '合格', '2020-02-29', '秦佳', NULL);
INSERT INTO `hiddendanger` VALUES (7, '5', '仪器', NULL, '及时修整', '2020-03-02', '秦佳', '合格', '2020-04-30', '秦佳', NULL);

-- ----------------------------
-- Table structure for qrtz_blob_triggers
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_blob_triggers`;
CREATE TABLE `qrtz_blob_triggers`  (
  `sched_name` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `trigger_name` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `trigger_group` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `blob_data` blob NULL,
  PRIMARY KEY (`sched_name`, `trigger_name`, `trigger_group`) USING BTREE,
  CONSTRAINT `qrtz_blob_triggers_ibfk_1` FOREIGN KEY (`sched_name`, `trigger_name`, `trigger_group`) REFERENCES `qrtz_triggers` (`sched_name`, `trigger_name`, `trigger_group`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for qrtz_calendars
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_calendars`;
CREATE TABLE `qrtz_calendars`  (
  `sched_name` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `calendar_name` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `calendar` blob NOT NULL,
  PRIMARY KEY (`sched_name`, `calendar_name`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for qrtz_cron_triggers
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_cron_triggers`;
CREATE TABLE `qrtz_cron_triggers`  (
  `sched_name` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `trigger_name` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `trigger_group` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `cron_expression` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `time_zone_id` varchar(80) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`sched_name`, `trigger_name`, `trigger_group`) USING BTREE,
  CONSTRAINT `qrtz_cron_triggers_ibfk_1` FOREIGN KEY (`sched_name`, `trigger_name`, `trigger_group`) REFERENCES `qrtz_triggers` (`sched_name`, `trigger_name`, `trigger_group`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of qrtz_cron_triggers
-- ----------------------------
INSERT INTO `qrtz_cron_triggers` VALUES ('RuoyiScheduler', 'TASK_CLASS_NAME1', 'DEFAULT', '0/10 * * * * ?', 'Asia/Shanghai');
INSERT INTO `qrtz_cron_triggers` VALUES ('RuoyiScheduler', 'TASK_CLASS_NAME2', 'DEFAULT', '0/15 * * * * ?', 'Asia/Shanghai');
INSERT INTO `qrtz_cron_triggers` VALUES ('RuoyiScheduler', 'TASK_CLASS_NAME3', 'DEFAULT', '0/20 * * * * ?', 'Asia/Shanghai');

-- ----------------------------
-- Table structure for qrtz_fired_triggers
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_fired_triggers`;
CREATE TABLE `qrtz_fired_triggers`  (
  `sched_name` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `entry_id` varchar(95) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `trigger_name` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `trigger_group` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `instance_name` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `fired_time` bigint(13) NOT NULL,
  `sched_time` bigint(13) NOT NULL,
  `priority` int(11) NOT NULL,
  `state` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `job_name` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `job_group` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `is_nonconcurrent` varchar(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `requests_recovery` varchar(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`sched_name`, `entry_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for qrtz_job_details
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_job_details`;
CREATE TABLE `qrtz_job_details`  (
  `sched_name` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `job_name` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `job_group` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `description` varchar(250) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `job_class_name` varchar(250) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `is_durable` varchar(1) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `is_nonconcurrent` varchar(1) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `is_update_data` varchar(1) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `requests_recovery` varchar(1) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `job_data` blob NULL,
  PRIMARY KEY (`sched_name`, `job_name`, `job_group`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of qrtz_job_details
-- ----------------------------
INSERT INTO `qrtz_job_details` VALUES ('RuoyiScheduler', 'TASK_CLASS_NAME1', 'DEFAULT', NULL, 'com.ruoyi.project.monitor.job.util.QuartzDisallowConcurrentExecution', '0', '1', '0', '0', 0xACED0005737200156F72672E71756172747A2E4A6F62446174614D61709FB083E8BFA9B0CB020000787200266F72672E71756172747A2E7574696C732E537472696E674B65794469727479466C61674D61708208E8C3FBC55D280200015A0013616C6C6F77735472616E7369656E74446174617872001D6F72672E71756172747A2E7574696C732E4469727479466C61674D617013E62EAD28760ACE0200025A000564697274794C00036D617074000F4C6A6176612F7574696C2F4D61703B787001737200116A6176612E7574696C2E486173684D61700507DAC1C31660D103000246000A6C6F6164466163746F724900097468726573686F6C6478703F4000000000000C7708000000100000000174000F5441534B5F50524F5045525449455373720028636F6D2E72756F79692E70726F6A6563742E6D6F6E69746F722E6A6F622E646F6D61696E2E4A6F6200000000000000010200084C000A636F6E63757272656E747400124C6A6176612F6C616E672F537472696E673B4C000E63726F6E45787072657373696F6E71007E00094C000C696E766F6B6554617267657471007E00094C00086A6F6247726F757071007E00094C00056A6F6249647400104C6A6176612F6C616E672F4C6F6E673B4C00076A6F624E616D6571007E00094C000D6D697366697265506F6C69637971007E00094C000673746174757371007E000978720029636F6D2E72756F79692E6672616D65776F726B2E7765622E646F6D61696E2E42617365456E7469747900000000000000010200074C0008637265617465427971007E00094C000A63726561746554696D657400104C6A6176612F7574696C2F446174653B4C0006706172616D7371007E00034C000672656D61726B71007E00094C000B73656172636856616C756571007E00094C0008757064617465427971007E00094C000A75706461746554696D6571007E000C787074000561646D696E7372000E6A6176612E7574696C2E44617465686A81014B59741903000078707708000001622CDE29E078707400007070707400013174000E302F3130202A202A202A202A203F74001172795461736B2E72794E6F506172616D7374000744454641554C547372000E6A6176612E6C616E672E4C6F6E673B8BE490CC8F23DF0200014A000576616C7565787200106A6176612E6C616E672E4E756D62657286AC951D0B94E08B02000078700000000000000001740018E7B3BBE7BB9FE9BB98E8AEA4EFBC88E697A0E58F82EFBC8974000133740001317800);
INSERT INTO `qrtz_job_details` VALUES ('RuoyiScheduler', 'TASK_CLASS_NAME2', 'DEFAULT', NULL, 'com.ruoyi.project.monitor.job.util.QuartzDisallowConcurrentExecution', '0', '1', '0', '0', 0xACED0005737200156F72672E71756172747A2E4A6F62446174614D61709FB083E8BFA9B0CB020000787200266F72672E71756172747A2E7574696C732E537472696E674B65794469727479466C61674D61708208E8C3FBC55D280200015A0013616C6C6F77735472616E7369656E74446174617872001D6F72672E71756172747A2E7574696C732E4469727479466C61674D617013E62EAD28760ACE0200025A000564697274794C00036D617074000F4C6A6176612F7574696C2F4D61703B787001737200116A6176612E7574696C2E486173684D61700507DAC1C31660D103000246000A6C6F6164466163746F724900097468726573686F6C6478703F4000000000000C7708000000100000000174000F5441534B5F50524F5045525449455373720028636F6D2E72756F79692E70726F6A6563742E6D6F6E69746F722E6A6F622E646F6D61696E2E4A6F6200000000000000010200084C000A636F6E63757272656E747400124C6A6176612F6C616E672F537472696E673B4C000E63726F6E45787072657373696F6E71007E00094C000C696E766F6B6554617267657471007E00094C00086A6F6247726F757071007E00094C00056A6F6249647400104C6A6176612F6C616E672F4C6F6E673B4C00076A6F624E616D6571007E00094C000D6D697366697265506F6C69637971007E00094C000673746174757371007E000978720029636F6D2E72756F79692E6672616D65776F726B2E7765622E646F6D61696E2E42617365456E7469747900000000000000010200074C0008637265617465427971007E00094C000A63726561746554696D657400104C6A6176612F7574696C2F446174653B4C0006706172616D7371007E00034C000672656D61726B71007E00094C000B73656172636856616C756571007E00094C0008757064617465427971007E00094C000A75706461746554696D6571007E000C787074000561646D696E7372000E6A6176612E7574696C2E44617465686A81014B59741903000078707708000001622CDE29E078707400007070707400013174000E302F3135202A202A202A202A203F74001572795461736B2E7279506172616D7328277279272974000744454641554C547372000E6A6176612E6C616E672E4C6F6E673B8BE490CC8F23DF0200014A000576616C7565787200106A6176612E6C616E672E4E756D62657286AC951D0B94E08B02000078700000000000000002740018E7B3BBE7BB9FE9BB98E8AEA4EFBC88E69C89E58F82EFBC8974000133740001317800);
INSERT INTO `qrtz_job_details` VALUES ('RuoyiScheduler', 'TASK_CLASS_NAME3', 'DEFAULT', NULL, 'com.ruoyi.project.monitor.job.util.QuartzDisallowConcurrentExecution', '0', '1', '0', '0', 0xACED0005737200156F72672E71756172747A2E4A6F62446174614D61709FB083E8BFA9B0CB020000787200266F72672E71756172747A2E7574696C732E537472696E674B65794469727479466C61674D61708208E8C3FBC55D280200015A0013616C6C6F77735472616E7369656E74446174617872001D6F72672E71756172747A2E7574696C732E4469727479466C61674D617013E62EAD28760ACE0200025A000564697274794C00036D617074000F4C6A6176612F7574696C2F4D61703B787001737200116A6176612E7574696C2E486173684D61700507DAC1C31660D103000246000A6C6F6164466163746F724900097468726573686F6C6478703F4000000000000C7708000000100000000174000F5441534B5F50524F5045525449455373720028636F6D2E72756F79692E70726F6A6563742E6D6F6E69746F722E6A6F622E646F6D61696E2E4A6F6200000000000000010200084C000A636F6E63757272656E747400124C6A6176612F6C616E672F537472696E673B4C000E63726F6E45787072657373696F6E71007E00094C000C696E766F6B6554617267657471007E00094C00086A6F6247726F757071007E00094C00056A6F6249647400104C6A6176612F6C616E672F4C6F6E673B4C00076A6F624E616D6571007E00094C000D6D697366697265506F6C69637971007E00094C000673746174757371007E000978720029636F6D2E72756F79692E6672616D65776F726B2E7765622E646F6D61696E2E42617365456E7469747900000000000000010200074C0008637265617465427971007E00094C000A63726561746554696D657400104C6A6176612F7574696C2F446174653B4C0006706172616D7371007E00034C000672656D61726B71007E00094C000B73656172636856616C756571007E00094C0008757064617465427971007E00094C000A75706461746554696D6571007E000C787074000561646D696E7372000E6A6176612E7574696C2E44617465686A81014B59741903000078707708000001622CDE29E078707400007070707400013174000E302F3230202A202A202A202A203F74003872795461736B2E72794D756C7469706C65506172616D7328277279272C20747275652C20323030304C2C203331362E3530442C203130302974000744454641554C547372000E6A6176612E6C616E672E4C6F6E673B8BE490CC8F23DF0200014A000576616C7565787200106A6176612E6C616E672E4E756D62657286AC951D0B94E08B02000078700000000000000003740018E7B3BBE7BB9FE9BB98E8AEA4EFBC88E5A49AE58F82EFBC8974000133740001317800);

-- ----------------------------
-- Table structure for qrtz_locks
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_locks`;
CREATE TABLE `qrtz_locks`  (
  `sched_name` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `lock_name` varchar(40) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  PRIMARY KEY (`sched_name`, `lock_name`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of qrtz_locks
-- ----------------------------
INSERT INTO `qrtz_locks` VALUES ('RuoyiScheduler', 'STATE_ACCESS');
INSERT INTO `qrtz_locks` VALUES ('RuoyiScheduler', 'TRIGGER_ACCESS');

-- ----------------------------
-- Table structure for qrtz_paused_trigger_grps
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_paused_trigger_grps`;
CREATE TABLE `qrtz_paused_trigger_grps`  (
  `sched_name` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `trigger_group` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  PRIMARY KEY (`sched_name`, `trigger_group`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for qrtz_scheduler_state
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_scheduler_state`;
CREATE TABLE `qrtz_scheduler_state`  (
  `sched_name` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `instance_name` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `last_checkin_time` bigint(13) NOT NULL,
  `checkin_interval` bigint(13) NOT NULL,
  PRIMARY KEY (`sched_name`, `instance_name`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of qrtz_scheduler_state
-- ----------------------------
INSERT INTO `qrtz_scheduler_state` VALUES ('RuoyiScheduler', 'SurfaceProSmileher1578322522695', 1578333299336, 15000);

-- ----------------------------
-- Table structure for qrtz_simple_triggers
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_simple_triggers`;
CREATE TABLE `qrtz_simple_triggers`  (
  `sched_name` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `trigger_name` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `trigger_group` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `repeat_count` bigint(7) NOT NULL,
  `repeat_interval` bigint(12) NOT NULL,
  `times_triggered` bigint(10) NOT NULL,
  PRIMARY KEY (`sched_name`, `trigger_name`, `trigger_group`) USING BTREE,
  CONSTRAINT `qrtz_simple_triggers_ibfk_1` FOREIGN KEY (`sched_name`, `trigger_name`, `trigger_group`) REFERENCES `qrtz_triggers` (`sched_name`, `trigger_name`, `trigger_group`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for qrtz_simprop_triggers
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_simprop_triggers`;
CREATE TABLE `qrtz_simprop_triggers`  (
  `sched_name` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `trigger_name` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `trigger_group` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `str_prop_1` varchar(512) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `str_prop_2` varchar(512) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `str_prop_3` varchar(512) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `int_prop_1` int(11) NULL DEFAULT NULL,
  `int_prop_2` int(11) NULL DEFAULT NULL,
  `long_prop_1` bigint(20) NULL DEFAULT NULL,
  `long_prop_2` bigint(20) NULL DEFAULT NULL,
  `dec_prop_1` decimal(13, 4) NULL DEFAULT NULL,
  `dec_prop_2` decimal(13, 4) NULL DEFAULT NULL,
  `bool_prop_1` varchar(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `bool_prop_2` varchar(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`sched_name`, `trigger_name`, `trigger_group`) USING BTREE,
  CONSTRAINT `qrtz_simprop_triggers_ibfk_1` FOREIGN KEY (`sched_name`, `trigger_name`, `trigger_group`) REFERENCES `qrtz_triggers` (`sched_name`, `trigger_name`, `trigger_group`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for qrtz_triggers
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_triggers`;
CREATE TABLE `qrtz_triggers`  (
  `sched_name` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `trigger_name` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `trigger_group` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `job_name` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `job_group` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `description` varchar(250) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `next_fire_time` bigint(13) NULL DEFAULT NULL,
  `prev_fire_time` bigint(13) NULL DEFAULT NULL,
  `priority` int(11) NULL DEFAULT NULL,
  `trigger_state` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `trigger_type` varchar(8) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `start_time` bigint(13) NOT NULL,
  `end_time` bigint(13) NULL DEFAULT NULL,
  `calendar_name` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `misfire_instr` smallint(2) NULL DEFAULT NULL,
  `job_data` blob NULL,
  PRIMARY KEY (`sched_name`, `trigger_name`, `trigger_group`) USING BTREE,
  INDEX `sched_name`(`sched_name`, `job_name`, `job_group`) USING BTREE,
  CONSTRAINT `qrtz_triggers_ibfk_1` FOREIGN KEY (`sched_name`, `job_name`, `job_group`) REFERENCES `qrtz_job_details` (`sched_name`, `job_name`, `job_group`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of qrtz_triggers
-- ----------------------------
INSERT INTO `qrtz_triggers` VALUES ('RuoyiScheduler', 'TASK_CLASS_NAME1', 'DEFAULT', 'TASK_CLASS_NAME1', 'DEFAULT', NULL, 1578322530000, -1, 5, 'PAUSED', 'CRON', 1578322523000, 0, NULL, 2, '');
INSERT INTO `qrtz_triggers` VALUES ('RuoyiScheduler', 'TASK_CLASS_NAME2', 'DEFAULT', 'TASK_CLASS_NAME2', 'DEFAULT', NULL, 1578322530000, -1, 5, 'PAUSED', 'CRON', 1578322523000, 0, NULL, 2, '');
INSERT INTO `qrtz_triggers` VALUES ('RuoyiScheduler', 'TASK_CLASS_NAME3', 'DEFAULT', 'TASK_CLASS_NAME3', 'DEFAULT', NULL, 1578322540000, -1, 5, 'PAUSED', 'CRON', 1578322523000, 0, NULL, 2, '');

-- ----------------------------
-- Table structure for riskidentification
-- ----------------------------
DROP TABLE IF EXISTS `riskidentification`;
CREATE TABLE `riskidentification`  (
  `Riskidentify_ID` int(4) NOT NULL AUTO_INCREMENT COMMENT '风险识别编号（主键）',
  `Name` char(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '名称',
  `Type` char(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '类别',
  `Address` char(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '发生地点',
  `State` char(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '状态',
  `Discription` char(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '隐患描述',
  `Avoid_method` char(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '规避方法',
  `Risk_level` char(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '风险等级',
  `Risk_factor` char(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '危险因素',
  `Time` date NULL DEFAULT NULL COMMENT '时间',
  `Remark` char(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`Riskidentify_ID`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '风险识别信息表\r\n' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of riskidentification
-- ----------------------------
INSERT INTO `riskidentification` VALUES (1, '隧道坍塌', '坍塌', '隧道', '过程中', '严格按照设计及批准的方案施工', '加强超前地质预报和监控量测', '中等', '施工坍塌', '2019-12-12', NULL);
INSERT INTO `riskidentification` VALUES (2, '火工品爆炸', '爆炸', '隧道及运输', '过程中', '统一指挥，并有经过专业培训', '值守人员要经公安部门培训持证上岗，', '高级', '易燃物品', '2020-01-06', NULL);

-- ----------------------------
-- Table structure for safecheck
-- ----------------------------
DROP TABLE IF EXISTS `safecheck`;
CREATE TABLE `safecheck`  (
  `SecuritycheckID` int(4) NOT NULL AUTO_INCREMENT COMMENT '安全检查编号(主键)',
  `Name` char(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '名称',
  `check_type` char(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '检查类型',
  `Check_person` char(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '检查人',
  `Check_department` char(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '检查部门',
  `Check_explain` char(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '说明',
  `Check_result` char(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '检查结果',
  `Solution` char(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '解决方案',
  `Check_time` date NULL DEFAULT NULL COMMENT '检查时间',
  `Sign` char(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '负责人签字',
  `Remark` char(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`SecuritycheckID`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '安全检查信息表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of safecheck
-- ----------------------------
INSERT INTO `safecheck` VALUES (1, '屋面漏水检测', '漏水检查', '张经理', '安全监督部门', '室外工程安全监督检查', '屋面不漏水', '无', '2019-09-03', '张经理', NULL);
INSERT INTO `safecheck` VALUES (2, '室内环境检测', '室内环境质量是否达标', '沙监理', '监理部门', '室内安全检查', '室内有害气体超标', '净化室内空气', '2019-10-29', '沙监理', NULL);

-- ----------------------------
-- Table structure for surveillanceaudit
-- ----------------------------
DROP TABLE IF EXISTS `surveillanceaudit`;
CREATE TABLE `surveillanceaudit`  (
  `Examine_ID` int(4) NOT NULL AUTO_INCREMENT COMMENT '审核编号(主键)',
  `Examine_content` char(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '审核内容',
  `Examine_reason` char(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '审核依据',
  `Examine_time` date NULL DEFAULT NULL COMMENT '审核日期',
  `Examine_Person` char(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '审核人',
  `Examine_department` char(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '审核对象',
  `Examine_result` char(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '审核结果',
  `Rectificate_opinions` char(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '整改意见',
  `Remark` char(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`Examine_ID`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '监督审核信息表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for sys_config
-- ----------------------------
DROP TABLE IF EXISTS `sys_config`;
CREATE TABLE `sys_config`  (
  `config_id` int(5) NOT NULL AUTO_INCREMENT COMMENT '参数主键',
  `config_name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '参数名称',
  `config_key` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '参数键名',
  `config_value` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '参数键值',
  `config_type` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT 'N' COMMENT '系统内置（Y是 N否）',
  `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`config_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '参数配置表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_config
-- ----------------------------
INSERT INTO `sys_config` VALUES (1, '主框架页-默认皮肤样式名称', 'sys.index.skinName', 'skin-blue', 'Y', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '蓝色 skin-blue、绿色 skin-green、紫色 skin-purple、红色 skin-red、黄色 skin-yellow');
INSERT INTO `sys_config` VALUES (2, '用户管理-账号初始密码', 'sys.user.initPassword', '123456', 'Y', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '初始化密码 123456');
INSERT INTO `sys_config` VALUES (3, '主框架页-侧边栏主题', 'sys.index.sideTheme', 'theme-dark', 'Y', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '深黑主题theme-dark，浅色主题theme-light，深蓝主题theme-blue');

-- ----------------------------
-- Table structure for sys_dept
-- ----------------------------
DROP TABLE IF EXISTS `sys_dept`;
CREATE TABLE `sys_dept`  (
  `dept_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '部门id',
  `parent_id` bigint(20) NULL DEFAULT 0 COMMENT '父部门id',
  `ancestors` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '祖级列表',
  `dept_name` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '部门名称',
  `order_num` int(4) NULL DEFAULT 0 COMMENT '显示顺序',
  `leader` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '负责人',
  `phone` varchar(11) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '联系电话',
  `email` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '邮箱',
  `status` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0' COMMENT '部门状态（0正常 1停用）',
  `del_flag` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0' COMMENT '删除标志（0代表存在 2代表删除）',
  `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`dept_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 204 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '部门表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_dept
-- ----------------------------
INSERT INTO `sys_dept` VALUES (100, 0, '0', '建筑安全管理', 0, '陈贺', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2018-03-16 11:33:00', 'admin', '2019-12-30 01:34:00');
INSERT INTO `sys_dept` VALUES (101, 100, '0,100', '徐州总公司', 1, '陈贺', '19851668369', 'ch@xzit.com', '0', '0', 'admin', '2018-03-16 11:33:00', 'admin', '2019-12-30 01:32:44');
INSERT INTO `sys_dept` VALUES (102, 100, '0,100', '北京分公司', 2, '郭宙', '19812344321', 'gz@qq.com', '0', '0', 'admin', '2018-03-16 11:33:00', 'admin', '2019-12-30 01:32:30');
INSERT INTO `sys_dept` VALUES (103, 101, '0,100,101', '徐州第1组', 1, '陈贺', '19851668369', 'ch@qq.com', '0', '0', 'admin', '2018-03-16 11:33:00', 'admin', '2019-12-30 01:31:19');
INSERT INTO `sys_dept` VALUES (104, 101, '0,100,101', '徐州第2组', 2, 'ch', '19812345678', 'ch@qq.com', '0', '0', 'admin', '2018-03-16 11:33:00', 'admin', '2019-12-30 01:32:44');
INSERT INTO `sys_dept` VALUES (108, 102, '0,100,102', '北京第1组', 1, 'gz', '15888888888', 'gz@qq.com', '0', '0', 'admin', '2018-03-16 11:33:00', 'admin', '2019-12-30 01:32:20');
INSERT INTO `sys_dept` VALUES (109, 102, '0,100,102', '北京第2组', 2, 'gz', '15888888888', 'gz@qq.com', '0', '0', 'admin', '2018-03-16 11:33:00', 'admin', '2019-12-30 01:32:30');
INSERT INTO `sys_dept` VALUES (200, 100, '0,100', '上海分公司', 3, 'guozhou', '19812345678', 'guozhou@qq.com', '0', '0', 'admin', '2019-12-29 21:34:31', 'admin', '2019-12-30 01:34:00');
INSERT INTO `sys_dept` VALUES (201, 200, '0,100,200', '上海第1组', 1, 'guozhou', '19812345678', 'guozhou@qq.com', '0', '0', 'admin', '2019-12-29 21:34:49', 'admin', '2019-12-30 01:33:39');
INSERT INTO `sys_dept` VALUES (202, 200, '0,100,200', '上海第2组', 2, 'guozhou', '19812345678', 'guozhou@qq.com', '0', '0', 'admin', '2019-12-29 21:35:11', 'admin', '2019-12-30 01:34:00');
INSERT INTO `sys_dept` VALUES (203, 100, '0,100', '管理员', 4, '陈贺', '19851668369', 'vchenhe@live.com', '0', '2', 'admin', '2019-12-29 21:35:35', '', NULL);

-- ----------------------------
-- Table structure for sys_dict_data
-- ----------------------------
DROP TABLE IF EXISTS `sys_dict_data`;
CREATE TABLE `sys_dict_data`  (
  `dict_code` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '字典编码',
  `dict_sort` int(4) NULL DEFAULT 0 COMMENT '字典排序',
  `dict_label` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '字典标签',
  `dict_value` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '字典键值',
  `dict_type` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '字典类型',
  `css_class` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '样式属性（其他样式扩展）',
  `list_class` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '表格回显样式',
  `is_default` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT 'N' COMMENT '是否默认（Y是 N否）',
  `status` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0' COMMENT '状态（0正常 1停用）',
  `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`dict_code`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 29 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '字典数据表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_dict_data
-- ----------------------------
INSERT INTO `sys_dict_data` VALUES (1, 1, '男', '0', 'sys_user_sex', '', '', 'Y', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '性别男');
INSERT INTO `sys_dict_data` VALUES (2, 2, '女', '1', 'sys_user_sex', '', '', 'N', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '性别女');
INSERT INTO `sys_dict_data` VALUES (3, 3, '未知', '2', 'sys_user_sex', '', '', 'N', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '性别未知');
INSERT INTO `sys_dict_data` VALUES (4, 1, '显示', '0', 'sys_show_hide', '', 'primary', 'Y', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '显示菜单');
INSERT INTO `sys_dict_data` VALUES (5, 2, '隐藏', '1', 'sys_show_hide', '', 'danger', 'N', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '隐藏菜单');
INSERT INTO `sys_dict_data` VALUES (6, 1, '正常', '0', 'sys_normal_disable', '', 'primary', 'Y', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '正常状态');
INSERT INTO `sys_dict_data` VALUES (7, 2, '停用', '1', 'sys_normal_disable', '', 'danger', 'N', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '停用状态');
INSERT INTO `sys_dict_data` VALUES (8, 1, '正常', '0', 'sys_job_status', '', 'primary', 'Y', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '正常状态');
INSERT INTO `sys_dict_data` VALUES (9, 2, '暂停', '1', 'sys_job_status', '', 'danger', 'N', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '停用状态');
INSERT INTO `sys_dict_data` VALUES (10, 1, '默认', 'DEFAULT', 'sys_job_group', '', '', 'Y', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '默认分组');
INSERT INTO `sys_dict_data` VALUES (11, 2, '系统', 'SYSTEM', 'sys_job_group', '', '', 'N', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '系统分组');
INSERT INTO `sys_dict_data` VALUES (12, 1, '是', 'Y', 'sys_yes_no', '', 'primary', 'Y', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '系统默认是');
INSERT INTO `sys_dict_data` VALUES (13, 2, '否', 'N', 'sys_yes_no', '', 'danger', 'N', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '系统默认否');
INSERT INTO `sys_dict_data` VALUES (14, 1, '通知', '1', 'sys_notice_type', '', 'warning', 'Y', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '通知');
INSERT INTO `sys_dict_data` VALUES (15, 2, '公告', '2', 'sys_notice_type', '', 'success', 'N', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '公告');
INSERT INTO `sys_dict_data` VALUES (16, 1, '正常', '0', 'sys_notice_status', '', 'primary', 'Y', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '正常状态');
INSERT INTO `sys_dict_data` VALUES (17, 2, '关闭', '1', 'sys_notice_status', '', 'danger', 'N', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '关闭状态');
INSERT INTO `sys_dict_data` VALUES (18, 1, '新增', '1', 'sys_oper_type', '', 'info', 'N', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '新增操作');
INSERT INTO `sys_dict_data` VALUES (19, 2, '修改', '2', 'sys_oper_type', '', 'info', 'N', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '修改操作');
INSERT INTO `sys_dict_data` VALUES (20, 3, '删除', '3', 'sys_oper_type', '', 'danger', 'N', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '删除操作');
INSERT INTO `sys_dict_data` VALUES (21, 4, '授权', '4', 'sys_oper_type', '', 'primary', 'N', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '授权操作');
INSERT INTO `sys_dict_data` VALUES (22, 5, '导出', '5', 'sys_oper_type', '', 'warning', 'N', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '导出操作');
INSERT INTO `sys_dict_data` VALUES (23, 6, '导入', '6', 'sys_oper_type', '', 'warning', 'N', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '导入操作');
INSERT INTO `sys_dict_data` VALUES (24, 7, '强退', '7', 'sys_oper_type', '', 'danger', 'N', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '强退操作');
INSERT INTO `sys_dict_data` VALUES (25, 8, '生成代码', '8', 'sys_oper_type', '', 'warning', 'N', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '生成操作');
INSERT INTO `sys_dict_data` VALUES (26, 9, '清空数据', '9', 'sys_oper_type', '', 'danger', 'N', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '清空操作');
INSERT INTO `sys_dict_data` VALUES (27, 1, '成功', '0', 'sys_common_status', '', 'primary', 'N', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '正常状态');
INSERT INTO `sys_dict_data` VALUES (28, 2, '失败', '1', 'sys_common_status', '', 'danger', 'N', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '停用状态');

-- ----------------------------
-- Table structure for sys_dict_type
-- ----------------------------
DROP TABLE IF EXISTS `sys_dict_type`;
CREATE TABLE `sys_dict_type`  (
  `dict_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '字典主键',
  `dict_name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '字典名称',
  `dict_type` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '字典类型',
  `status` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0' COMMENT '状态（0正常 1停用）',
  `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`dict_id`) USING BTREE,
  UNIQUE INDEX `dict_type`(`dict_type`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 11 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '字典类型表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_dict_type
-- ----------------------------
INSERT INTO `sys_dict_type` VALUES (1, '用户性别', 'sys_user_sex', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '用户性别列表');
INSERT INTO `sys_dict_type` VALUES (2, '菜单状态', 'sys_show_hide', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '菜单状态列表');
INSERT INTO `sys_dict_type` VALUES (3, '系统开关', 'sys_normal_disable', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '系统开关列表');
INSERT INTO `sys_dict_type` VALUES (4, '任务状态', 'sys_job_status', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '任务状态列表');
INSERT INTO `sys_dict_type` VALUES (5, '任务分组', 'sys_job_group', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '任务分组列表');
INSERT INTO `sys_dict_type` VALUES (6, '系统是否', 'sys_yes_no', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '系统是否列表');
INSERT INTO `sys_dict_type` VALUES (7, '通知类型', 'sys_notice_type', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '通知类型列表');
INSERT INTO `sys_dict_type` VALUES (8, '通知状态', 'sys_notice_status', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '通知状态列表');
INSERT INTO `sys_dict_type` VALUES (9, '操作类型', 'sys_oper_type', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '操作类型列表');
INSERT INTO `sys_dict_type` VALUES (10, '系统状态', 'sys_common_status', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '登录状态列表');

-- ----------------------------
-- Table structure for sys_job
-- ----------------------------
DROP TABLE IF EXISTS `sys_job`;
CREATE TABLE `sys_job`  (
  `job_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '任务ID',
  `job_name` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '任务名称',
  `job_group` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT 'DEFAULT' COMMENT '任务组名',
  `invoke_target` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '调用目标字符串',
  `cron_expression` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT 'cron执行表达式',
  `misfire_policy` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '3' COMMENT '计划执行错误策略（1立即执行 2执行一次 3放弃执行）',
  `concurrent` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '1' COMMENT '是否并发执行（0允许 1禁止）',
  `status` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0' COMMENT '状态（0正常 1暂停）',
  `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '备注信息',
  PRIMARY KEY (`job_id`, `job_name`, `job_group`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '定时任务调度表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_job
-- ----------------------------
INSERT INTO `sys_job` VALUES (1, '系统默认（无参）', 'DEFAULT', 'ryTask.ryNoParams', '0/10 * * * * ?', '3', '1', '1', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_job` VALUES (2, '系统默认（有参）', 'DEFAULT', 'ryTask.ryParams(\'ry\')', '0/15 * * * * ?', '3', '1', '1', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_job` VALUES (3, '系统默认（多参）', 'DEFAULT', 'ryTask.ryMultipleParams(\'ry\', true, 2000L, 316.50D, 100)', '0/20 * * * * ?', '3', '1', '1', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');

-- ----------------------------
-- Table structure for sys_job_log
-- ----------------------------
DROP TABLE IF EXISTS `sys_job_log`;
CREATE TABLE `sys_job_log`  (
  `job_log_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '任务日志ID',
  `job_name` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '任务名称',
  `job_group` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '任务组名',
  `invoke_target` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '调用目标字符串',
  `job_message` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '日志信息',
  `status` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0' COMMENT '执行状态（0正常 1失败）',
  `exception_info` varchar(2000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '异常信息',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`job_log_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '定时任务调度日志表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for sys_logininfor
-- ----------------------------
DROP TABLE IF EXISTS `sys_logininfor`;
CREATE TABLE `sys_logininfor`  (
  `info_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '访问ID',
  `login_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '登录账号',
  `ipaddr` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '登录IP地址',
  `login_location` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '登录地点',
  `browser` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '浏览器类型',
  `os` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '操作系统',
  `status` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0' COMMENT '登录状态（0成功 1失败）',
  `msg` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '提示消息',
  `login_time` datetime(0) NULL DEFAULT NULL COMMENT '访问时间',
  PRIMARY KEY (`info_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 219 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '系统访问记录' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_logininfor
-- ----------------------------
INSERT INTO `sys_logininfor` VALUES (100, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '登录成功', '2019-12-29 20:04:49');
INSERT INTO `sys_logininfor` VALUES (101, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '退出成功', '2019-12-29 20:39:36');
INSERT INTO `sys_logininfor` VALUES (102, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '登录成功', '2019-12-29 20:39:41');
INSERT INTO `sys_logininfor` VALUES (103, 'chenhe', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '登录成功', '2019-12-29 20:39:52');
INSERT INTO `sys_logininfor` VALUES (104, 'chenhe', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '退出成功', '2019-12-29 20:48:43');
INSERT INTO `sys_logininfor` VALUES (105, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '1', '验证码错误', '2019-12-29 20:48:48');
INSERT INTO `sys_logininfor` VALUES (106, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '登录成功', '2019-12-29 20:48:51');
INSERT INTO `sys_logininfor` VALUES (107, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '退出成功', '2019-12-29 20:49:19');
INSERT INTO `sys_logininfor` VALUES (108, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '登录成功', '2019-12-29 21:09:44');
INSERT INTO `sys_logininfor` VALUES (109, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '退出成功', '2019-12-29 21:17:10');
INSERT INTO `sys_logininfor` VALUES (110, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '登录成功', '2019-12-29 21:17:15');
INSERT INTO `sys_logininfor` VALUES (111, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '登录成功', '2019-12-29 22:02:26');
INSERT INTO `sys_logininfor` VALUES (112, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '1', '验证码错误', '2019-12-29 22:13:06');
INSERT INTO `sys_logininfor` VALUES (113, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '1', '密码输入错误1次', '2019-12-29 22:13:12');
INSERT INTO `sys_logininfor` VALUES (114, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '登录成功', '2019-12-29 22:13:17');
INSERT INTO `sys_logininfor` VALUES (115, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '1', '验证码错误', '2019-12-29 22:39:40');
INSERT INTO `sys_logininfor` VALUES (116, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '登录成功', '2019-12-29 22:39:44');
INSERT INTO `sys_logininfor` VALUES (117, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '1', '验证码错误', '2019-12-29 22:49:38');
INSERT INTO `sys_logininfor` VALUES (118, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '登录成功', '2019-12-29 22:49:41');
INSERT INTO `sys_logininfor` VALUES (119, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '退出成功', '2019-12-29 22:50:11');
INSERT INTO `sys_logininfor` VALUES (120, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '登录成功', '2019-12-29 23:01:00');
INSERT INTO `sys_logininfor` VALUES (121, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '1', '验证码错误', '2019-12-29 23:05:29');
INSERT INTO `sys_logininfor` VALUES (122, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '1', '验证码错误', '2019-12-29 23:43:44');
INSERT INTO `sys_logininfor` VALUES (123, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '登录成功', '2019-12-29 23:53:16');
INSERT INTO `sys_logininfor` VALUES (124, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '退出成功', '2019-12-29 23:53:40');
INSERT INTO `sys_logininfor` VALUES (125, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '登录成功', '2019-12-29 23:58:41');
INSERT INTO `sys_logininfor` VALUES (126, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '退出成功', '2019-12-30 01:34:37');
INSERT INTO `sys_logininfor` VALUES (127, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '登录成功', '2019-12-30 01:34:41');
INSERT INTO `sys_logininfor` VALUES (128, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '退出成功', '2019-12-30 02:13:34');
INSERT INTO `sys_logininfor` VALUES (129, 'chenhe1', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '登录成功', '2019-12-30 02:13:48');
INSERT INTO `sys_logininfor` VALUES (130, 'chenhe1', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '退出成功', '2019-12-30 02:21:47');
INSERT INTO `sys_logininfor` VALUES (131, 'chenhe1', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '登录成功', '2019-12-30 02:22:30');
INSERT INTO `sys_logininfor` VALUES (132, 'chenhe1', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '退出成功', '2019-12-30 02:22:38');
INSERT INTO `sys_logininfor` VALUES (133, 'chenhe2', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '登录成功', '2019-12-30 02:22:46');
INSERT INTO `sys_logininfor` VALUES (134, 'chenhe2', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '退出成功', '2019-12-30 02:22:55');
INSERT INTO `sys_logininfor` VALUES (135, 'chenhe3', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '登录成功', '2019-12-30 02:23:02');
INSERT INTO `sys_logininfor` VALUES (136, 'chenhe3', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '退出成功', '2019-12-30 02:23:30');
INSERT INTO `sys_logininfor` VALUES (137, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '登录成功', '2019-12-30 02:23:40');
INSERT INTO `sys_logininfor` VALUES (138, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '退出成功', '2019-12-30 02:24:42');
INSERT INTO `sys_logininfor` VALUES (139, 'chenhe2', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '登录成功', '2019-12-30 02:24:50');
INSERT INTO `sys_logininfor` VALUES (140, 'chenhe2', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '退出成功', '2019-12-30 02:25:09');
INSERT INTO `sys_logininfor` VALUES (141, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '登录成功', '2019-12-30 02:25:15');
INSERT INTO `sys_logininfor` VALUES (142, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '退出成功', '2019-12-30 02:30:04');
INSERT INTO `sys_logininfor` VALUES (143, 'chenhe1', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '登录成功', '2019-12-30 02:30:09');
INSERT INTO `sys_logininfor` VALUES (144, 'chenhe1', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '退出成功', '2019-12-30 02:31:13');
INSERT INTO `sys_logininfor` VALUES (145, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '登录成功', '2019-12-30 02:31:19');
INSERT INTO `sys_logininfor` VALUES (146, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '退出成功', '2019-12-30 02:32:03');
INSERT INTO `sys_logininfor` VALUES (147, 'chenhe1', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '登录成功', '2019-12-30 02:32:13');
INSERT INTO `sys_logininfor` VALUES (148, 'chenhe1', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '退出成功', '2019-12-30 02:33:14');
INSERT INTO `sys_logininfor` VALUES (149, 'chenhe2', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '登录成功', '2019-12-30 02:33:19');
INSERT INTO `sys_logininfor` VALUES (150, 'chenhe2', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '退出成功', '2019-12-30 02:33:40');
INSERT INTO `sys_logininfor` VALUES (151, 'chenhe3', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '登录成功', '2019-12-30 02:33:45');
INSERT INTO `sys_logininfor` VALUES (152, 'chenhe3', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '退出成功', '2019-12-30 02:34:19');
INSERT INTO `sys_logininfor` VALUES (153, 'chenhe1', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '登录成功', '2019-12-30 02:43:00');
INSERT INTO `sys_logininfor` VALUES (154, 'chenhe1', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '登录成功', '2019-12-30 02:51:20');
INSERT INTO `sys_logininfor` VALUES (155, 'chenhe1', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '退出成功', '2019-12-30 02:51:26');
INSERT INTO `sys_logininfor` VALUES (156, 'chenhe1', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '登录成功', '2019-12-30 03:10:09');
INSERT INTO `sys_logininfor` VALUES (157, 'chenhe1', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '退出成功', '2019-12-30 03:10:40');
INSERT INTO `sys_logininfor` VALUES (158, 'chenhe1', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '登录成功', '2019-12-30 09:41:31');
INSERT INTO `sys_logininfor` VALUES (159, 'qinjia1', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '登录成功', '2019-12-30 13:56:06');
INSERT INTO `sys_logininfor` VALUES (160, 'qinjia1', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '退出成功', '2019-12-30 13:57:02');
INSERT INTO `sys_logininfor` VALUES (161, 'chenhe1', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '1', '验证码错误', '2019-12-30 14:07:16');
INSERT INTO `sys_logininfor` VALUES (162, 'chenhe1', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '登录成功', '2019-12-30 14:07:20');
INSERT INTO `sys_logininfor` VALUES (163, 'chenhe1', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '登录成功', '2019-12-30 21:07:47');
INSERT INTO `sys_logininfor` VALUES (164, 'chenhe1', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '退出成功', '2019-12-30 21:08:20');
INSERT INTO `sys_logininfor` VALUES (165, 'chenhe2', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '登录成功', '2019-12-30 21:08:28');
INSERT INTO `sys_logininfor` VALUES (166, 'chenhe2', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '退出成功', '2019-12-30 21:08:39');
INSERT INTO `sys_logininfor` VALUES (167, 'chenhe3', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '登录成功', '2019-12-30 21:08:47');
INSERT INTO `sys_logininfor` VALUES (168, 'chenhe3', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '退出成功', '2019-12-30 21:09:15');
INSERT INTO `sys_logininfor` VALUES (169, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '登录成功', '2019-12-30 21:09:23');
INSERT INTO `sys_logininfor` VALUES (170, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '退出成功', '2019-12-30 21:11:41');
INSERT INTO `sys_logininfor` VALUES (171, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '登录成功', '2019-12-30 21:52:16');
INSERT INTO `sys_logininfor` VALUES (172, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '退出成功', '2020-01-03 08:54:44');
INSERT INTO `sys_logininfor` VALUES (173, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '登录成功', '2020-01-03 08:54:54');
INSERT INTO `sys_logininfor` VALUES (174, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '退出成功', '2020-01-03 08:59:37');
INSERT INTO `sys_logininfor` VALUES (175, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '登录成功', '2020-01-03 08:59:53');
INSERT INTO `sys_logininfor` VALUES (176, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '登录成功', '2020-01-03 09:08:03');
INSERT INTO `sys_logininfor` VALUES (177, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '登录成功', '2020-01-03 09:13:43');
INSERT INTO `sys_logininfor` VALUES (178, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '退出成功', '2020-01-03 09:53:20');
INSERT INTO `sys_logininfor` VALUES (179, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '登录成功', '2020-01-03 09:53:38');
INSERT INTO `sys_logininfor` VALUES (180, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '退出成功', '2020-01-03 10:00:38');
INSERT INTO `sys_logininfor` VALUES (181, 'chenhe1', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '登录成功', '2020-01-03 10:11:42');
INSERT INTO `sys_logininfor` VALUES (182, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '登录成功', '2020-01-05 22:06:01');
INSERT INTO `sys_logininfor` VALUES (183, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '退出成功', '2020-01-05 22:25:23');
INSERT INTO `sys_logininfor` VALUES (184, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '登录成功', '2020-01-06 08:45:29');
INSERT INTO `sys_logininfor` VALUES (185, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '退出成功', '2020-01-06 09:16:28');
INSERT INTO `sys_logininfor` VALUES (186, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '登录成功', '2020-01-06 09:17:01');
INSERT INTO `sys_logininfor` VALUES (187, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '1', '验证码错误', '2020-01-06 09:48:59');
INSERT INTO `sys_logininfor` VALUES (188, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '登录成功', '2020-01-06 09:49:01');
INSERT INTO `sys_logininfor` VALUES (189, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '登录成功', '2020-01-06 14:37:29');
INSERT INTO `sys_logininfor` VALUES (190, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '退出成功', '2020-01-06 14:40:45');
INSERT INTO `sys_logininfor` VALUES (191, 'chenhe3', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '登录成功', '2020-01-06 14:40:54');
INSERT INTO `sys_logininfor` VALUES (192, 'chenhe3', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '退出成功', '2020-01-06 14:41:21');
INSERT INTO `sys_logininfor` VALUES (193, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '登录成功', '2020-01-06 14:41:26');
INSERT INTO `sys_logininfor` VALUES (194, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '退出成功', '2020-01-06 14:42:20');
INSERT INTO `sys_logininfor` VALUES (195, 'chenhe3', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '登录成功', '2020-01-06 14:42:26');
INSERT INTO `sys_logininfor` VALUES (196, 'chenhe3', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '退出成功', '2020-01-06 14:42:42');
INSERT INTO `sys_logininfor` VALUES (197, 'chenhe1', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '登录成功', '2020-01-06 14:42:48');
INSERT INTO `sys_logininfor` VALUES (198, 'chenhe1', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '退出成功', '2020-01-06 14:43:17');
INSERT INTO `sys_logininfor` VALUES (199, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '登录成功', '2020-01-06 14:43:22');
INSERT INTO `sys_logininfor` VALUES (200, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '退出成功', '2020-01-06 14:52:28');
INSERT INTO `sys_logininfor` VALUES (201, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '登录成功', '2020-01-06 14:52:36');
INSERT INTO `sys_logininfor` VALUES (202, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '1', '验证码错误', '2020-01-06 14:53:27');
INSERT INTO `sys_logininfor` VALUES (203, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '登录成功', '2020-01-06 14:53:30');
INSERT INTO `sys_logininfor` VALUES (204, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '登录成功', '2020-01-06 15:04:41');
INSERT INTO `sys_logininfor` VALUES (205, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '登录成功', '2020-01-06 15:13:36');
INSERT INTO `sys_logininfor` VALUES (206, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '1', '验证码错误', '2020-01-06 15:31:33');
INSERT INTO `sys_logininfor` VALUES (207, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '登录成功', '2020-01-06 15:31:37');
INSERT INTO `sys_logininfor` VALUES (208, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '登录成功', '2020-01-06 15:38:08');
INSERT INTO `sys_logininfor` VALUES (209, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '登录成功', '2020-01-06 15:46:19');
INSERT INTO `sys_logininfor` VALUES (210, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '登录成功', '2020-01-06 16:43:03');
INSERT INTO `sys_logininfor` VALUES (211, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '登录成功', '2020-01-06 16:54:50');
INSERT INTO `sys_logininfor` VALUES (212, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '退出成功', '2020-01-06 16:55:17');
INSERT INTO `sys_logininfor` VALUES (213, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '登录成功', '2020-01-06 19:10:53');
INSERT INTO `sys_logininfor` VALUES (214, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '1', '验证码错误', '2020-01-06 22:56:18');
INSERT INTO `sys_logininfor` VALUES (215, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '登录成功', '2020-01-06 22:56:21');
INSERT INTO `sys_logininfor` VALUES (216, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '登录成功', '2020-01-06 23:17:15');
INSERT INTO `sys_logininfor` VALUES (217, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '登录成功', '2020-01-07 00:06:58');
INSERT INTO `sys_logininfor` VALUES (218, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '登录成功', '2020-01-07 00:55:21');

-- ----------------------------
-- Table structure for sys_menu
-- ----------------------------
DROP TABLE IF EXISTS `sys_menu`;
CREATE TABLE `sys_menu`  (
  `menu_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '菜单ID',
  `menu_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '菜单名称',
  `parent_id` bigint(20) NULL DEFAULT 0 COMMENT '父菜单ID',
  `order_num` int(4) NULL DEFAULT 0 COMMENT '显示顺序',
  `url` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '#' COMMENT '请求地址',
  `target` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '打开方式（menuItem页签 menuBlank新窗口）',
  `menu_type` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '菜单类型（M目录 C菜单 F按钮）',
  `visible` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0' COMMENT '菜单状态（0显示 1隐藏）',
  `perms` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '权限标识',
  `icon` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '#' COMMENT '菜单图标',
  `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '备注',
  PRIMARY KEY (`menu_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2057 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '菜单权限表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_menu
-- ----------------------------
INSERT INTO `sys_menu` VALUES (1, '***系统管理', 0, 9, '#', 'menuItem', 'M', '0', '', 'fa fa-gear', 'admin', '2018-03-16 11:33:00', 'admin', '2019-12-30 01:22:01', '系统管理目录');
INSERT INTO `sys_menu` VALUES (2, '***系统监控', 0, 7, '#', 'menuItem', 'M', '0', '', 'fa fa-video-camera', 'admin', '2018-03-16 11:33:00', 'admin', '2019-12-30 01:21:52', '系统监控目录');
INSERT INTO `sys_menu` VALUES (3, '***系统工具', 0, 8, '#', 'menuItem', 'M', '0', '', 'fa fa-bars', 'admin', '2018-03-16 11:33:00', 'admin', '2019-12-30 01:21:57', '系统工具目录');
INSERT INTO `sys_menu` VALUES (100, '用户管理', 1, 1, '/system/user', '', 'C', '0', 'system:user:view', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '用户管理菜单');
INSERT INTO `sys_menu` VALUES (101, '角色管理', 1, 2, '/system/role', '', 'C', '0', 'system:role:view', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '角色管理菜单');
INSERT INTO `sys_menu` VALUES (102, '菜单管理', 1, 3, '/system/menu', '', 'C', '0', 'system:menu:view', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '菜单管理菜单');
INSERT INTO `sys_menu` VALUES (103, '部门管理', 1, 4, '/system/dept', '', 'C', '0', 'system:dept:view', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '部门管理菜单');
INSERT INTO `sys_menu` VALUES (104, '岗位管理', 1, 5, '/system/post', '', 'C', '0', 'system:post:view', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '岗位管理菜单');
INSERT INTO `sys_menu` VALUES (105, '字典管理', 1, 6, '/system/dict', '', 'C', '0', 'system:dict:view', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '字典管理菜单');
INSERT INTO `sys_menu` VALUES (106, '参数设置', 1, 7, '/system/config', '', 'C', '0', 'system:config:view', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '参数设置菜单');
INSERT INTO `sys_menu` VALUES (108, '日志管理', 1, 9, '#', '', 'M', '0', '', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '日志管理菜单');
INSERT INTO `sys_menu` VALUES (109, '在线用户', 2, 1, '/monitor/online', '', 'C', '0', 'monitor:online:view', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '在线用户菜单');
INSERT INTO `sys_menu` VALUES (110, '定时任务', 2, 2, '/monitor/job', '', 'C', '0', 'monitor:job:view', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '定时任务菜单');
INSERT INTO `sys_menu` VALUES (111, '数据监控', 2, 3, '/monitor/data', '', 'C', '0', 'monitor:data:view', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '数据监控菜单');
INSERT INTO `sys_menu` VALUES (112, '服务监控', 2, 3, '/monitor/server', '', 'C', '0', 'monitor:server:view', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '服务监控菜单');
INSERT INTO `sys_menu` VALUES (113, '表单构建', 3, 1, '/tool/build', '', 'C', '0', 'tool:build:view', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '表单构建菜单');
INSERT INTO `sys_menu` VALUES (114, '代码生成', 3, 2, '/tool/gen', '', 'C', '0', 'tool:gen:view', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '代码生成菜单');
INSERT INTO `sys_menu` VALUES (115, '系统接口', 3, 3, '/tool/swagger', '', 'C', '0', 'tool:swagger:view', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '系统接口菜单');
INSERT INTO `sys_menu` VALUES (500, '操作日志', 108, 1, '/monitor/operlog', '', 'C', '0', 'monitor:operlog:view', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '操作日志菜单');
INSERT INTO `sys_menu` VALUES (501, '登录日志', 108, 2, '/monitor/logininfor', '', 'C', '0', 'monitor:logininfor:view', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '登录日志菜单');
INSERT INTO `sys_menu` VALUES (1000, '用户查询', 100, 1, '#', '', 'F', '0', 'system:user:list', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1001, '用户新增', 100, 2, '#', '', 'F', '0', 'system:user:add', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1002, '用户修改', 100, 3, '#', '', 'F', '0', 'system:user:edit', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1003, '用户删除', 100, 4, '#', '', 'F', '0', 'system:user:remove', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1004, '用户导出', 100, 5, '#', '', 'F', '0', 'system:user:export', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1005, '用户导入', 100, 6, '#', '', 'F', '0', 'system:user:import', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1006, '重置密码', 100, 7, '#', '', 'F', '0', 'system:user:resetPwd', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1007, '角色查询', 101, 1, '#', '', 'F', '0', 'system:role:list', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1008, '角色新增', 101, 2, '#', '', 'F', '0', 'system:role:add', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1009, '角色修改', 101, 3, '#', '', 'F', '0', 'system:role:edit', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1010, '角色删除', 101, 4, '#', '', 'F', '0', 'system:role:remove', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1011, '角色导出', 101, 5, '#', '', 'F', '0', 'system:role:export', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1012, '菜单查询', 102, 1, '#', '', 'F', '0', 'system:menu:list', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1013, '菜单新增', 102, 2, '#', '', 'F', '0', 'system:menu:add', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1014, '菜单修改', 102, 3, '#', '', 'F', '0', 'system:menu:edit', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1015, '菜单删除', 102, 4, '#', '', 'F', '0', 'system:menu:remove', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1016, '部门查询', 103, 1, '#', '', 'F', '0', 'system:dept:list', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1017, '部门新增', 103, 2, '#', '', 'F', '0', 'system:dept:add', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1018, '部门修改', 103, 3, '#', '', 'F', '0', 'system:dept:edit', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1019, '部门删除', 103, 4, '#', '', 'F', '0', 'system:dept:remove', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1020, '岗位查询', 104, 1, '#', '', 'F', '0', 'system:post:list', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1021, '岗位新增', 104, 2, '#', '', 'F', '0', 'system:post:add', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1022, '岗位修改', 104, 3, '#', '', 'F', '0', 'system:post:edit', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1023, '岗位删除', 104, 4, '#', '', 'F', '0', 'system:post:remove', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1024, '岗位导出', 104, 5, '#', '', 'F', '0', 'system:post:export', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1025, '字典查询', 105, 1, '#', '', 'F', '0', 'system:dict:list', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1026, '字典新增', 105, 2, '#', '', 'F', '0', 'system:dict:add', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1027, '字典修改', 105, 3, '#', '', 'F', '0', 'system:dict:edit', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1028, '字典删除', 105, 4, '#', '', 'F', '0', 'system:dict:remove', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1029, '字典导出', 105, 5, '#', '', 'F', '0', 'system:dict:export', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1030, '参数查询', 106, 1, '#', '', 'F', '0', 'system:config:list', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1031, '参数新增', 106, 2, '#', '', 'F', '0', 'system:config:add', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1032, '参数修改', 106, 3, '#', '', 'F', '0', 'system:config:edit', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1033, '参数删除', 106, 4, '#', '', 'F', '0', 'system:config:remove', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1034, '参数导出', 106, 5, '#', '', 'F', '0', 'system:config:export', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1035, '公告查询', 2000, 1, '#', '', 'F', '0', 'system:notice:list', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1036, '公告新增', 2000, 2, '#', '', 'F', '0', 'system:notice:add', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1037, '公告修改', 2000, 3, '#', '', 'F', '0', 'system:notice:edit', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1038, '公告删除', 2000, 4, '#', '', 'F', '0', 'system:notice:remove', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1039, '操作查询', 500, 1, '#', '', 'F', '0', 'monitor:operlog:list', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1040, '操作删除', 500, 2, '#', '', 'F', '0', 'monitor:operlog:remove', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1041, '详细信息', 500, 3, '#', '', 'F', '0', 'monitor:operlog:detail', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1042, '日志导出', 500, 4, '#', '', 'F', '0', 'monitor:operlog:export', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1043, '登录查询', 501, 1, '#', '', 'F', '0', 'monitor:logininfor:list', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1044, '登录删除', 501, 2, '#', '', 'F', '0', 'monitor:logininfor:remove', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1045, '日志导出', 501, 3, '#', '', 'F', '0', 'monitor:logininfor:export', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1046, '账户解锁', 501, 4, '#', '', 'F', '0', 'monitor:logininfor:unlock', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1047, '在线查询', 109, 1, '#', '', 'F', '0', 'monitor:online:list', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1048, '批量强退', 109, 2, '#', '', 'F', '0', 'monitor:online:batchForceLogout', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1049, '单条强退', 109, 3, '#', '', 'F', '0', 'monitor:online:forceLogout', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1050, '任务查询', 110, 1, '#', '', 'F', '0', 'monitor:job:list', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1051, '任务新增', 110, 2, '#', '', 'F', '0', 'monitor:job:add', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1052, '任务修改', 110, 3, '#', '', 'F', '0', 'monitor:job:edit', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1053, '任务删除', 110, 4, '#', '', 'F', '0', 'monitor:job:remove', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1054, '状态修改', 110, 5, '#', '', 'F', '0', 'monitor:job:changeStatus', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1055, '任务详细', 110, 6, '#', '', 'F', '0', 'monitor:job:detail', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1056, '任务导出', 110, 7, '#', '', 'F', '0', 'monitor:job:export', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1057, '生成查询', 114, 1, '#', '', 'F', '0', 'tool:gen:list', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1058, '生成修改', 114, 2, '#', '', 'F', '0', 'tool:gen:edit', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1059, '生成删除', 114, 3, '#', '', 'F', '0', 'tool:gen:remove', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1060, '预览代码', 114, 4, '#', '', 'F', '0', 'tool:gen:preview', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1061, '生成代码', 114, 5, '#', '', 'F', '0', 'tool:gen:code', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (2000, '安全检查管理', 0, 1, '/system/notice', 'menuItem', 'C', '0', 'system:notice:view', 'fa fa-american-sign-language-interpreting', 'admin', '2019-12-29 21:22:37', 'admin', '2020-01-07 00:08:16', '');
INSERT INTO `sys_menu` VALUES (2006, '施工事故管理', 0, 5, '/system/accident', 'menuItem', 'C', '0', 'system:accident:view', 'fa fa-binoculars', 'admin', '2019-12-29 21:29:17', 'admin', '2020-01-06 15:40:51', '');
INSERT INTO `sys_menu` VALUES (2028, '施工事故管理查询', 2006, 1, '#', 'menuItem', 'F', '0', 'system:accident:list', '#', 'admin', '2018-03-01 00:00:00', 'admin', '2020-01-03 09:35:08', '');
INSERT INTO `sys_menu` VALUES (2029, '施工事故管理新增', 2006, 2, '#', 'menuItem', 'F', '0', 'system:accident:add', '#', 'admin', '2018-03-01 00:00:00', 'admin', '2020-01-03 09:35:31', '');
INSERT INTO `sys_menu` VALUES (2030, '施工事故管理修改', 2006, 3, '#', 'menuItem', 'F', '0', 'system:accident:edit', '#', 'admin', '2018-03-01 00:00:00', 'admin', '2020-01-03 09:35:47', '');
INSERT INTO `sys_menu` VALUES (2031, '施工事故管理删除', 2006, 4, '#', 'menuItem', 'F', '0', 'system:accident:remove', '#', 'admin', '2018-03-01 00:00:00', 'admin', '2020-01-03 09:36:04', '');
INSERT INTO `sys_menu` VALUES (2032, '施工事故管理导出', 2006, 5, '#', 'menuItem', 'F', '0', 'system:accident:export', '#', 'admin', '2018-03-01 00:00:00', 'admin', '2020-01-03 09:36:21', '');
INSERT INTO `sys_menu` VALUES (2033, '现场隐患信息', 0, 3, '/system/hiddendanger', 'menuItem', 'C', '0', 'system:hiddendanger:view', 'fa fa-bell', 'admin', '2018-03-01 00:00:00', 'admin', '2020-01-06 15:40:20', '安全隐患信息菜单');
INSERT INTO `sys_menu` VALUES (2034, '安全隐患信息查询', 2033, 1, '#', '', 'F', '0', 'system:hiddendanger:list', '#', 'admin', '2018-03-01 00:00:00', 'ry', '2018-03-01 00:00:00', '');
INSERT INTO `sys_menu` VALUES (2035, '安全隐患信息新增', 2033, 2, '#', '', 'F', '0', 'system:hiddendanger:add', '#', 'admin', '2018-03-01 00:00:00', 'ry', '2018-03-01 00:00:00', '');
INSERT INTO `sys_menu` VALUES (2036, '安全隐患信息修改', 2033, 3, '#', '', 'F', '0', 'system:hiddendanger:edit', '#', 'admin', '2018-03-01 00:00:00', 'ry', '2018-03-01 00:00:00', '');
INSERT INTO `sys_menu` VALUES (2037, '安全隐患信息删除', 2033, 4, '#', '', 'F', '0', 'system:hiddendanger:remove', '#', 'admin', '2018-03-01 00:00:00', 'ry', '2018-03-01 00:00:00', '');
INSERT INTO `sys_menu` VALUES (2038, '安全隐患信息导出', 2033, 5, '#', '', 'F', '0', 'system:hiddendanger:export', '#', 'admin', '2018-03-01 00:00:00', 'ry', '2018-03-01 00:00:00', '');
INSERT INTO `sys_menu` VALUES (2039, '施工风险管理', 0, 2, '/system/riskidentification', 'menuItem', 'C', '0', 'system:riskidentification:view', 'fa fa-book', 'admin', '2018-03-01 00:00:00', 'admin', '2020-01-06 15:40:16', '风险识别信息菜单');
INSERT INTO `sys_menu` VALUES (2040, '风险识别信息查询', 2039, 1, '#', '', 'F', '0', 'system:riskidentification:list', '#', 'admin', '2018-03-01 00:00:00', 'ry', '2018-03-01 00:00:00', '');
INSERT INTO `sys_menu` VALUES (2041, '风险识别信息新增', 2039, 2, '#', '', 'F', '0', 'system:riskidentification:add', '#', 'admin', '2018-03-01 00:00:00', 'ry', '2018-03-01 00:00:00', '');
INSERT INTO `sys_menu` VALUES (2042, '风险识别信息修改', 2039, 3, '#', '', 'F', '0', 'system:riskidentification:edit', '#', 'admin', '2018-03-01 00:00:00', 'ry', '2018-03-01 00:00:00', '');
INSERT INTO `sys_menu` VALUES (2043, '风险识别信息删除', 2039, 4, '#', '', 'F', '0', 'system:riskidentification:remove', '#', 'admin', '2018-03-01 00:00:00', 'ry', '2018-03-01 00:00:00', '');
INSERT INTO `sys_menu` VALUES (2044, '风险识别信息导出', 2039, 5, '#', '', 'F', '0', 'system:riskidentification:export', '#', 'admin', '2018-03-01 00:00:00', 'ry', '2018-03-01 00:00:00', '');
INSERT INTO `sys_menu` VALUES (2045, '应急预案管理', 0, 4, '/system/contingencyplan', 'menuItem', 'C', '0', 'system:contingencyplan:view', 'fa fa-blind', 'admin', '2018-03-01 00:00:00', 'admin', '2020-01-06 15:40:47', '应急预案信息 \r\n菜单');
INSERT INTO `sys_menu` VALUES (2046, '应急预案信息 \r\n查询', 2045, 1, '#', '', 'F', '0', 'system:contingencyplan:list', '#', 'admin', '2018-03-01 00:00:00', 'ry', '2018-03-01 00:00:00', '');
INSERT INTO `sys_menu` VALUES (2047, '应急预案信息 \r\n新增', 2045, 2, '#', '', 'F', '0', 'system:contingencyplan:add', '#', 'admin', '2018-03-01 00:00:00', 'ry', '2018-03-01 00:00:00', '');
INSERT INTO `sys_menu` VALUES (2048, '应急预案信息 \r\n修改', 2045, 3, '#', '', 'F', '0', 'system:contingencyplan:edit', '#', 'admin', '2018-03-01 00:00:00', 'ry', '2018-03-01 00:00:00', '');
INSERT INTO `sys_menu` VALUES (2049, '应急预案信息 \r\n删除', 2045, 4, '#', '', 'F', '0', 'system:contingencyplan:remove', '#', 'admin', '2018-03-01 00:00:00', 'ry', '2018-03-01 00:00:00', '');
INSERT INTO `sys_menu` VALUES (2050, '应急预案信息 \r\n导出', 2045, 5, '#', '', 'F', '0', 'system:contingencyplan:export', '#', 'admin', '2018-03-01 00:00:00', 'ry', '2018-03-01 00:00:00', '');
INSERT INTO `sys_menu` VALUES (2051, '安全监督审核管理', 0, 6, '/system/safecheck', 'menuItem', 'C', '0', 'system:safecheck:view', 'fa fa-bar-chart-o', 'admin', '2018-03-01 00:00:00', 'admin', '2020-01-06 15:40:56', '安全检查信息菜单');
INSERT INTO `sys_menu` VALUES (2052, '安全检查信息查询', 2051, 1, '#', '', 'F', '0', 'system:safecheck:list', '#', 'admin', '2018-03-01 00:00:00', 'ry', '2018-03-01 00:00:00', '');
INSERT INTO `sys_menu` VALUES (2053, '安全检查信息新增', 2051, 2, '#', '', 'F', '0', 'system:safecheck:add', '#', 'admin', '2018-03-01 00:00:00', 'ry', '2018-03-01 00:00:00', '');
INSERT INTO `sys_menu` VALUES (2054, '安全检查信息修改', 2051, 3, '#', '', 'F', '0', 'system:safecheck:edit', '#', 'admin', '2018-03-01 00:00:00', 'ry', '2018-03-01 00:00:00', '');
INSERT INTO `sys_menu` VALUES (2055, '安全检查信息删除', 2051, 4, '#', '', 'F', '0', 'system:safecheck:remove', '#', 'admin', '2018-03-01 00:00:00', 'ry', '2018-03-01 00:00:00', '');
INSERT INTO `sys_menu` VALUES (2056, '安全检查信息导出', 2051, 5, '#', '', 'F', '0', 'system:safecheck:export', '#', 'admin', '2018-03-01 00:00:00', 'ry', '2018-03-01 00:00:00', '');

-- ----------------------------
-- Table structure for sys_notice
-- ----------------------------
DROP TABLE IF EXISTS `sys_notice`;
CREATE TABLE `sys_notice`  (
  `notice_id` int(4) NOT NULL AUTO_INCREMENT COMMENT '公告ID',
  `notice_title` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '公告标题',
  `notice_type` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '公告类型（1通知 2公告）',
  `notice_content` varchar(2000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '公告内容',
  `status` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0' COMMENT '公告状态（0正常 1关闭）',
  `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`notice_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 14 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '通知公告表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_notice
-- ----------------------------
INSERT INTO `sys_notice` VALUES (10, '【紧急】【全体】安全施工管理测试', '1', '<p>【紧急】安全施工管理测试【紧急】安全施工管理测试【紧急】安全施工管理测试【紧急】安全施工管理测试【紧急】安全施工管理测试【紧急】安全施工管理测试【紧急】安全施工管理测试【紧急】安全施工管理测试【紧急】安全施工管理测试【紧急】安全施工管理测试【紧急】安全施工管理测试【紧急】安全施工管理测试<br></p>', '0', 'admin', '2019-12-29 21:12:56', 'chenhe1', '2019-12-30 02:32:46', NULL);
INSERT INTO `sys_notice` VALUES (11, '【徐州总公司】紧急通知，迎接安全检查', '2', '<p>【陈贺】紧急通知，迎接安全检查【陈贺】紧急通知，迎接安全检查【陈贺】紧急通知，迎接安全检查【陈贺】紧急通知，迎接安全检查【陈贺】紧急通知，迎接安全检查【陈贺】紧急通知，迎接安全检查【陈贺】紧急通知，迎接安全检查【陈贺】紧急通知，迎接安全检查【陈贺】紧急通知，迎接安全检查<br></p>', '0', 'admin', '2019-12-29 21:13:41', 'chenhe1', '2019-12-30 02:33:02', NULL);
INSERT INTO `sys_notice` VALUES (12, '【紧急通知】大家要好好学习啊', '1', '<p>大家要好好学习啊大家要好好学习啊大家要好好学习啊大家要好好学习啊大家要好好学习啊大家要好好学习啊大家要好好学习啊大家要好好学习啊大家要好好学习啊大家要好好学习啊大家要好好学习啊大家要好好学习啊<br></p>', '0', 'admin', '2020-01-03 09:28:38', '', NULL, NULL);
INSERT INTO `sys_notice` VALUES (13, '【特别】好男人就是我', '2', '<p>【特别】好男人就是我，我就是陈贺<br></p>', '0', 'admin', '2020-01-03 09:49:43', '', NULL, NULL);

-- ----------------------------
-- Table structure for sys_oper_log
-- ----------------------------
DROP TABLE IF EXISTS `sys_oper_log`;
CREATE TABLE `sys_oper_log`  (
  `oper_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '日志主键',
  `title` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '模块标题',
  `business_type` int(2) NULL DEFAULT 0 COMMENT '业务类型（0其它 1新增 2修改 3删除）',
  `method` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '方法名称',
  `request_method` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '请求方式',
  `operator_type` int(1) NULL DEFAULT 0 COMMENT '操作类别（0其它 1后台用户 2手机端用户）',
  `oper_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '操作人员',
  `dept_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '部门名称',
  `oper_url` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '请求URL',
  `oper_ip` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '主机地址',
  `oper_location` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '操作地点',
  `oper_param` varchar(2000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '请求参数',
  `json_result` varchar(2000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '返回参数',
  `status` int(1) NULL DEFAULT 0 COMMENT '操作状态（0正常 1异常）',
  `error_msg` varchar(2000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '错误消息',
  `oper_time` datetime(0) NULL DEFAULT NULL COMMENT '操作时间',
  PRIMARY KEY (`oper_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 390 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '操作日志记录' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_oper_log
-- ----------------------------
INSERT INTO `sys_oper_log` VALUES (100, '代码生成', 6, 'com.ruoyi.generator.controller.GenController.importTableSave()', 'POST', 1, 'admin', '研发部门', '/tool/gen/importTable', '127.0.0.1', '内网IP', '{\r\n  \"tables\" : [ \"user\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2019-12-29 20:05:13');
INSERT INTO `sys_oper_log` VALUES (101, '代码生成', 8, 'com.ruoyi.generator.controller.GenController.genCode()', 'GET', 1, 'admin', '研发部门', '/tool/gen/genCode/user', '127.0.0.1', '内网IP', '{ }', 'null', 0, NULL, '2019-12-29 20:05:23');
INSERT INTO `sys_oper_log` VALUES (102, '用户管理', 1, 'com.ruoyi.web.controller.system.SysUserController.addSave()', 'POST', 1, 'admin', '研发部门', '/system/user/add', '127.0.0.1', '内网IP', '{\r\n  \"deptId\" : [ \"103\" ],\r\n  \"userName\" : [ \"陈贺\" ],\r\n  \"deptName\" : [ \"研发部门\" ],\r\n  \"phonenumber\" : [ \"13215616089\" ],\r\n  \"email\" : [ \"fjsdlk@qq.com\" ],\r\n  \"loginName\" : [ \"chenhe\" ],\r\n  \"password\" : [ \"123456\" ],\r\n  \"sex\" : [ \"0\" ],\r\n  \"role\" : [ \"1\" ],\r\n  \"remark\" : [ \"\" ],\r\n  \"status\" : [ \"0\" ],\r\n  \"roleIds\" : [ \"1\" ],\r\n  \"postIds\" : [ \"4\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2019-12-29 20:39:30');
INSERT INTO `sys_oper_log` VALUES (103, '在线用户', 7, 'com.ruoyi.web.controller.monitor.SysUserOnlineController.forceLogout()', 'POST', 1, 'admin', '研发部门', '/monitor/online/forceLogout', '127.0.0.1', '内网IP', '{\r\n  \"sessionId\" : [ \"23058721-694a-4131-aabc-5e8e89b7c326\" ]\r\n}', '{\r\n  \"msg\" : \"当前登陆用户无法强退\",\r\n  \"code\" : 500\r\n}', 0, NULL, '2019-12-29 21:10:10');
INSERT INTO `sys_oper_log` VALUES (104, '在线用户', 7, 'com.ruoyi.web.controller.monitor.SysUserOnlineController.forceLogout()', 'POST', 1, 'admin', '研发部门', '/monitor/online/forceLogout', '127.0.0.1', '内网IP', '{\r\n  \"sessionId\" : [ \"bf5e9d29-0cde-44fd-b649-9b6c79b8c389\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2019-12-29 21:10:13');
INSERT INTO `sys_oper_log` VALUES (105, '用户管理', 3, 'com.ruoyi.web.controller.system.SysUserController.remove()', 'POST', 1, 'admin', '研发部门', '/system/user/remove', '127.0.0.1', '内网IP', '{\r\n  \"ids\" : [ \"100,2\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2019-12-29 21:10:29');
INSERT INTO `sys_oper_log` VALUES (106, '用户管理', 2, 'com.ruoyi.web.controller.system.SysUserController.editSave()', 'POST', 1, 'admin', '研发部门', '/system/user/edit', '127.0.0.1', '内网IP', '{\r\n  \"userId\" : [ \"1\" ],\r\n  \"deptId\" : [ \"103\" ],\r\n  \"userName\" : [ \"陈贺\" ],\r\n  \"dept.deptName\" : [ \"研发部门\" ],\r\n  \"phonenumber\" : [ \"18888888888\" ],\r\n  \"email\" : [ \"ch@xzit.com\" ],\r\n  \"loginName\" : [ \"admin\" ],\r\n  \"sex\" : [ \"1\" ],\r\n  \"role\" : [ \"1\" ],\r\n  \"remark\" : [ \"管理员\" ],\r\n  \"status\" : [ \"0\" ],\r\n  \"roleIds\" : [ \"1\" ],\r\n  \"postIds\" : [ \"1\" ]\r\n}', 'null', 1, '不允许操作超级管理员用户', '2019-12-29 21:11:30');
INSERT INTO `sys_oper_log` VALUES (107, '用户管理', 2, 'com.ruoyi.web.controller.system.SysUserController.editSave()', 'POST', 1, 'admin', '研发部门', '/system/user/edit', '127.0.0.1', '内网IP', '{\r\n  \"userId\" : [ \"1\" ],\r\n  \"deptId\" : [ \"103\" ],\r\n  \"userName\" : [ \"陈贺\" ],\r\n  \"dept.deptName\" : [ \"研发部门\" ],\r\n  \"phonenumber\" : [ \"18888888888\" ],\r\n  \"email\" : [ \"ch@xzit.com\" ],\r\n  \"loginName\" : [ \"admin\" ],\r\n  \"sex\" : [ \"1\" ],\r\n  \"role\" : [ \"1\" ],\r\n  \"remark\" : [ \"管理员\" ],\r\n  \"status\" : [ \"0\" ],\r\n  \"roleIds\" : [ \"1\" ],\r\n  \"postIds\" : [ \"1\" ]\r\n}', 'null', 1, '不允许操作超级管理员用户', '2019-12-29 21:11:44');
INSERT INTO `sys_oper_log` VALUES (108, '用户管理', 2, 'com.ruoyi.web.controller.system.SysUserController.editSave()', 'POST', 1, 'admin', '研发部门', '/system/user/edit', '127.0.0.1', '内网IP', '{\r\n  \"userId\" : [ \"1\" ],\r\n  \"deptId\" : [ \"104\" ],\r\n  \"userName\" : [ \"若依\" ],\r\n  \"dept.deptName\" : [ \"市场部门\" ],\r\n  \"phonenumber\" : [ \"15888888888\" ],\r\n  \"email\" : [ \"ry@163.com\" ],\r\n  \"loginName\" : [ \"admin\" ],\r\n  \"sex\" : [ \"1\" ],\r\n  \"role\" : [ \"1\" ],\r\n  \"remark\" : [ \"管理员\" ],\r\n  \"status\" : [ \"0\" ],\r\n  \"roleIds\" : [ \"1\" ],\r\n  \"postIds\" : [ \"1\" ]\r\n}', 'null', 1, '不允许操作超级管理员用户', '2019-12-29 21:11:53');
INSERT INTO `sys_oper_log` VALUES (109, '通知公告', 1, 'com.ruoyi.web.controller.system.SysNoticeController.addSave()', 'POST', 1, 'admin', '研发部门', '/system/notice/add', '127.0.0.1', '内网IP', '{\r\n  \"noticeTitle\" : [ \"【紧急】安全施工管理测试\" ],\r\n  \"noticeType\" : [ \"1\" ],\r\n  \"noticeContent\" : [ \"<p>【紧急】安全施工管理测试【紧急】安全施工管理测试【紧急】安全施工管理测试【紧急】安全施工管理测试【紧急】安全施工管理测试【紧急】安全施工管理测试【紧急】安全施工管理测试【紧急】安全施工管理测试【紧急】安全施工管理测试【紧急】安全施工管理测试【紧急】安全施工管理测试【紧急】安全施工管理测试<br></p>\" ],\r\n  \"status\" : [ \"0\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2019-12-29 21:12:56');
INSERT INTO `sys_oper_log` VALUES (110, '通知公告', 3, 'com.ruoyi.web.controller.system.SysNoticeController.remove()', 'POST', 1, 'admin', '研发部门', '/system/notice/remove', '127.0.0.1', '内网IP', '{\r\n  \"ids\" : [ \"1,2\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2019-12-29 21:13:01');
INSERT INTO `sys_oper_log` VALUES (111, '通知公告', 1, 'com.ruoyi.web.controller.system.SysNoticeController.addSave()', 'POST', 1, 'admin', '研发部门', '/system/notice/add', '127.0.0.1', '内网IP', '{\r\n  \"noticeTitle\" : [ \"【陈贺】紧急通知，迎接安全检查\" ],\r\n  \"noticeType\" : [ \"2\" ],\r\n  \"noticeContent\" : [ \"<p>【陈贺】紧急通知，迎接安全检查【陈贺】紧急通知，迎接安全检查【陈贺】紧急通知，迎接安全检查【陈贺】紧急通知，迎接安全检查【陈贺】紧急通知，迎接安全检查【陈贺】紧急通知，迎接安全检查【陈贺】紧急通知，迎接安全检查【陈贺】紧急通知，迎接安全检查【陈贺】紧急通知，迎接安全检查<br></p>\" ],\r\n  \"status\" : [ \"0\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2019-12-29 21:13:41');
INSERT INTO `sys_oper_log` VALUES (112, '重置密码', 2, 'com.ruoyi.web.controller.system.SysProfileController.resetPwd()', 'POST', 1, 'admin', '研发部门', '/system/user/profile/resetPwd', '127.0.0.1', '内网IP', '{\r\n  \"userId\" : [ \"1\" ],\r\n  \"loginName\" : [ \"admin\" ],\r\n  \"oldPassword\" : [ \"admin123\" ],\r\n  \"newPassword\" : [ \"admin\" ],\r\n  \"confirm\" : [ \"admin\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2019-12-29 21:14:27');
INSERT INTO `sys_oper_log` VALUES (113, '个人信息', 2, 'com.ruoyi.web.controller.system.SysProfileController.updateAvatar()', 'POST', 1, 'admin', '研发部门', '/system/user/profile/updateAvatar', '127.0.0.1', '内网IP', '{ }', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2019-12-29 21:16:29');
INSERT INTO `sys_oper_log` VALUES (114, '个人信息', 2, 'com.ruoyi.web.controller.system.SysProfileController.update()', 'POST', 1, 'admin', '研发部门', '/system/user/profile/update', '127.0.0.1', '内网IP', '{\r\n  \"id\" : [ \"\" ],\r\n  \"userName\" : [ \"陈贺\" ],\r\n  \"phonenumber\" : [ \"19851668369\" ],\r\n  \"email\" : [ \"ch@xzit.com\" ],\r\n  \"sex\" : [ \"0\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2019-12-29 21:17:03');
INSERT INTO `sys_oper_log` VALUES (115, '代码生成', 3, 'com.ruoyi.generator.controller.GenController.remove()', 'POST', 1, 'admin', '研发部门', '/tool/gen/remove', '127.0.0.1', '内网IP', '{\r\n  \"ids\" : [ \"1\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2019-12-29 21:19:55');
INSERT INTO `sys_oper_log` VALUES (116, '菜单管理', 1, 'com.ruoyi.web.controller.system.SysMenuController.addSave()', 'POST', 1, 'admin', '研发部门', '/system/menu/add', '127.0.0.1', '内网IP', '{\r\n  \"parentId\" : [ \"0\" ],\r\n  \"menuType\" : [ \"M\" ],\r\n  \"menuName\" : [ \"现场安全检查管理\" ],\r\n  \"url\" : [ \"\" ],\r\n  \"target\" : [ \"menuItem\" ],\r\n  \"perms\" : [ \"\" ],\r\n  \"orderNum\" : [ \"1\" ],\r\n  \"icon\" : [ \"fa fa-american-sign-language-interpreting\" ],\r\n  \"visible\" : [ \"0\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2019-12-29 21:22:37');
INSERT INTO `sys_oper_log` VALUES (117, '菜单管理', 1, 'com.ruoyi.web.controller.system.SysMenuController.addSave()', 'POST', 1, 'admin', '研发部门', '/system/menu/add', '127.0.0.1', '内网IP', '{\r\n  \"parentId\" : [ \"2000\" ],\r\n  \"menuType\" : [ \"M\" ],\r\n  \"menuName\" : [ \"安全检查通知\" ],\r\n  \"url\" : [ \"\" ],\r\n  \"target\" : [ \"menuItem\" ],\r\n  \"perms\" : [ \"\" ],\r\n  \"orderNum\" : [ \"1\" ],\r\n  \"icon\" : [ \"fa fa-bars\" ],\r\n  \"visible\" : [ \"0\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2019-12-29 21:23:48');
INSERT INTO `sys_oper_log` VALUES (118, '菜单管理', 1, 'com.ruoyi.web.controller.system.SysMenuController.addSave()', 'POST', 1, 'admin', '研发部门', '/system/menu/add', '127.0.0.1', '内网IP', '{\r\n  \"parentId\" : [ \"2000\" ],\r\n  \"menuType\" : [ \"C\" ],\r\n  \"menuName\" : [ \"安全检查接收\" ],\r\n  \"url\" : [ \"#\" ],\r\n  \"target\" : [ \"menuItem\" ],\r\n  \"perms\" : [ \"\" ],\r\n  \"orderNum\" : [ \"2\" ],\r\n  \"icon\" : [ \"\" ],\r\n  \"visible\" : [ \"0\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2019-12-29 21:25:26');
INSERT INTO `sys_oper_log` VALUES (119, '菜单管理', 2, 'com.ruoyi.web.controller.system.SysMenuController.editSave()', 'POST', 1, 'admin', '研发部门', '/system/menu/edit', '127.0.0.1', '内网IP', '{\r\n  \"menuId\" : [ \"1\" ],\r\n  \"parentId\" : [ \"0\" ],\r\n  \"menuType\" : [ \"M\" ],\r\n  \"menuName\" : [ \"系统管理\" ],\r\n  \"url\" : [ \"#\" ],\r\n  \"target\" : [ \"menuItem\" ],\r\n  \"perms\" : [ \"\" ],\r\n  \"orderNum\" : [ \"9\" ],\r\n  \"icon\" : [ \"fa fa-gear\" ],\r\n  \"visible\" : [ \"0\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2019-12-29 21:27:01');
INSERT INTO `sys_oper_log` VALUES (120, '菜单管理', 2, 'com.ruoyi.web.controller.system.SysMenuController.editSave()', 'POST', 1, 'admin', '研发部门', '/system/menu/edit', '127.0.0.1', '内网IP', '{\r\n  \"menuId\" : [ \"3\" ],\r\n  \"parentId\" : [ \"0\" ],\r\n  \"menuType\" : [ \"M\" ],\r\n  \"menuName\" : [ \"系统工具\" ],\r\n  \"url\" : [ \"#\" ],\r\n  \"target\" : [ \"menuItem\" ],\r\n  \"perms\" : [ \"\" ],\r\n  \"orderNum\" : [ \"8\" ],\r\n  \"icon\" : [ \"fa fa-bars\" ],\r\n  \"visible\" : [ \"0\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2019-12-29 21:27:10');
INSERT INTO `sys_oper_log` VALUES (121, '菜单管理', 2, 'com.ruoyi.web.controller.system.SysMenuController.editSave()', 'POST', 1, 'admin', '研发部门', '/system/menu/edit', '127.0.0.1', '内网IP', '{\r\n  \"menuId\" : [ \"2\" ],\r\n  \"parentId\" : [ \"0\" ],\r\n  \"menuType\" : [ \"M\" ],\r\n  \"menuName\" : [ \"系统监控\" ],\r\n  \"url\" : [ \"#\" ],\r\n  \"target\" : [ \"menuItem\" ],\r\n  \"perms\" : [ \"\" ],\r\n  \"orderNum\" : [ \"7\" ],\r\n  \"icon\" : [ \"fa fa-video-camera\" ],\r\n  \"visible\" : [ \"0\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2019-12-29 21:27:15');
INSERT INTO `sys_oper_log` VALUES (122, '菜单管理', 1, 'com.ruoyi.web.controller.system.SysMenuController.addSave()', 'POST', 1, 'admin', '研发部门', '/system/menu/add', '127.0.0.1', '内网IP', '{\r\n  \"parentId\" : [ \"0\" ],\r\n  \"menuType\" : [ \"M\" ],\r\n  \"menuName\" : [ \"现场隐患管理\" ],\r\n  \"url\" : [ \"\" ],\r\n  \"target\" : [ \"menuItem\" ],\r\n  \"perms\" : [ \"\" ],\r\n  \"orderNum\" : [ \"2\" ],\r\n  \"icon\" : [ \"fa fa-bell\" ],\r\n  \"visible\" : [ \"0\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2019-12-29 21:28:08');
INSERT INTO `sys_oper_log` VALUES (123, '菜单管理', 1, 'com.ruoyi.web.controller.system.SysMenuController.addSave()', 'POST', 1, 'admin', '研发部门', '/system/menu/add', '127.0.0.1', '内网IP', '{\r\n  \"parentId\" : [ \"0\" ],\r\n  \"menuType\" : [ \"M\" ],\r\n  \"menuName\" : [ \"施工风险管理\" ],\r\n  \"url\" : [ \"\" ],\r\n  \"target\" : [ \"menuItem\" ],\r\n  \"perms\" : [ \"\" ],\r\n  \"orderNum\" : [ \"3\" ],\r\n  \"icon\" : [ \"fa fa-book\" ],\r\n  \"visible\" : [ \"0\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2019-12-29 21:28:34');
INSERT INTO `sys_oper_log` VALUES (124, '菜单管理', 1, 'com.ruoyi.web.controller.system.SysMenuController.addSave()', 'POST', 1, 'admin', '研发部门', '/system/menu/add', '127.0.0.1', '内网IP', '{\r\n  \"parentId\" : [ \"0\" ],\r\n  \"menuType\" : [ \"M\" ],\r\n  \"menuName\" : [ \"应急预案管理\" ],\r\n  \"url\" : [ \"\" ],\r\n  \"target\" : [ \"menuItem\" ],\r\n  \"perms\" : [ \"\" ],\r\n  \"orderNum\" : [ \"4\" ],\r\n  \"icon\" : [ \"fa fa-blind\" ],\r\n  \"visible\" : [ \"0\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2019-12-29 21:28:59');
INSERT INTO `sys_oper_log` VALUES (125, '菜单管理', 1, 'com.ruoyi.web.controller.system.SysMenuController.addSave()', 'POST', 1, 'admin', '研发部门', '/system/menu/add', '127.0.0.1', '内网IP', '{\r\n  \"parentId\" : [ \"0\" ],\r\n  \"menuType\" : [ \"M\" ],\r\n  \"menuName\" : [ \"施工事故管理\" ],\r\n  \"url\" : [ \"\" ],\r\n  \"target\" : [ \"menuItem\" ],\r\n  \"perms\" : [ \"\" ],\r\n  \"orderNum\" : [ \"5\" ],\r\n  \"icon\" : [ \"fa fa-blind\" ],\r\n  \"visible\" : [ \"0\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2019-12-29 21:29:17');
INSERT INTO `sys_oper_log` VALUES (126, '菜单管理', 2, 'com.ruoyi.web.controller.system.SysMenuController.editSave()', 'POST', 1, 'admin', '研发部门', '/system/menu/edit', '127.0.0.1', '内网IP', '{\r\n  \"menuId\" : [ \"2006\" ],\r\n  \"parentId\" : [ \"0\" ],\r\n  \"menuType\" : [ \"M\" ],\r\n  \"menuName\" : [ \"施工事故管理\" ],\r\n  \"url\" : [ \"#\" ],\r\n  \"target\" : [ \"menuItem\" ],\r\n  \"perms\" : [ \"\" ],\r\n  \"orderNum\" : [ \"5\" ],\r\n  \"icon\" : [ \"fa fa-binoculars\" ],\r\n  \"visible\" : [ \"0\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2019-12-29 21:29:34');
INSERT INTO `sys_oper_log` VALUES (127, '菜单管理', 1, 'com.ruoyi.web.controller.system.SysMenuController.addSave()', 'POST', 1, 'admin', '研发部门', '/system/menu/add', '127.0.0.1', '内网IP', '{\r\n  \"parentId\" : [ \"0\" ],\r\n  \"menuType\" : [ \"M\" ],\r\n  \"menuName\" : [ \"安全监督审核管理\" ],\r\n  \"url\" : [ \"\" ],\r\n  \"target\" : [ \"menuItem\" ],\r\n  \"perms\" : [ \"\" ],\r\n  \"orderNum\" : [ \"6\" ],\r\n  \"icon\" : [ \"fa fa-bar-chart-o\" ],\r\n  \"visible\" : [ \"0\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2019-12-29 21:30:08');
INSERT INTO `sys_oper_log` VALUES (128, '部门管理', 3, 'com.ruoyi.web.controller.system.SysDeptController.remove()', 'GET', 1, 'admin', '研发部门', '/system/dept/remove/103', '127.0.0.1', '内网IP', '{ }', '{\r\n  \"msg\" : \"部门存在用户,不允许删除\",\r\n  \"code\" : 301\r\n}', 0, NULL, '2019-12-29 21:31:15');
INSERT INTO `sys_oper_log` VALUES (129, '部门管理', 2, 'com.ruoyi.web.controller.system.SysDeptController.editSave()', 'POST', 1, 'admin', '研发部门', '/system/dept/edit', '127.0.0.1', '内网IP', '{\r\n  \"deptId\" : [ \"101\" ],\r\n  \"parentId\" : [ \"100\" ],\r\n  \"parentName\" : [ \"若依科技\" ],\r\n  \"deptName\" : [ \"项目经理\" ],\r\n  \"orderNum\" : [ \"1\" ],\r\n  \"leader\" : [ \"陈贺\" ],\r\n  \"phone\" : [ \"19851668369\" ],\r\n  \"email\" : [ \"ch@xzit.com\" ],\r\n  \"status\" : [ \"0\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2019-12-29 21:32:25');
INSERT INTO `sys_oper_log` VALUES (130, '部门管理', 2, 'com.ruoyi.web.controller.system.SysDeptController.editSave()', 'POST', 1, 'admin', '研发部门', '/system/dept/edit', '127.0.0.1', '内网IP', '{\r\n  \"deptId\" : [ \"103\" ],\r\n  \"parentId\" : [ \"101\" ],\r\n  \"parentName\" : [ \"项目经理\" ],\r\n  \"deptName\" : [ \"第1组\" ],\r\n  \"orderNum\" : [ \"1\" ],\r\n  \"leader\" : [ \"若依\" ],\r\n  \"phone\" : [ \"15888888888\" ],\r\n  \"email\" : [ \"ry@qq.com\" ],\r\n  \"status\" : [ \"0\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2019-12-29 21:32:39');
INSERT INTO `sys_oper_log` VALUES (131, '部门管理', 2, 'com.ruoyi.web.controller.system.SysDeptController.editSave()', 'POST', 1, 'admin', '研发部门', '/system/dept/edit', '127.0.0.1', '内网IP', '{\r\n  \"deptId\" : [ \"104\" ],\r\n  \"parentId\" : [ \"101\" ],\r\n  \"parentName\" : [ \"项目经理\" ],\r\n  \"deptName\" : [ \"第2组\" ],\r\n  \"orderNum\" : [ \"2\" ],\r\n  \"leader\" : [ \"若依\" ],\r\n  \"phone\" : [ \"15888888888\" ],\r\n  \"email\" : [ \"ry@qq.com\" ],\r\n  \"status\" : [ \"0\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2019-12-29 21:32:51');
INSERT INTO `sys_oper_log` VALUES (132, '部门管理', 2, 'com.ruoyi.web.controller.system.SysDeptController.editSave()', 'POST', 1, 'admin', '研发部门', '/system/dept/edit', '127.0.0.1', '内网IP', '{\r\n  \"deptId\" : [ \"105\" ],\r\n  \"parentId\" : [ \"101\" ],\r\n  \"parentName\" : [ \"项目经理\" ],\r\n  \"deptName\" : [ \"第3组\" ],\r\n  \"orderNum\" : [ \"3\" ],\r\n  \"leader\" : [ \"若依\" ],\r\n  \"phone\" : [ \"15888888888\" ],\r\n  \"email\" : [ \"ry@qq.com\" ],\r\n  \"status\" : [ \"0\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2019-12-29 21:32:57');
INSERT INTO `sys_oper_log` VALUES (133, '部门管理', 3, 'com.ruoyi.web.controller.system.SysDeptController.remove()', 'GET', 1, 'admin', '研发部门', '/system/dept/remove/106', '127.0.0.1', '内网IP', '{ }', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2019-12-29 21:33:01');
INSERT INTO `sys_oper_log` VALUES (134, '部门管理', 3, 'com.ruoyi.web.controller.system.SysDeptController.remove()', 'GET', 1, 'admin', '研发部门', '/system/dept/remove/107', '127.0.0.1', '内网IP', '{ }', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2019-12-29 21:33:03');
INSERT INTO `sys_oper_log` VALUES (135, '部门管理', 3, 'com.ruoyi.web.controller.system.SysDeptController.remove()', 'GET', 1, 'admin', '研发部门', '/system/dept/remove/105', '127.0.0.1', '内网IP', '{ }', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2019-12-29 21:33:11');
INSERT INTO `sys_oper_log` VALUES (136, '部门管理', 2, 'com.ruoyi.web.controller.system.SysDeptController.editSave()', 'POST', 1, 'admin', '研发部门', '/system/dept/edit', '127.0.0.1', '内网IP', '{\r\n  \"deptId\" : [ \"102\" ],\r\n  \"parentId\" : [ \"100\" ],\r\n  \"parentName\" : [ \"若依科技\" ],\r\n  \"deptName\" : [ \"监理\" ],\r\n  \"orderNum\" : [ \"2\" ],\r\n  \"leader\" : [ \"若依\" ],\r\n  \"phone\" : [ \"15888888888\" ],\r\n  \"email\" : [ \"ry@qq.com\" ],\r\n  \"status\" : [ \"0\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2019-12-29 21:33:24');
INSERT INTO `sys_oper_log` VALUES (137, '部门管理', 2, 'com.ruoyi.web.controller.system.SysDeptController.editSave()', 'POST', 1, 'admin', '研发部门', '/system/dept/edit', '127.0.0.1', '内网IP', '{\r\n  \"deptId\" : [ \"108\" ],\r\n  \"parentId\" : [ \"102\" ],\r\n  \"parentName\" : [ \"监理\" ],\r\n  \"deptName\" : [ \"第1组\" ],\r\n  \"orderNum\" : [ \"1\" ],\r\n  \"leader\" : [ \"若依\" ],\r\n  \"phone\" : [ \"15888888888\" ],\r\n  \"email\" : [ \"ry@qq.com\" ],\r\n  \"status\" : [ \"0\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2019-12-29 21:33:31');
INSERT INTO `sys_oper_log` VALUES (138, '部门管理', 2, 'com.ruoyi.web.controller.system.SysDeptController.editSave()', 'POST', 1, 'admin', '研发部门', '/system/dept/edit', '127.0.0.1', '内网IP', '{\r\n  \"deptId\" : [ \"109\" ],\r\n  \"parentId\" : [ \"102\" ],\r\n  \"parentName\" : [ \"监理\" ],\r\n  \"deptName\" : [ \"第2组\" ],\r\n  \"orderNum\" : [ \"2\" ],\r\n  \"leader\" : [ \"若依\" ],\r\n  \"phone\" : [ \"15888888888\" ],\r\n  \"email\" : [ \"ry@qq.com\" ],\r\n  \"status\" : [ \"0\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2019-12-29 21:33:37');
INSERT INTO `sys_oper_log` VALUES (139, '部门管理', 1, 'com.ruoyi.web.controller.system.SysDeptController.addSave()', 'POST', 1, 'admin', '研发部门', '/system/dept/add', '127.0.0.1', '内网IP', '{\r\n  \"parentId\" : [ \"100\" ],\r\n  \"deptName\" : [ \"施工人员\" ],\r\n  \"orderNum\" : [ \"3\" ],\r\n  \"leader\" : [ \"陈贺\" ],\r\n  \"phone\" : [ \"19851668369\" ],\r\n  \"email\" : [ \"vchenhe@live.com\" ],\r\n  \"status\" : [ \"0\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2019-12-29 21:34:31');
INSERT INTO `sys_oper_log` VALUES (140, '部门管理', 1, 'com.ruoyi.web.controller.system.SysDeptController.addSave()', 'POST', 1, 'admin', '研发部门', '/system/dept/add', '127.0.0.1', '内网IP', '{\r\n  \"parentId\" : [ \"200\" ],\r\n  \"deptName\" : [ \"第1组\" ],\r\n  \"orderNum\" : [ \"1\" ],\r\n  \"leader\" : [ \"陈贺\" ],\r\n  \"phone\" : [ \"19851668369\" ],\r\n  \"email\" : [ \"vchenhe@live.com\" ],\r\n  \"status\" : [ \"0\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2019-12-29 21:34:49');
INSERT INTO `sys_oper_log` VALUES (141, '部门管理', 1, 'com.ruoyi.web.controller.system.SysDeptController.addSave()', 'POST', 1, 'admin', '研发部门', '/system/dept/add', '127.0.0.1', '内网IP', '{\r\n  \"parentId\" : [ \"200\" ],\r\n  \"deptName\" : [ \"第2组\" ],\r\n  \"orderNum\" : [ \"2\" ],\r\n  \"leader\" : [ \"陈贺\" ],\r\n  \"phone\" : [ \"19851668369\" ],\r\n  \"email\" : [ \"vchenhe@live.com\" ],\r\n  \"status\" : [ \"0\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2019-12-29 21:35:11');
INSERT INTO `sys_oper_log` VALUES (142, '部门管理', 1, 'com.ruoyi.web.controller.system.SysDeptController.addSave()', 'POST', 1, 'admin', '研发部门', '/system/dept/add', '127.0.0.1', '内网IP', '{\r\n  \"parentId\" : [ \"100\" ],\r\n  \"deptName\" : [ \"管理员\" ],\r\n  \"orderNum\" : [ \"4\" ],\r\n  \"leader\" : [ \"陈贺\" ],\r\n  \"phone\" : [ \"19851668369\" ],\r\n  \"email\" : [ \"vchenhe@live.com\" ],\r\n  \"status\" : [ \"0\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2019-12-29 21:35:35');
INSERT INTO `sys_oper_log` VALUES (143, '用户管理', 2, 'com.ruoyi.web.controller.system.SysUserController.editSave()', 'POST', 1, 'admin', '研发部门', '/system/user/edit', '127.0.0.1', '内网IP', '{\r\n  \"userId\" : [ \"1\" ],\r\n  \"deptId\" : [ \"203\" ],\r\n  \"userName\" : [ \"陈贺\" ],\r\n  \"dept.deptName\" : [ \"管理员\" ],\r\n  \"phonenumber\" : [ \"19851668369\" ],\r\n  \"email\" : [ \"ch@xzit.com\" ],\r\n  \"loginName\" : [ \"admin\" ],\r\n  \"sex\" : [ \"0\" ],\r\n  \"role\" : [ \"1\" ],\r\n  \"remark\" : [ \"管理员\" ],\r\n  \"status\" : [ \"0\" ],\r\n  \"roleIds\" : [ \"1\" ],\r\n  \"postIds\" : [ \"1\" ]\r\n}', 'null', 1, '不允许操作超级管理员用户', '2019-12-29 21:36:01');
INSERT INTO `sys_oper_log` VALUES (144, '重置密码', 2, 'com.ruoyi.web.controller.system.SysProfileController.resetPwd()', 'POST', 1, 'admin', '第1组', '/system/user/profile/resetPwd', '127.0.0.1', '内网IP', '{\r\n  \"userId\" : [ \"1\" ],\r\n  \"loginName\" : [ \"admin\" ],\r\n  \"oldPassword\" : [ \"admin\" ],\r\n  \"newPassword\" : [ \"admin123\" ],\r\n  \"confirm\" : [ \"admin123\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2019-12-29 22:39:55');
INSERT INTO `sys_oper_log` VALUES (145, '重置密码', 2, 'com.ruoyi.web.controller.system.SysProfileController.resetPwd()', 'POST', 1, 'admin', '第1组', '/system/user/profile/resetPwd', '127.0.0.1', '内网IP', '{\r\n  \"userId\" : [ \"1\" ],\r\n  \"loginName\" : [ \"admin\" ],\r\n  \"oldPassword\" : [ \"admin123\" ],\r\n  \"newPassword\" : [ \"admin\" ],\r\n  \"confirm\" : [ \"admin\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2019-12-29 23:53:30');
INSERT INTO `sys_oper_log` VALUES (146, '在线用户', 7, 'com.ruoyi.web.controller.monitor.SysUserOnlineController.forceLogout()', 'POST', 1, 'admin', '第1组', '/monitor/online/forceLogout', '127.0.0.1', '内网IP', '{\r\n  \"sessionId\" : [ \"dc8a3414-64e3-4017-9629-03ba01789cfd\" ]\r\n}', '{\r\n  \"msg\" : \"当前登陆用户无法强退\",\r\n  \"code\" : 500\r\n}', 0, NULL, '2019-12-29 23:53:35');
INSERT INTO `sys_oper_log` VALUES (147, '菜单管理', 2, 'com.ruoyi.web.controller.system.SysMenuController.editSave()', 'POST', 1, 'admin', '第1组', '/system/menu/edit', '127.0.0.1', '内网IP', '{\r\n  \"menuId\" : [ \"2002\" ],\r\n  \"parentId\" : [ \"2000\" ],\r\n  \"menuType\" : [ \"C\" ],\r\n  \"menuName\" : [ \"安全检查接收\" ],\r\n  \"url\" : [ \"#\" ],\r\n  \"target\" : [ \"menuItem\" ],\r\n  \"perms\" : [ \"\" ],\r\n  \"orderNum\" : [ \"2\" ],\r\n  \"icon\" : [ \"fa fa-bars\" ],\r\n  \"visible\" : [ \"0\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2019-12-30 01:06:09');
INSERT INTO `sys_oper_log` VALUES (148, '菜单管理', 2, 'com.ruoyi.web.controller.system.SysMenuController.editSave()', 'POST', 1, 'admin', '第1组', '/system/menu/edit', '127.0.0.1', '内网IP', '{\r\n  \"menuId\" : [ \"2001\" ],\r\n  \"parentId\" : [ \"2000\" ],\r\n  \"menuType\" : [ \"C\" ],\r\n  \"menuName\" : [ \"安全检查通知\" ],\r\n  \"url\" : [ \"/\" ],\r\n  \"target\" : [ \"menuItem\" ],\r\n  \"perms\" : [ \"\" ],\r\n  \"orderNum\" : [ \"1\" ],\r\n  \"icon\" : [ \"fa fa-bars\" ],\r\n  \"visible\" : [ \"0\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2019-12-30 01:06:51');
INSERT INTO `sys_oper_log` VALUES (149, '菜单管理', 2, 'com.ruoyi.web.controller.system.SysMenuController.editSave()', 'POST', 1, 'admin', '第1组', '/system/menu/edit', '127.0.0.1', '内网IP', '{\r\n  \"menuId\" : [ \"2002\" ],\r\n  \"parentId\" : [ \"2000\" ],\r\n  \"menuType\" : [ \"C\" ],\r\n  \"menuName\" : [ \"安全检查接收\" ],\r\n  \"url\" : [ \"/\" ],\r\n  \"target\" : [ \"menuItem\" ],\r\n  \"perms\" : [ \"\" ],\r\n  \"orderNum\" : [ \"2\" ],\r\n  \"icon\" : [ \"fa fa-bars\" ],\r\n  \"visible\" : [ \"0\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2019-12-30 01:06:57');
INSERT INTO `sys_oper_log` VALUES (150, '菜单管理', 2, 'com.ruoyi.web.controller.system.SysMenuController.editSave()', 'POST', 1, 'admin', '第1组', '/system/menu/edit', '127.0.0.1', '内网IP', '{\r\n  \"menuId\" : [ \"2001\" ],\r\n  \"parentId\" : [ \"2000\" ],\r\n  \"menuType\" : [ \"C\" ],\r\n  \"menuName\" : [ \"安全检查通知\" ],\r\n  \"url\" : [ \"/error\" ],\r\n  \"target\" : [ \"menuItem\" ],\r\n  \"perms\" : [ \"\" ],\r\n  \"orderNum\" : [ \"1\" ],\r\n  \"icon\" : [ \"fa fa-bars\" ],\r\n  \"visible\" : [ \"0\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2019-12-30 01:07:51');
INSERT INTO `sys_oper_log` VALUES (151, '菜单管理', 2, 'com.ruoyi.web.controller.system.SysMenuController.editSave()', 'POST', 1, 'admin', '第1组', '/system/menu/edit', '127.0.0.1', '内网IP', '{\r\n  \"menuId\" : [ \"2002\" ],\r\n  \"parentId\" : [ \"2000\" ],\r\n  \"menuType\" : [ \"C\" ],\r\n  \"menuName\" : [ \"安全检查接收\" ],\r\n  \"url\" : [ \"/error\" ],\r\n  \"target\" : [ \"menuItem\" ],\r\n  \"perms\" : [ \"\" ],\r\n  \"orderNum\" : [ \"2\" ],\r\n  \"icon\" : [ \"fa fa-bars\" ],\r\n  \"visible\" : [ \"0\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2019-12-30 01:08:43');
INSERT INTO `sys_oper_log` VALUES (152, '菜单管理', 1, 'com.ruoyi.web.controller.system.SysMenuController.addSave()', 'POST', 1, 'admin', '第1组', '/system/menu/add', '127.0.0.1', '内网IP', '{\r\n  \"parentId\" : [ \"2000\" ],\r\n  \"menuType\" : [ \"C\" ],\r\n  \"menuName\" : [ \"检查结果上报\" ],\r\n  \"url\" : [ \"/error\" ],\r\n  \"target\" : [ \"menuItem\" ],\r\n  \"perms\" : [ \"\" ],\r\n  \"orderNum\" : [ \"3\" ],\r\n  \"icon\" : [ \"fa fa-bars\" ],\r\n  \"visible\" : [ \"0\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2019-12-30 01:10:09');
INSERT INTO `sys_oper_log` VALUES (153, '菜单管理', 1, 'com.ruoyi.web.controller.system.SysMenuController.addSave()', 'POST', 1, 'admin', '第1组', '/system/menu/add', '127.0.0.1', '内网IP', '{\r\n  \"parentId\" : [ \"2000\" ],\r\n  \"menuType\" : [ \"C\" ],\r\n  \"menuName\" : [ \"项目安全整改\" ],\r\n  \"url\" : [ \"/error\" ],\r\n  \"target\" : [ \"menuItem\" ],\r\n  \"perms\" : [ \"\" ],\r\n  \"orderNum\" : [ \"4\" ],\r\n  \"icon\" : [ \"fa fa-bars\" ],\r\n  \"visible\" : [ \"0\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2019-12-30 01:10:36');
INSERT INTO `sys_oper_log` VALUES (154, '菜单管理', 1, 'com.ruoyi.web.controller.system.SysMenuController.addSave()', 'POST', 1, 'admin', '第1组', '/system/menu/add', '127.0.0.1', '内网IP', '{\r\n  \"parentId\" : [ \"2003\" ],\r\n  \"menuType\" : [ \"C\" ],\r\n  \"menuName\" : [ \"部门自检\" ],\r\n  \"url\" : [ \"/error\" ],\r\n  \"target\" : [ \"menuItem\" ],\r\n  \"perms\" : [ \"\" ],\r\n  \"orderNum\" : [ \"1\" ],\r\n  \"icon\" : [ \"fa fa-bars\" ],\r\n  \"visible\" : [ \"0\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2019-12-30 01:11:03');
INSERT INTO `sys_oper_log` VALUES (155, '菜单管理', 1, 'com.ruoyi.web.controller.system.SysMenuController.addSave()', 'POST', 1, 'admin', '第1组', '/system/menu/add', '127.0.0.1', '内网IP', '{\r\n  \"parentId\" : [ \"2003\" ],\r\n  \"menuType\" : [ \"C\" ],\r\n  \"menuName\" : [ \"隐患整改\" ],\r\n  \"url\" : [ \"/error\" ],\r\n  \"target\" : [ \"menuItem\" ],\r\n  \"perms\" : [ \"\" ],\r\n  \"orderNum\" : [ \"3\" ],\r\n  \"icon\" : [ \"fa fa-bars\" ],\r\n  \"visible\" : [ \"0\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2019-12-30 01:11:30');
INSERT INTO `sys_oper_log` VALUES (156, '菜单管理', 2, 'com.ruoyi.web.controller.system.SysMenuController.editSave()', 'POST', 1, 'admin', '第1组', '/system/menu/edit', '127.0.0.1', '内网IP', '{\r\n  \"menuId\" : [ \"2011\" ],\r\n  \"parentId\" : [ \"2003\" ],\r\n  \"menuType\" : [ \"C\" ],\r\n  \"menuName\" : [ \"隐患整改\" ],\r\n  \"url\" : [ \"/error\" ],\r\n  \"target\" : [ \"menuItem\" ],\r\n  \"perms\" : [ \"\" ],\r\n  \"orderNum\" : [ \"2\" ],\r\n  \"icon\" : [ \"fa fa-bars\" ],\r\n  \"visible\" : [ \"0\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2019-12-30 01:11:38');
INSERT INTO `sys_oper_log` VALUES (157, '菜单管理', 1, 'com.ruoyi.web.controller.system.SysMenuController.addSave()', 'POST', 1, 'admin', '第1组', '/system/menu/add', '127.0.0.1', '内网IP', '{\r\n  \"parentId\" : [ \"2003\" ],\r\n  \"menuType\" : [ \"C\" ],\r\n  \"menuName\" : [ \"结果上传\" ],\r\n  \"url\" : [ \"/error\" ],\r\n  \"target\" : [ \"menuItem\" ],\r\n  \"perms\" : [ \"\" ],\r\n  \"orderNum\" : [ \"3\" ],\r\n  \"icon\" : [ \"fa fa-bars\" ],\r\n  \"visible\" : [ \"0\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2019-12-30 01:12:08');
INSERT INTO `sys_oper_log` VALUES (158, '菜单管理', 1, 'com.ruoyi.web.controller.system.SysMenuController.addSave()', 'POST', 1, 'admin', '第1组', '/system/menu/add', '127.0.0.1', '内网IP', '{\r\n  \"parentId\" : [ \"2004\" ],\r\n  \"menuType\" : [ \"C\" ],\r\n  \"menuName\" : [ \"影响因子识别\" ],\r\n  \"url\" : [ \"/error\" ],\r\n  \"target\" : [ \"menuItem\" ],\r\n  \"perms\" : [ \"\" ],\r\n  \"orderNum\" : [ \"1\" ],\r\n  \"icon\" : [ \"fa fa-bars\" ],\r\n  \"visible\" : [ \"0\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2019-12-30 01:13:01');
INSERT INTO `sys_oper_log` VALUES (159, '菜单管理', 1, 'com.ruoyi.web.controller.system.SysMenuController.addSave()', 'POST', 1, 'admin', '第1组', '/system/menu/add', '127.0.0.1', '内网IP', '{\r\n  \"parentId\" : [ \"2004\" ],\r\n  \"menuType\" : [ \"C\" ],\r\n  \"menuName\" : [ \"风险源判断\" ],\r\n  \"url\" : [ \"/error\" ],\r\n  \"target\" : [ \"menuItem\" ],\r\n  \"perms\" : [ \"\" ],\r\n  \"orderNum\" : [ \"2\" ],\r\n  \"icon\" : [ \"fa fa-bars\" ],\r\n  \"visible\" : [ \"0\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2019-12-30 01:13:54');
INSERT INTO `sys_oper_log` VALUES (160, '菜单管理', 1, 'com.ruoyi.web.controller.system.SysMenuController.addSave()', 'POST', 1, 'admin', '第1组', '/system/menu/add', '127.0.0.1', '内网IP', '{\r\n  \"parentId\" : [ \"2004\" ],\r\n  \"menuType\" : [ \"C\" ],\r\n  \"menuName\" : [ \"风险管理方案制定\" ],\r\n  \"url\" : [ \"/error\" ],\r\n  \"target\" : [ \"menuItem\" ],\r\n  \"perms\" : [ \"\" ],\r\n  \"orderNum\" : [ \"3\" ],\r\n  \"icon\" : [ \"fa fa-bars\" ],\r\n  \"visible\" : [ \"0\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2019-12-30 01:14:18');
INSERT INTO `sys_oper_log` VALUES (161, '菜单管理', 1, 'com.ruoyi.web.controller.system.SysMenuController.addSave()', 'POST', 1, 'admin', '第1组', '/system/menu/add', '127.0.0.1', '内网IP', '{\r\n  \"parentId\" : [ \"2005\" ],\r\n  \"menuType\" : [ \"C\" ],\r\n  \"menuName\" : [ \"预案编写\" ],\r\n  \"url\" : [ \"/error\" ],\r\n  \"target\" : [ \"menuItem\" ],\r\n  \"perms\" : [ \"\" ],\r\n  \"orderNum\" : [ \"1\" ],\r\n  \"icon\" : [ \"fa fa-bars\" ],\r\n  \"visible\" : [ \"0\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2019-12-30 01:14:42');
INSERT INTO `sys_oper_log` VALUES (162, '菜单管理', 1, 'com.ruoyi.web.controller.system.SysMenuController.addSave()', 'POST', 1, 'admin', '第1组', '/system/menu/add', '127.0.0.1', '内网IP', '{\r\n  \"parentId\" : [ \"2005\" ],\r\n  \"menuType\" : [ \"C\" ],\r\n  \"menuName\" : [ \"安全培训\" ],\r\n  \"url\" : [ \"/error\" ],\r\n  \"target\" : [ \"menuItem\" ],\r\n  \"perms\" : [ \"\" ],\r\n  \"orderNum\" : [ \"2\" ],\r\n  \"icon\" : [ \"fa fa-bars\" ],\r\n  \"visible\" : [ \"0\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2019-12-30 01:15:00');
INSERT INTO `sys_oper_log` VALUES (163, '菜单管理', 1, 'com.ruoyi.web.controller.system.SysMenuController.addSave()', 'POST', 1, 'admin', '第1组', '/system/menu/add', '127.0.0.1', '内网IP', '{\r\n  \"parentId\" : [ \"2005\" ],\r\n  \"menuType\" : [ \"C\" ],\r\n  \"menuName\" : [ \"预案执行\" ],\r\n  \"url\" : [ \"/error\" ],\r\n  \"target\" : [ \"menuItem\" ],\r\n  \"perms\" : [ \"\" ],\r\n  \"orderNum\" : [ \"3\" ],\r\n  \"icon\" : [ \"fa fa-bars\" ],\r\n  \"visible\" : [ \"0\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2019-12-30 01:15:26');
INSERT INTO `sys_oper_log` VALUES (164, '菜单管理', 1, 'com.ruoyi.web.controller.system.SysMenuController.addSave()', 'POST', 1, 'admin', '第1组', '/system/menu/add', '127.0.0.1', '内网IP', '{\r\n  \"parentId\" : [ \"2005\" ],\r\n  \"menuType\" : [ \"C\" ],\r\n  \"menuName\" : [ \"预案演练\" ],\r\n  \"url\" : [ \"/error\" ],\r\n  \"target\" : [ \"menuItem\" ],\r\n  \"perms\" : [ \"\" ],\r\n  \"orderNum\" : [ \"4\" ],\r\n  \"icon\" : [ \"fa fa-bars\" ],\r\n  \"visible\" : [ \"0\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2019-12-30 01:15:48');
INSERT INTO `sys_oper_log` VALUES (165, '菜单管理', 2, 'com.ruoyi.web.controller.system.SysMenuController.editSave()', 'POST', 1, 'admin', '第1组', '/system/menu/edit', '127.0.0.1', '内网IP', '{\r\n  \"menuId\" : [ \"2\" ],\r\n  \"parentId\" : [ \"0\" ],\r\n  \"menuType\" : [ \"M\" ],\r\n  \"menuName\" : [ \"*系统监控\" ],\r\n  \"url\" : [ \"#\" ],\r\n  \"target\" : [ \"menuItem\" ],\r\n  \"perms\" : [ \"\" ],\r\n  \"orderNum\" : [ \"7\" ],\r\n  \"icon\" : [ \"fa fa-video-camera\" ],\r\n  \"visible\" : [ \"0\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2019-12-30 01:16:23');
INSERT INTO `sys_oper_log` VALUES (166, '菜单管理', 2, 'com.ruoyi.web.controller.system.SysMenuController.editSave()', 'POST', 1, 'admin', '第1组', '/system/menu/edit', '127.0.0.1', '内网IP', '{\r\n  \"menuId\" : [ \"3\" ],\r\n  \"parentId\" : [ \"0\" ],\r\n  \"menuType\" : [ \"M\" ],\r\n  \"menuName\" : [ \"*系统工具\" ],\r\n  \"url\" : [ \"#\" ],\r\n  \"target\" : [ \"menuItem\" ],\r\n  \"perms\" : [ \"\" ],\r\n  \"orderNum\" : [ \"8\" ],\r\n  \"icon\" : [ \"fa fa-bars\" ],\r\n  \"visible\" : [ \"0\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2019-12-30 01:16:28');
INSERT INTO `sys_oper_log` VALUES (167, '菜单管理', 2, 'com.ruoyi.web.controller.system.SysMenuController.editSave()', 'POST', 1, 'admin', '第1组', '/system/menu/edit', '127.0.0.1', '内网IP', '{\r\n  \"menuId\" : [ \"1\" ],\r\n  \"parentId\" : [ \"0\" ],\r\n  \"menuType\" : [ \"M\" ],\r\n  \"menuName\" : [ \"*系统管理\" ],\r\n  \"url\" : [ \"#\" ],\r\n  \"target\" : [ \"menuItem\" ],\r\n  \"perms\" : [ \"\" ],\r\n  \"orderNum\" : [ \"9\" ],\r\n  \"icon\" : [ \"fa fa-gear\" ],\r\n  \"visible\" : [ \"0\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2019-12-30 01:16:32');
INSERT INTO `sys_oper_log` VALUES (168, '菜单管理', 1, 'com.ruoyi.web.controller.system.SysMenuController.addSave()', 'POST', 1, 'admin', '第1组', '/system/menu/add', '127.0.0.1', '内网IP', '{\r\n  \"parentId\" : [ \"2006\" ],\r\n  \"menuType\" : [ \"C\" ],\r\n  \"menuName\" : [ \"事故资料收集\" ],\r\n  \"url\" : [ \"/error\" ],\r\n  \"target\" : [ \"menuItem\" ],\r\n  \"perms\" : [ \"\" ],\r\n  \"orderNum\" : [ \"1\" ],\r\n  \"icon\" : [ \"fa fa-bars\" ],\r\n  \"visible\" : [ \"0\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2019-12-30 01:17:27');
INSERT INTO `sys_oper_log` VALUES (169, '菜单管理', 1, 'com.ruoyi.web.controller.system.SysMenuController.addSave()', 'POST', 1, 'admin', '第1组', '/system/menu/add', '127.0.0.1', '内网IP', '{\r\n  \"parentId\" : [ \"2006\" ],\r\n  \"menuType\" : [ \"C\" ],\r\n  \"menuName\" : [ \"事故调查分析\" ],\r\n  \"url\" : [ \"/error\" ],\r\n  \"target\" : [ \"menuItem\" ],\r\n  \"perms\" : [ \"\" ],\r\n  \"orderNum\" : [ \"2\" ],\r\n  \"icon\" : [ \"fa fa-bars\" ],\r\n  \"visible\" : [ \"0\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2019-12-30 01:17:47');
INSERT INTO `sys_oper_log` VALUES (170, '菜单管理', 1, 'com.ruoyi.web.controller.system.SysMenuController.addSave()', 'POST', 1, 'admin', '第1组', '/system/menu/add', '127.0.0.1', '内网IP', '{\r\n  \"parentId\" : [ \"2006\" ],\r\n  \"menuType\" : [ \"C\" ],\r\n  \"menuName\" : [ \"事故防范措施\" ],\r\n  \"url\" : [ \"/error\" ],\r\n  \"target\" : [ \"menuItem\" ],\r\n  \"perms\" : [ \"\" ],\r\n  \"orderNum\" : [ \"3\" ],\r\n  \"icon\" : [ \"fa fa-bars\" ],\r\n  \"visible\" : [ \"0\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2019-12-30 01:18:03');
INSERT INTO `sys_oper_log` VALUES (171, '菜单管理', 1, 'com.ruoyi.web.controller.system.SysMenuController.addSave()', 'POST', 1, 'admin', '第1组', '/system/menu/add', '127.0.0.1', '内网IP', '{\r\n  \"parentId\" : [ \"2006\" ],\r\n  \"menuType\" : [ \"C\" ],\r\n  \"menuName\" : [ \"事故文档备案\" ],\r\n  \"url\" : [ \"/error\" ],\r\n  \"target\" : [ \"menuItem\" ],\r\n  \"perms\" : [ \"\" ],\r\n  \"orderNum\" : [ \"4\" ],\r\n  \"icon\" : [ \"fa fa-bars\" ],\r\n  \"visible\" : [ \"0\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2019-12-30 01:18:28');
INSERT INTO `sys_oper_log` VALUES (172, '菜单管理', 1, 'com.ruoyi.web.controller.system.SysMenuController.addSave()', 'POST', 1, 'admin', '第1组', '/system/menu/add', '127.0.0.1', '内网IP', '{\r\n  \"parentId\" : [ \"2007\" ],\r\n  \"menuType\" : [ \"C\" ],\r\n  \"menuName\" : [ \"内部安全审核\" ],\r\n  \"url\" : [ \"/error\" ],\r\n  \"target\" : [ \"menuItem\" ],\r\n  \"perms\" : [ \"\" ],\r\n  \"orderNum\" : [ \"1\" ],\r\n  \"icon\" : [ \"fa fa-bars\" ],\r\n  \"visible\" : [ \"0\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2019-12-30 01:19:00');
INSERT INTO `sys_oper_log` VALUES (173, '菜单管理', 1, 'com.ruoyi.web.controller.system.SysMenuController.addSave()', 'POST', 1, 'admin', '第1组', '/system/menu/add', '127.0.0.1', '内网IP', '{\r\n  \"parentId\" : [ \"2007\" ],\r\n  \"menuType\" : [ \"C\" ],\r\n  \"menuName\" : [ \"施工过程评审\" ],\r\n  \"url\" : [ \"/error\" ],\r\n  \"target\" : [ \"menuItem\" ],\r\n  \"perms\" : [ \"\" ],\r\n  \"orderNum\" : [ \"2\" ],\r\n  \"icon\" : [ \"fa fa-bars\" ],\r\n  \"visible\" : [ \"0\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2019-12-30 01:19:19');
INSERT INTO `sys_oper_log` VALUES (174, '菜单管理', 1, 'com.ruoyi.web.controller.system.SysMenuController.addSave()', 'POST', 1, 'admin', '第1组', '/system/menu/add', '127.0.0.1', '内网IP', '{\r\n  \"parentId\" : [ \"2007\" ],\r\n  \"menuType\" : [ \"C\" ],\r\n  \"menuName\" : [ \"施工纠错改正\" ],\r\n  \"url\" : [ \"/error\" ],\r\n  \"target\" : [ \"menuItem\" ],\r\n  \"perms\" : [ \"\" ],\r\n  \"orderNum\" : [ \"3\" ],\r\n  \"icon\" : [ \"fa fa-bars\" ],\r\n  \"visible\" : [ \"0\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2019-12-30 01:19:39');
INSERT INTO `sys_oper_log` VALUES (175, '菜单管理', 2, 'com.ruoyi.web.controller.system.SysMenuController.editSave()', 'POST', 1, 'admin', '第1组', '/system/menu/edit', '127.0.0.1', '内网IP', '{\r\n  \"menuId\" : [ \"2\" ],\r\n  \"parentId\" : [ \"0\" ],\r\n  \"menuType\" : [ \"M\" ],\r\n  \"menuName\" : [ \"***系统监控\" ],\r\n  \"url\" : [ \"#\" ],\r\n  \"target\" : [ \"menuItem\" ],\r\n  \"perms\" : [ \"\" ],\r\n  \"orderNum\" : [ \"7\" ],\r\n  \"icon\" : [ \"fa fa-video-camera\" ],\r\n  \"visible\" : [ \"0\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2019-12-30 01:21:52');
INSERT INTO `sys_oper_log` VALUES (176, '菜单管理', 2, 'com.ruoyi.web.controller.system.SysMenuController.editSave()', 'POST', 1, 'admin', '第1组', '/system/menu/edit', '127.0.0.1', '内网IP', '{\r\n  \"menuId\" : [ \"3\" ],\r\n  \"parentId\" : [ \"0\" ],\r\n  \"menuType\" : [ \"M\" ],\r\n  \"menuName\" : [ \"***系统工具\" ],\r\n  \"url\" : [ \"#\" ],\r\n  \"target\" : [ \"menuItem\" ],\r\n  \"perms\" : [ \"\" ],\r\n  \"orderNum\" : [ \"8\" ],\r\n  \"icon\" : [ \"fa fa-bars\" ],\r\n  \"visible\" : [ \"0\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2019-12-30 01:21:57');
INSERT INTO `sys_oper_log` VALUES (177, '菜单管理', 2, 'com.ruoyi.web.controller.system.SysMenuController.editSave()', 'POST', 1, 'admin', '第1组', '/system/menu/edit', '127.0.0.1', '内网IP', '{\r\n  \"menuId\" : [ \"1\" ],\r\n  \"parentId\" : [ \"0\" ],\r\n  \"menuType\" : [ \"M\" ],\r\n  \"menuName\" : [ \"***系统管理\" ],\r\n  \"url\" : [ \"#\" ],\r\n  \"target\" : [ \"menuItem\" ],\r\n  \"perms\" : [ \"\" ],\r\n  \"orderNum\" : [ \"9\" ],\r\n  \"icon\" : [ \"fa fa-gear\" ],\r\n  \"visible\" : [ \"0\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2019-12-30 01:22:01');
INSERT INTO `sys_oper_log` VALUES (178, '岗位管理', 2, 'com.ruoyi.web.controller.system.SysPostController.editSave()', 'POST', 1, 'admin', '第1组', '/system/post/edit', '127.0.0.1', '内网IP', '{\r\n  \"postId\" : [ \"1\" ],\r\n  \"postName\" : [ \"施工人员\" ],\r\n  \"postCode\" : [ \"sgry\" ],\r\n  \"postSort\" : [ \"1\" ],\r\n  \"status\" : [ \"0\" ],\r\n  \"remark\" : [ \"\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2019-12-30 01:23:08');
INSERT INTO `sys_oper_log` VALUES (179, '岗位管理', 2, 'com.ruoyi.web.controller.system.SysPostController.editSave()', 'POST', 1, 'admin', '第1组', '/system/post/edit', '127.0.0.1', '内网IP', '{\r\n  \"postId\" : [ \"2\" ],\r\n  \"postName\" : [ \"项目经理\" ],\r\n  \"postCode\" : [ \"xmjl\" ],\r\n  \"postSort\" : [ \"2\" ],\r\n  \"status\" : [ \"0\" ],\r\n  \"remark\" : [ \"\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2019-12-30 01:23:33');
INSERT INTO `sys_oper_log` VALUES (180, '岗位管理', 2, 'com.ruoyi.web.controller.system.SysPostController.editSave()', 'POST', 1, 'admin', '第1组', '/system/post/edit', '127.0.0.1', '内网IP', '{\r\n  \"postId\" : [ \"3\" ],\r\n  \"postName\" : [ \"监理\" ],\r\n  \"postCode\" : [ \"jl\" ],\r\n  \"postSort\" : [ \"3\" ],\r\n  \"status\" : [ \"0\" ],\r\n  \"remark\" : [ \"\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2019-12-30 01:23:58');
INSERT INTO `sys_oper_log` VALUES (181, '岗位管理', 2, 'com.ruoyi.web.controller.system.SysPostController.editSave()', 'POST', 1, 'admin', '第1组', '/system/post/edit', '127.0.0.1', '内网IP', '{\r\n  \"postId\" : [ \"4\" ],\r\n  \"postName\" : [ \"系统管理员\" ],\r\n  \"postCode\" : [ \"admin\" ],\r\n  \"postSort\" : [ \"4\" ],\r\n  \"status\" : [ \"0\" ],\r\n  \"remark\" : [ \"\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2019-12-30 01:24:13');
INSERT INTO `sys_oper_log` VALUES (182, '岗位管理', 2, 'com.ruoyi.web.controller.system.SysPostController.editSave()', 'POST', 1, 'admin', '第1组', '/system/post/edit', '127.0.0.1', '内网IP', '{\r\n  \"postId\" : [ \"3\" ],\r\n  \"postName\" : [ \"监理\" ],\r\n  \"postCode\" : [ \"supervisor\" ],\r\n  \"postSort\" : [ \"3\" ],\r\n  \"status\" : [ \"0\" ],\r\n  \"remark\" : [ \"\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2019-12-30 01:24:38');
INSERT INTO `sys_oper_log` VALUES (183, '岗位管理', 2, 'com.ruoyi.web.controller.system.SysPostController.editSave()', 'POST', 1, 'admin', '第1组', '/system/post/edit', '127.0.0.1', '内网IP', '{\r\n  \"postId\" : [ \"2\" ],\r\n  \"postName\" : [ \"项目经理\" ],\r\n  \"postCode\" : [ \"project manager\" ],\r\n  \"postSort\" : [ \"2\" ],\r\n  \"status\" : [ \"0\" ],\r\n  \"remark\" : [ \"\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2019-12-30 01:24:53');
INSERT INTO `sys_oper_log` VALUES (184, '岗位管理', 2, 'com.ruoyi.web.controller.system.SysPostController.editSave()', 'POST', 1, 'admin', '第1组', '/system/post/edit', '127.0.0.1', '内网IP', '{\r\n  \"postId\" : [ \"1\" ],\r\n  \"postName\" : [ \"施工人员\" ],\r\n  \"postCode\" : [ \"cp\" ],\r\n  \"postSort\" : [ \"1\" ],\r\n  \"status\" : [ \"0\" ],\r\n  \"remark\" : [ \"\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2019-12-30 01:25:14');
INSERT INTO `sys_oper_log` VALUES (185, '岗位管理', 2, 'com.ruoyi.web.controller.system.SysPostController.editSave()', 'POST', 1, 'admin', '第1组', '/system/post/edit', '127.0.0.1', '内网IP', '{\r\n  \"postId\" : [ \"2\" ],\r\n  \"postName\" : [ \"项目经理\" ],\r\n  \"postCode\" : [ \"pm\" ],\r\n  \"postSort\" : [ \"2\" ],\r\n  \"status\" : [ \"0\" ],\r\n  \"remark\" : [ \"\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2019-12-30 01:25:19');
INSERT INTO `sys_oper_log` VALUES (186, '岗位管理', 2, 'com.ruoyi.web.controller.system.SysPostController.editSave()', 'POST', 1, 'admin', '第1组', '/system/post/edit', '127.0.0.1', '内网IP', '{\r\n  \"postId\" : [ \"3\" ],\r\n  \"postName\" : [ \"监理\" ],\r\n  \"postCode\" : [ \"sv\" ],\r\n  \"postSort\" : [ \"3\" ],\r\n  \"status\" : [ \"0\" ],\r\n  \"remark\" : [ \"\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2019-12-30 01:25:26');
INSERT INTO `sys_oper_log` VALUES (187, '岗位管理', 2, 'com.ruoyi.web.controller.system.SysPostController.editSave()', 'POST', 1, 'admin', '第1组', '/system/post/edit', '127.0.0.1', '内网IP', '{\r\n  \"postId\" : [ \"4\" ],\r\n  \"postName\" : [ \"系统管理员\" ],\r\n  \"postCode\" : [ \"admin\" ],\r\n  \"postSort\" : [ \"4\" ],\r\n  \"status\" : [ \"0\" ],\r\n  \"remark\" : [ \"\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2019-12-30 01:25:30');
INSERT INTO `sys_oper_log` VALUES (188, '用户管理', 2, 'com.ruoyi.web.controller.system.SysUserController.editSave()', 'POST', 1, 'admin', '第1组', '/system/user/edit', '127.0.0.1', '内网IP', '{\r\n  \"userId\" : [ \"1\" ],\r\n  \"deptId\" : [ \"103\" ],\r\n  \"userName\" : [ \"陈贺\" ],\r\n  \"dept.deptName\" : [ \"第1组\" ],\r\n  \"phonenumber\" : [ \"19851668369\" ],\r\n  \"email\" : [ \"ch@xzit.com\" ],\r\n  \"loginName\" : [ \"admin\" ],\r\n  \"sex\" : [ \"0\" ],\r\n  \"role\" : [ \"1\" ],\r\n  \"remark\" : [ \"管理员\" ],\r\n  \"status\" : [ \"0\" ],\r\n  \"roleIds\" : [ \"1\" ],\r\n  \"postIds\" : [ \"4\" ]\r\n}', 'null', 1, '不允许操作超级管理员用户', '2019-12-30 01:27:50');
INSERT INTO `sys_oper_log` VALUES (189, '用户管理', 2, 'com.ruoyi.web.controller.system.SysUserController.editSave()', 'POST', 1, 'admin', '第1组', '/system/user/edit', '127.0.0.1', '内网IP', '{\r\n  \"userId\" : [ \"1\" ],\r\n  \"deptId\" : [ \"103\" ],\r\n  \"userName\" : [ \"陈贺\" ],\r\n  \"dept.deptName\" : [ \"第1组\" ],\r\n  \"phonenumber\" : [ \"19851668369\" ],\r\n  \"email\" : [ \"ch@xzit.com\" ],\r\n  \"loginName\" : [ \"admin\" ],\r\n  \"sex\" : [ \"0\" ],\r\n  \"role\" : [ \"1\" ],\r\n  \"remark\" : [ \"管理员\" ],\r\n  \"status\" : [ \"0\" ],\r\n  \"roleIds\" : [ \"1\" ],\r\n  \"postIds\" : [ \"4\" ]\r\n}', 'null', 1, '不允许操作超级管理员用户', '2019-12-30 01:27:53');
INSERT INTO `sys_oper_log` VALUES (190, '岗位管理', 2, 'com.ruoyi.web.controller.system.SysPostController.editSave()', 'POST', 1, 'admin', '第1组', '/system/post/edit', '127.0.0.1', '内网IP', '{\r\n  \"postId\" : [ \"1\" ],\r\n  \"postName\" : [ \"系统管理员1\" ],\r\n  \"postCode\" : [ \"admin1\" ],\r\n  \"postSort\" : [ \"1\" ],\r\n  \"status\" : [ \"0\" ],\r\n  \"remark\" : [ \"\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2019-12-30 01:28:25');
INSERT INTO `sys_oper_log` VALUES (191, '岗位管理', 2, 'com.ruoyi.web.controller.system.SysPostController.editSave()', 'POST', 1, 'admin', '第1组', '/system/post/edit', '127.0.0.1', '内网IP', '{\r\n  \"postId\" : [ \"4\" ],\r\n  \"postName\" : [ \"施工人员\" ],\r\n  \"postCode\" : [ \"cp\" ],\r\n  \"postSort\" : [ \"1\" ],\r\n  \"status\" : [ \"0\" ],\r\n  \"remark\" : [ \"\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2019-12-30 01:28:41');
INSERT INTO `sys_oper_log` VALUES (192, '岗位管理', 2, 'com.ruoyi.web.controller.system.SysPostController.editSave()', 'POST', 1, 'admin', '第1组', '/system/post/edit', '127.0.0.1', '内网IP', '{\r\n  \"postId\" : [ \"1\" ],\r\n  \"postName\" : [ \"系统管理员\" ],\r\n  \"postCode\" : [ \"admin\" ],\r\n  \"postSort\" : [ \"4\" ],\r\n  \"status\" : [ \"0\" ],\r\n  \"remark\" : [ \"\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2019-12-30 01:28:51');
INSERT INTO `sys_oper_log` VALUES (193, '部门管理', 3, 'com.ruoyi.web.controller.system.SysDeptController.remove()', 'GET', 1, 'admin', '第1组', '/system/dept/remove/203', '127.0.0.1', '内网IP', '{ }', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2019-12-30 01:29:30');
INSERT INTO `sys_oper_log` VALUES (194, '部门管理', 2, 'com.ruoyi.web.controller.system.SysDeptController.editSave()', 'POST', 1, 'admin', '第1组', '/system/dept/edit', '127.0.0.1', '内网IP', '{\r\n  \"deptId\" : [ \"101\" ],\r\n  \"parentId\" : [ \"100\" ],\r\n  \"parentName\" : [ \"建筑安全管理\" ],\r\n  \"deptName\" : [ \"徐州分公司\" ],\r\n  \"orderNum\" : [ \"1\" ],\r\n  \"leader\" : [ \"陈贺\" ],\r\n  \"phone\" : [ \"19851668369\" ],\r\n  \"email\" : [ \"ch@xzit.com\" ],\r\n  \"status\" : [ \"0\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2019-12-30 01:29:54');
INSERT INTO `sys_oper_log` VALUES (195, '部门管理', 2, 'com.ruoyi.web.controller.system.SysDeptController.editSave()', 'POST', 1, 'admin', '第1组', '/system/dept/edit', '127.0.0.1', '内网IP', '{\r\n  \"deptId\" : [ \"102\" ],\r\n  \"parentId\" : [ \"100\" ],\r\n  \"parentName\" : [ \"建筑安全管理\" ],\r\n  \"deptName\" : [ \"北京分公司\" ],\r\n  \"orderNum\" : [ \"2\" ],\r\n  \"leader\" : [ \"若依\" ],\r\n  \"phone\" : [ \"15888888888\" ],\r\n  \"email\" : [ \"ry@qq.com\" ],\r\n  \"status\" : [ \"0\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2019-12-30 01:30:05');
INSERT INTO `sys_oper_log` VALUES (196, '部门管理', 2, 'com.ruoyi.web.controller.system.SysDeptController.editSave()', 'POST', 1, 'admin', '第1组', '/system/dept/edit', '127.0.0.1', '内网IP', '{\r\n  \"deptId\" : [ \"200\" ],\r\n  \"parentId\" : [ \"100\" ],\r\n  \"parentName\" : [ \"建筑安全管理\" ],\r\n  \"deptName\" : [ \"上海分公司\" ],\r\n  \"orderNum\" : [ \"3\" ],\r\n  \"leader\" : [ \"陈贺\" ],\r\n  \"phone\" : [ \"19851668369\" ],\r\n  \"email\" : [ \"vchenhe@live.com\" ],\r\n  \"status\" : [ \"0\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2019-12-30 01:30:13');
INSERT INTO `sys_oper_log` VALUES (197, '部门管理', 2, 'com.ruoyi.web.controller.system.SysDeptController.editSave()', 'POST', 1, 'admin', '第1组', '/system/dept/edit', '127.0.0.1', '内网IP', '{\r\n  \"deptId\" : [ \"101\" ],\r\n  \"parentId\" : [ \"100\" ],\r\n  \"parentName\" : [ \"建筑安全管理\" ],\r\n  \"deptName\" : [ \"徐州总公司\" ],\r\n  \"orderNum\" : [ \"1\" ],\r\n  \"leader\" : [ \"陈贺\" ],\r\n  \"phone\" : [ \"19851668369\" ],\r\n  \"email\" : [ \"ch@xzit.com\" ],\r\n  \"status\" : [ \"0\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2019-12-30 01:30:19');
INSERT INTO `sys_oper_log` VALUES (198, '部门管理', 2, 'com.ruoyi.web.controller.system.SysDeptController.editSave()', 'POST', 1, 'admin', '第1组', '/system/dept/edit', '127.0.0.1', '内网IP', '{\r\n  \"deptId\" : [ \"103\" ],\r\n  \"parentId\" : [ \"101\" ],\r\n  \"parentName\" : [ \"徐州总公司\" ],\r\n  \"deptName\" : [ \"徐州第1组\" ],\r\n  \"orderNum\" : [ \"1\" ],\r\n  \"leader\" : [ \"陈贺\" ],\r\n  \"phone\" : [ \"19851668369\" ],\r\n  \"email\" : [ \"ch@qq.com\" ],\r\n  \"status\" : [ \"0\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2019-12-30 01:31:19');
INSERT INTO `sys_oper_log` VALUES (199, '部门管理', 2, 'com.ruoyi.web.controller.system.SysDeptController.editSave()', 'POST', 1, 'admin', '第1组', '/system/dept/edit', '127.0.0.1', '内网IP', '{\r\n  \"deptId\" : [ \"104\" ],\r\n  \"parentId\" : [ \"101\" ],\r\n  \"parentName\" : [ \"徐州总公司\" ],\r\n  \"deptName\" : [ \"徐州第2组\" ],\r\n  \"orderNum\" : [ \"2\" ],\r\n  \"leader\" : [ \"郭宙\" ],\r\n  \"phone\" : [ \"19812345678\" ],\r\n  \"email\" : [ \"gz@qq.com\" ],\r\n  \"status\" : [ \"0\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2019-12-30 01:31:37');
INSERT INTO `sys_oper_log` VALUES (200, '部门管理', 2, 'com.ruoyi.web.controller.system.SysDeptController.editSave()', 'POST', 1, 'admin', '第1组', '/system/dept/edit', '127.0.0.1', '内网IP', '{\r\n  \"deptId\" : [ \"102\" ],\r\n  \"parentId\" : [ \"100\" ],\r\n  \"parentName\" : [ \"建筑安全管理\" ],\r\n  \"deptName\" : [ \"北京分公司\" ],\r\n  \"orderNum\" : [ \"2\" ],\r\n  \"leader\" : [ \"郭宙\" ],\r\n  \"phone\" : [ \"19812344321\" ],\r\n  \"email\" : [ \"gz@qq.com\" ],\r\n  \"status\" : [ \"0\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2019-12-30 01:32:05');
INSERT INTO `sys_oper_log` VALUES (201, '部门管理', 2, 'com.ruoyi.web.controller.system.SysDeptController.editSave()', 'POST', 1, 'admin', '第1组', '/system/dept/edit', '127.0.0.1', '内网IP', '{\r\n  \"deptId\" : [ \"108\" ],\r\n  \"parentId\" : [ \"102\" ],\r\n  \"parentName\" : [ \"北京分公司\" ],\r\n  \"deptName\" : [ \"北京第1组\" ],\r\n  \"orderNum\" : [ \"1\" ],\r\n  \"leader\" : [ \"gz\" ],\r\n  \"phone\" : [ \"15888888888\" ],\r\n  \"email\" : [ \"gz@qq.com\" ],\r\n  \"status\" : [ \"0\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2019-12-30 01:32:20');
INSERT INTO `sys_oper_log` VALUES (202, '部门管理', 2, 'com.ruoyi.web.controller.system.SysDeptController.editSave()', 'POST', 1, 'admin', '第1组', '/system/dept/edit', '127.0.0.1', '内网IP', '{\r\n  \"deptId\" : [ \"109\" ],\r\n  \"parentId\" : [ \"102\" ],\r\n  \"parentName\" : [ \"北京分公司\" ],\r\n  \"deptName\" : [ \"北京第2组\" ],\r\n  \"orderNum\" : [ \"2\" ],\r\n  \"leader\" : [ \"gz\" ],\r\n  \"phone\" : [ \"15888888888\" ],\r\n  \"email\" : [ \"gz@qq.com\" ],\r\n  \"status\" : [ \"0\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2019-12-30 01:32:30');
INSERT INTO `sys_oper_log` VALUES (203, '部门管理', 2, 'com.ruoyi.web.controller.system.SysDeptController.editSave()', 'POST', 1, 'admin', '第1组', '/system/dept/edit', '127.0.0.1', '内网IP', '{\r\n  \"deptId\" : [ \"104\" ],\r\n  \"parentId\" : [ \"101\" ],\r\n  \"parentName\" : [ \"徐州总公司\" ],\r\n  \"deptName\" : [ \"徐州第2组\" ],\r\n  \"orderNum\" : [ \"2\" ],\r\n  \"leader\" : [ \"ch\" ],\r\n  \"phone\" : [ \"19812345678\" ],\r\n  \"email\" : [ \"ch@qq.com\" ],\r\n  \"status\" : [ \"0\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2019-12-30 01:32:44');
INSERT INTO `sys_oper_log` VALUES (204, '部门管理', 2, 'com.ruoyi.web.controller.system.SysDeptController.editSave()', 'POST', 1, 'admin', '第1组', '/system/dept/edit', '127.0.0.1', '内网IP', '{\r\n  \"deptId\" : [ \"200\" ],\r\n  \"parentId\" : [ \"100\" ],\r\n  \"parentName\" : [ \"建筑安全管理\" ],\r\n  \"deptName\" : [ \"上海分公司\" ],\r\n  \"orderNum\" : [ \"3\" ],\r\n  \"leader\" : [ \"guozhou\" ],\r\n  \"phone\" : [ \"19851668369\" ],\r\n  \"email\" : [ \"guozhou@qq.com\" ],\r\n  \"status\" : [ \"0\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2019-12-30 01:33:06');
INSERT INTO `sys_oper_log` VALUES (205, '部门管理', 2, 'com.ruoyi.web.controller.system.SysDeptController.editSave()', 'POST', 1, 'admin', '第1组', '/system/dept/edit', '127.0.0.1', '内网IP', '{\r\n  \"deptId\" : [ \"201\" ],\r\n  \"parentId\" : [ \"200\" ],\r\n  \"parentName\" : [ \"上海分公司\" ],\r\n  \"deptName\" : [ \"上海第1组\" ],\r\n  \"orderNum\" : [ \"1\" ],\r\n  \"leader\" : [ \"陈贺\" ],\r\n  \"phone\" : [ \"19851668369\" ],\r\n  \"email\" : [ \"vchenhe@live.com\" ],\r\n  \"status\" : [ \"0\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2019-12-30 01:33:15');
INSERT INTO `sys_oper_log` VALUES (206, '部门管理', 2, 'com.ruoyi.web.controller.system.SysDeptController.editSave()', 'POST', 1, 'admin', '第1组', '/system/dept/edit', '127.0.0.1', '内网IP', '{\r\n  \"deptId\" : [ \"202\" ],\r\n  \"parentId\" : [ \"200\" ],\r\n  \"parentName\" : [ \"上海分公司\" ],\r\n  \"deptName\" : [ \"上海第2组\" ],\r\n  \"orderNum\" : [ \"2\" ],\r\n  \"leader\" : [ \"陈贺\" ],\r\n  \"phone\" : [ \"19851668369\" ],\r\n  \"email\" : [ \"vchenhe@live.com\" ],\r\n  \"status\" : [ \"0\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2019-12-30 01:33:18');
INSERT INTO `sys_oper_log` VALUES (207, '部门管理', 2, 'com.ruoyi.web.controller.system.SysDeptController.editSave()', 'POST', 1, 'admin', '第1组', '/system/dept/edit', '127.0.0.1', '内网IP', '{\r\n  \"deptId\" : [ \"201\" ],\r\n  \"parentId\" : [ \"200\" ],\r\n  \"parentName\" : [ \"上海分公司\" ],\r\n  \"deptName\" : [ \"上海第1组\" ],\r\n  \"orderNum\" : [ \"1\" ],\r\n  \"leader\" : [ \"guozhou\" ],\r\n  \"phone\" : [ \"19812345678\" ],\r\n  \"email\" : [ \"guozhou@qq.com\" ],\r\n  \"status\" : [ \"0\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2019-12-30 01:33:39');
INSERT INTO `sys_oper_log` VALUES (208, '部门管理', 2, 'com.ruoyi.web.controller.system.SysDeptController.editSave()', 'POST', 1, 'admin', '第1组', '/system/dept/edit', '127.0.0.1', '内网IP', '{\r\n  \"deptId\" : [ \"200\" ],\r\n  \"parentId\" : [ \"100\" ],\r\n  \"parentName\" : [ \"建筑安全管理\" ],\r\n  \"deptName\" : [ \"上海分公司\" ],\r\n  \"orderNum\" : [ \"3\" ],\r\n  \"leader\" : [ \"guozhou\" ],\r\n  \"phone\" : [ \"19812345678\" ],\r\n  \"email\" : [ \"guozhou@qq.com\" ],\r\n  \"status\" : [ \"0\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2019-12-30 01:33:47');
INSERT INTO `sys_oper_log` VALUES (209, '部门管理', 2, 'com.ruoyi.web.controller.system.SysDeptController.editSave()', 'POST', 1, 'admin', '第1组', '/system/dept/edit', '127.0.0.1', '内网IP', '{\r\n  \"deptId\" : [ \"202\" ],\r\n  \"parentId\" : [ \"200\" ],\r\n  \"parentName\" : [ \"上海分公司\" ],\r\n  \"deptName\" : [ \"上海第2组\" ],\r\n  \"orderNum\" : [ \"2\" ],\r\n  \"leader\" : [ \"guozhou\" ],\r\n  \"phone\" : [ \"19812345678\" ],\r\n  \"email\" : [ \"guozhou@qq.com\" ],\r\n  \"status\" : [ \"0\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2019-12-30 01:34:00');
INSERT INTO `sys_oper_log` VALUES (210, '用户管理', 5, 'com.ruoyi.web.controller.system.SysUserController.export()', 'POST', 1, 'admin', '徐州第1组', '/system/user/export', '127.0.0.1', '内网IP', '{\r\n  \"deptId\" : [ \"\" ],\r\n  \"parentId\" : [ \"\" ],\r\n  \"loginName\" : [ \"\" ],\r\n  \"phonenumber\" : [ \"\" ],\r\n  \"status\" : [ \"\" ],\r\n  \"params[beginTime]\" : [ \"\" ],\r\n  \"params[endTime]\" : [ \"\" ]\r\n}', '{\r\n  \"msg\" : \"bf30a36c-aa1d-4bfd-88cc-4ca35429021a_用户数据.xlsx\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2019-12-30 01:36:08');
INSERT INTO `sys_oper_log` VALUES (211, '用户管理', 6, 'com.ruoyi.web.controller.system.SysUserController.importData()', 'POST', 1, 'admin', '徐州第1组', '/system/user/importData', '127.0.0.1', '内网IP', '{\r\n  \"updateSupport\" : [ \"false\" ]\r\n}', 'null', 1, '很抱歉，导入失败！共 1 条数据格式不正确，错误如下：<br/>1、账号 chenhe1 导入失败：\r\n### Error updating database.  Cause: java.sql.SQLIntegrityConstraintViolationException: Duplicate entry \'2\' for key \'PRIMARY\'\r\n### The error may involve com.ruoyi.system.mapper.SysUserMapper.insertUser-Inline\r\n### The error occurred while setting parameters\r\n### SQL: insert into sys_user(      user_id,             login_name,       user_name,       email,             phonenumber,       sex,       password,             status,       create_by,            create_time    )values(      ?,             ?,       ?,       ?,             ?,       ?,       ?,             ?,       ?,            sysdate()    )\r\n### Cause: java.sql.SQLIntegrityConstraintViolationException: Duplicate entry \'2\' for key \'PRIMARY\'\n; Duplicate entry \'2\' for key \'PRIMARY\'; nested exception is java.sql.SQLIntegrityConstraintViolationException: Duplicate entry \'2\' for key \'PRIMARY\'', '2019-12-30 01:48:35');
INSERT INTO `sys_oper_log` VALUES (212, '用户管理', 6, 'com.ruoyi.web.controller.system.SysUserController.importData()', 'POST', 1, 'admin', '徐州第1组', '/system/user/importData', '127.0.0.1', '内网IP', '{\r\n  \"updateSupport\" : [ \"false\" ]\r\n}', 'null', 1, '很抱歉，导入失败！共 120 条数据格式不正确，错误如下：<br/>1、账号 chenhe1 导入失败：\r\n### Error updating database.  Cause: java.sql.SQLIntegrityConstraintViolationException: Duplicate entry \'2\' for key \'PRIMARY\'\r\n### The error may involve com.ruoyi.system.mapper.SysUserMapper.insertUser-Inline\r\n### The error occurred while setting parameters\r\n### SQL: insert into sys_user(      user_id,             login_name,       user_name,       email,             phonenumber,       sex,       password,             status,       create_by,            create_time    )values(      ?,             ?,       ?,       ?,             ?,       ?,       ?,             ?,       ?,            sysdate()    )\r\n### Cause: java.sql.SQLIntegrityConstraintViolationException: Duplicate entry \'2\' for key \'PRIMARY\'\n; Duplicate entry \'2\' for key \'PRIMARY\'; nested exception is java.sql.SQLIntegrityConstraintViolationException: Duplicate entry \'2\' for key \'PRIMARY\'<br/>2、账号 guozhou1 已存在<br/>3、账号 qinjia1 已存在<br/>4、账号 dongjuanjuan1 已存在<br/>5、账号 shatianwei1 已存在<br/>6、账号 chenhe2 已存在<br/>7、账号 guozhou2 已存在<br/>8、账号 qinjia2 已存在<br/>9、账号 dongjuanjuan2 已存在<br/>10、账号 shatianwei2 已存在<br/>11、账号 chenhe3 已存在<br/>12、账号 guozhou3 已存在<br/>13、账号 qinjia3 已存在<br/>14、账号 dongjuanjuan3 已存在<br/>15、账号 shatianwei3 已存在<br/>16、账号 chenhe4 已存在<br/>17、账号 guozhou4 已存在<br/>18、账号 qinjia4 已存在<br/>19、账号 dongjuanjuan4 已存在<br/>20、账号 shatianwei4 已存在<br/>21、账号 chenhe5 已存在<br/>22、账号 guozhou5 已存在<br/>23、账号 qinjia5 已存在<br/>24、账号 dongjuanjuan5 已存在<br/>25、账号 shatianwei5 已存在<br/>26、账号 chenhe6 已存在<br/>27、账号 guozhou6 已存在<br/>28、账号 qinjia6 已存在<br/>29、账号 dongjuanjuan6 已存在<br/>30、账号 shatianwei6 已存在<br/>31、账号 chenhe7 已存在<br/>32、账号 guozhou7 已存在<br/>33、账号 qinjia7 已存在<br/>34、账号 dongjuanjuan7 已存在<br/>35、账号 shatianwei7 已存在<br/>36、账号 chenhe8 已存在<br/>37、账号 guozhou8 已存在<br/>38、账号 qinjia8 已存在<br/>39、账号 dongjuanjuan8 已存在<br/>40、账号 shatianwei8 已存在<br/>41、账号 chenhe9 已存在<br/>42、账号 guozhou9 已存在<br/>43、账号 qinjia9 已存在<br/>44、账号 dongjuanjuan9 已存在<br/>45、账号 shatianwei9 已存在<br/>46、账号 chenhe10 已存在<br/>47、', '2019-12-30 01:49:12');
INSERT INTO `sys_oper_log` VALUES (213, '角色管理', 3, 'com.ruoyi.web.controller.system.SysRoleController.remove()', 'POST', 1, 'admin', '徐州第1组', '/system/role/remove', '127.0.0.1', '内网IP', '{\r\n  \"ids\" : [ \"2\" ]\r\n}', '{\r\n  \"msg\" : \"普通角色已分配,不能删除\",\r\n  \"code\" : 500\r\n}', 0, NULL, '2019-12-30 01:59:42');
INSERT INTO `sys_oper_log` VALUES (214, '角色管理', 4, 'com.ruoyi.web.controller.system.SysRoleController.cancelAuthUser()', 'POST', 1, 'admin', '徐州第1组', '/system/role/authUser/cancel', '127.0.0.1', '内网IP', '{\r\n  \"roleId\" : [ \"2\" ],\r\n  \"userId\" : [ \"2\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2019-12-30 01:59:50');
INSERT INTO `sys_oper_log` VALUES (215, '角色管理', 4, 'com.ruoyi.web.controller.system.SysRoleController.cancelAuthUser()', 'POST', 1, 'admin', '徐州第1组', '/system/role/authUser/cancel', '127.0.0.1', '内网IP', '{\r\n  \"roleId\" : [ \"1\" ],\r\n  \"userId\" : [ \"1\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2019-12-30 01:59:58');
INSERT INTO `sys_oper_log` VALUES (216, '角色管理', 2, 'com.ruoyi.web.controller.system.SysRoleController.editSave()', 'POST', 1, 'admin', '徐州第1组', '/system/role/edit', '127.0.0.1', '内网IP', '{\r\n  \"roleId\" : [ \"2\" ],\r\n  \"roleName\" : [ \"高级员工\" ],\r\n  \"roleKey\" : [ \"common\" ],\r\n  \"roleSort\" : [ \"2\" ],\r\n  \"status\" : [ \"0\" ],\r\n  \"remark\" : [ \"高级员工\" ],\r\n  \"menuIds\" : [ \"2000,2001,2002,2008,2009,2003,2010,2011,2012,2004,2013,2014,2015,2005,2016,2017,2018,2019,2006,2020,2021,2022,2023,2007,2024,2025,2026\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2019-12-30 02:02:00');
INSERT INTO `sys_oper_log` VALUES (217, '角色管理', 2, 'com.ruoyi.web.controller.system.SysRoleController.editSave()', 'POST', 1, 'admin', '徐州第1组', '/system/role/edit', '127.0.0.1', '内网IP', '{\r\n  \"roleId\" : [ \"2\" ],\r\n  \"roleName\" : [ \"普通员工\" ],\r\n  \"roleKey\" : [ \"common\" ],\r\n  \"roleSort\" : [ \"2\" ],\r\n  \"status\" : [ \"0\" ],\r\n  \"remark\" : [ \"高级员工\" ],\r\n  \"menuIds\" : [ \"2000,2001,2002,2008,2009,2003,2010,2011,2012,2004,2013,2014,2015,2005,2016,2017,2018,2019,2006,2020,2021,2022,2023,2007,2024,2025,2026\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2019-12-30 02:02:20');
INSERT INTO `sys_oper_log` VALUES (218, '角色管理', 2, 'com.ruoyi.web.controller.system.SysRoleController.editSave()', 'POST', 1, 'admin', '徐州第1组', '/system/role/edit', '127.0.0.1', '内网IP', '{\r\n  \"roleId\" : [ \"2\" ],\r\n  \"roleName\" : [ \"普通员工\" ],\r\n  \"roleKey\" : [ \"common\" ],\r\n  \"roleSort\" : [ \"3\" ],\r\n  \"status\" : [ \"0\" ],\r\n  \"remark\" : [ \"高级员工\" ],\r\n  \"menuIds\" : [ \"2000,2001,2002,2008,2009,2003,2010,2011,2012,2004,2013,2014,2015,2005,2016,2017,2018,2019,2006,2020,2021,2022,2023,2007,2024,2025,2026\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2019-12-30 02:02:31');
INSERT INTO `sys_oper_log` VALUES (219, '角色管理', 1, 'com.ruoyi.web.controller.system.SysRoleController.addSave()', 'POST', 1, 'admin', '徐州第1组', '/system/role/add', '127.0.0.1', '内网IP', '{\r\n  \"roleName\" : [ \"高级员工\" ],\r\n  \"roleKey\" : [ \"commo\" ],\r\n  \"roleSort\" : [ \"2\" ],\r\n  \"status\" : [ \"0\" ],\r\n  \"remark\" : [ \"高级员工\" ],\r\n  \"menuIds\" : [ \"2000,2001,2002,2008,2009,2003,2010,2011,2012,2004,2013,2014,2015,2005,2016,2017,2018,2019,2006,2020,2021,2022,2023,2007,2024,2025,2026\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2019-12-30 02:03:31');
INSERT INTO `sys_oper_log` VALUES (220, '角色管理', 2, 'com.ruoyi.web.controller.system.SysRoleController.editSave()', 'POST', 1, 'admin', '徐州第1组', '/system/role/edit', '127.0.0.1', '内网IP', '{\r\n  \"roleId\" : [ \"100\" ],\r\n  \"roleName\" : [ \"高级员工\" ],\r\n  \"roleKey\" : [ \"senior\" ],\r\n  \"roleSort\" : [ \"2\" ],\r\n  \"status\" : [ \"0\" ],\r\n  \"remark\" : [ \"高级员工\" ],\r\n  \"menuIds\" : [ \"2000,2001,2002,2008,2009,2003,2010,2011,2012,2004,2013,2014,2015,2005,2016,2017,2018,2019,2006,2020,2021,2022,2023,2007,2024,2025,2026\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2019-12-30 02:03:53');
INSERT INTO `sys_oper_log` VALUES (221, '角色管理', 2, 'com.ruoyi.web.controller.system.SysRoleController.editSave()', 'POST', 1, 'admin', '徐州第1组', '/system/role/edit', '127.0.0.1', '内网IP', '{\r\n  \"roleId\" : [ \"100\" ],\r\n  \"roleName\" : [ \"部门领导\" ],\r\n  \"roleKey\" : [ \"senior\" ],\r\n  \"roleSort\" : [ \"2\" ],\r\n  \"status\" : [ \"0\" ],\r\n  \"remark\" : [ \"部门领导\" ],\r\n  \"menuIds\" : [ \"2000,2001,2002,2008,2009,2003,2010,2011,2012,2004,2013,2014,2015,2005,2016,2017,2018,2019,2006,2020,2021,2022,2023,2007,2024,2025,2026\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2019-12-30 02:04:23');
INSERT INTO `sys_oper_log` VALUES (222, '角色管理', 2, 'com.ruoyi.web.controller.system.SysRoleController.authDataScopeSave()', 'POST', 1, 'admin', '徐州第1组', '/system/role/authDataScope', '127.0.0.1', '内网IP', '{\r\n  \"roleId\" : [ \"100\" ],\r\n  \"roleName\" : [ \"部门领导\" ],\r\n  \"roleKey\" : [ \"senior\" ],\r\n  \"dataScope\" : [ \"3\" ],\r\n  \"deptIds\" : [ \"\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2019-12-30 02:04:32');
INSERT INTO `sys_oper_log` VALUES (223, '角色管理', 2, 'com.ruoyi.web.controller.system.SysRoleController.authDataScopeSave()', 'POST', 1, 'admin', '徐州第1组', '/system/role/authDataScope', '127.0.0.1', '内网IP', '{\r\n  \"roleId\" : [ \"2\" ],\r\n  \"roleName\" : [ \"普通员工\" ],\r\n  \"roleKey\" : [ \"common\" ],\r\n  \"dataScope\" : [ \"5\" ],\r\n  \"deptIds\" : [ \"\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2019-12-30 02:04:43');
INSERT INTO `sys_oper_log` VALUES (224, '角色管理', 2, 'com.ruoyi.web.controller.system.SysRoleController.editSave()', 'POST', 1, 'admin', '徐州第1组', '/system/role/edit', '127.0.0.1', '内网IP', '{\r\n  \"roleId\" : [ \"2\" ],\r\n  \"roleName\" : [ \"普通员工\" ],\r\n  \"roleKey\" : [ \"common\" ],\r\n  \"roleSort\" : [ \"3\" ],\r\n  \"status\" : [ \"0\" ],\r\n  \"remark\" : [ \"普通员工只能查看本人数据\" ],\r\n  \"menuIds\" : [ \"2000,2001,2002,2008,2009,2003,2010,2011,2012,2004,2013,2014,2015,2005,2016,2017,2018,2019,2006,2020,2021,2022,2023,2007,2024,2025,2026\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2019-12-30 02:05:02');
INSERT INTO `sys_oper_log` VALUES (225, '角色管理', 2, 'com.ruoyi.web.controller.system.SysRoleController.editSave()', 'POST', 1, 'admin', '徐州第1组', '/system/role/edit', '127.0.0.1', '内网IP', '{\r\n  \"roleId\" : [ \"100\" ],\r\n  \"roleName\" : [ \"部门领导\" ],\r\n  \"roleKey\" : [ \"senior\" ],\r\n  \"roleSort\" : [ \"2\" ],\r\n  \"status\" : [ \"0\" ],\r\n  \"remark\" : [ \"部门领导可以查看本部门的数据\" ],\r\n  \"menuIds\" : [ \"2000,2001,2002,2008,2009,2003,2010,2011,2012,2004,2013,2014,2015,2005,2016,2017,2018,2019,2006,2020,2021,2022,2023,2007,2024,2025,2026\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2019-12-30 02:05:16');
INSERT INTO `sys_oper_log` VALUES (226, '角色管理', 2, 'com.ruoyi.web.controller.system.SysRoleController.editSave()', 'POST', 1, 'admin', '徐州第1组', '/system/role/edit', '127.0.0.1', '内网IP', '{\r\n  \"roleId\" : [ \"2\" ],\r\n  \"roleName\" : [ \"普通员工\" ],\r\n  \"roleKey\" : [ \"common\" ],\r\n  \"roleSort\" : [ \"4\" ],\r\n  \"status\" : [ \"0\" ],\r\n  \"remark\" : [ \"普通员工只能查看本人数据\" ],\r\n  \"menuIds\" : [ \"2000,2001,2002,2008,2009,2003,2010,2011,2012,2004,2013,2014,2015,2005,2016,2017,2018,2019,2006,2020,2021,2022,2023,2007,2024,2025,2026\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2019-12-30 02:05:30');
INSERT INTO `sys_oper_log` VALUES (227, '角色管理', 2, 'com.ruoyi.web.controller.system.SysRoleController.editSave()', 'POST', 1, 'admin', '徐州第1组', '/system/role/edit', '127.0.0.1', '内网IP', '{\r\n  \"roleId\" : [ \"100\" ],\r\n  \"roleName\" : [ \"部门领导\" ],\r\n  \"roleKey\" : [ \"senior\" ],\r\n  \"roleSort\" : [ \"3\" ],\r\n  \"status\" : [ \"0\" ],\r\n  \"remark\" : [ \"部门领导可以查看本部门的数据\" ],\r\n  \"menuIds\" : [ \"2000,2001,2002,2008,2009,2003,2010,2011,2012,2004,2013,2014,2015,2005,2016,2017,2018,2019,2006,2020,2021,2022,2023,2007,2024,2025,2026\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2019-12-30 02:05:33');
INSERT INTO `sys_oper_log` VALUES (228, '角色管理', 1, 'com.ruoyi.web.controller.system.SysRoleController.addSave()', 'POST', 1, 'admin', '徐州第1组', '/system/role/add', '127.0.0.1', '内网IP', '{\r\n  \"roleName\" : [ \"公司领导\" ],\r\n  \"roleKey\" : [ \"ceo\" ],\r\n  \"roleSort\" : [ \"2\" ],\r\n  \"status\" : [ \"0\" ],\r\n  \"remark\" : [ \"公司领导可以查看所有公司的数据\" ],\r\n  \"menuIds\" : [ \"2000,2001,2002,2008,2009,2003,2010,2011,2012,2004,2013,2014,2015,2005,2016,2017,2018,2019,2006,2020,2021,2022,2023,2007,2024,2025,2026\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2019-12-30 02:06:20');
INSERT INTO `sys_oper_log` VALUES (229, '角色管理', 2, 'com.ruoyi.web.controller.system.SysRoleController.editSave()', 'POST', 1, 'admin', '徐州第1组', '/system/role/edit', '127.0.0.1', '内网IP', '{\r\n  \"roleId\" : [ \"1\" ],\r\n  \"roleName\" : [ \"管理员\" ],\r\n  \"roleKey\" : [ \"admin\" ],\r\n  \"roleSort\" : [ \"1\" ],\r\n  \"status\" : [ \"0\" ],\r\n  \"remark\" : [ \"管理员可以对系统进行维护管理\" ],\r\n  \"menuIds\" : [ \"\" ]\r\n}', 'null', 1, '不允许操作超级管理员角色', '2019-12-30 02:06:33');
INSERT INTO `sys_oper_log` VALUES (230, '角色管理', 2, 'com.ruoyi.web.controller.system.SysRoleController.editSave()', 'POST', 1, 'admin', '徐州第1组', '/system/role/edit', '127.0.0.1', '内网IP', '{\r\n  \"roleId\" : [ \"1\" ],\r\n  \"roleName\" : [ \"管理员\" ],\r\n  \"roleKey\" : [ \"admin\" ],\r\n  \"roleSort\" : [ \"1\" ],\r\n  \"status\" : [ \"0\" ],\r\n  \"remark\" : [ \"管理员可以对系统进行维护管理\" ],\r\n  \"menuIds\" : [ \"\" ]\r\n}', 'null', 1, '不允许操作超级管理员角色', '2019-12-30 02:06:35');
INSERT INTO `sys_oper_log` VALUES (231, '角色管理', 2, 'com.ruoyi.web.controller.system.SysRoleController.editSave()', 'POST', 1, 'admin', '徐州第1组', '/system/role/edit', '127.0.0.1', '内网IP', '{\r\n  \"roleId\" : [ \"1\" ],\r\n  \"roleName\" : [ \"管理员\" ],\r\n  \"roleKey\" : [ \"admin\" ],\r\n  \"roleSort\" : [ \"1\" ],\r\n  \"status\" : [ \"0\" ],\r\n  \"remark\" : [ \"管理员\" ],\r\n  \"menuIds\" : [ \"2000,2001,2002,2008,2009,2003,2010,2011,2012,2004,2013,2014,2015,2005,2016,2017,2018,2019,2006,2020,2021,2022,2023,2007,2024,2025,2026,2,109,1047,1048,1049,110,1050,1051,1052,1053,1054,1055,1056,111,112,3,113,114,1057,1058,1059,1060,1061,115,1,100,1000,1001,1002,1003,1004,1005,1006,101,1007,1008,1009,1010,1011,102,1012,1013,1014,1015,103,1016,1017,1018,1019,104,1020,1021,1022,1023,1024,105,1025,1026,1027,1028,1029,106,1030,1031,1032,1033,1034,107,1035,1036,1037,1038,108,500,1039,1040,1041,1042,501,1043,1044,1045,1046\" ]\r\n}', 'null', 1, '不允许操作超级管理员角色', '2019-12-30 02:06:55');
INSERT INTO `sys_oper_log` VALUES (232, '角色管理', 2, 'com.ruoyi.web.controller.system.SysRoleController.authDataScopeSave()', 'POST', 1, 'admin', '徐州第1组', '/system/role/authDataScope', '127.0.0.1', '内网IP', '{\r\n  \"roleId\" : [ \"101\" ],\r\n  \"roleName\" : [ \"公司领导\" ],\r\n  \"roleKey\" : [ \"ceo\" ],\r\n  \"dataScope\" : [ \"1\" ],\r\n  \"deptIds\" : [ \"\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2019-12-30 02:07:24');
INSERT INTO `sys_oper_log` VALUES (233, '角色管理', 4, 'com.ruoyi.web.controller.system.SysRoleController.selectAuthUserAll()', 'POST', 1, 'admin', '徐州第1组', '/system/role/authUser/selectAll', '127.0.0.1', '内网IP', '{\r\n  \"roleId\" : [ \"1\" ],\r\n  \"userIds\" : [ \"1\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2019-12-30 02:07:42');
INSERT INTO `sys_oper_log` VALUES (234, '角色管理', 4, 'com.ruoyi.web.controller.system.SysRoleController.selectAuthUserAll()', 'POST', 1, 'admin', '徐州第1组', '/system/role/authUser/selectAll', '127.0.0.1', '内网IP', '{\r\n  \"roleId\" : [ \"101\" ],\r\n  \"userIds\" : [ \"2,5,3,4,6\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2019-12-30 02:08:30');
INSERT INTO `sys_oper_log` VALUES (235, '角色管理', 4, 'com.ruoyi.web.controller.system.SysRoleController.selectAuthUserAll()', 'POST', 1, 'admin', '徐州第1组', '/system/role/authUser/selectAll', '127.0.0.1', '内网IP', '{\r\n  \"roleId\" : [ \"100\" ],\r\n  \"userIds\" : [ \"7,10,8,11,9\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2019-12-30 02:09:14');
INSERT INTO `sys_oper_log` VALUES (236, '角色管理', 4, 'com.ruoyi.web.controller.system.SysRoleController.selectAuthUserAll()', 'POST', 1, 'admin', '徐州第1组', '/system/role/authUser/selectAll', '127.0.0.1', '内网IP', '{\r\n  \"roleId\" : [ \"2\" ],\r\n  \"userIds\" : [ \"65,113,116,112,115,66,114,16,12,64,15,63,14,62,13\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2019-12-30 02:09:54');
INSERT INTO `sys_oper_log` VALUES (237, '角色管理', 4, 'com.ruoyi.web.controller.system.SysRoleController.selectAuthUserAll()', 'POST', 1, 'admin', '徐州第1组', '/system/role/authUser/selectAll', '127.0.0.1', '内网IP', '{\r\n  \"roleId\" : [ \"2\" ],\r\n  \"userIds\" : [ \"118,69,121,117,68,120,71,67,119,70,21,17,20,19,18\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2019-12-30 02:10:20');
INSERT INTO `sys_oper_log` VALUES (238, '角色管理', 4, 'com.ruoyi.web.controller.system.SysRoleController.selectAuthUserAll()', 'POST', 1, 'admin', '徐州第1组', '/system/role/authUser/selectAll', '127.0.0.1', '内网IP', '{\r\n  \"roleId\" : [ \"2\" ],\r\n  \"userIds\" : [ \"74,73,76,72,75,26,22,25,24,23\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2019-12-30 02:10:32');
INSERT INTO `sys_oper_log` VALUES (239, '角色管理', 4, 'com.ruoyi.web.controller.system.SysRoleController.selectAuthUserAll()', 'POST', 1, 'admin', '徐州第1组', '/system/role/authUser/selectAll', '127.0.0.1', '内网IP', '{\r\n  \"roleId\" : [ \"2\" ],\r\n  \"userIds\" : [ \"79,78,81,77,80,31,27,30,29,28\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2019-12-30 02:10:43');
INSERT INTO `sys_oper_log` VALUES (240, '角色管理', 4, 'com.ruoyi.web.controller.system.SysRoleController.selectAuthUserAll()', 'POST', 1, 'admin', '徐州第1组', '/system/role/authUser/selectAll', '127.0.0.1', '内网IP', '{\r\n  \"roleId\" : [ \"2\" ],\r\n  \"userIds\" : [ \"85,84,83,86,82,36,32,35,34,33\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2019-12-30 02:10:54');
INSERT INTO `sys_oper_log` VALUES (241, '角色管理', 4, 'com.ruoyi.web.controller.system.SysRoleController.selectAuthUserAll()', 'POST', 1, 'admin', '徐州第1组', '/system/role/authUser/selectAll', '127.0.0.1', '内网IP', '{\r\n  \"roleId\" : [ \"2\" ],\r\n  \"userIds\" : [ \"90,89,88,91,87,41,37,40,39,38\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2019-12-30 02:11:03');
INSERT INTO `sys_oper_log` VALUES (242, '角色管理', 4, 'com.ruoyi.web.controller.system.SysRoleController.selectAuthUserAll()', 'POST', 1, 'admin', '徐州第1组', '/system/role/authUser/selectAll', '127.0.0.1', '内网IP', '{\r\n  \"roleId\" : [ \"2\" ],\r\n  \"userIds\" : [ \"95,94,93,96,92,46,42,45,44,43\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2019-12-30 02:11:10');
INSERT INTO `sys_oper_log` VALUES (243, '角色管理', 4, 'com.ruoyi.web.controller.system.SysRoleController.selectAuthUserAll()', 'POST', 1, 'admin', '徐州第1组', '/system/role/authUser/selectAll', '127.0.0.1', '内网IP', '{\r\n  \"roleId\" : [ \"2\" ],\r\n  \"userIds\" : [ \"51,47,50,49,48\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2019-12-30 02:11:19');
INSERT INTO `sys_oper_log` VALUES (244, '角色管理', 2, 'com.ruoyi.web.controller.system.SysRoleController.editSave()', 'POST', 1, 'admin', '徐州第1组', '/system/role/edit', '127.0.0.1', '内网IP', '{\r\n  \"roleId\" : [ \"101\" ],\r\n  \"roleName\" : [ \"总公司领导\" ],\r\n  \"roleKey\" : [ \"ceo\" ],\r\n  \"roleSort\" : [ \"2\" ],\r\n  \"status\" : [ \"0\" ],\r\n  \"remark\" : [ \"总公司领导可以查看所有公司的数据\" ],\r\n  \"menuIds\" : [ \"2000,2001,2002,2008,2009,2003,2010,2011,2012,2004,2013,2014,2015,2005,2016,2017,2018,2019,2006,2020,2021,2022,2023,2007,2024,2025,2026\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2019-12-30 02:12:12');
INSERT INTO `sys_oper_log` VALUES (245, '角色管理', 2, 'com.ruoyi.web.controller.system.SysRoleController.editSave()', 'POST', 1, 'admin', '徐州第1组', '/system/role/edit', '127.0.0.1', '内网IP', '{\r\n  \"roleId\" : [ \"100\" ],\r\n  \"roleName\" : [ \"分公司领导\" ],\r\n  \"roleKey\" : [ \"senior\" ],\r\n  \"roleSort\" : [ \"3\" ],\r\n  \"status\" : [ \"0\" ],\r\n  \"remark\" : [ \"分公司领导可以查看本部门的数据\" ],\r\n  \"menuIds\" : [ \"2000,2001,2002,2008,2009,2003,2010,2011,2012,2004,2013,2014,2015,2005,2016,2017,2018,2019,2006,2020,2021,2022,2023,2007,2024,2025,2026\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2019-12-30 02:12:21');
INSERT INTO `sys_oper_log` VALUES (246, '角色管理', 2, 'com.ruoyi.web.controller.system.SysRoleController.editSave()', 'POST', 1, 'admin', '徐州第1组', '/system/role/edit', '127.0.0.1', '内网IP', '{\r\n  \"roleId\" : [ \"101\" ],\r\n  \"roleName\" : [ \"总公司领导\" ],\r\n  \"roleKey\" : [ \"ceo\" ],\r\n  \"roleSort\" : [ \"2\" ],\r\n  \"status\" : [ \"0\" ],\r\n  \"remark\" : [ \"总公司领导可以查看所有公司的数据\" ],\r\n  \"menuIds\" : [ \"2000,2001,2002,2008,2009,2003,2010,2011,2012,2004,2013,2014,2015,2005,2016,2017,2018,2019,2006,2020,2021,2022,2023,2007,2024,2025,2026,1,100,1000,1001,1002,1003,1004,1005,1006,101,1007,1008,1009,1010,1011,103,1016,1017,1018,1019,104,1020,1021,1022,1023,1024,107,1035,1036,1037,1038\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2019-12-30 02:28:35');
INSERT INTO `sys_oper_log` VALUES (247, '角色管理', 2, 'com.ruoyi.web.controller.system.SysRoleController.editSave()', 'POST', 1, 'admin', '徐州第1组', '/system/role/edit', '127.0.0.1', '内网IP', '{\r\n  \"roleId\" : [ \"100\" ],\r\n  \"roleName\" : [ \"分公司领导\" ],\r\n  \"roleKey\" : [ \"senior\" ],\r\n  \"roleSort\" : [ \"3\" ],\r\n  \"status\" : [ \"0\" ],\r\n  \"remark\" : [ \"分公司领导可以查看本部门的数据\" ],\r\n  \"menuIds\" : [ \"2000,2001,2002,2008,2009,2003,2010,2011,2012,2004,2013,2014,2015,2005,2016,2017,2018,2019,2006,2020,2021,2022,2023,2007,2024,2025,2026,1,100,1000,1001,1002,1003,1004,1005,1006,104,1020,1021,1022,1023,1024\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2019-12-30 02:29:12');
INSERT INTO `sys_oper_log` VALUES (248, '角色管理', 2, 'com.ruoyi.web.controller.system.SysRoleController.editSave()', 'POST', 1, 'admin', '徐州第1组', '/system/role/edit', '127.0.0.1', '内网IP', '{\r\n  \"roleId\" : [ \"100\" ],\r\n  \"roleName\" : [ \"分公司领导\" ],\r\n  \"roleKey\" : [ \"senior\" ],\r\n  \"roleSort\" : [ \"3\" ],\r\n  \"status\" : [ \"0\" ],\r\n  \"remark\" : [ \"分公司领导可以查看本部门的数据\" ],\r\n  \"menuIds\" : [ \"2000,2001,2002,2008,2009,2003,2010,2011,2012,2004,2013,2014,2015,2005,2016,2017,2018,2019,2006,2020,2021,2022,2023,2007,2024,2025,2026,1,100,1000,1001,1002,1003,1004,1005,1006,104,1020,1021,1022,1023,1024,107,1035,1036,1037,1038\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2019-12-30 02:29:39');
INSERT INTO `sys_oper_log` VALUES (249, '角色管理', 2, 'com.ruoyi.web.controller.system.SysRoleController.editSave()', 'POST', 1, 'admin', '徐州第1组', '/system/role/edit', '127.0.0.1', '内网IP', '{\r\n  \"roleId\" : [ \"2\" ],\r\n  \"roleName\" : [ \"普通员工\" ],\r\n  \"roleKey\" : [ \"common\" ],\r\n  \"roleSort\" : [ \"4\" ],\r\n  \"status\" : [ \"0\" ],\r\n  \"remark\" : [ \"普通员工只能查看本人数据\" ],\r\n  \"menuIds\" : [ \"2000,2001,2002,2008,2009,2003,2010,2011,2012,2004,2013,2014,2015,2005,2016,2017,2018,2019,2006,2020,2021,2022,2023,2007,2024,2025,2026,1,107,1035\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2019-12-30 02:30:00');
INSERT INTO `sys_oper_log` VALUES (250, '角色管理', 2, 'com.ruoyi.web.controller.system.SysRoleController.editSave()', 'POST', 1, 'admin', '徐州第1组', '/system/role/edit', '127.0.0.1', '内网IP', '{\r\n  \"roleId\" : [ \"101\" ],\r\n  \"roleName\" : [ \"总公司领导\" ],\r\n  \"roleKey\" : [ \"ceo\" ],\r\n  \"roleSort\" : [ \"2\" ],\r\n  \"status\" : [ \"0\" ],\r\n  \"remark\" : [ \"总公司领导可以查看所有公司的数据\" ],\r\n  \"menuIds\" : [ \"2000,2001,2002,2008,2009,2003,2010,2011,2012,2004,2013,2014,2015,2005,2016,2017,2018,2019,2006,2020,2021,2022,2023,2007,2024,2025,2026,1,100,1000,1001,1002,1003,1004,1005,1006,107,1035,1036,1037,1038\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2019-12-30 02:31:40');
INSERT INTO `sys_oper_log` VALUES (251, '角色管理', 2, 'com.ruoyi.web.controller.system.SysRoleController.editSave()', 'POST', 1, 'admin', '徐州第1组', '/system/role/edit', '127.0.0.1', '内网IP', '{\r\n  \"roleId\" : [ \"100\" ],\r\n  \"roleName\" : [ \"分公司领导\" ],\r\n  \"roleKey\" : [ \"senior\" ],\r\n  \"roleSort\" : [ \"3\" ],\r\n  \"status\" : [ \"0\" ],\r\n  \"remark\" : [ \"分公司领导可以查看本部门的数据\" ],\r\n  \"menuIds\" : [ \"2000,2001,2002,2008,2009,2003,2010,2011,2012,2004,2013,2014,2015,2005,2016,2017,2018,2019,2006,2020,2021,2022,2023,2007,2024,2025,2026,1,100,1000,1001,1002,1003,1004,1005,1006,107,1035,1036,1037,1038\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2019-12-30 02:31:50');
INSERT INTO `sys_oper_log` VALUES (252, '角色管理', 2, 'com.ruoyi.web.controller.system.SysRoleController.editSave()', 'POST', 1, 'admin', '徐州第1组', '/system/role/edit', '127.0.0.1', '内网IP', '{\r\n  \"roleId\" : [ \"101\" ],\r\n  \"roleName\" : [ \"总公司领导\" ],\r\n  \"roleKey\" : [ \"ceo\" ],\r\n  \"roleSort\" : [ \"2\" ],\r\n  \"status\" : [ \"0\" ],\r\n  \"remark\" : [ \"总公司领导可以查看所有公司的数据\" ],\r\n  \"menuIds\" : [ \"2000,2001,2002,2008,2009,2003,2010,2011,2012,2004,2013,2014,2015,2005,2016,2017,2018,2019,2006,2020,2021,2022,2023,2007,2024,2025,2026,1,100,1000,1001,1002,1003,1004,1005,1006,107,1035,1036,1037,1038\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2019-12-30 02:31:55');
INSERT INTO `sys_oper_log` VALUES (253, '角色管理', 2, 'com.ruoyi.web.controller.system.SysRoleController.editSave()', 'POST', 1, 'admin', '徐州第1组', '/system/role/edit', '127.0.0.1', '内网IP', '{\r\n  \"roleId\" : [ \"2\" ],\r\n  \"roleName\" : [ \"普通员工\" ],\r\n  \"roleKey\" : [ \"common\" ],\r\n  \"roleSort\" : [ \"4\" ],\r\n  \"status\" : [ \"0\" ],\r\n  \"remark\" : [ \"普通员工只能查看本人数据\" ],\r\n  \"menuIds\" : [ \"2000,2001,2002,2008,2009,2003,2010,2011,2012,2004,2013,2014,2015,2005,2016,2017,2018,2019,2006,2020,2021,2022,2023,2007,2024,2025,2026,1,107,1035\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2019-12-30 02:32:01');
INSERT INTO `sys_oper_log` VALUES (254, '通知公告', 2, 'com.ruoyi.web.controller.system.SysNoticeController.editSave()', 'POST', 1, 'chenhe1', '徐州第1组', '/system/notice/edit', '127.0.0.1', '内网IP', '{\r\n  \"noticeId\" : [ \"10\" ],\r\n  \"noticeTitle\" : [ \"【紧急】【全体】安全施工管理测试\" ],\r\n  \"noticeType\" : [ \"1\" ],\r\n  \"noticeContent\" : [ \"<p>【紧急】安全施工管理测试【紧急】安全施工管理测试【紧急】安全施工管理测试【紧急】安全施工管理测试【紧急】安全施工管理测试【紧急】安全施工管理测试【紧急】安全施工管理测试【紧急】安全施工管理测试【紧急】安全施工管理测试【紧急】安全施工管理测试【紧急】安全施工管理测试【紧急】安全施工管理测试<br></p>\" ],\r\n  \"status\" : [ \"0\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2019-12-30 02:32:46');
INSERT INTO `sys_oper_log` VALUES (255, '通知公告', 2, 'com.ruoyi.web.controller.system.SysNoticeController.editSave()', 'POST', 1, 'chenhe1', '徐州第1组', '/system/notice/edit', '127.0.0.1', '内网IP', '{\r\n  \"noticeId\" : [ \"11\" ],\r\n  \"noticeTitle\" : [ \"【徐州总公司】紧急通知，迎接安全检查\" ],\r\n  \"noticeType\" : [ \"2\" ],\r\n  \"noticeContent\" : [ \"<p>【陈贺】紧急通知，迎接安全检查【陈贺】紧急通知，迎接安全检查【陈贺】紧急通知，迎接安全检查【陈贺】紧急通知，迎接安全检查【陈贺】紧急通知，迎接安全检查【陈贺】紧急通知，迎接安全检查【陈贺】紧急通知，迎接安全检查【陈贺】紧急通知，迎接安全检查【陈贺】紧急通知，迎接安全检查<br></p>\" ],\r\n  \"status\" : [ \"0\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2019-12-30 02:33:02');
INSERT INTO `sys_oper_log` VALUES (256, '菜单管理', 3, 'com.ruoyi.project.system.menu.controller.MenuController.remove()', 'GET', 1, 'admin', '徐州第1组', '/system/menu/remove/2020', '127.0.0.1', '内网IP', '{}', '{\"msg\":\"菜单已分配,不允许删除\",\"code\":301}', 0, NULL, '2020-01-03 08:55:46');
INSERT INTO `sys_oper_log` VALUES (257, '菜单管理', 2, 'com.ruoyi.project.system.menu.controller.MenuController.editSave()', 'POST', 1, 'admin', '徐州第1组', '/system/menu/edit', '127.0.0.1', '内网IP', '{\"menuId\":[\"2020\"],\"parentId\":[\"2006\"],\"menuType\":[\"F\"],\"menuName\":[\"事故资料收集\"],\"url\":[\"/error\"],\"target\":[\"menuItem\"],\"perms\":[\"\"],\"orderNum\":[\"1\"],\"icon\":[\"fa fa-bars\"],\"visible\":[\"0\"]}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2020-01-03 08:55:58');
INSERT INTO `sys_oper_log` VALUES (258, '菜单管理', 3, 'com.ruoyi.project.system.menu.controller.MenuController.remove()', 'GET', 1, 'admin', '徐州第1组', '/system/menu/remove/2020', '127.0.0.1', '内网IP', '{}', '{\"msg\":\"菜单已分配,不允许删除\",\"code\":301}', 0, NULL, '2020-01-03 08:56:04');
INSERT INTO `sys_oper_log` VALUES (259, '菜单管理', 2, 'com.ruoyi.project.system.menu.controller.MenuController.editSave()', 'POST', 1, 'admin', '徐州第1组', '/system/menu/edit', '127.0.0.1', '内网IP', '{\"menuId\":[\"2006\"],\"parentId\":[\"0\"],\"menuType\":[\"F\"],\"menuName\":[\"施工事故管理\"],\"url\":[\"#\"],\"target\":[\"menuItem\"],\"perms\":[\"\"],\"orderNum\":[\"5\"],\"icon\":[\"fa fa-binoculars\"],\"visible\":[\"0\"]}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2020-01-03 08:56:23');
INSERT INTO `sys_oper_log` VALUES (260, '菜单管理', 3, 'com.ruoyi.project.system.menu.controller.MenuController.remove()', 'GET', 1, 'admin', '徐州第1组', '/system/menu/remove/2006', '127.0.0.1', '内网IP', '{}', '{\"msg\":\"存在子菜单,不允许删除\",\"code\":301}', 0, NULL, '2020-01-03 08:56:29');
INSERT INTO `sys_oper_log` VALUES (261, '菜单管理', 3, 'com.ruoyi.project.system.menu.controller.MenuController.remove()', 'GET', 1, 'admin', '徐州第1组', '/system/menu/remove/2020', '127.0.0.1', '内网IP', '{}', '{\"msg\":\"菜单已分配,不允许删除\",\"code\":301}', 0, NULL, '2020-01-03 08:56:34');
INSERT INTO `sys_oper_log` VALUES (262, '菜单管理', 2, 'com.ruoyi.project.system.menu.controller.MenuController.editSave()', 'POST', 1, 'admin', '徐州第1组', '/system/menu/edit', '127.0.0.1', '内网IP', '{\"menuId\":[\"2020\"],\"parentId\":[\"2006\"],\"menuType\":[\"C\"],\"menuName\":[\"事故资料收集\"],\"url\":[\"\"],\"target\":[\"menuItem\"],\"perms\":[\"\"],\"orderNum\":[\"1\"],\"icon\":[\"fa fa-bars\"],\"visible\":[\"0\"]}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2020-01-03 08:57:11');
INSERT INTO `sys_oper_log` VALUES (263, '菜单管理', 2, 'com.ruoyi.project.system.menu.controller.MenuController.editSave()', 'POST', 1, 'admin', '徐州第1组', '/system/menu/edit', '127.0.0.1', '内网IP', '{\"menuId\":[\"2020\"],\"parentId\":[\"2006\"],\"menuType\":[\"C\"],\"menuName\":[\"事故资料收集\"],\"url\":[\"\"],\"target\":[\"menuItem\"],\"perms\":[\"\"],\"orderNum\":[\"1\"],\"icon\":[\"fa fa-bars\"],\"visible\":[\"0\"]}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2020-01-03 08:57:22');
INSERT INTO `sys_oper_log` VALUES (264, '菜单管理', 3, 'com.ruoyi.project.system.menu.controller.MenuController.remove()', 'GET', 1, 'admin', '徐州第1组', '/system/menu/remove/2020', '127.0.0.1', '内网IP', '{}', '{\"msg\":\"菜单已分配,不允许删除\",\"code\":301}', 0, NULL, '2020-01-03 08:57:29');
INSERT INTO `sys_oper_log` VALUES (265, '菜单管理', 2, 'com.ruoyi.project.system.menu.controller.MenuController.editSave()', 'POST', 1, 'admin', '徐州第1组', '/system/menu/edit', '127.0.0.1', '内网IP', '{\"menuId\":[\"2006\"],\"parentId\":[\"0\"],\"menuType\":[\"C\"],\"menuName\":[\"施工事故管理\"],\"url\":[\"#\"],\"target\":[\"menuItem\"],\"perms\":[\"\"],\"orderNum\":[\"5\"],\"icon\":[\"fa fa-binoculars\"],\"visible\":[\"0\"]}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2020-01-03 09:00:35');
INSERT INTO `sys_oper_log` VALUES (266, '代码生成', 6, 'com.ruoyi.project.tool.gen.controller.GenController.importTableSave()', 'POST', 1, 'admin', '徐州第1组', '/tool/gen/importTable', '127.0.0.1', '内网IP', '{\"tables\":[\"accidentinformation\"]}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2020-01-03 09:01:22');
INSERT INTO `sys_oper_log` VALUES (267, '代码生成', 2, 'com.ruoyi.project.tool.gen.controller.GenController.editSave()', 'POST', 1, 'admin', '徐州第1组', '/tool/gen/edit', '127.0.0.1', '内网IP', '{\"tableId\":[\"1\"],\"tableName\":[\"accidentinformation\"],\"tableComment\":[\"事故信息表 \"],\"className\":[\"Accidentinformation\"],\"functionAuthor\":[\"chenhe\"],\"remark\":[\"\"],\"columns[0].columnId\":[\"1\"],\"columns[0].sort\":[\"1\"],\"columns[0].columnComment\":[\"事故编号(主键)\"],\"columns[0].javaType\":[\"Integer\"],\"columns[0].javaField\":[\"accidentId\"],\"columns[0].isInsert\":[\"1\"],\"columns[0].queryType\":[\"EQ\"],\"columns[0].htmlType\":[\"input\"],\"columns[0].dictType\":[\"\"],\"columns[1].columnId\":[\"2\"],\"columns[1].sort\":[\"2\"],\"columns[1].columnComment\":[\"发生时间\"],\"columns[1].javaType\":[\"Date\"],\"columns[1].javaField\":[\"happenTime\"],\"columns[1].isInsert\":[\"1\"],\"columns[1].isEdit\":[\"1\"],\"columns[1].isList\":[\"1\"],\"columns[1].isQuery\":[\"1\"],\"columns[1].queryType\":[\"EQ\"],\"columns[1].htmlType\":[\"datetime\"],\"columns[1].dictType\":[\"\"],\"columns[2].columnId\":[\"3\"],\"columns[2].sort\":[\"3\"],\"columns[2].columnComment\":[\"发生地点\"],\"columns[2].javaType\":[\"String\"],\"columns[2].javaField\":[\"happenAddress\"],\"columns[2].isInsert\":[\"1\"],\"columns[2].isEdit\":[\"1\"],\"columns[2].isList\":[\"1\"],\"columns[2].isQuery\":[\"1\"],\"columns[2].queryType\":[\"EQ\"],\"columns[2].htmlType\":[\"input\"],\"columns[2].dictType\":[\"\"],\"columns[3].columnId\":[\"4\"],\"columns[3].sort\":[\"4\"],\"columns[3].columnComment\":[\"责任人\"],\"columns[3].javaType\":[\"String\"],\"columns[3].javaField\":[\"blamePerson\"],\"columns[3].isInsert\":[\"1\"],\"columns[3].isEdit\":[\"1\"],\"columns[3].isList\":[\"1\"],\"columns[3].isQuery\":[\"1\"],\"columns[3].queryType\":[\"EQ\"],\"columns[3].htmlType\":[\"input\"],\"columns[3].dictType\":[\"\"],\"columns[4].columnId\":[\"5\"],\"columns[4].sort\":[\"5\"],\"columns[4].columnComment\":[\"事故类型\"],\"columns[4].javaType\":[\"String\"],\"columns[4].javaField\":[\"accidentType\"],\"columns[4].isInsert\":[\"1\"],\"columns[4].isEdit\":[\"1\"],\"columns[4].isList\":[\"1\"],\"columns[4].isQuery\":[\"1\"],\"columns[4].queryType\":[\"EQ\"],\"columns[4].htmlType\":[\"input\"],\"columns[4].dictType\":[\"\"],\"columns[5].columnId\":[\"6\"],\"columns[5].sort\":[\"6\"],\"columns[5].columnComment\":[\"损失程度\"],\"columns[5].javaType\":[\"String\"],\"columns[5].jav', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2020-01-03 09:02:15');
INSERT INTO `sys_oper_log` VALUES (268, '代码生成', 2, 'com.ruoyi.project.tool.gen.controller.GenController.editSave()', 'POST', 1, 'admin', '徐州第1组', '/tool/gen/edit', '127.0.0.1', '内网IP', '{\"tableId\":[\"1\"],\"tableName\":[\"accident\"],\"tableComment\":[\"施工事故管理\"],\"className\":[\"Accident\"],\"functionAuthor\":[\"陈贺\"],\"remark\":[\"作者：陈贺\"],\"columns[0].columnId\":[\"1\"],\"columns[0].sort\":[\"1\"],\"columns[0].columnComment\":[\"事故编号(主键)\"],\"columns[0].javaType\":[\"Integer\"],\"columns[0].javaField\":[\"accidentId\"],\"columns[0].isInsert\":[\"1\"],\"columns[0].queryType\":[\"EQ\"],\"columns[0].htmlType\":[\"input\"],\"columns[0].dictType\":[\"\"],\"columns[1].columnId\":[\"2\"],\"columns[1].sort\":[\"2\"],\"columns[1].columnComment\":[\"发生时间\"],\"columns[1].javaType\":[\"Date\"],\"columns[1].javaField\":[\"happenTime\"],\"columns[1].isInsert\":[\"1\"],\"columns[1].isEdit\":[\"1\"],\"columns[1].isList\":[\"1\"],\"columns[1].isQuery\":[\"1\"],\"columns[1].queryType\":[\"EQ\"],\"columns[1].htmlType\":[\"datetime\"],\"columns[1].dictType\":[\"\"],\"columns[2].columnId\":[\"3\"],\"columns[2].sort\":[\"3\"],\"columns[2].columnComment\":[\"发生地点\"],\"columns[2].javaType\":[\"String\"],\"columns[2].javaField\":[\"happenAddress\"],\"columns[2].isInsert\":[\"1\"],\"columns[2].isEdit\":[\"1\"],\"columns[2].isList\":[\"1\"],\"columns[2].isQuery\":[\"1\"],\"columns[2].queryType\":[\"EQ\"],\"columns[2].htmlType\":[\"input\"],\"columns[2].dictType\":[\"\"],\"columns[3].columnId\":[\"4\"],\"columns[3].sort\":[\"4\"],\"columns[3].columnComment\":[\"责任人\"],\"columns[3].javaType\":[\"String\"],\"columns[3].javaField\":[\"blamePerson\"],\"columns[3].isInsert\":[\"1\"],\"columns[3].isEdit\":[\"1\"],\"columns[3].isList\":[\"1\"],\"columns[3].isQuery\":[\"1\"],\"columns[3].queryType\":[\"EQ\"],\"columns[3].htmlType\":[\"input\"],\"columns[3].dictType\":[\"\"],\"columns[4].columnId\":[\"5\"],\"columns[4].sort\":[\"5\"],\"columns[4].columnComment\":[\"事故类型\"],\"columns[4].javaType\":[\"String\"],\"columns[4].javaField\":[\"accidentType\"],\"columns[4].isInsert\":[\"1\"],\"columns[4].isEdit\":[\"1\"],\"columns[4].isList\":[\"1\"],\"columns[4].isQuery\":[\"1\"],\"columns[4].queryType\":[\"EQ\"],\"columns[4].htmlType\":[\"input\"],\"columns[4].dictType\":[\"\"],\"columns[5].columnId\":[\"6\"],\"columns[5].sort\":[\"6\"],\"columns[5].columnComment\":[\"损失程度\"],\"columns[5].javaType\":[\"String\"],\"columns[5].javaField\":[\"loseLevel\"]', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2020-01-03 09:03:33');
INSERT INTO `sys_oper_log` VALUES (269, '代码生成', 2, 'com.ruoyi.project.tool.gen.controller.GenController.editSave()', 'POST', 1, 'admin', '徐州第1组', '/tool/gen/edit', '127.0.0.1', '内网IP', '{\"tableId\":[\"1\"],\"tableName\":[\"accident\"],\"tableComment\":[\"施工事故管理\"],\"className\":[\"Accident\"],\"functionAuthor\":[\"陈贺\"],\"remark\":[\"\"],\"columns[0].columnId\":[\"1\"],\"columns[0].sort\":[\"1\"],\"columns[0].columnComment\":[\"事故编号(主键)\"],\"columns[0].javaType\":[\"Integer\"],\"columns[0].javaField\":[\"accidentId\"],\"columns[0].isInsert\":[\"1\"],\"columns[0].queryType\":[\"EQ\"],\"columns[0].htmlType\":[\"input\"],\"columns[0].dictType\":[\"\"],\"columns[1].columnId\":[\"2\"],\"columns[1].sort\":[\"2\"],\"columns[1].columnComment\":[\"发生时间\"],\"columns[1].javaType\":[\"Date\"],\"columns[1].javaField\":[\"happenTime\"],\"columns[1].isInsert\":[\"1\"],\"columns[1].isEdit\":[\"1\"],\"columns[1].isList\":[\"1\"],\"columns[1].isQuery\":[\"1\"],\"columns[1].queryType\":[\"EQ\"],\"columns[1].htmlType\":[\"datetime\"],\"columns[1].dictType\":[\"\"],\"columns[2].columnId\":[\"3\"],\"columns[2].sort\":[\"3\"],\"columns[2].columnComment\":[\"发生地点\"],\"columns[2].javaType\":[\"String\"],\"columns[2].javaField\":[\"happenAddress\"],\"columns[2].isInsert\":[\"1\"],\"columns[2].isEdit\":[\"1\"],\"columns[2].isList\":[\"1\"],\"columns[2].isQuery\":[\"1\"],\"columns[2].queryType\":[\"EQ\"],\"columns[2].htmlType\":[\"input\"],\"columns[2].dictType\":[\"\"],\"columns[3].columnId\":[\"4\"],\"columns[3].sort\":[\"4\"],\"columns[3].columnComment\":[\"责任人\"],\"columns[3].javaType\":[\"String\"],\"columns[3].javaField\":[\"blamePerson\"],\"columns[3].isInsert\":[\"1\"],\"columns[3].isEdit\":[\"1\"],\"columns[3].isList\":[\"1\"],\"columns[3].isQuery\":[\"1\"],\"columns[3].queryType\":[\"EQ\"],\"columns[3].htmlType\":[\"input\"],\"columns[3].dictType\":[\"\"],\"columns[4].columnId\":[\"5\"],\"columns[4].sort\":[\"5\"],\"columns[4].columnComment\":[\"事故类型\"],\"columns[4].javaType\":[\"String\"],\"columns[4].javaField\":[\"accidentType\"],\"columns[4].isInsert\":[\"1\"],\"columns[4].isEdit\":[\"1\"],\"columns[4].isList\":[\"1\"],\"columns[4].isQuery\":[\"1\"],\"columns[4].queryType\":[\"EQ\"],\"columns[4].htmlType\":[\"input\"],\"columns[4].dictType\":[\"\"],\"columns[5].columnId\":[\"6\"],\"columns[5].sort\":[\"6\"],\"columns[5].columnComment\":[\"损失程度\"],\"columns[5].javaType\":[\"String\"],\"columns[5].javaField\":[\"loseLevel\"],\"col', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2020-01-03 09:04:05');
INSERT INTO `sys_oper_log` VALUES (270, '代码生成', 2, 'com.ruoyi.project.tool.gen.controller.GenController.editSave()', 'POST', 1, 'admin', '徐州第1组', '/tool/gen/edit', '127.0.0.1', '内网IP', '{\"tableId\":[\"1\"],\"tableName\":[\"accident\"],\"tableComment\":[\"施工事故管理\"],\"className\":[\"Accident\"],\"functionAuthor\":[\"陈贺\"],\"remark\":[\"作者：陈贺\"],\"columns[0].columnId\":[\"1\"],\"columns[0].sort\":[\"1\"],\"columns[0].columnComment\":[\"事故编号(主键)\"],\"columns[0].javaType\":[\"Integer\"],\"columns[0].javaField\":[\"accidentId\"],\"columns[0].isInsert\":[\"1\"],\"columns[0].queryType\":[\"EQ\"],\"columns[0].htmlType\":[\"input\"],\"columns[0].dictType\":[\"\"],\"columns[1].columnId\":[\"2\"],\"columns[1].sort\":[\"2\"],\"columns[1].columnComment\":[\"发生时间\"],\"columns[1].javaType\":[\"Date\"],\"columns[1].javaField\":[\"happenTime\"],\"columns[1].isInsert\":[\"1\"],\"columns[1].isEdit\":[\"1\"],\"columns[1].isList\":[\"1\"],\"columns[1].isQuery\":[\"1\"],\"columns[1].queryType\":[\"EQ\"],\"columns[1].htmlType\":[\"datetime\"],\"columns[1].dictType\":[\"\"],\"columns[2].columnId\":[\"3\"],\"columns[2].sort\":[\"3\"],\"columns[2].columnComment\":[\"发生地点\"],\"columns[2].javaType\":[\"String\"],\"columns[2].javaField\":[\"happenAddress\"],\"columns[2].isInsert\":[\"1\"],\"columns[2].isEdit\":[\"1\"],\"columns[2].isList\":[\"1\"],\"columns[2].isQuery\":[\"1\"],\"columns[2].queryType\":[\"EQ\"],\"columns[2].htmlType\":[\"input\"],\"columns[2].dictType\":[\"\"],\"columns[3].columnId\":[\"4\"],\"columns[3].sort\":[\"4\"],\"columns[3].columnComment\":[\"责任人\"],\"columns[3].javaType\":[\"String\"],\"columns[3].javaField\":[\"blamePerson\"],\"columns[3].isInsert\":[\"1\"],\"columns[3].isEdit\":[\"1\"],\"columns[3].isList\":[\"1\"],\"columns[3].isQuery\":[\"1\"],\"columns[3].queryType\":[\"EQ\"],\"columns[3].htmlType\":[\"input\"],\"columns[3].dictType\":[\"\"],\"columns[4].columnId\":[\"5\"],\"columns[4].sort\":[\"5\"],\"columns[4].columnComment\":[\"事故类型\"],\"columns[4].javaType\":[\"String\"],\"columns[4].javaField\":[\"accidentType\"],\"columns[4].isInsert\":[\"1\"],\"columns[4].isEdit\":[\"1\"],\"columns[4].isList\":[\"1\"],\"columns[4].isQuery\":[\"1\"],\"columns[4].queryType\":[\"EQ\"],\"columns[4].htmlType\":[\"input\"],\"columns[4].dictType\":[\"\"],\"columns[5].columnId\":[\"6\"],\"columns[5].sort\":[\"6\"],\"columns[5].columnComment\":[\"损失程度\"],\"columns[5].javaType\":[\"String\"],\"columns[5].javaField\":[\"loseLevel\"]', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2020-01-03 09:04:27');
INSERT INTO `sys_oper_log` VALUES (271, '代码生成', 2, 'com.ruoyi.project.tool.gen.controller.GenController.editSave()', 'POST', 1, 'admin', '徐州第1组', '/tool/gen/edit', '127.0.0.1', '内网IP', '{\"tableId\":[\"1\"],\"tableName\":[\"accident\"],\"tableComment\":[\"施工事故管理\"],\"className\":[\"Accident\"],\"functionAuthor\":[\"陈贺\"],\"remark\":[\"\"],\"columns[0].columnId\":[\"1\"],\"columns[0].sort\":[\"1\"],\"columns[0].columnComment\":[\"事故编号(主键)\"],\"columns[0].javaType\":[\"Integer\"],\"columns[0].javaField\":[\"accidentId\"],\"columns[0].isInsert\":[\"1\"],\"columns[0].queryType\":[\"EQ\"],\"columns[0].htmlType\":[\"input\"],\"columns[0].dictType\":[\"\"],\"columns[1].columnId\":[\"2\"],\"columns[1].sort\":[\"2\"],\"columns[1].columnComment\":[\"发生时间\"],\"columns[1].javaType\":[\"Date\"],\"columns[1].javaField\":[\"happenTime\"],\"columns[1].isInsert\":[\"1\"],\"columns[1].isEdit\":[\"1\"],\"columns[1].isList\":[\"1\"],\"columns[1].isQuery\":[\"1\"],\"columns[1].queryType\":[\"EQ\"],\"columns[1].htmlType\":[\"datetime\"],\"columns[1].dictType\":[\"\"],\"columns[2].columnId\":[\"3\"],\"columns[2].sort\":[\"3\"],\"columns[2].columnComment\":[\"发生地点\"],\"columns[2].javaType\":[\"String\"],\"columns[2].javaField\":[\"happenAddress\"],\"columns[2].isInsert\":[\"1\"],\"columns[2].isEdit\":[\"1\"],\"columns[2].isList\":[\"1\"],\"columns[2].isQuery\":[\"1\"],\"columns[2].queryType\":[\"EQ\"],\"columns[2].htmlType\":[\"input\"],\"columns[2].dictType\":[\"\"],\"columns[3].columnId\":[\"4\"],\"columns[3].sort\":[\"4\"],\"columns[3].columnComment\":[\"责任人\"],\"columns[3].javaType\":[\"String\"],\"columns[3].javaField\":[\"blamePerson\"],\"columns[3].isInsert\":[\"1\"],\"columns[3].isEdit\":[\"1\"],\"columns[3].isList\":[\"1\"],\"columns[3].isQuery\":[\"1\"],\"columns[3].queryType\":[\"EQ\"],\"columns[3].htmlType\":[\"input\"],\"columns[3].dictType\":[\"\"],\"columns[4].columnId\":[\"5\"],\"columns[4].sort\":[\"5\"],\"columns[4].columnComment\":[\"事故类型\"],\"columns[4].javaType\":[\"String\"],\"columns[4].javaField\":[\"accidentType\"],\"columns[4].isInsert\":[\"1\"],\"columns[4].isEdit\":[\"1\"],\"columns[4].isList\":[\"1\"],\"columns[4].isQuery\":[\"1\"],\"columns[4].queryType\":[\"EQ\"],\"columns[4].htmlType\":[\"input\"],\"columns[4].dictType\":[\"\"],\"columns[5].columnId\":[\"6\"],\"columns[5].sort\":[\"6\"],\"columns[5].columnComment\":[\"损失程度\"],\"columns[5].javaType\":[\"String\"],\"columns[5].javaField\":[\"loseLevel\"],\"col', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2020-01-03 09:04:38');
INSERT INTO `sys_oper_log` VALUES (272, '代码生成', 8, 'com.ruoyi.project.tool.gen.controller.GenController.genCode()', 'GET', 1, 'admin', '徐州第1组', '/tool/gen/genCode/accident', '127.0.0.1', '内网IP', '{}', 'null', 0, NULL, '2020-01-03 09:04:43');
INSERT INTO `sys_oper_log` VALUES (273, '施工事故管理', 1, 'com.ruoyi.project.system.accident.controller.AccidentController.addSave()', 'POST', 1, 'admin', '徐州第1组', '/system/accident/add', '127.0.0.1', '内网IP', '{\"happenTime\":[\"2020-01-01\"],\"happenAddress\":[\"徐州\"],\"blamePerson\":[\"陈贺\"],\"accidentType\":[\"1\"],\"loseLevel\":[\"高\"],\"accidentDescrible\":[\"撞车\"]}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2020-01-03 09:15:38');
INSERT INTO `sys_oper_log` VALUES (274, '施工事故管理', 2, 'com.ruoyi.project.system.accident.controller.AccidentController.editSave()', 'POST', 1, 'admin', '徐州第1组', '/system/accident/edit', '127.0.0.1', '内网IP', '{\"accidentId\":[\"1\"],\"happenTime\":[\"2020-01-01\"],\"happenAddress\":[\"徐州\"],\"blamePerson\":[\"陈贺1\"],\"accidentType\":[\"1\"],\"loseLevel\":[\"高\"],\"accidentDescrible\":[\"撞车\"]}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2020-01-03 09:15:48');
INSERT INTO `sys_oper_log` VALUES (275, '施工事故管理', 3, 'com.ruoyi.project.system.accident.controller.AccidentController.remove()', 'POST', 1, 'admin', '徐州第1组', '/system/accident/remove', '127.0.0.1', '内网IP', '{\"ids\":[\"1\"]}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2020-01-03 09:15:53');
INSERT INTO `sys_oper_log` VALUES (276, '菜单管理', 2, 'com.ruoyi.project.system.menu.controller.MenuController.editSave()', 'POST', 1, 'admin', '徐州第1组', '/system/menu/edit', '127.0.0.1', '内网IP', '{\"menuId\":[\"2027\"],\"parentId\":[\"2006\"],\"menuType\":[\"C\"],\"menuName\":[\"施工事故管理\"],\"url\":[\"/system/accident\"],\"target\":[\"menuItem\"],\"perms\":[\"system:accident:view\"],\"orderNum\":[\"1\"],\"icon\":[\"#\"],\"visible\":[\"0\"]}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2020-01-03 09:16:22');
INSERT INTO `sys_oper_log` VALUES (277, '菜单管理', 2, 'com.ruoyi.project.system.menu.controller.MenuController.editSave()', 'POST', 1, 'admin', '徐州第1组', '/system/menu/edit', '127.0.0.1', '内网IP', '{\"menuId\":[\"107\"],\"parentId\":[\"2000\"],\"menuType\":[\"C\"],\"menuName\":[\"通知公告\"],\"url\":[\"/system/notice\"],\"target\":[\"menuItem\"],\"perms\":[\"system:notice:view\"],\"orderNum\":[\"8\"],\"icon\":[\"#\"],\"visible\":[\"0\"]}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2020-01-03 09:19:47');
INSERT INTO `sys_oper_log` VALUES (278, '菜单管理', 3, 'com.ruoyi.project.system.menu.controller.MenuController.remove()', 'GET', 1, 'admin', '徐州第1组', '/system/menu/remove/2001', '127.0.0.1', '内网IP', '{}', '{\"msg\":\"菜单已分配,不允许删除\",\"code\":301}', 0, NULL, '2020-01-03 09:21:38');
INSERT INTO `sys_oper_log` VALUES (279, '施工事故管理', 1, 'com.ruoyi.project.system.accident.controller.AccidentController.addSave()', 'POST', 1, 'admin', '徐州第1组', '/system/accident/add', '127.0.0.1', '内网IP', '{\"happenTime\":[\"2019-12-01\"],\"happenAddress\":[\"徐州\"],\"blamePerson\":[\"陈贺\"],\"accidentType\":[\"1\"],\"loseLevel\":[\"高\"],\"accidentDescrible\":[\"材料不合格引起的\"]}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2020-01-03 09:23:43');
INSERT INTO `sys_oper_log` VALUES (280, '施工事故管理', 1, 'com.ruoyi.project.system.accident.controller.AccidentController.addSave()', 'POST', 1, 'admin', '徐州第1组', '/system/accident/add', '127.0.0.1', '内网IP', '{\"happenTime\":[\"2019-12-31\"],\"happenAddress\":[\"徐州\"],\"blamePerson\":[\"陈贺\"],\"accidentType\":[\"2\"],\"loseLevel\":[\"中\"],\"accidentDescrible\":[\"质量不合格\"]}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2020-01-03 09:24:37');
INSERT INTO `sys_oper_log` VALUES (281, '施工事故管理', 1, 'com.ruoyi.project.system.accident.controller.AccidentController.addSave()', 'POST', 1, 'admin', '徐州第1组', '/system/accident/add', '127.0.0.1', '内网IP', '{\"happenTime\":[\"2020-01-01\"],\"happenAddress\":[\"苏州\"],\"blamePerson\":[\"陈贺\"],\"accidentType\":[\"3\"],\"loseLevel\":[\"中\"],\"accidentDescrible\":[\"人员分配不合理引起的\"]}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2020-01-03 09:25:09');
INSERT INTO `sys_oper_log` VALUES (282, '施工事故管理', 1, 'com.ruoyi.project.system.accident.controller.AccidentController.addSave()', 'POST', 1, 'admin', '徐州第1组', '/system/accident/add', '127.0.0.1', '内网IP', '{\"happenTime\":[\"2020-01-02\"],\"happenAddress\":[\"上海\"],\"blamePerson\":[\"陈\"],\"accidentType\":[\"2\"],\"loseLevel\":[\"高\"],\"accidentDescrible\":[\"缺少人员\"]}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2020-01-03 09:25:41');
INSERT INTO `sys_oper_log` VALUES (283, '施工事故管理', 2, 'com.ruoyi.project.system.accident.controller.AccidentController.editSave()', 'POST', 1, 'admin', '徐州第1组', '/system/accident/edit', '127.0.0.1', '内网IP', '{\"accidentId\":[\"4\"],\"happenTime\":[\"2020-01-01\"],\"happenAddress\":[\"北京\"],\"blamePerson\":[\"贺\"],\"accidentType\":[\"3\"],\"loseLevel\":[\"中\"],\"accidentDescrible\":[\"人员分配不合理引起的\"]}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2020-01-03 09:25:51');
INSERT INTO `sys_oper_log` VALUES (284, '施工事故管理', 1, 'com.ruoyi.project.system.accident.controller.AccidentController.addSave()', 'POST', 1, 'admin', '徐州第1组', '/system/accident/add', '127.0.0.1', '内网IP', '{\"happenTime\":[\"2020-01-15\"],\"happenAddress\":[\"上海\"],\"blamePerson\":[\"陈\"],\"accidentType\":[\"1\"],\"loseLevel\":[\"高\"],\"accidentDescrible\":[\"缺少人员缺少人员缺少人员\"]}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2020-01-03 09:26:12');
INSERT INTO `sys_oper_log` VALUES (285, '施工事故管理', 1, 'com.ruoyi.project.system.accident.controller.AccidentController.addSave()', 'POST', 1, 'admin', '徐州第1组', '/system/accident/add', '127.0.0.1', '内网IP', '{\"happenTime\":[\"2020-01-04\"],\"happenAddress\":[\"北京\"],\"blamePerson\":[\"贺\"],\"accidentType\":[\"2\"],\"loseLevel\":[\"中\"],\"accidentDescrible\":[\"人员分配不合理引起的\"]}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2020-01-03 09:26:39');
INSERT INTO `sys_oper_log` VALUES (286, '施工事故管理', 1, 'com.ruoyi.project.system.accident.controller.AccidentController.addSave()', 'POST', 1, 'admin', '徐州第1组', '/system/accident/add', '127.0.0.1', '内网IP', '{\"happenTime\":[\"2020-01-02\"],\"happenAddress\":[\"上海\"],\"blamePerson\":[\"陈\"],\"accidentType\":[\"2\"],\"loseLevel\":[\"高\"],\"accidentDescrible\":[\"人员分配不合理引起的\"]}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2020-01-03 09:26:54');
INSERT INTO `sys_oper_log` VALUES (287, '菜单管理', 2, 'com.ruoyi.project.system.menu.controller.MenuController.editSave()', 'POST', 1, 'admin', '徐州第1组', '/system/menu/edit', '127.0.0.1', '内网IP', '{\"menuId\":[\"107\"],\"parentId\":[\"2000\"],\"menuType\":[\"C\"],\"menuName\":[\"安全检查通知公告\"],\"url\":[\"/system/notice\"],\"target\":[\"menuItem\"],\"perms\":[\"system:notice:view\"],\"orderNum\":[\"8\"],\"icon\":[\"#\"],\"visible\":[\"0\"]}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2020-01-03 09:27:59');
INSERT INTO `sys_oper_log` VALUES (288, '通知公告', 1, 'com.ruoyi.project.system.notice.controller.NoticeController.addSave()', 'POST', 1, 'admin', '徐州第1组', '/system/notice/add', '127.0.0.1', '内网IP', '{\"noticeTitle\":[\"【紧急通知】大家要好好学习啊\"],\"noticeType\":[\"1\"],\"noticeContent\":[\"<p>大家要好好学习啊大家要好好学习啊大家要好好学习啊大家要好好学习啊大家要好好学习啊大家要好好学习啊大家要好好学习啊大家要好好学习啊大家要好好学习啊大家要好好学习啊大家要好好学习啊大家要好好学习啊<br></p>\"],\"status\":[\"0\"]}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2020-01-03 09:28:38');
INSERT INTO `sys_oper_log` VALUES (289, '菜单管理', 2, 'com.ruoyi.project.system.menu.controller.MenuController.editSave()', 'POST', 1, 'admin', '徐州第1组', '/system/menu/edit', '127.0.0.1', '内网IP', '{\"menuId\":[\"2000\"],\"parentId\":[\"0\"],\"menuType\":[\"C\"],\"menuName\":[\"现场安全检查管理\"],\"url\":[\"/system/notice\"],\"target\":[\"menuItem\"],\"perms\":[\"\"],\"orderNum\":[\"1\"],\"icon\":[\"fa fa-american-sign-language-interpreting\"],\"visible\":[\"0\"]}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2020-01-03 09:29:26');
INSERT INTO `sys_oper_log` VALUES (290, '菜单管理', 2, 'com.ruoyi.project.system.menu.controller.MenuController.editSave()', 'POST', 1, 'admin', '徐州第1组', '/system/menu/edit', '127.0.0.1', '内网IP', '{\"menuId\":[\"107\"],\"parentId\":[\"2000\"],\"menuType\":[\"C\"],\"menuName\":[\"安全检查通知公告\"],\"url\":[\"/system/notice\"],\"target\":[\"menuItem\"],\"perms\":[\"system:notice:view\"],\"orderNum\":[\"8\"],\"icon\":[\"#\"],\"visible\":[\"0\"]}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2020-01-03 09:29:36');
INSERT INTO `sys_oper_log` VALUES (291, '菜单管理', 2, 'com.ruoyi.project.system.menu.controller.MenuController.editSave()', 'POST', 1, 'admin', '徐州第1组', '/system/menu/edit', '127.0.0.1', '内网IP', '{\"menuId\":[\"2000\"],\"parentId\":[\"0\"],\"menuType\":[\"C\"],\"menuName\":[\"现场安全检查管理\"],\"url\":[\"/system/notice\"],\"target\":[\"menuItem\"],\"perms\":[\"system:notice:view\"],\"orderNum\":[\"1\"],\"icon\":[\"fa fa-american-sign-language-interpreting\"],\"visible\":[\"0\"]}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2020-01-03 09:29:41');
INSERT INTO `sys_oper_log` VALUES (292, '菜单管理', 3, 'com.ruoyi.project.system.menu.controller.MenuController.remove()', 'GET', 1, 'admin', '徐州第1组', '/system/menu/remove/107', '127.0.0.1', '内网IP', '{}', '{\"msg\":\"存在子菜单,不允许删除\",\"code\":301}', 0, NULL, '2020-01-03 09:30:04');
INSERT INTO `sys_oper_log` VALUES (293, '菜单管理', 2, 'com.ruoyi.project.system.menu.controller.MenuController.editSave()', 'POST', 1, 'admin', '徐州第1组', '/system/menu/edit', '127.0.0.1', '内网IP', '{\"menuId\":[\"2006\"],\"parentId\":[\"0\"],\"menuType\":[\"M\"],\"menuName\":[\"施工事故管理\"],\"url\":[\"#\"],\"target\":[\"menuItem\"],\"perms\":[\"\"],\"orderNum\":[\"5\"],\"icon\":[\"fa fa-binoculars\"],\"visible\":[\"0\"]}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2020-01-03 09:33:00');
INSERT INTO `sys_oper_log` VALUES (294, '菜单管理', 2, 'com.ruoyi.project.system.menu.controller.MenuController.editSave()', 'POST', 1, 'admin', '徐州第1组', '/system/menu/edit', '127.0.0.1', '内网IP', '{\"menuId\":[\"2027\"],\"parentId\":[\"2006\"],\"menuType\":[\"F\"],\"menuName\":[\"施工事故管理\"],\"url\":[\"/system/accident\"],\"target\":[\"menuItem\"],\"perms\":[\"system:accident:view\"],\"orderNum\":[\"1\"],\"icon\":[\"#\"],\"visible\":[\"0\"]}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2020-01-03 09:33:54');
INSERT INTO `sys_oper_log` VALUES (295, '菜单管理', 2, 'com.ruoyi.project.system.menu.controller.MenuController.editSave()', 'POST', 1, 'admin', '徐州第1组', '/system/menu/edit', '127.0.0.1', '内网IP', '{\"menuId\":[\"2006\"],\"parentId\":[\"0\"],\"menuType\":[\"C\"],\"menuName\":[\"施工事故管理\"],\"url\":[\"/system/accident\"],\"target\":[\"menuItem\"],\"perms\":[\"\"],\"orderNum\":[\"5\"],\"icon\":[\"fa fa-binoculars\"],\"visible\":[\"0\"]}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2020-01-03 09:34:28');
INSERT INTO `sys_oper_log` VALUES (296, '菜单管理', 2, 'com.ruoyi.project.system.menu.controller.MenuController.editSave()', 'POST', 1, 'admin', '徐州第1组', '/system/menu/edit', '127.0.0.1', '内网IP', '{\"menuId\":[\"2006\"],\"parentId\":[\"0\"],\"menuType\":[\"C\"],\"menuName\":[\"施工事故管理\"],\"url\":[\"/system/accident\"],\"target\":[\"menuItem\"],\"perms\":[\"system:accident:view\"],\"orderNum\":[\"5\"],\"icon\":[\"fa fa-binoculars\"],\"visible\":[\"0\"]}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2020-01-03 09:34:43');
INSERT INTO `sys_oper_log` VALUES (297, '菜单管理', 2, 'com.ruoyi.project.system.menu.controller.MenuController.editSave()', 'POST', 1, 'admin', '徐州第1组', '/system/menu/edit', '127.0.0.1', '内网IP', '{\"menuId\":[\"2028\"],\"parentId\":[\"2006\"],\"menuType\":[\"F\"],\"menuName\":[\"施工事故管理查询\"],\"url\":[\"#\"],\"target\":[\"menuItem\"],\"perms\":[\"system:accident:list\"],\"orderNum\":[\"1\"],\"icon\":[\"#\"],\"visible\":[\"0\"]}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2020-01-03 09:35:08');
INSERT INTO `sys_oper_log` VALUES (298, '菜单管理', 2, 'com.ruoyi.project.system.menu.controller.MenuController.editSave()', 'POST', 1, 'admin', '徐州第1组', '/system/menu/edit', '127.0.0.1', '内网IP', '{\"menuId\":[\"2029\"],\"parentId\":[\"2006\"],\"menuType\":[\"F\"],\"menuName\":[\"施工事故管理新增\"],\"url\":[\"#\"],\"target\":[\"menuItem\"],\"perms\":[\"system:accident:add\"],\"orderNum\":[\"2\"],\"icon\":[\"#\"],\"visible\":[\"0\"]}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2020-01-03 09:35:31');
INSERT INTO `sys_oper_log` VALUES (299, '菜单管理', 2, 'com.ruoyi.project.system.menu.controller.MenuController.editSave()', 'POST', 1, 'admin', '徐州第1组', '/system/menu/edit', '127.0.0.1', '内网IP', '{\"menuId\":[\"2030\"],\"parentId\":[\"2006\"],\"menuType\":[\"F\"],\"menuName\":[\"施工事故管理修改\"],\"url\":[\"#\"],\"target\":[\"menuItem\"],\"perms\":[\"system:accident:edit\"],\"orderNum\":[\"3\"],\"icon\":[\"#\"],\"visible\":[\"0\"]}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2020-01-03 09:35:47');
INSERT INTO `sys_oper_log` VALUES (300, '菜单管理', 2, 'com.ruoyi.project.system.menu.controller.MenuController.editSave()', 'POST', 1, 'admin', '徐州第1组', '/system/menu/edit', '127.0.0.1', '内网IP', '{\"menuId\":[\"2031\"],\"parentId\":[\"2006\"],\"menuType\":[\"F\"],\"menuName\":[\"施工事故管理删除\"],\"url\":[\"#\"],\"target\":[\"menuItem\"],\"perms\":[\"system:accident:remove\"],\"orderNum\":[\"4\"],\"icon\":[\"#\"],\"visible\":[\"0\"]}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2020-01-03 09:36:04');
INSERT INTO `sys_oper_log` VALUES (301, '菜单管理', 2, 'com.ruoyi.project.system.menu.controller.MenuController.editSave()', 'POST', 1, 'admin', '徐州第1组', '/system/menu/edit', '127.0.0.1', '内网IP', '{\"menuId\":[\"2032\"],\"parentId\":[\"2006\"],\"menuType\":[\"F\"],\"menuName\":[\"施工事故管理导出\"],\"url\":[\"#\"],\"target\":[\"menuItem\"],\"perms\":[\"system:accident:export\"],\"orderNum\":[\"5\"],\"icon\":[\"#\"],\"visible\":[\"0\"]}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2020-01-03 09:36:21');
INSERT INTO `sys_oper_log` VALUES (302, '菜单管理', 3, 'com.ruoyi.project.system.menu.controller.MenuController.remove()', 'GET', 1, 'admin', '徐州第1组', '/system/menu/remove/2027', '127.0.0.1', '内网IP', '{}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2020-01-03 09:36:28');
INSERT INTO `sys_oper_log` VALUES (303, '菜单管理', 1, 'com.ruoyi.project.system.menu.controller.MenuController.addSave()', 'POST', 1, 'admin', '徐州第1组', '/system/menu/add', '127.0.0.1', '内网IP', '{\"parentId\":[\"2000\"],\"menuType\":[\"F\"],\"menuName\":[\"现场安全检查管理查询\"],\"url\":[\"\"],\"target\":[\"menuItem\"],\"perms\":[\"system:notice:list\"],\"orderNum\":[\"1\"],\"icon\":[\"\"],\"visible\":[\"0\"]}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2020-01-03 09:38:44');
INSERT INTO `sys_oper_log` VALUES (304, '菜单管理', 1, 'com.ruoyi.project.system.menu.controller.MenuController.addSave()', 'POST', 1, 'admin', '徐州第1组', '/system/menu/add', '127.0.0.1', '内网IP', '{\"parentId\":[\"2033\"],\"menuType\":[\"F\"],\"menuName\":[\"现场安全检查管理新增\"],\"url\":[\"\"],\"target\":[\"menuItem\"],\"perms\":[\"system:notice:add\"],\"orderNum\":[\"2\"],\"icon\":[\"\"],\"visible\":[\"0\"]}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2020-01-03 09:44:01');
INSERT INTO `sys_oper_log` VALUES (305, '菜单管理', 3, 'com.ruoyi.project.system.menu.controller.MenuController.remove()', 'GET', 1, 'admin', '徐州第1组', '/system/menu/remove/2034', '127.0.0.1', '内网IP', '{}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2020-01-03 09:44:18');
INSERT INTO `sys_oper_log` VALUES (306, '菜单管理', 3, 'com.ruoyi.project.system.menu.controller.MenuController.remove()', 'GET', 1, 'admin', '徐州第1组', '/system/menu/remove/2033', '127.0.0.1', '内网IP', '{}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2020-01-03 09:46:18');
INSERT INTO `sys_oper_log` VALUES (307, '菜单管理', 2, 'com.ruoyi.project.system.menu.controller.MenuController.editSave()', 'POST', 1, 'admin', '徐州第1组', '/system/menu/edit', '127.0.0.1', '内网IP', '{\"menuId\":[\"2000\"],\"parentId\":[\"0\"],\"menuType\":[\"C\"],\"menuName\":[\"检查通知公告管理\"],\"url\":[\"/system/notice\"],\"target\":[\"menuItem\"],\"perms\":[\"system:notice:view\"],\"orderNum\":[\"1\"],\"icon\":[\"fa fa-american-sign-language-interpreting\"],\"visible\":[\"0\"]}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2020-01-03 09:47:26');
INSERT INTO `sys_oper_log` VALUES (308, '菜单管理', 2, 'com.ruoyi.project.system.menu.controller.MenuController.editSave()', 'POST', 1, 'admin', '徐州第1组', '/system/menu/edit', '127.0.0.1', '内网IP', '{\"menuId\":[\"2000\"],\"parentId\":[\"0\"],\"menuType\":[\"C\"],\"menuName\":[\"通知公告管理\"],\"url\":[\"/system/notice\"],\"target\":[\"menuItem\"],\"perms\":[\"system:notice:view\"],\"orderNum\":[\"1\"],\"icon\":[\"fa fa-american-sign-language-interpreting\"],\"visible\":[\"0\"]}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2020-01-03 09:47:37');
INSERT INTO `sys_oper_log` VALUES (309, '通知公告', 1, 'com.ruoyi.project.system.notice.controller.NoticeController.addSave()', 'POST', 1, 'admin', '徐州第1组', '/system/notice/add', '127.0.0.1', '内网IP', '{\"noticeTitle\":[\"【特别】好男人就是我\"],\"noticeType\":[\"2\"],\"noticeContent\":[\"<p>【特别】好男人就是我，我就是陈贺<br></p>\"],\"status\":[\"0\"]}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2020-01-03 09:49:43');
INSERT INTO `sys_oper_log` VALUES (310, '菜单管理', 2, 'com.ruoyi.project.system.menu.controller.MenuController.editSave()', 'POST', 1, 'admin', '徐州第1组', '/system/menu/edit', '127.0.0.1', '内网IP', '{\"menuId\":[\"2014\"],\"parentId\":[\"2004\"],\"menuType\":[\"C\"],\"menuName\":[\"风险源判断\"],\"url\":[\"/system/user/jump\"],\"target\":[\"menuItem\"],\"perms\":[\"\"],\"orderNum\":[\"2\"],\"icon\":[\"fa fa-bars\"],\"visible\":[\"0\"]}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2020-01-03 09:53:03');
INSERT INTO `sys_oper_log` VALUES (311, '角色管理', 2, 'com.ruoyi.project.system.role.controller.RoleController.editSave()', 'POST', 1, 'admin', '徐州第1组', '/system/role/edit', '127.0.0.1', '内网IP', '{\"roleId\":[\"2\"],\"roleName\":[\"普通员工\"],\"roleKey\":[\"common\"],\"roleSort\":[\"4\"],\"status\":[\"0\"],\"remark\":[\"普通员工只能查看本人数据\"],\"menuIds\":[\"2000,1035,2003,2010,2011,2012,2004,2013,2014,2015,2005,2016,2017,2018,2019,2006,2007,2024,2025,2026\"]}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2020-01-03 09:54:52');
INSERT INTO `sys_oper_log` VALUES (312, '角色管理', 2, 'com.ruoyi.project.system.role.controller.RoleController.editSave()', 'POST', 1, 'admin', '徐州第1组', '/system/role/edit', '127.0.0.1', '内网IP', '{\"roleId\":[\"101\"],\"roleName\":[\"总公司领导\"],\"roleKey\":[\"ceo\"],\"roleSort\":[\"2\"],\"status\":[\"0\"],\"remark\":[\"总公司领导可以查看所有公司的数据\"],\"menuIds\":[\"2000,1035,1036,1037,1038,2003,2010,2011,2012,2004,2013,2014,2015,2005,2016,2017,2018,2019,2006,2028,2029,2030,2031,2032,2007,2024,2025,2026,1,100,1000,1001,1002,1003,1004,1005,1006\"]}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2020-01-03 09:55:25');
INSERT INTO `sys_oper_log` VALUES (313, '角色管理', 2, 'com.ruoyi.project.system.role.controller.RoleController.editSave()', 'POST', 1, 'admin', '徐州第1组', '/system/role/edit', '127.0.0.1', '内网IP', '{\"roleId\":[\"100\"],\"roleName\":[\"分公司领导\"],\"roleKey\":[\"senior\"],\"roleSort\":[\"3\"],\"status\":[\"0\"],\"remark\":[\"分公司领导可以查看本部门的数据\"],\"menuIds\":[\"2000,1035,1036,1037,1038,2003,2010,2011,2012,2004,2013,2014,2015,2005,2016,2017,2018,2019,2006,2028,2029,2030,2031,2032,2007,2024,2025,2026,1,100,1000,1001,1002,1003,1004,1005,1006\"]}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2020-01-03 09:55:34');
INSERT INTO `sys_oper_log` VALUES (314, '角色管理', 2, 'com.ruoyi.project.system.role.controller.RoleController.editSave()', 'POST', 1, 'admin', '徐州第1组', '/system/role/edit', '127.0.0.1', '内网IP', '{\"roleId\":[\"2\"],\"roleName\":[\"普通员工\"],\"roleKey\":[\"common\"],\"roleSort\":[\"4\"],\"status\":[\"0\"],\"remark\":[\"普通员工只能查看本人数据\"],\"menuIds\":[\"2000,1035,2003,2010,2011,2012,2004,2013,2014,2015,2005,2016,2017,2018,2019,2006,2028,2029,2030,2031,2032,2007,2024,2025,2026\"]}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2020-01-03 09:55:55');
INSERT INTO `sys_oper_log` VALUES (315, '施工事故管理', 5, 'com.ruoyi.project.system.accident.controller.AccidentController.export()', 'POST', 1, 'admin', '徐州第1组', '/system/accident/export', '127.0.0.1', '内网IP', '{\"params[beginHappenTime]\":[\"\"],\"params[endHappenTime]\":[\"\"],\"happenAddress\":[\"\"],\"blamePerson\":[\"\"],\"accidentType\":[\"\"],\"loseLevel\":[\"\"],\"accidentDescrible\":[\"\"],\"remark\":[\"\"]}', '{\"msg\":\"ab28fd7a-640f-44d7-b381-62acd54e8682_accident.xlsx\",\"code\":0}', 0, NULL, '2020-01-05 22:18:59');
INSERT INTO `sys_oper_log` VALUES (316, '代码生成', 8, 'com.ruoyi.project.tool.gen.controller.GenController.genCode()', 'GET', 1, 'admin', '徐州第1组', '/tool/gen/genCode/accident', '127.0.0.1', '内网IP', '{}', 'null', 0, NULL, '2020-01-05 22:19:14');
INSERT INTO `sys_oper_log` VALUES (317, '代码生成', 6, 'com.ruoyi.project.tool.gen.controller.GenController.importTableSave()', 'POST', 1, 'admin', '徐州第1组', '/tool/gen/importTable', '127.0.0.1', '内网IP', '{\"tables\":[\"hiddendanger,contingencyplan\"]}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2020-01-06 08:48:25');
INSERT INTO `sys_oper_log` VALUES (318, '在线用户', 7, 'com.ruoyi.project.monitor.online.controller.UserOnlineController.batchForceLogout()', 'POST', 1, 'admin', '徐州第1组', '/monitor/online/batchForceLogout', '127.0.0.1', '内网IP', '{\"ids[]\":[\"185a37cc-73a9-49af-ab57-e6df6c0d0932\"]}', '{\"msg\":\"当前登陆用户无法强退\",\"code\":500}', 0, NULL, '2020-01-06 09:13:01');
INSERT INTO `sys_oper_log` VALUES (319, '代码生成', 3, 'com.ruoyi.project.tool.gen.controller.GenController.remove()', 'POST', 1, 'admin', '徐州第1组', '/tool/gen/remove', '127.0.0.1', '内网IP', '{\"ids\":[\"2,3,1\"]}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2020-01-06 09:13:22');
INSERT INTO `sys_oper_log` VALUES (320, '角色管理', 2, 'com.ruoyi.project.system.role.controller.RoleController.editSave()', 'POST', 1, 'admin', '徐州第1组', '/system/role/edit', '127.0.0.1', '内网IP', '{\"roleId\":[\"2\"],\"roleName\":[\"普通员工\"],\"roleKey\":[\"common\"],\"roleSort\":[\"4\"],\"status\":[\"0\"],\"remark\":[\"普通员工只能查看本人数据\"],\"menuIds\":[\"2000,1035,2003,2010,2011,2012,2004,2013,2014,2015,2005,2016,2017,2018,2019,2006,2028,2032,2007,2024,2025,2026\"]}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2020-01-06 14:42:18');
INSERT INTO `sys_oper_log` VALUES (321, '菜单管理', 2, 'com.ruoyi.project.system.menu.controller.MenuController.editSave()', 'POST', 1, 'admin', '徐州第1组', '/system/menu/edit', '127.0.0.1', '内网IP', '{\"menuId\":[\"2003\"],\"parentId\":[\"0\"],\"menuType\":[\"C\"],\"menuName\":[\"现场隐患管理\"],\"url\":[\"#\"],\"target\":[\"menuItem\"],\"perms\":[\"\"],\"orderNum\":[\"2\"],\"icon\":[\"fa fa-bell\"],\"visible\":[\"0\"]}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2020-01-06 14:44:44');
INSERT INTO `sys_oper_log` VALUES (322, '代码生成', 6, 'com.ruoyi.project.tool.gen.controller.GenController.importTableSave()', 'POST', 1, 'admin', '徐州第1组', '/tool/gen/importTable', '127.0.0.1', '内网IP', '{\"tables\":[\"hiddendanger\"]}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2020-01-06 14:45:55');
INSERT INTO `sys_oper_log` VALUES (323, '字典类型', 1, 'com.ruoyi.project.system.dict.controller.DictTypeController.addSave()', 'POST', 1, 'admin', '徐州第1组', '/system/dict/add', '127.0.0.1', '内网IP', '{\"dictName\":[\"hidden_type\"],\"dictType\":[\"sys_hidden_type\"],\"status\":[\"0\"],\"remark\":[\"隐患类型\"]}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2020-01-06 14:50:51');
INSERT INTO `sys_oper_log` VALUES (324, '字典类型', 3, 'com.ruoyi.project.system.dict.controller.DictTypeController.remove()', 'POST', 1, 'admin', '徐州第1组', '/system/dict/remove', '127.0.0.1', '内网IP', '{\"ids\":[\"11\"]}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2020-01-06 14:51:12');
INSERT INTO `sys_oper_log` VALUES (325, '代码生成', 2, 'com.ruoyi.project.tool.gen.controller.GenController.editSave()', 'POST', 1, 'admin', '徐州第1组', '/tool/gen/edit', '127.0.0.1', '内网IP', '{\"tableId\":[\"4\"],\"tableName\":[\"hiddendanger\"],\"tableComment\":[\"安全隐患信息表\"],\"className\":[\"Hiddendanger\"],\"functionAuthor\":[\"chenhe\"],\"remark\":[\"\"],\"columns[0].columnId\":[\"28\"],\"columns[0].sort\":[\"1\"],\"columns[0].columnComment\":[\"安全隐患编号（主键）\"],\"columns[0].javaType\":[\"Integer\"],\"columns[0].javaField\":[\"dangerid\"],\"columns[0].isInsert\":[\"1\"],\"columns[0].queryType\":[\"EQ\"],\"columns[0].htmlType\":[\"input\"],\"columns[0].dictType\":[\"\"],\"columns[1].columnId\":[\"29\"],\"columns[1].sort\":[\"2\"],\"columns[1].columnComment\":[\"名称\"],\"columns[1].javaType\":[\"String\"],\"columns[1].javaField\":[\"name\"],\"columns[1].isInsert\":[\"1\"],\"columns[1].isEdit\":[\"1\"],\"columns[1].isList\":[\"1\"],\"columns[1].isQuery\":[\"1\"],\"columns[1].queryType\":[\"LIKE\"],\"columns[1].isRequired\":[\"1\"],\"columns[1].htmlType\":[\"input\"],\"columns[1].dictType\":[\"\"],\"columns[2].columnId\":[\"30\"],\"columns[2].sort\":[\"3\"],\"columns[2].columnComment\":[\"隐患阐述\"],\"columns[2].javaType\":[\"String\"],\"columns[2].javaField\":[\"elaborate\"],\"columns[2].isInsert\":[\"1\"],\"columns[2].isEdit\":[\"1\"],\"columns[2].isList\":[\"1\"],\"columns[2].isQuery\":[\"1\"],\"columns[2].queryType\":[\"EQ\"],\"columns[2].htmlType\":[\"input\"],\"columns[2].dictType\":[\"\"],\"columns[3].columnId\":[\"31\"],\"columns[3].sort\":[\"4\"],\"columns[3].columnComment\":[\"类型\"],\"columns[3].javaType\":[\"String\"],\"columns[3].javaField\":[\"type\"],\"columns[3].isInsert\":[\"1\"],\"columns[3].isEdit\":[\"1\"],\"columns[3].isList\":[\"1\"],\"columns[3].isQuery\":[\"1\"],\"columns[3].queryType\":[\"EQ\"],\"columns[3].htmlType\":[\"input\"],\"columns[3].dictType\":[\"sys_job_group\"],\"columns[4].columnId\":[\"32\"],\"columns[4].sort\":[\"5\"],\"columns[4].columnComment\":[\"整改建议\"],\"columns[4].javaType\":[\"String\"],\"columns[4].javaField\":[\"suggestions\"],\"columns[4].isInsert\":[\"1\"],\"columns[4].isEdit\":[\"1\"],\"columns[4].isList\":[\"1\"],\"columns[4].isQuery\":[\"1\"],\"columns[4].queryType\":[\"EQ\"],\"columns[4].htmlType\":[\"input\"],\"columns[4].dictType\":[\"\"],\"columns[5].columnId\":[\"33\"],\"columns[5].sort\":[\"6\"],\"columns[5].columnComment\":[\"期限\"],\"columns[5].javaType\":[\"Date\"],\"co', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2020-01-06 14:51:30');
INSERT INTO `sys_oper_log` VALUES (326, '代码生成', 8, 'com.ruoyi.project.tool.gen.controller.GenController.batchGenCode()', 'GET', 1, 'admin', '徐州第1组', '/tool/gen/batchGenCode', '127.0.0.1', '内网IP', '{\"tables\":[\"hiddendanger\"]}', 'null', 0, NULL, '2020-01-06 14:51:35');
INSERT INTO `sys_oper_log` VALUES (327, '安全隐患信息', 1, 'com.ruoyi.project.system.hiddendanger.controller.HiddendangerController.addSave()', 'POST', 1, 'admin', '徐州第1组', '/system/hiddendanger/add', '127.0.0.1', '内网IP', '{\"name\":[\"1\"],\"elaborate\":[\"1\"],\"type\":[\"1\"],\"suggestions\":[\"1\"],\"deadline\":[\"2020-01-01\"],\"testPerson\":[\"1\"],\"reviewResults\":[\"1\"],\"reviewTime\":[\"2020-01-01\"],\"confirmSign\":[\"1\"]}', 'null', 1, '\r\n### Error updating database.  Cause: java.sql.SQLException: Field \'DangerID\' doesn\'t have a default value\r\n### The error may involve com.ruoyi.project.system.hiddendanger.mapper.HiddendangerMapper.insertHiddendanger-Inline\r\n### The error occurred while setting parameters\r\n### SQL: insert into hiddendanger          ( Name,             Elaborate,             Type,             Suggestions,             Deadline,             Test_person,             Review_results,             Review_time,             Confirm_sign )           values ( ?,             ?,             ?,             ?,             ?,             ?,             ?,             ?,             ? )\r\n### Cause: java.sql.SQLException: Field \'DangerID\' doesn\'t have a default value\n; Field \'DangerID\' doesn\'t have a default value; nested exception is java.sql.SQLException: Field \'DangerID\' doesn\'t have a default value', '2020-01-06 14:54:03');
INSERT INTO `sys_oper_log` VALUES (328, '安全隐患信息', 1, 'com.ruoyi.project.system.hiddendanger.controller.HiddendangerController.addSave()', 'POST', 1, 'admin', '徐州第1组', '/system/hiddendanger/add', '127.0.0.1', '内网IP', '{\"name\":[\"1\"],\"elaborate\":[\"1\"],\"type\":[\"1\"],\"suggestions\":[\"1\"],\"deadline\":[\"2020-01-01\"],\"testPerson\":[\"1\"],\"reviewResults\":[\"1\"],\"reviewTime\":[\"2020-01-02\"],\"confirmSign\":[\"1\"]}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2020-01-06 14:56:30');
INSERT INTO `sys_oper_log` VALUES (329, '安全隐患信息', 2, 'com.ruoyi.project.system.hiddendanger.controller.HiddendangerController.editSave()', 'POST', 1, 'admin', '徐州第1组', '/system/hiddendanger/edit', '127.0.0.1', '内网IP', '{\"dangerid\":[\"1\"],\"name\":[\"2\"],\"elaborate\":[\"1\"],\"type\":[\"1\"],\"suggestions\":[\"1\"],\"deadline\":[\"2020-01-01\"],\"testPerson\":[\"1\"],\"reviewResults\":[\"1\"],\"reviewTime\":[\"2020-01-02\"],\"confirmSign\":[\"1\"]}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2020-01-06 14:56:35');
INSERT INTO `sys_oper_log` VALUES (330, '安全隐患信息', 3, 'com.ruoyi.project.system.hiddendanger.controller.HiddendangerController.remove()', 'POST', 1, 'admin', '徐州第1组', '/system/hiddendanger/remove', '127.0.0.1', '内网IP', '{\"ids\":[\"1\"]}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2020-01-06 14:56:37');
INSERT INTO `sys_oper_log` VALUES (331, '菜单管理', 2, 'com.ruoyi.project.system.menu.controller.MenuController.editSave()', 'POST', 1, 'admin', '徐州第1组', '/system/menu/edit', '127.0.0.1', '内网IP', '{\"menuId\":[\"2003\"],\"parentId\":[\"0\"],\"menuType\":[\"C\"],\"menuName\":[\"现场隐患管理\"],\"url\":[\"/system/hiddendanger\"],\"target\":[\"menuItem\"],\"perms\":[\"\"],\"orderNum\":[\"2\"],\"icon\":[\"fa fa-bell\"],\"visible\":[\"0\"]}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2020-01-06 14:58:13');
INSERT INTO `sys_oper_log` VALUES (332, '菜单管理', 2, 'com.ruoyi.project.system.menu.controller.MenuController.editSave()', 'POST', 1, 'admin', '徐州第1组', '/system/menu/edit', '127.0.0.1', '内网IP', '{\"menuId\":[\"2033\"],\"parentId\":[\"0\"],\"menuType\":[\"C\"],\"menuName\":[\"现场隐患信息\"],\"url\":[\"/system/hiddendanger\"],\"target\":[\"menuItem\"],\"perms\":[\"system:hiddendanger:view\"],\"orderNum\":[\"1\"],\"icon\":[\"fa fa-bell\"],\"visible\":[\"0\"]}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2020-01-06 15:02:19');
INSERT INTO `sys_oper_log` VALUES (333, '菜单管理', 3, 'com.ruoyi.project.system.menu.controller.MenuController.remove()', 'GET', 1, 'admin', '徐州第1组', '/system/menu/remove/2003', '127.0.0.1', '内网IP', '{}', '{\"msg\":\"菜单已分配,不允许删除\",\"code\":301}', 0, NULL, '2020-01-06 15:02:28');
INSERT INTO `sys_oper_log` VALUES (334, '菜单管理', 2, 'com.ruoyi.project.system.menu.controller.MenuController.editSave()', 'POST', 1, 'admin', '徐州第1组', '/system/menu/edit', '127.0.0.1', '内网IP', '{\"menuId\":[\"2003\"],\"parentId\":[\"0\"],\"menuType\":[\"C\"],\"menuName\":[\"123456\"],\"url\":[\"/system/hiddendanger\"],\"target\":[\"menuItem\"],\"perms\":[\"\"],\"orderNum\":[\"2\"],\"icon\":[\"fa fa-bell\"],\"visible\":[\"0\"]}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2020-01-06 15:02:37');
INSERT INTO `sys_oper_log` VALUES (335, '安全隐患信息', 1, 'com.ruoyi.project.system.hiddendanger.controller.HiddendangerController.addSave()', 'POST', 1, 'admin', '徐州第1组', '/system/hiddendanger/add', '127.0.0.1', '内网IP', '{\"name\":[\"xushaokang\"],\"elaborate\":[\"baozha\"],\"type\":[\"1\"],\"suggestions\":[\"1\"],\"deadline\":[\"\"],\"testPerson\":[\"\"],\"reviewResults\":[\"\"],\"reviewTime\":[\"\"],\"confirmSign\":[\"\"]}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2020-01-06 15:05:13');
INSERT INTO `sys_oper_log` VALUES (336, '安全隐患信息', 5, 'com.ruoyi.project.system.hiddendanger.controller.HiddendangerController.export()', 'POST', 1, 'admin', '徐州第1组', '/system/hiddendanger/export', '127.0.0.1', '内网IP', '{\"name\":[\"\"],\"elaborate\":[\"\"],\"type\":[\"\"],\"suggestions\":[\"\"],\"params[beginDeadline]\":[\"\"],\"params[endDeadline]\":[\"\"],\"testPerson\":[\"\"],\"reviewResults\":[\"\"],\"params[beginReviewTime]\":[\"\"],\"params[endReviewTime]\":[\"\"],\"confirmSign\":[\"\"],\"remark\":[\"\"]}', '{\"msg\":\"4df6587f-e7c4-423c-9eb4-5b9d72589e9d_hiddendanger.xlsx\",\"code\":0}', 0, NULL, '2020-01-06 15:05:19');
INSERT INTO `sys_oper_log` VALUES (337, '安全隐患信息', 3, 'com.ruoyi.project.system.hiddendanger.controller.HiddendangerController.remove()', 'POST', 1, 'admin', '徐州第1组', '/system/hiddendanger/remove', '127.0.0.1', '内网IP', '{\"ids\":[\"2\"]}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2020-01-06 15:05:27');
INSERT INTO `sys_oper_log` VALUES (338, '代码生成', 6, 'com.ruoyi.project.tool.gen.controller.GenController.importTableSave()', 'POST', 1, 'admin', '徐州第1组', '/tool/gen/importTable', '127.0.0.1', '内网IP', '{\"tables\":[\"riskidentification\"]}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2020-01-06 15:10:52');
INSERT INTO `sys_oper_log` VALUES (339, '代码生成', 3, 'com.ruoyi.project.tool.gen.controller.GenController.remove()', 'POST', 1, 'admin', '徐州第1组', '/tool/gen/remove', '127.0.0.1', '内网IP', '{\"ids\":[\"4\"]}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2020-01-06 15:10:59');
INSERT INTO `sys_oper_log` VALUES (340, '代码生成', 2, 'com.ruoyi.project.tool.gen.controller.GenController.editSave()', 'POST', 1, 'admin', '徐州第1组', '/tool/gen/edit', '127.0.0.1', '内网IP', '{\"tableId\":[\"5\"],\"tableName\":[\"riskidentification\"],\"tableComment\":[\"风险识别信息表\"],\"className\":[\"Riskidentification\"],\"functionAuthor\":[\"chenhe\"],\"remark\":[\"\"],\"columns[0].columnId\":[\"39\"],\"columns[0].sort\":[\"1\"],\"columns[0].columnComment\":[\"风险识别编号（主键）\"],\"columns[0].javaType\":[\"Integer\"],\"columns[0].javaField\":[\"riskidentifyId\"],\"columns[0].isInsert\":[\"1\"],\"columns[0].queryType\":[\"EQ\"],\"columns[0].htmlType\":[\"input\"],\"columns[0].dictType\":[\"\"],\"columns[1].columnId\":[\"40\"],\"columns[1].sort\":[\"2\"],\"columns[1].columnComment\":[\"名称\"],\"columns[1].javaType\":[\"String\"],\"columns[1].javaField\":[\"name\"],\"columns[1].isInsert\":[\"1\"],\"columns[1].isEdit\":[\"1\"],\"columns[1].isList\":[\"1\"],\"columns[1].isQuery\":[\"1\"],\"columns[1].queryType\":[\"LIKE\"],\"columns[1].htmlType\":[\"input\"],\"columns[1].dictType\":[\"\"],\"columns[2].columnId\":[\"41\"],\"columns[2].sort\":[\"3\"],\"columns[2].columnComment\":[\"类别\"],\"columns[2].javaType\":[\"String\"],\"columns[2].javaField\":[\"type\"],\"columns[2].isInsert\":[\"1\"],\"columns[2].isEdit\":[\"1\"],\"columns[2].isList\":[\"1\"],\"columns[2].isQuery\":[\"1\"],\"columns[2].queryType\":[\"EQ\"],\"columns[2].htmlType\":[\"input\"],\"columns[2].dictType\":[\"\"],\"columns[3].columnId\":[\"42\"],\"columns[3].sort\":[\"4\"],\"columns[3].columnComment\":[\"发生地点\"],\"columns[3].javaType\":[\"String\"],\"columns[3].javaField\":[\"address\"],\"columns[3].isInsert\":[\"1\"],\"columns[3].isEdit\":[\"1\"],\"columns[3].isList\":[\"1\"],\"columns[3].isQuery\":[\"1\"],\"columns[3].queryType\":[\"EQ\"],\"columns[3].htmlType\":[\"input\"],\"columns[3].dictType\":[\"\"],\"columns[4].columnId\":[\"43\"],\"columns[4].sort\":[\"5\"],\"columns[4].columnComment\":[\"状态\"],\"columns[4].javaType\":[\"String\"],\"columns[4].javaField\":[\"state\"],\"columns[4].isInsert\":[\"1\"],\"columns[4].isEdit\":[\"1\"],\"columns[4].isList\":[\"1\"],\"columns[4].isQuery\":[\"1\"],\"columns[4].queryType\":[\"EQ\"],\"columns[4].htmlType\":[\"input\"],\"columns[4].dictType\":[\"\"],\"columns[5].columnId\":[\"44\"],\"columns[5].sort\":[\"6\"],\"columns[5].columnComment\":[\"隐患描述\"],\"columns[5].javaType\":[\"String\"],\"columns[5].javaField\":[\"discripti', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2020-01-06 15:11:51');
INSERT INTO `sys_oper_log` VALUES (341, '代码生成', 8, 'com.ruoyi.project.tool.gen.controller.GenController.genCode()', 'GET', 1, 'admin', '徐州第1组', '/tool/gen/genCode/riskidentification', '127.0.0.1', '内网IP', '{}', 'null', 0, NULL, '2020-01-06 15:11:54');
INSERT INTO `sys_oper_log` VALUES (342, '菜单管理', 2, 'com.ruoyi.project.system.menu.controller.MenuController.editSave()', 'POST', 1, 'admin', '徐州第1组', '/system/menu/edit', '127.0.0.1', '内网IP', '{\"menuId\":[\"2004\"],\"parentId\":[\"0\"],\"menuType\":[\"M\"],\"menuName\":[\"1234\"],\"url\":[\"#\"],\"target\":[\"menuItem\"],\"perms\":[\"\"],\"orderNum\":[\"3\"],\"icon\":[\"fa fa-book\"],\"visible\":[\"0\"]}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2020-01-06 15:14:18');
INSERT INTO `sys_oper_log` VALUES (343, '菜单管理', 2, 'com.ruoyi.project.system.menu.controller.MenuController.editSave()', 'POST', 1, 'admin', '徐州第1组', '/system/menu/edit', '127.0.0.1', '内网IP', '{\"menuId\":[\"2039\"],\"parentId\":[\"3\"],\"menuType\":[\"C\"],\"menuName\":[\"施工风险管理\"],\"url\":[\"/system/riskidentification\"],\"target\":[\"menuItem\"],\"perms\":[\"system:riskidentification:view\"],\"orderNum\":[\"1\"],\"icon\":[\"fa fa-book\"],\"visible\":[\"0\"]}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2020-01-06 15:14:45');
INSERT INTO `sys_oper_log` VALUES (344, '代码生成', 3, 'com.ruoyi.project.tool.gen.controller.GenController.remove()', 'POST', 1, 'admin', '徐州第1组', '/tool/gen/remove', '127.0.0.1', '内网IP', '{\"ids\":[\"5\"]}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2020-01-06 15:16:54');
INSERT INTO `sys_oper_log` VALUES (345, '代码生成', 6, 'com.ruoyi.project.tool.gen.controller.GenController.importTableSave()', 'POST', 1, 'admin', '徐州第1组', '/tool/gen/importTable', '127.0.0.1', '内网IP', '{\"tables\":[\"contingencyplan\"]}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2020-01-06 15:17:14');
INSERT INTO `sys_oper_log` VALUES (346, '代码生成', 8, 'com.ruoyi.project.tool.gen.controller.GenController.genCode()', 'GET', 1, 'admin', '徐州第1组', '/tool/gen/genCode/contingencyplan', '127.0.0.1', '内网IP', '{}', 'null', 0, NULL, '2020-01-06 15:17:29');
INSERT INTO `sys_oper_log` VALUES (347, '菜单管理', 2, 'com.ruoyi.project.system.menu.controller.MenuController.editSave()', 'POST', 1, 'admin', '徐州第1组', '/system/menu/edit', '127.0.0.1', '内网IP', '{\"menuId\":[\"2045\"],\"parentId\":[\"3\"],\"menuType\":[\"C\"],\"menuName\":[\"应急预案管理\"],\"url\":[\"/system/contingencyplan\"],\"target\":[\"menuItem\"],\"perms\":[\"system:contingencyplan:view\"],\"orderNum\":[\"1\"],\"icon\":[\"#\"],\"visible\":[\"0\"]}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2020-01-06 15:18:44');
INSERT INTO `sys_oper_log` VALUES (348, '菜单管理', 2, 'com.ruoyi.project.system.menu.controller.MenuController.editSave()', 'POST', 1, 'admin', '徐州第1组', '/system/menu/edit', '127.0.0.1', '内网IP', '{\"menuId\":[\"2045\"],\"parentId\":[\"3\"],\"menuType\":[\"C\"],\"menuName\":[\"应急预案管理\"],\"url\":[\"/system/contingencyplan\"],\"target\":[\"menuItem\"],\"perms\":[\"system:contingencyplan:view\"],\"orderNum\":[\"1\"],\"icon\":[\"fa fa-blind\"],\"visible\":[\"0\"]}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2020-01-06 15:18:59');
INSERT INTO `sys_oper_log` VALUES (349, '代码生成', 3, 'com.ruoyi.project.tool.gen.controller.GenController.remove()', 'POST', 1, 'admin', '徐州第1组', '/tool/gen/remove', '127.0.0.1', '内网IP', '{\"ids\":[\"6\"]}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2020-01-06 15:34:58');
INSERT INTO `sys_oper_log` VALUES (350, '代码生成', 6, 'com.ruoyi.project.tool.gen.controller.GenController.importTableSave()', 'POST', 1, 'admin', '徐州第1组', '/tool/gen/importTable', '127.0.0.1', '内网IP', '{\"tables\":[\"safecheck\"]}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2020-01-06 15:35:44');
INSERT INTO `sys_oper_log` VALUES (351, '代码生成', 2, 'com.ruoyi.project.tool.gen.controller.GenController.editSave()', 'POST', 1, 'admin', '徐州第1组', '/tool/gen/edit', '127.0.0.1', '内网IP', '{\"tableId\":[\"7\"],\"tableName\":[\"safecheck\"],\"tableComment\":[\"安全检查信息表\"],\"className\":[\"Safecheck\"],\"functionAuthor\":[\"chenhe\"],\"remark\":[\"\"],\"columns[0].columnId\":[\"58\"],\"columns[0].sort\":[\"1\"],\"columns[0].columnComment\":[\"安全检查编号(主键)\"],\"columns[0].javaType\":[\"Integer\"],\"columns[0].javaField\":[\"securitycheckid\"],\"columns[0].isInsert\":[\"1\"],\"columns[0].queryType\":[\"EQ\"],\"columns[0].htmlType\":[\"input\"],\"columns[0].dictType\":[\"\"],\"columns[1].columnId\":[\"59\"],\"columns[1].sort\":[\"2\"],\"columns[1].columnComment\":[\"名称\"],\"columns[1].javaType\":[\"String\"],\"columns[1].javaField\":[\"name\"],\"columns[1].isInsert\":[\"1\"],\"columns[1].isEdit\":[\"1\"],\"columns[1].isList\":[\"1\"],\"columns[1].isQuery\":[\"1\"],\"columns[1].queryType\":[\"LIKE\"],\"columns[1].htmlType\":[\"input\"],\"columns[1].dictType\":[\"\"],\"columns[2].columnId\":[\"60\"],\"columns[2].sort\":[\"3\"],\"columns[2].columnComment\":[\"检查类型\"],\"columns[2].javaType\":[\"String\"],\"columns[2].javaField\":[\"checkType\"],\"columns[2].isInsert\":[\"1\"],\"columns[2].isEdit\":[\"1\"],\"columns[2].isList\":[\"1\"],\"columns[2].isQuery\":[\"1\"],\"columns[2].queryType\":[\"EQ\"],\"columns[2].htmlType\":[\"input\"],\"columns[2].dictType\":[\"\"],\"columns[3].columnId\":[\"61\"],\"columns[3].sort\":[\"4\"],\"columns[3].columnComment\":[\"检查人\"],\"columns[3].javaType\":[\"String\"],\"columns[3].javaField\":[\"checkPerson\"],\"columns[3].isInsert\":[\"1\"],\"columns[3].isEdit\":[\"1\"],\"columns[3].isList\":[\"1\"],\"columns[3].isQuery\":[\"1\"],\"columns[3].queryType\":[\"EQ\"],\"columns[3].htmlType\":[\"input\"],\"columns[3].dictType\":[\"\"],\"columns[4].columnId\":[\"62\"],\"columns[4].sort\":[\"5\"],\"columns[4].columnComment\":[\"检查部门\"],\"columns[4].javaType\":[\"String\"],\"columns[4].javaField\":[\"checkDepartment\"],\"columns[4].isInsert\":[\"1\"],\"columns[4].isEdit\":[\"1\"],\"columns[4].isList\":[\"1\"],\"columns[4].isQuery\":[\"1\"],\"columns[4].queryType\":[\"EQ\"],\"columns[4].htmlType\":[\"input\"],\"columns[4].dictType\":[\"\"],\"columns[5].columnId\":[\"63\"],\"columns[5].sort\":[\"6\"],\"columns[5].columnComment\":[\"说明\"],\"columns[5].javaType\":[\"String\"],\"columns[5].javaField\":[\"checkE', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2020-01-06 15:35:55');
INSERT INTO `sys_oper_log` VALUES (352, '代码生成', 8, 'com.ruoyi.project.tool.gen.controller.GenController.genCode()', 'GET', 1, 'admin', '徐州第1组', '/tool/gen/genCode/safecheck', '127.0.0.1', '内网IP', '{}', 'null', 0, NULL, '2020-01-06 15:36:15');
INSERT INTO `sys_oper_log` VALUES (353, '菜单管理', 2, 'com.ruoyi.project.system.menu.controller.MenuController.editSave()', 'POST', 1, 'admin', '徐州第1组', '/system/menu/edit', '127.0.0.1', '内网IP', '{\"menuId\":[\"2051\"],\"parentId\":[\"3\"],\"menuType\":[\"C\"],\"menuName\":[\"安全监督审核管理\"],\"url\":[\"/system/safecheck\"],\"target\":[\"menuItem\"],\"perms\":[\"system:safecheck:view\"],\"orderNum\":[\"1\"],\"icon\":[\"#\"],\"visible\":[\"0\"]}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2020-01-06 15:38:33');
INSERT INTO `sys_oper_log` VALUES (354, '菜单管理', 2, 'com.ruoyi.project.system.menu.controller.MenuController.editSave()', 'POST', 1, 'admin', '徐州第1组', '/system/menu/edit', '127.0.0.1', '内网IP', '{\"menuId\":[\"2007\"],\"parentId\":[\"0\"],\"menuType\":[\"M\"],\"menuName\":[\"123\"],\"url\":[\"#\"],\"target\":[\"menuItem\"],\"perms\":[\"\"],\"orderNum\":[\"6\"],\"icon\":[\"fa fa-bar-chart-o\"],\"visible\":[\"0\"]}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2020-01-06 15:38:37');
INSERT INTO `sys_oper_log` VALUES (355, '菜单管理', 2, 'com.ruoyi.project.system.menu.controller.MenuController.editSave()', 'POST', 1, 'admin', '徐州第1组', '/system/menu/edit', '127.0.0.1', '内网IP', '{\"menuId\":[\"2051\"],\"parentId\":[\"3\"],\"menuType\":[\"C\"],\"menuName\":[\"安全监督审核管理\"],\"url\":[\"/system/safecheck\"],\"target\":[\"menuItem\"],\"perms\":[\"system:safecheck:view\"],\"orderNum\":[\"1\"],\"icon\":[\"fa fa-bar-chart-o\"],\"visible\":[\"0\"]}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2020-01-06 15:38:51');
INSERT INTO `sys_oper_log` VALUES (356, '菜单管理', 2, 'com.ruoyi.project.system.menu.controller.MenuController.editSave()', 'POST', 1, 'admin', '徐州第1组', '/system/menu/edit', '127.0.0.1', '内网IP', '{\"menuId\":[\"2051\"],\"parentId\":[\"0\"],\"menuType\":[\"C\"],\"menuName\":[\"安全监督审核管理\"],\"url\":[\"/system/safecheck\"],\"target\":[\"menuItem\"],\"perms\":[\"system:safecheck:view\"],\"orderNum\":[\"7\"],\"icon\":[\"fa fa-bar-chart-o\"],\"visible\":[\"0\"]}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2020-01-06 15:39:41');
INSERT INTO `sys_oper_log` VALUES (357, '菜单管理', 2, 'com.ruoyi.project.system.menu.controller.MenuController.editSave()', 'POST', 1, 'admin', '徐州第1组', '/system/menu/edit', '127.0.0.1', '内网IP', '{\"menuId\":[\"2006\"],\"parentId\":[\"0\"],\"menuType\":[\"C\"],\"menuName\":[\"施工事故管理\"],\"url\":[\"/system/accident\"],\"target\":[\"menuItem\"],\"perms\":[\"system:accident:view\"],\"orderNum\":[\"6\"],\"icon\":[\"fa fa-binoculars\"],\"visible\":[\"0\"]}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2020-01-06 15:40:01');
INSERT INTO `sys_oper_log` VALUES (358, '菜单管理', 2, 'com.ruoyi.project.system.menu.controller.MenuController.editSave()', 'POST', 1, 'admin', '徐州第1组', '/system/menu/edit', '127.0.0.1', '内网IP', '{\"menuId\":[\"2045\"],\"parentId\":[\"0\"],\"menuType\":[\"C\"],\"menuName\":[\"应急预案管理\"],\"url\":[\"/system/contingencyplan\"],\"target\":[\"menuItem\"],\"perms\":[\"system:contingencyplan:view\"],\"orderNum\":[\"5\"],\"icon\":[\"fa fa-blind\"],\"visible\":[\"0\"]}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2020-01-06 15:40:08');
INSERT INTO `sys_oper_log` VALUES (359, '菜单管理', 2, 'com.ruoyi.project.system.menu.controller.MenuController.editSave()', 'POST', 1, 'admin', '徐州第1组', '/system/menu/edit', '127.0.0.1', '内网IP', '{\"menuId\":[\"2039\"],\"parentId\":[\"0\"],\"menuType\":[\"C\"],\"menuName\":[\"施工风险管理\"],\"url\":[\"/system/riskidentification\"],\"target\":[\"menuItem\"],\"perms\":[\"system:riskidentification:view\"],\"orderNum\":[\"2\"],\"icon\":[\"fa fa-book\"],\"visible\":[\"0\"]}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2020-01-06 15:40:16');
INSERT INTO `sys_oper_log` VALUES (360, '菜单管理', 2, 'com.ruoyi.project.system.menu.controller.MenuController.editSave()', 'POST', 1, 'admin', '徐州第1组', '/system/menu/edit', '127.0.0.1', '内网IP', '{\"menuId\":[\"2033\"],\"parentId\":[\"0\"],\"menuType\":[\"C\"],\"menuName\":[\"现场隐患信息\"],\"url\":[\"/system/hiddendanger\"],\"target\":[\"menuItem\"],\"perms\":[\"system:hiddendanger:view\"],\"orderNum\":[\"3\"],\"icon\":[\"fa fa-bell\"],\"visible\":[\"0\"]}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2020-01-06 15:40:20');
INSERT INTO `sys_oper_log` VALUES (361, '菜单管理', 2, 'com.ruoyi.project.system.menu.controller.MenuController.editSave()', 'POST', 1, 'admin', '徐州第1组', '/system/menu/edit', '127.0.0.1', '内网IP', '{\"menuId\":[\"2051\"],\"parentId\":[\"0\"],\"menuType\":[\"C\"],\"menuName\":[\"安全监督审核管理\"],\"url\":[\"/system/safecheck\"],\"target\":[\"menuItem\"],\"perms\":[\"system:safecheck:view\"],\"orderNum\":[\"8\"],\"icon\":[\"fa fa-bar-chart-o\"],\"visible\":[\"0\"]}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2020-01-06 15:40:31');
INSERT INTO `sys_oper_log` VALUES (362, '菜单管理', 2, 'com.ruoyi.project.system.menu.controller.MenuController.editSave()', 'POST', 1, 'admin', '徐州第1组', '/system/menu/edit', '127.0.0.1', '内网IP', '{\"menuId\":[\"2045\"],\"parentId\":[\"0\"],\"menuType\":[\"C\"],\"menuName\":[\"应急预案管理\"],\"url\":[\"/system/contingencyplan\"],\"target\":[\"menuItem\"],\"perms\":[\"system:contingencyplan:view\"],\"orderNum\":[\"4\"],\"icon\":[\"fa fa-blind\"],\"visible\":[\"0\"]}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2020-01-06 15:40:47');
INSERT INTO `sys_oper_log` VALUES (363, '菜单管理', 2, 'com.ruoyi.project.system.menu.controller.MenuController.editSave()', 'POST', 1, 'admin', '徐州第1组', '/system/menu/edit', '127.0.0.1', '内网IP', '{\"menuId\":[\"2006\"],\"parentId\":[\"0\"],\"menuType\":[\"C\"],\"menuName\":[\"施工事故管理\"],\"url\":[\"/system/accident\"],\"target\":[\"menuItem\"],\"perms\":[\"system:accident:view\"],\"orderNum\":[\"5\"],\"icon\":[\"fa fa-binoculars\"],\"visible\":[\"0\"]}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2020-01-06 15:40:51');
INSERT INTO `sys_oper_log` VALUES (364, '菜单管理', 2, 'com.ruoyi.project.system.menu.controller.MenuController.editSave()', 'POST', 1, 'admin', '徐州第1组', '/system/menu/edit', '127.0.0.1', '内网IP', '{\"menuId\":[\"2051\"],\"parentId\":[\"0\"],\"menuType\":[\"C\"],\"menuName\":[\"安全监督审核管理\"],\"url\":[\"/system/safecheck\"],\"target\":[\"menuItem\"],\"perms\":[\"system:safecheck:view\"],\"orderNum\":[\"6\"],\"icon\":[\"fa fa-bar-chart-o\"],\"visible\":[\"0\"]}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2020-01-06 15:40:56');
INSERT INTO `sys_oper_log` VALUES (365, '应急预案信息 ', 1, 'com.ruoyi.project.system.contingencyplan.controller.ContingencyplanController.addSave()', 'POST', 1, 'admin', '徐州第1组', '/system/contingencyplan/add', '127.0.0.1', '内网IP', '{\"preparedPerson\":[\"董娟娟1\"],\"preparedTime\":[\"2019-12-30\"],\"riskType\":[\"\"],\"riskHandle\":[\"完善设施\"],\"operateStandard\":[\"设备就绪\"],\"notice\":[\"无\"]}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2020-01-06 15:49:04');
INSERT INTO `sys_oper_log` VALUES (366, '应急预案信息 ', 1, 'com.ruoyi.project.system.contingencyplan.controller.ContingencyplanController.addSave()', 'POST', 1, 'admin', '徐州第1组', '/system/contingencyplan/add', '127.0.0.1', '内网IP', '{\"preparedPerson\":[\"郭宙\"],\"preparedTime\":[\"2019-12-31\"],\"riskType\":[\"\"],\"riskHandle\":[\"钉子扎脚\"],\"operateStandard\":[\"伤口包扎处理\"],\"notice\":[\"注意地面\"]}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2020-01-06 15:53:09');
INSERT INTO `sys_oper_log` VALUES (367, '应急预案信息 ', 1, 'com.ruoyi.project.system.contingencyplan.controller.ContingencyplanController.addSave()', 'POST', 1, 'admin', '徐州第1组', '/system/contingencyplan/add', '127.0.0.1', '内网IP', '{\"preparedPerson\":[\"陈贺\"],\"preparedTime\":[\"2020-01-01\"],\"riskType\":[\"\"],\"riskHandle\":[\"电死电伤\"],\"operateStandard\":[\"及时关闭电源\"],\"notice\":[\"无\"]}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2020-01-06 15:54:22');
INSERT INTO `sys_oper_log` VALUES (368, '应急预案信息 ', 1, 'com.ruoyi.project.system.contingencyplan.controller.ContingencyplanController.addSave()', 'POST', 1, 'admin', '徐州第1组', '/system/contingencyplan/add', '127.0.0.1', '内网IP', '{\"preparedPerson\":[\"陈贺1\"],\"preparedTime\":[\"2020-01-03\"],\"riskType\":[\"\"],\"riskHandle\":[\"吊车坠落处理\"],\"operateStandard\":[\"使用前检查吊车\"],\"notice\":[\"注意绳缆\"]}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2020-01-06 15:56:37');
INSERT INTO `sys_oper_log` VALUES (369, '应急预案信息 ', 1, 'com.ruoyi.project.system.contingencyplan.controller.ContingencyplanController.addSave()', 'POST', 1, 'admin', '徐州第1组', '/system/contingencyplan/add', '127.0.0.1', '内网IP', '{\"preparedPerson\":[\"郭宙2\"],\"preparedTime\":[\"2020-01-04\"],\"riskType\":[\"\"],\"riskHandle\":[\"雾天塔吊\"],\"operateStandard\":[\"更改塔吊日期\"],\"notice\":[\"避免雾天塔吊\"]}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2020-01-06 15:58:17');
INSERT INTO `sys_oper_log` VALUES (370, '应急预案信息 ', 1, 'com.ruoyi.project.system.contingencyplan.controller.ContingencyplanController.addSave()', 'POST', 1, 'admin', '徐州第1组', '/system/contingencyplan/add', '127.0.0.1', '内网IP', '{\"preparedPerson\":[\"董娟娟2\"],\"preparedTime\":[\"2020-01-05\"],\"riskType\":[\"\"],\"riskHandle\":[\"坍塌\"],\"operateStandard\":[\"根据土质机械设备重量堆放\"],\"notice\":[\"无\"]}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2020-01-06 16:02:50');
INSERT INTO `sys_oper_log` VALUES (371, '应急预案信息 ', 1, 'com.ruoyi.project.system.contingencyplan.controller.ContingencyplanController.addSave()', 'POST', 1, 'admin', '徐州第1组', '/system/contingencyplan/add', '127.0.0.1', '内网IP', '{\"preparedPerson\":[\"郭宙3\"],\"preparedTime\":[\"2020-01-06\"],\"riskType\":[\"\"],\"riskHandle\":[\"机械伤害\"],\"operateStandard\":[\"及时止血\"],\"notice\":[\"及时拨打120\"]}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2020-01-06 16:04:12');
INSERT INTO `sys_oper_log` VALUES (372, '安全隐患信息', 1, 'com.ruoyi.project.system.hiddendanger.controller.HiddendangerController.addSave()', 'POST', 1, 'admin', '徐州第1组', '/system/hiddendanger/add', '127.0.0.1', '内网IP', '{\"name\":[\"1\"],\"elaborate\":[\"设备故障\"],\"type\":[\"\"],\"suggestions\":[\"维修\"],\"deadline\":[\"2020-01-01\"],\"testPerson\":[\"秦佳\"],\"reviewResults\":[\"合格\"],\"reviewTime\":[\"2020-01-31\"],\"confirmSign\":[\"秦佳\"]}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2020-01-06 16:09:44');
INSERT INTO `sys_oper_log` VALUES (373, '安全隐患信息', 1, 'com.ruoyi.project.system.hiddendanger.controller.HiddendangerController.addSave()', 'POST', 1, 'admin', '徐州第1组', '/system/hiddendanger/add', '127.0.0.1', '内网IP', '{\"name\":[\"2\"],\"elaborate\":[\"用电\"],\"type\":[\"\"],\"suggestions\":[\"及时保修\"],\"deadline\":[\"2020-01-08\"],\"testPerson\":[\"秦佳\"],\"reviewResults\":[\"合格\"],\"reviewTime\":[\"2020-01-24\"],\"confirmSign\":[\"秦佳\"]}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2020-01-06 16:10:26');
INSERT INTO `sys_oper_log` VALUES (374, '安全隐患信息', 1, 'com.ruoyi.project.system.hiddendanger.controller.HiddendangerController.addSave()', 'POST', 1, 'admin', '徐州第1组', '/system/hiddendanger/add', '127.0.0.1', '内网IP', '{\"name\":[\"2\"],\"elaborate\":[\"用电\"],\"type\":[\"\"],\"suggestions\":[\"及时报修\"],\"deadline\":[\"2020-01-22\"],\"testPerson\":[\"秦佳\"],\"reviewResults\":[\"合格\"],\"reviewTime\":[\"2020-01-31\"],\"confirmSign\":[\"秦佳\"]}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2020-01-06 16:11:19');
INSERT INTO `sys_oper_log` VALUES (375, '安全隐患信息', 2, 'com.ruoyi.project.system.hiddendanger.controller.HiddendangerController.editSave()', 'POST', 1, 'admin', '徐州第1组', '/system/hiddendanger/edit', '127.0.0.1', '内网IP', '{\"dangerid\":[\"5\"],\"name\":[\"3\"],\"elaborate\":[\"电源开关\"],\"type\":[\"\"],\"suggestions\":[\"按时断电\"],\"deadline\":[\"2020-01-22\"],\"testPerson\":[\"秦佳\"],\"reviewResults\":[\"合格\"],\"reviewTime\":[\"2020-01-31\"],\"confirmSign\":[\"秦佳\"]}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2020-01-06 16:12:01');
INSERT INTO `sys_oper_log` VALUES (376, '安全隐患信息', 1, 'com.ruoyi.project.system.hiddendanger.controller.HiddendangerController.addSave()', 'POST', 1, 'admin', '徐州第1组', '/system/hiddendanger/add', '127.0.0.1', '内网IP', '{\"name\":[\"4\"],\"elaborate\":[\"灯管损坏\"],\"type\":[\"\"],\"suggestions\":[\"修整\"],\"deadline\":[\"2020-02-01\"],\"testPerson\":[\"秦佳\"],\"reviewResults\":[\"合格\"],\"reviewTime\":[\"2020-02-29\"],\"confirmSign\":[\"秦佳\"]}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2020-01-06 16:12:57');
INSERT INTO `sys_oper_log` VALUES (377, '安全隐患信息', 1, 'com.ruoyi.project.system.hiddendanger.controller.HiddendangerController.addSave()', 'POST', 1, 'admin', '徐州第1组', '/system/hiddendanger/add', '127.0.0.1', '内网IP', '{\"name\":[\"5\"],\"elaborate\":[\"仪器\"],\"type\":[\"\"],\"suggestions\":[\"及时修整\"],\"deadline\":[\"2020-03-02\"],\"testPerson\":[\"秦佳\"],\"reviewResults\":[\"合格\"],\"reviewTime\":[\"2020-04-30\"],\"confirmSign\":[\"秦佳\"]}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2020-01-06 16:13:46');
INSERT INTO `sys_oper_log` VALUES (378, '安全检查信息', 1, 'com.ruoyi.project.system.safecheck.controller.SafecheckController.addSave()', 'POST', 1, 'admin', '徐州第1组', '/system/safecheck/add', '127.0.0.1', '内网IP', '{\"name\":[\"屋面漏水检测\"],\"checkType\":[\"漏水检查\"],\"checkPerson\":[\"张经理\"],\"checkDepartment\":[\"安全监督部门\"],\"checkExplain\":[\"室外工程安全监督检查\"],\"checkResult\":[\"屋面不漏水\"],\"solution\":[\"无\"],\"checkTime\":[\"2019-09-03\"],\"sign\":[\"张经理\"]}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2020-01-06 16:20:47');
INSERT INTO `sys_oper_log` VALUES (379, '安全检查信息', 1, 'com.ruoyi.project.system.safecheck.controller.SafecheckController.addSave()', 'POST', 1, 'admin', '徐州第1组', '/system/safecheck/add', '127.0.0.1', '内网IP', '{\"name\":[\"室内环境检测\"],\"checkType\":[\"室内环境质量是否达标\"],\"checkPerson\":[\"沙监理\"],\"checkDepartment\":[\"监理部门\"],\"checkExplain\":[\"室内安全检查\"],\"checkResult\":[\"室内有害气体超标\"],\"solution\":[\"净化室内空气\"],\"checkTime\":[\"2019-10-29\"],\"sign\":[\"沙监理\"]}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2020-01-06 16:27:35');
INSERT INTO `sys_oper_log` VALUES (380, '风险识别信息', 1, 'com.ruoyi.project.system.riskidentification.controller.RiskidentificationController.addSave()', 'POST', 1, 'admin', '徐州第1组', '/system/riskidentification/add', '127.0.0.1', '内网IP', '{\"name\":[\"坍塌\"],\"type\":[\"坍塌\"],\"address\":[\"坍塌\"],\"state\":[\"过程中\"],\"discription\":[\"严格按照设计及批准的方案施工，及时支护和衬砌，严守隧道安全步距红线要求。\"],\"avoidMethod\":[\"加强超前地质预报和监控量测。加强施工支护、仰拱、衬砌施工的质量安全检查\"],\"riskLevel\":[\"中等\"],\"riskFactor\":[\"施工坍塌\"],\"time\":[\"2019-12-12\"]}', 'null', 1, '\r\n### Error updating database.  Cause: com.mysql.cj.jdbc.exceptions.MysqlDataTruncation: Data truncation: Data too long for column \'Discription\' at row 1\r\n### The error may involve com.ruoyi.project.system.riskidentification.mapper.RiskidentificationMapper.insertRiskidentification-Inline\r\n### The error occurred while setting parameters\r\n### SQL: insert into riskidentification          ( Name,             Type,             Address,             State,             Discription,             Avoid_method,             Risk_level,             Risk_factor,             Time )           values ( ?,             ?,             ?,             ?,             ?,             ?,             ?,             ?,             ? )\r\n### Cause: com.mysql.cj.jdbc.exceptions.MysqlDataTruncation: Data truncation: Data too long for column \'Discription\' at row 1\n; Data truncation: Data too long for column \'Discription\' at row 1; nested exception is com.mysql.cj.jdbc.exceptions.MysqlDataTruncation: Data truncation: Data too long for column \'Discription\' at row 1', '2020-01-06 16:38:11');
INSERT INTO `sys_oper_log` VALUES (381, '风险识别信息', 1, 'com.ruoyi.project.system.riskidentification.controller.RiskidentificationController.addSave()', 'POST', 1, 'admin', '徐州第1组', '/system/riskidentification/add', '127.0.0.1', '内网IP', '{\"name\":[\"坍塌\"],\"type\":[\"坍塌\"],\"address\":[\"坍塌\"],\"state\":[\"过程中\"],\"discription\":[\"严格按照设计及批准的方案施工\"],\"avoidMethod\":[\"加强超前地质预报和监控量测\"],\"riskLevel\":[\"中等\"],\"riskFactor\":[\"施工坍塌\"],\"time\":[\"2019-12-12\"]}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2020-01-06 16:38:34');
INSERT INTO `sys_oper_log` VALUES (382, '风险识别信息', 2, 'com.ruoyi.project.system.riskidentification.controller.RiskidentificationController.editSave()', 'POST', 1, 'admin', '徐州第1组', '/system/riskidentification/edit', '127.0.0.1', '内网IP', '{\"riskidentifyId\":[\"1\"],\"name\":[\"隧道坍塌\"],\"type\":[\"坍塌\"],\"address\":[\"隧道\"],\"state\":[\"过程中\"],\"discription\":[\"严格按照设计及批准的方案施工\"],\"avoidMethod\":[\"加强超前地质预报和监控量测\"],\"riskLevel\":[\"中等\"],\"riskFactor\":[\"施工坍塌\"],\"time\":[\"2019-12-12\"]}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2020-01-06 16:39:01');
INSERT INTO `sys_oper_log` VALUES (383, '风险识别信息', 1, 'com.ruoyi.project.system.riskidentification.controller.RiskidentificationController.addSave()', 'POST', 1, 'admin', '徐州第1组', '/system/riskidentification/add', '127.0.0.1', '内网IP', '{\"name\":[\"火工品爆炸\"],\"type\":[\"爆炸\"],\"address\":[\"隧道及运输存储过程\"],\"state\":[\"过程中\"],\"discription\":[\"统一指挥，并有经过专业培训持有爆破作业资质的证书的爆破员担任\"],\"avoidMethod\":[\"值守人员要经公安部门培训持证上岗，并严格执行火工品库房管理制度和安全操作规程\"],\"riskLevel\":[\"高级\"],\"riskFactor\":[\"易燃物品\"],\"time\":[\"2020-01-06\"]}', 'null', 1, '\r\n### Error updating database.  Cause: com.mysql.cj.jdbc.exceptions.MysqlDataTruncation: Data truncation: Data too long for column \'Discription\' at row 1\r\n### The error may involve com.ruoyi.project.system.riskidentification.mapper.RiskidentificationMapper.insertRiskidentification-Inline\r\n### The error occurred while setting parameters\r\n### SQL: insert into riskidentification          ( Name,             Type,             Address,             State,             Discription,             Avoid_method,             Risk_level,             Risk_factor,             Time )           values ( ?,             ?,             ?,             ?,             ?,             ?,             ?,             ?,             ? )\r\n### Cause: com.mysql.cj.jdbc.exceptions.MysqlDataTruncation: Data truncation: Data too long for column \'Discription\' at row 1\n; Data truncation: Data too long for column \'Discription\' at row 1; nested exception is com.mysql.cj.jdbc.exceptions.MysqlDataTruncation: Data truncation: Data too long for column \'Discription\' at row 1', '2020-01-06 16:40:57');
INSERT INTO `sys_oper_log` VALUES (384, '风险识别信息', 1, 'com.ruoyi.project.system.riskidentification.controller.RiskidentificationController.addSave()', 'POST', 1, 'admin', '徐州第1组', '/system/riskidentification/add', '127.0.0.1', '内网IP', '{\"name\":[\"火工品爆炸\"],\"type\":[\"爆炸\"],\"address\":[\"隧道及运输存储过程\"],\"state\":[\"过程中\"],\"discription\":[\"统一指挥，并有经过专业培训持有爆破作业资质的证书的爆破员担任\"],\"avoidMethod\":[\"值守人员要经公安部门培训持证上岗，\"],\"riskLevel\":[\"高级\"],\"riskFactor\":[\"易燃物品\"],\"time\":[\"2020-01-06\"]}', 'null', 1, '\r\n### Error updating database.  Cause: com.mysql.cj.jdbc.exceptions.MysqlDataTruncation: Data truncation: Data too long for column \'Discription\' at row 1\r\n### The error may involve com.ruoyi.project.system.riskidentification.mapper.RiskidentificationMapper.insertRiskidentification-Inline\r\n### The error occurred while setting parameters\r\n### SQL: insert into riskidentification          ( Name,             Type,             Address,             State,             Discription,             Avoid_method,             Risk_level,             Risk_factor,             Time )           values ( ?,             ?,             ?,             ?,             ?,             ?,             ?,             ?,             ? )\r\n### Cause: com.mysql.cj.jdbc.exceptions.MysqlDataTruncation: Data truncation: Data too long for column \'Discription\' at row 1\n; Data truncation: Data too long for column \'Discription\' at row 1; nested exception is com.mysql.cj.jdbc.exceptions.MysqlDataTruncation: Data truncation: Data too long for column \'Discription\' at row 1', '2020-01-06 16:41:05');
INSERT INTO `sys_oper_log` VALUES (385, '风险识别信息', 1, 'com.ruoyi.project.system.riskidentification.controller.RiskidentificationController.addSave()', 'POST', 1, 'admin', '徐州第1组', '/system/riskidentification/add', '127.0.0.1', '内网IP', '{\"name\":[\"火工品爆炸\"],\"type\":[\"爆炸\"],\"address\":[\"隧道及运输存储过程\"],\"state\":[\"过程中\"],\"discription\":[\"统一指挥，并有经过专业培训持有爆破作业资质的\"],\"avoidMethod\":[\"值守人员要经公安部门培训持证上岗，\"],\"riskLevel\":[\"高级\"],\"riskFactor\":[\"易燃物品\"],\"time\":[\"2020-01-06\"]}', 'null', 1, '\r\n### Error updating database.  Cause: com.mysql.cj.jdbc.exceptions.MysqlDataTruncation: Data truncation: Data too long for column \'Discription\' at row 1\r\n### The error may involve com.ruoyi.project.system.riskidentification.mapper.RiskidentificationMapper.insertRiskidentification-Inline\r\n### The error occurred while setting parameters\r\n### SQL: insert into riskidentification          ( Name,             Type,             Address,             State,             Discription,             Avoid_method,             Risk_level,             Risk_factor,             Time )           values ( ?,             ?,             ?,             ?,             ?,             ?,             ?,             ?,             ? )\r\n### Cause: com.mysql.cj.jdbc.exceptions.MysqlDataTruncation: Data truncation: Data too long for column \'Discription\' at row 1\n; Data truncation: Data too long for column \'Discription\' at row 1; nested exception is com.mysql.cj.jdbc.exceptions.MysqlDataTruncation: Data truncation: Data too long for column \'Discription\' at row 1', '2020-01-06 16:41:09');
INSERT INTO `sys_oper_log` VALUES (386, '风险识别信息', 1, 'com.ruoyi.project.system.riskidentification.controller.RiskidentificationController.addSave()', 'POST', 1, 'admin', '徐州第1组', '/system/riskidentification/add', '127.0.0.1', '内网IP', '{\"name\":[\"火工品爆炸\"],\"type\":[\"爆炸\"],\"address\":[\"隧道及运输\"],\"state\":[\"过程中\"],\"discription\":[\"统一指挥，并有经过专业培训持有爆破作业资质的\"],\"avoidMethod\":[\"值守人员要经公安部门培训持证上岗，\"],\"riskLevel\":[\"高级\"],\"riskFactor\":[\"易燃物品\"],\"time\":[\"2020-01-06\"]}', 'null', 1, '\r\n### Error updating database.  Cause: com.mysql.cj.jdbc.exceptions.MysqlDataTruncation: Data truncation: Data too long for column \'Discription\' at row 1\r\n### The error may involve com.ruoyi.project.system.riskidentification.mapper.RiskidentificationMapper.insertRiskidentification-Inline\r\n### The error occurred while setting parameters\r\n### SQL: insert into riskidentification          ( Name,             Type,             Address,             State,             Discription,             Avoid_method,             Risk_level,             Risk_factor,             Time )           values ( ?,             ?,             ?,             ?,             ?,             ?,             ?,             ?,             ? )\r\n### Cause: com.mysql.cj.jdbc.exceptions.MysqlDataTruncation: Data truncation: Data too long for column \'Discription\' at row 1\n; Data truncation: Data too long for column \'Discription\' at row 1; nested exception is com.mysql.cj.jdbc.exceptions.MysqlDataTruncation: Data truncation: Data too long for column \'Discription\' at row 1', '2020-01-06 16:41:17');
INSERT INTO `sys_oper_log` VALUES (387, '风险识别信息', 1, 'com.ruoyi.project.system.riskidentification.controller.RiskidentificationController.addSave()', 'POST', 1, 'admin', '徐州第1组', '/system/riskidentification/add', '127.0.0.1', '内网IP', '{\"name\":[\"火工品爆炸\"],\"type\":[\"爆炸\"],\"address\":[\"隧道及运输\"],\"state\":[\"过程中\"],\"discription\":[\"统一指挥，并有经过专业培训\"],\"avoidMethod\":[\"值守人员要经公安部门培训持证上岗，\"],\"riskLevel\":[\"高级\"],\"riskFactor\":[\"易燃物品\"],\"time\":[\"2020-01-06\"]}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2020-01-06 16:41:30');
INSERT INTO `sys_oper_log` VALUES (388, '菜单管理', 2, 'com.ruoyi.project.system.menu.controller.MenuController.editSave()', 'POST', 1, 'admin', '徐州第1组', '/system/menu/edit', '127.0.0.1', '内网IP', '{\"menuId\":[\"2000\"],\"parentId\":[\"0\"],\"menuType\":[\"C\"],\"menuName\":[\"现场安全检查管理\"],\"url\":[\"/system/notice\"],\"target\":[\"menuItem\"],\"perms\":[\"system:notice:view\"],\"orderNum\":[\"1\"],\"icon\":[\"fa fa-american-sign-language-interpreting\"],\"visible\":[\"0\"]}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2020-01-07 00:07:17');
INSERT INTO `sys_oper_log` VALUES (389, '菜单管理', 2, 'com.ruoyi.project.system.menu.controller.MenuController.editSave()', 'POST', 1, 'admin', '徐州第1组', '/system/menu/edit', '127.0.0.1', '内网IP', '{\"menuId\":[\"2000\"],\"parentId\":[\"0\"],\"menuType\":[\"C\"],\"menuName\":[\"安全检查管理\"],\"url\":[\"/system/notice\"],\"target\":[\"menuItem\"],\"perms\":[\"system:notice:view\"],\"orderNum\":[\"1\"],\"icon\":[\"fa fa-american-sign-language-interpreting\"],\"visible\":[\"0\"]}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2020-01-07 00:08:16');

-- ----------------------------
-- Table structure for sys_post
-- ----------------------------
DROP TABLE IF EXISTS `sys_post`;
CREATE TABLE `sys_post`  (
  `post_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '岗位ID',
  `post_code` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '岗位编码',
  `post_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '岗位名称',
  `post_sort` int(4) NOT NULL COMMENT '显示顺序',
  `status` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '状态（0正常 1停用）',
  `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`post_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '岗位信息表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_post
-- ----------------------------
INSERT INTO `sys_post` VALUES (1, 'admin', '系统管理员', 4, '0', 'admin', '2018-03-16 11:33:00', 'admin', '2019-12-30 01:28:51', '');
INSERT INTO `sys_post` VALUES (2, 'pm', '项目经理', 2, '0', 'admin', '2018-03-16 11:33:00', 'admin', '2019-12-30 01:25:19', '');
INSERT INTO `sys_post` VALUES (3, 'sv', '监理', 3, '0', 'admin', '2018-03-16 11:33:00', 'admin', '2019-12-30 01:25:26', '');
INSERT INTO `sys_post` VALUES (4, 'cp', '施工人员', 1, '0', 'admin', '2018-03-16 11:33:00', 'admin', '2019-12-30 01:28:41', '');

-- ----------------------------
-- Table structure for sys_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_role`;
CREATE TABLE `sys_role`  (
  `role_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '角色ID',
  `role_name` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '角色名称',
  `role_key` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '角色权限字符串',
  `role_sort` int(4) NOT NULL COMMENT '显示顺序',
  `data_scope` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '1' COMMENT '数据范围（1：全部数据权限 2：自定数据权限 3：本部门数据权限 4：本部门及以下数据权限）',
  `status` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '角色状态（0正常 1停用）',
  `del_flag` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0' COMMENT '删除标志（0代表存在 2代表删除）',
  `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`role_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 102 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '角色信息表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_role
-- ----------------------------
INSERT INTO `sys_role` VALUES (1, '管理员', 'admin', 1, '1', '0', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '管理员');
INSERT INTO `sys_role` VALUES (2, '普通员工', 'common', 4, '5', '0', '0', 'admin', '2018-03-16 11:33:00', 'admin', '2020-01-06 14:42:18', '普通员工只能查看本人数据');
INSERT INTO `sys_role` VALUES (100, '分公司领导', 'senior', 3, '3', '0', '0', 'admin', '2019-12-30 02:03:31', 'admin', '2020-01-03 09:55:34', '分公司领导可以查看本部门的数据');
INSERT INTO `sys_role` VALUES (101, '总公司领导', 'ceo', 2, '1', '0', '0', 'admin', '2019-12-30 02:06:20', 'admin', '2020-01-03 09:55:25', '总公司领导可以查看所有公司的数据');

-- ----------------------------
-- Table structure for sys_role_dept
-- ----------------------------
DROP TABLE IF EXISTS `sys_role_dept`;
CREATE TABLE `sys_role_dept`  (
  `role_id` bigint(20) NOT NULL COMMENT '角色ID',
  `dept_id` bigint(20) NOT NULL COMMENT '部门ID',
  PRIMARY KEY (`role_id`, `dept_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '角色和部门关联表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for sys_role_menu
-- ----------------------------
DROP TABLE IF EXISTS `sys_role_menu`;
CREATE TABLE `sys_role_menu`  (
  `role_id` bigint(20) NOT NULL COMMENT '角色ID',
  `menu_id` bigint(20) NOT NULL COMMENT '菜单ID',
  PRIMARY KEY (`role_id`, `menu_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '角色和菜单关联表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_role_menu
-- ----------------------------
INSERT INTO `sys_role_menu` VALUES (2, 1035);
INSERT INTO `sys_role_menu` VALUES (2, 2000);
INSERT INTO `sys_role_menu` VALUES (2, 2003);
INSERT INTO `sys_role_menu` VALUES (2, 2004);
INSERT INTO `sys_role_menu` VALUES (2, 2005);
INSERT INTO `sys_role_menu` VALUES (2, 2006);
INSERT INTO `sys_role_menu` VALUES (2, 2007);
INSERT INTO `sys_role_menu` VALUES (2, 2010);
INSERT INTO `sys_role_menu` VALUES (2, 2011);
INSERT INTO `sys_role_menu` VALUES (2, 2012);
INSERT INTO `sys_role_menu` VALUES (2, 2013);
INSERT INTO `sys_role_menu` VALUES (2, 2014);
INSERT INTO `sys_role_menu` VALUES (2, 2015);
INSERT INTO `sys_role_menu` VALUES (2, 2016);
INSERT INTO `sys_role_menu` VALUES (2, 2017);
INSERT INTO `sys_role_menu` VALUES (2, 2018);
INSERT INTO `sys_role_menu` VALUES (2, 2019);
INSERT INTO `sys_role_menu` VALUES (2, 2024);
INSERT INTO `sys_role_menu` VALUES (2, 2025);
INSERT INTO `sys_role_menu` VALUES (2, 2026);
INSERT INTO `sys_role_menu` VALUES (2, 2028);
INSERT INTO `sys_role_menu` VALUES (2, 2032);
INSERT INTO `sys_role_menu` VALUES (100, 1);
INSERT INTO `sys_role_menu` VALUES (100, 100);
INSERT INTO `sys_role_menu` VALUES (100, 1000);
INSERT INTO `sys_role_menu` VALUES (100, 1001);
INSERT INTO `sys_role_menu` VALUES (100, 1002);
INSERT INTO `sys_role_menu` VALUES (100, 1003);
INSERT INTO `sys_role_menu` VALUES (100, 1004);
INSERT INTO `sys_role_menu` VALUES (100, 1005);
INSERT INTO `sys_role_menu` VALUES (100, 1006);
INSERT INTO `sys_role_menu` VALUES (100, 1035);
INSERT INTO `sys_role_menu` VALUES (100, 1036);
INSERT INTO `sys_role_menu` VALUES (100, 1037);
INSERT INTO `sys_role_menu` VALUES (100, 1038);
INSERT INTO `sys_role_menu` VALUES (100, 2000);
INSERT INTO `sys_role_menu` VALUES (100, 2003);
INSERT INTO `sys_role_menu` VALUES (100, 2004);
INSERT INTO `sys_role_menu` VALUES (100, 2005);
INSERT INTO `sys_role_menu` VALUES (100, 2006);
INSERT INTO `sys_role_menu` VALUES (100, 2007);
INSERT INTO `sys_role_menu` VALUES (100, 2010);
INSERT INTO `sys_role_menu` VALUES (100, 2011);
INSERT INTO `sys_role_menu` VALUES (100, 2012);
INSERT INTO `sys_role_menu` VALUES (100, 2013);
INSERT INTO `sys_role_menu` VALUES (100, 2014);
INSERT INTO `sys_role_menu` VALUES (100, 2015);
INSERT INTO `sys_role_menu` VALUES (100, 2016);
INSERT INTO `sys_role_menu` VALUES (100, 2017);
INSERT INTO `sys_role_menu` VALUES (100, 2018);
INSERT INTO `sys_role_menu` VALUES (100, 2019);
INSERT INTO `sys_role_menu` VALUES (100, 2024);
INSERT INTO `sys_role_menu` VALUES (100, 2025);
INSERT INTO `sys_role_menu` VALUES (100, 2026);
INSERT INTO `sys_role_menu` VALUES (100, 2028);
INSERT INTO `sys_role_menu` VALUES (100, 2029);
INSERT INTO `sys_role_menu` VALUES (100, 2030);
INSERT INTO `sys_role_menu` VALUES (100, 2031);
INSERT INTO `sys_role_menu` VALUES (100, 2032);
INSERT INTO `sys_role_menu` VALUES (101, 1);
INSERT INTO `sys_role_menu` VALUES (101, 100);
INSERT INTO `sys_role_menu` VALUES (101, 1000);
INSERT INTO `sys_role_menu` VALUES (101, 1001);
INSERT INTO `sys_role_menu` VALUES (101, 1002);
INSERT INTO `sys_role_menu` VALUES (101, 1003);
INSERT INTO `sys_role_menu` VALUES (101, 1004);
INSERT INTO `sys_role_menu` VALUES (101, 1005);
INSERT INTO `sys_role_menu` VALUES (101, 1006);
INSERT INTO `sys_role_menu` VALUES (101, 1035);
INSERT INTO `sys_role_menu` VALUES (101, 1036);
INSERT INTO `sys_role_menu` VALUES (101, 1037);
INSERT INTO `sys_role_menu` VALUES (101, 1038);
INSERT INTO `sys_role_menu` VALUES (101, 2000);
INSERT INTO `sys_role_menu` VALUES (101, 2003);
INSERT INTO `sys_role_menu` VALUES (101, 2004);
INSERT INTO `sys_role_menu` VALUES (101, 2005);
INSERT INTO `sys_role_menu` VALUES (101, 2006);
INSERT INTO `sys_role_menu` VALUES (101, 2007);
INSERT INTO `sys_role_menu` VALUES (101, 2010);
INSERT INTO `sys_role_menu` VALUES (101, 2011);
INSERT INTO `sys_role_menu` VALUES (101, 2012);
INSERT INTO `sys_role_menu` VALUES (101, 2013);
INSERT INTO `sys_role_menu` VALUES (101, 2014);
INSERT INTO `sys_role_menu` VALUES (101, 2015);
INSERT INTO `sys_role_menu` VALUES (101, 2016);
INSERT INTO `sys_role_menu` VALUES (101, 2017);
INSERT INTO `sys_role_menu` VALUES (101, 2018);
INSERT INTO `sys_role_menu` VALUES (101, 2019);
INSERT INTO `sys_role_menu` VALUES (101, 2024);
INSERT INTO `sys_role_menu` VALUES (101, 2025);
INSERT INTO `sys_role_menu` VALUES (101, 2026);
INSERT INTO `sys_role_menu` VALUES (101, 2028);
INSERT INTO `sys_role_menu` VALUES (101, 2029);
INSERT INTO `sys_role_menu` VALUES (101, 2030);
INSERT INTO `sys_role_menu` VALUES (101, 2031);
INSERT INTO `sys_role_menu` VALUES (101, 2032);

-- ----------------------------
-- Table structure for sys_user
-- ----------------------------
DROP TABLE IF EXISTS `sys_user`;
CREATE TABLE `sys_user`  (
  `user_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '用户ID',
  `dept_id` bigint(20) NULL DEFAULT NULL COMMENT '部门ID',
  `login_name` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '登录账号',
  `user_name` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '用户昵称',
  `user_type` varchar(2) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '00' COMMENT '用户类型（00系统用户）',
  `email` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '用户邮箱',
  `phonenumber` varchar(11) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '手机号码',
  `sex` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0' COMMENT '用户性别（0男 1女 2未知）',
  `avatar` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '头像路径',
  `password` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '密码',
  `salt` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '盐加密',
  `status` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0' COMMENT '帐号状态（0正常 1停用）',
  `del_flag` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0' COMMENT '删除标志（0代表存在 2代表删除）',
  `login_ip` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '最后登陆IP',
  `login_date` datetime(0) NULL DEFAULT NULL COMMENT '最后登陆时间',
  `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`user_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 122 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '用户信息表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_user
-- ----------------------------
INSERT INTO `sys_user` VALUES (1, 103, 'admin', '陈贺', '00', 'ch@xzit.com', '19851668369', '0', '/profile/avatar/2019/12/29/d1c8a0e72540f4e349981ed23a03ed96.png', '2297674054f369cf4732193a2ce3611e', '45cd99', '0', '0', '127.0.0.1', '2020-01-07 00:55:22', 'admin', '2019-12-28 11:33:00', 'ry', '2020-01-07 00:55:21', '管理员');
INSERT INTO `sys_user` VALUES (2, 103, 'chenhe1', '陈贺1', '00', 'chenhe1@qq.com', '19888888888', '0', '', '595546e6ad03691e94522aba7a6f08a2', '', '0', '0', '127.0.0.1', '2020-01-06 14:42:49', 'admin', '2019-12-30 01:55:39', '', '2020-01-06 14:42:48', NULL);
INSERT INTO `sys_user` VALUES (3, 103, 'guozhou1', '郭宙1', '00', 'guozhou1@qq.com', '19888888888', '0', '', 'c68a3282870daf08e55e151aa27020b5', '', '0', '0', '', NULL, 'admin', '2019-12-30 01:48:34', 'admin', '2019-12-30 01:55:39', NULL);
INSERT INTO `sys_user` VALUES (4, 103, 'qinjia1', '秦佳1', '00', 'qinjia1@qq.com', '19888888888', '1', '', '6e5c753ca2a9ba4e91fb1de0584c8f0c', '', '0', '0', '127.0.0.1', '2019-12-30 13:56:06', 'admin', '2019-12-30 01:48:34', 'admin', '2019-12-30 13:56:06', NULL);
INSERT INTO `sys_user` VALUES (5, 103, 'dongjuanjuan1', '董娟娟1', '00', 'dongjuanjuan1@qq.com', '19888888888', '1', '', '5089ee8a599fb3d15a44a95aea1c5702', '', '0', '0', '', NULL, 'admin', '2019-12-30 01:48:34', 'admin', '2019-12-30 01:55:39', NULL);
INSERT INTO `sys_user` VALUES (6, 103, 'shatianwei1', '沙天威1', '00', 'shatianwei1@qq.com', '19888888888', '0', '', '5280720ba9a6ba0881dac7540790150d', '', '0', '0', '', NULL, 'admin', '2019-12-30 01:48:34', 'admin', '2019-12-30 01:55:39', NULL);
INSERT INTO `sys_user` VALUES (7, 104, 'chenhe2', '陈贺2', '00', 'chenhe2@qq.com', '19888888888', '0', '', '4d10d4bc961536a035eada67f2a8bfb3', '', '0', '0', '127.0.0.1', '2019-12-30 21:08:29', 'admin', '2019-12-30 01:48:34', 'admin', '2019-12-30 21:08:28', NULL);
INSERT INTO `sys_user` VALUES (8, 104, 'guozhou2', '郭宙2', '00', 'guozhou2@qq.com', '19888888888', '0', '', 'dcd3b86b5e25783eafbb266e95ab8045', '', '0', '0', '', NULL, 'admin', '2019-12-30 01:48:34', 'admin', '2019-12-30 01:55:39', NULL);
INSERT INTO `sys_user` VALUES (9, 104, 'qinjia2', '秦佳2', '00', 'qinjia2@qq.com', '19888888888', '1', '', 'f8cd586348c4b0cf559f1e4a1d6aca0f', '', '0', '0', '', NULL, 'admin', '2019-12-30 01:48:34', 'admin', '2019-12-30 01:55:39', NULL);
INSERT INTO `sys_user` VALUES (10, 104, 'dongjuanjuan2', '董娟娟2', '00', 'dongjuanjuan2@qq.com', '19888888888', '1', '', '39e5ffa49f2e48df8dd55265bd9fc807', '', '0', '0', '', NULL, 'admin', '2019-12-30 01:48:34', 'admin', '2019-12-30 01:55:39', NULL);
INSERT INTO `sys_user` VALUES (11, 104, 'shatianwei2', '沙天威2', '00', 'shatianwei2@qq.com', '19888888888', '0', '', '78685581a0bcf3af607c0762a7fad7cb', '', '0', '0', '', NULL, 'admin', '2019-12-30 01:48:34', 'admin', '2019-12-30 01:55:39', NULL);
INSERT INTO `sys_user` VALUES (12, 201, 'chenhe3', '陈贺3', '00', 'chenhe3@qq.com', '19888888888', '0', '', '783efbd1d9026417f7e592d96e530e8d', '', '0', '0', '127.0.0.1', '2020-01-06 14:42:26', 'admin', '2019-12-30 01:48:34', 'admin', '2020-01-06 14:42:26', NULL);
INSERT INTO `sys_user` VALUES (13, 201, 'guozhou3', '郭宙3', '00', 'guozhou3@qq.com', '19888888888', '0', '', 'f7a39630238bdf4280d67488f3219764', '', '0', '0', '', NULL, 'admin', '2019-12-30 01:48:34', 'admin', '2019-12-30 01:55:39', NULL);
INSERT INTO `sys_user` VALUES (14, 201, 'qinjia3', '秦佳3', '00', 'qinjia3@qq.com', '19888888888', '1', '', 'b71fc103d795381546f9919ef11e5c31', '', '0', '0', '', NULL, 'admin', '2019-12-30 01:48:34', 'admin', '2019-12-30 01:55:39', NULL);
INSERT INTO `sys_user` VALUES (15, 201, 'dongjuanjuan3', '董娟娟3', '00', 'dongjuanjuan3@qq.com', '19888888888', '1', '', '37cabd5f7687c573f5740076f59afdad', '', '0', '0', '', NULL, 'admin', '2019-12-30 01:48:34', 'admin', '2019-12-30 01:55:39', NULL);
INSERT INTO `sys_user` VALUES (16, 201, 'shatianwei3', '沙天威3', '00', 'shatianwei3@qq.com', '19888888888', '0', '', '53d8e5665abf092b8ae089f049fba286', '', '0', '0', '', NULL, 'admin', '2019-12-30 01:48:34', 'admin', '2019-12-30 01:55:39', NULL);
INSERT INTO `sys_user` VALUES (17, 202, 'chenhe4', '陈贺4', '00', 'chenhe4@qq.com', '19888888888', '0', '', '03ad885e56927a2b1e7a763723dc9944', '', '0', '0', '', NULL, 'admin', '2019-12-30 01:48:34', 'admin', '2019-12-30 01:55:39', NULL);
INSERT INTO `sys_user` VALUES (18, 202, 'guozhou4', '郭宙4', '00', 'guozhou4@qq.com', '19888888888', '0', '', '7af79348d9cd9f29c3c7d924f2ef3c23', '', '0', '0', '', NULL, 'admin', '2019-12-30 01:48:34', 'admin', '2019-12-30 01:55:39', NULL);
INSERT INTO `sys_user` VALUES (19, 202, 'qinjia4', '秦佳4', '00', 'qinjia4@qq.com', '19888888888', '1', '', '87ac6da3c1f4f520bcd43d14b47da1b2', '', '0', '0', '', NULL, 'admin', '2019-12-30 01:48:34', 'admin', '2019-12-30 01:55:39', NULL);
INSERT INTO `sys_user` VALUES (20, 202, 'dongjuanjuan4', '董娟娟4', '00', 'dongjuanjuan4@qq.com', '19888888888', '1', '', 'a20ed384e7d54cb8678797a60317cfee', '', '0', '0', '', NULL, 'admin', '2019-12-30 01:48:34', 'admin', '2019-12-30 01:55:39', NULL);
INSERT INTO `sys_user` VALUES (21, 202, 'shatianwei4', '沙天威4', '00', 'shatianwei4@qq.com', '19888888888', '0', '', '116e43e5d9e4ae4808a770c8393fde63', '', '0', '0', '', NULL, 'admin', '2019-12-30 01:48:34', 'admin', '2019-12-30 01:55:39', NULL);
INSERT INTO `sys_user` VALUES (22, 108, 'chenhe5', '陈贺5', '00', 'chenhe5@qq.com', '19888888888', '0', '', '5b75b971bdb0c0316f3c062cd22cbb14', '', '0', '0', '', NULL, 'admin', '2019-12-30 01:48:34', 'admin', '2019-12-30 01:55:39', NULL);
INSERT INTO `sys_user` VALUES (23, 108, 'guozhou5', '郭宙5', '00', 'guozhou5@qq.com', '19888888888', '0', '', '172d9e31d067314f4682484f814dcda7', '', '0', '0', '', NULL, 'admin', '2019-12-30 01:48:34', 'admin', '2019-12-30 01:55:39', NULL);
INSERT INTO `sys_user` VALUES (24, 108, 'qinjia5', '秦佳5', '00', 'qinjia5@qq.com', '19888888888', '1', '', '2e6ad18a36047e04c3bc7646b944fe93', '', '0', '0', '', NULL, 'admin', '2019-12-30 01:48:34', 'admin', '2019-12-30 01:55:39', NULL);
INSERT INTO `sys_user` VALUES (25, 108, 'dongjuanjuan5', '董娟娟5', '00', 'dongjuanjuan5@qq.com', '19888888888', '1', '', '1db3c8394edc7c546fdd9187ee2f42da', '', '0', '0', '', NULL, 'admin', '2019-12-30 01:48:34', 'admin', '2019-12-30 01:55:39', NULL);
INSERT INTO `sys_user` VALUES (26, 108, 'shatianwei5', '沙天威5', '00', 'shatianwei5@qq.com', '19888888888', '0', '', 'ae1e68611dcaccd775cbd99ca1b28707', '', '0', '0', '', NULL, 'admin', '2019-12-30 01:48:34', 'admin', '2019-12-30 01:55:39', NULL);
INSERT INTO `sys_user` VALUES (27, 109, 'chenhe6', '陈贺6', '00', 'chenhe6@qq.com', '19888888888', '0', '', 'ef499d3be7c213c23f5d1a9806cb4aef', '', '0', '0', '', NULL, 'admin', '2019-12-30 01:48:34', 'admin', '2019-12-30 01:55:39', NULL);
INSERT INTO `sys_user` VALUES (28, 109, 'guozhou6', '郭宙6', '00', 'guozhou6@qq.com', '19888888888', '0', '', '3df63d5ca7b2a483b5f97a492c845313', '', '0', '0', '', NULL, 'admin', '2019-12-30 01:48:34', 'admin', '2019-12-30 01:55:39', NULL);
INSERT INTO `sys_user` VALUES (29, 109, 'qinjia6', '秦佳6', '00', 'qinjia6@qq.com', '19888888888', '1', '', '066854fe3f9c9a6e326fe22e56d84df7', '', '0', '0', '', NULL, 'admin', '2019-12-30 01:48:34', 'admin', '2019-12-30 01:55:39', NULL);
INSERT INTO `sys_user` VALUES (30, 109, 'dongjuanjuan6', '董娟娟6', '00', 'dongjuanjuan6@qq.com', '19888888888', '1', '', 'e7b984645d2761c0f547fbc877070282', '', '0', '0', '', NULL, 'admin', '2019-12-30 01:48:34', 'admin', '2019-12-30 01:55:39', NULL);
INSERT INTO `sys_user` VALUES (31, 109, 'shatianwei6', '沙天威6', '00', 'shatianwei6@qq.com', '19888888888', '0', '', '869b76f5687bef48f5e77033cbe30571', '', '0', '0', '', NULL, 'admin', '2019-12-30 01:48:34', 'admin', '2019-12-30 01:55:39', NULL);
INSERT INTO `sys_user` VALUES (32, 103, 'chenhe7', '陈贺7', '00', 'chenhe7@qq.com', '19888888888', '0', '', '8a58c77e149c6ec9c06a39c934d27086', '', '0', '0', '', NULL, 'admin', '2019-12-30 01:48:34', 'admin', '2019-12-30 01:55:39', NULL);
INSERT INTO `sys_user` VALUES (33, 103, 'guozhou7', '郭宙7', '00', 'guozhou7@qq.com', '19888888888', '0', '', '82ffd9106b487a4b62d4717759b7e52f', '', '0', '0', '', NULL, 'admin', '2019-12-30 01:48:34', 'admin', '2019-12-30 01:55:39', NULL);
INSERT INTO `sys_user` VALUES (34, 103, 'qinjia7', '秦佳7', '00', 'qinjia7@qq.com', '19888888888', '1', '', '62e8dcd8b8754d25a249b75a1c0daea4', '', '0', '0', '', NULL, 'admin', '2019-12-30 01:48:34', 'admin', '2019-12-30 01:55:39', NULL);
INSERT INTO `sys_user` VALUES (35, 103, 'dongjuanjuan7', '董娟娟7', '00', 'dongjuanjuan7@qq.com', '19888888888', '1', '', '8a9c91e8e606416b52bf6d9c0d232d33', '', '0', '0', '', NULL, 'admin', '2019-12-30 01:48:34', 'admin', '2019-12-30 01:55:39', NULL);
INSERT INTO `sys_user` VALUES (36, 103, 'shatianwei7', '沙天威7', '00', 'shatianwei7@qq.com', '19888888888', '0', '', '6c73d00ec5a923b40ed4e9bf3df8aff1', '', '0', '0', '', NULL, 'admin', '2019-12-30 01:48:34', 'admin', '2019-12-30 01:55:39', NULL);
INSERT INTO `sys_user` VALUES (37, 104, 'chenhe8', '陈贺8', '00', 'chenhe8@qq.com', '19888888888', '0', '', 'ae4bef7ff5acf5c479a53ff0ed2a18e5', '', '0', '0', '', NULL, 'admin', '2019-12-30 01:48:34', 'admin', '2019-12-30 01:55:39', NULL);
INSERT INTO `sys_user` VALUES (38, 104, 'guozhou8', '郭宙8', '00', 'guozhou8@qq.com', '19888888888', '0', '', '0a890de61677bedce39f994c2eff8901', '', '0', '0', '', NULL, 'admin', '2019-12-30 01:48:34', 'admin', '2019-12-30 01:55:39', NULL);
INSERT INTO `sys_user` VALUES (39, 104, 'qinjia8', '秦佳8', '00', 'qinjia8@qq.com', '19888888888', '1', '', '9574f42b9887e61f12c1a10acfcd77c3', '', '0', '0', '', NULL, 'admin', '2019-12-30 01:48:34', 'admin', '2019-12-30 01:55:39', NULL);
INSERT INTO `sys_user` VALUES (40, 104, 'dongjuanjuan8', '董娟娟8', '00', 'dongjuanjuan8@qq.com', '19888888888', '1', '', '4fdfc74cd1f84cd71759c7482b5cd07e', '', '0', '0', '', NULL, 'admin', '2019-12-30 01:48:34', 'admin', '2019-12-30 01:55:39', NULL);
INSERT INTO `sys_user` VALUES (41, 104, 'shatianwei8', '沙天威8', '00', 'shatianwei8@qq.com', '19888888888', '0', '', '368b93d0ef57aeff3813316d21e429d0', '', '0', '0', '', NULL, 'admin', '2019-12-30 01:48:34', 'admin', '2019-12-30 01:55:39', NULL);
INSERT INTO `sys_user` VALUES (42, 201, 'chenhe9', '陈贺9', '00', 'chenhe9@qq.com', '19888888888', '0', '', 'e43822dfcfe294268224b92042894e19', '', '0', '0', '', NULL, 'admin', '2019-12-30 01:48:34', 'admin', '2019-12-30 01:55:39', NULL);
INSERT INTO `sys_user` VALUES (43, 201, 'guozhou9', '郭宙9', '00', 'guozhou9@qq.com', '19888888888', '0', '', 'b9d677aa9179378782568835c23aa6a5', '', '0', '0', '', NULL, 'admin', '2019-12-30 01:48:34', 'admin', '2019-12-30 01:55:39', NULL);
INSERT INTO `sys_user` VALUES (44, 201, 'qinjia9', '秦佳9', '00', 'qinjia9@qq.com', '19888888888', '1', '', '7675eb43192ae26072d63f4505d515ae', '', '0', '0', '', NULL, 'admin', '2019-12-30 01:48:34', 'admin', '2019-12-30 01:55:39', NULL);
INSERT INTO `sys_user` VALUES (45, 201, 'dongjuanjuan9', '董娟娟9', '00', 'dongjuanjuan9@qq.com', '19888888888', '1', '', '3efb26a6a49a2e111cfa84a9f97de925', '', '0', '0', '', NULL, 'admin', '2019-12-30 01:48:34', 'admin', '2019-12-30 01:55:39', NULL);
INSERT INTO `sys_user` VALUES (46, 201, 'shatianwei9', '沙天威9', '00', 'shatianwei9@qq.com', '19888888888', '0', '', '60a2ae28ea12668d3ff43120d8045935', '', '0', '0', '', NULL, 'admin', '2019-12-30 01:48:34', 'admin', '2019-12-30 01:55:39', NULL);
INSERT INTO `sys_user` VALUES (47, 202, 'chenhe10', '陈贺10', '00', 'chenhe10@qq.com', '19888888888', '0', '', '82bfcb2fc33123ed430601677803f1bf', '', '0', '0', '', NULL, 'admin', '2019-12-30 01:48:34', 'admin', '2019-12-30 01:55:39', NULL);
INSERT INTO `sys_user` VALUES (48, 202, 'guozhou10', '郭宙10', '00', 'guozhou10@qq.com', '19888888888', '0', '', '69483f309481d24bdeef7dac73de3baf', '', '0', '0', '', NULL, 'admin', '2019-12-30 01:48:34', 'admin', '2019-12-30 01:55:39', NULL);
INSERT INTO `sys_user` VALUES (49, 202, 'qinjia10', '秦佳10', '00', 'qinjia10@qq.com', '19888888888', '1', '', '03c76ea99b2890511f28f50ba985bb73', '', '0', '0', '', NULL, 'admin', '2019-12-30 01:48:34', 'admin', '2019-12-30 01:55:39', NULL);
INSERT INTO `sys_user` VALUES (50, 202, 'dongjuanjuan10', '董娟娟10', '00', 'dongjuanjuan10@qq.com', '19888888888', '1', '', 'fbd7426d35659bbc5d02eadafd337f94', '', '0', '0', '', NULL, 'admin', '2019-12-30 01:48:34', 'admin', '2019-12-30 01:55:39', NULL);
INSERT INTO `sys_user` VALUES (51, 202, 'shatianwei10', '沙天威10', '00', 'shatianwei10@qq.com', '19888888888', '0', '', '8836bd9a0712bfa606024936716ae0fe', '', '0', '0', '', NULL, 'admin', '2019-12-30 01:48:34', 'admin', '2019-12-30 01:55:39', NULL);
INSERT INTO `sys_user` VALUES (52, 108, 'chenhe11', '陈贺11', '00', 'chenhe11@qq.com', '19888888888', '0', '', 'bd639f43efd1c9ed02585a6063bd54e2', '', '0', '0', '', NULL, 'admin', '2019-12-30 01:48:34', 'admin', '2019-12-30 01:55:39', NULL);
INSERT INTO `sys_user` VALUES (53, 108, 'guozhou11', '郭宙11', '00', 'guozhou11@qq.com', '19888888888', '0', '', 'a94447ed725b5485284812385e84f8df', '', '0', '0', '', NULL, 'admin', '2019-12-30 01:48:34', 'admin', '2019-12-30 01:55:40', NULL);
INSERT INTO `sys_user` VALUES (54, 108, 'qinjia11', '秦佳11', '00', 'qinjia11@qq.com', '19888888888', '1', '', '07ffecc5a3dd55a6ac0afc910df75274', '', '0', '0', '', NULL, 'admin', '2019-12-30 01:48:34', 'admin', '2019-12-30 01:55:40', NULL);
INSERT INTO `sys_user` VALUES (55, 108, 'dongjuanjuan11', '董娟娟11', '00', 'dongjuanjuan11@qq.com', '19888888888', '1', '', '8c5133872b452011c9621cdcf64fc3fd', '', '0', '0', '', NULL, 'admin', '2019-12-30 01:48:34', 'admin', '2019-12-30 01:55:40', NULL);
INSERT INTO `sys_user` VALUES (56, 108, 'shatianwei11', '沙天威11', '00', 'shatianwei11@qq.com', '19888888888', '0', '', 'cf47e6a4602f15f0f34bedfa82e1974a', '', '0', '0', '', NULL, 'admin', '2019-12-30 01:48:34', 'admin', '2019-12-30 01:55:40', NULL);
INSERT INTO `sys_user` VALUES (57, 109, 'chenhe12', '陈贺12', '00', 'chenhe12@qq.com', '19888888888', '0', '', 'f0a0903f72407aabf6c111b3953e57e0', '', '0', '0', '', NULL, 'admin', '2019-12-30 01:48:34', 'admin', '2019-12-30 01:55:40', NULL);
INSERT INTO `sys_user` VALUES (58, 109, 'guozhou12', '郭宙12', '00', 'guozhou12@qq.com', '19888888888', '0', '', 'bfd6b9b94a8a8f62cb7c25ddc89addb6', '', '0', '0', '', NULL, 'admin', '2019-12-30 01:48:34', 'admin', '2019-12-30 01:55:40', NULL);
INSERT INTO `sys_user` VALUES (59, 109, 'qinjia12', '秦佳12', '00', 'qinjia12@qq.com', '19888888888', '1', '', 'd0a05c6446377296c0b9f85e4da2651a', '', '0', '0', '', NULL, 'admin', '2019-12-30 01:48:34', 'admin', '2019-12-30 01:55:40', NULL);
INSERT INTO `sys_user` VALUES (60, 109, 'dongjuanjuan12', '董娟娟12', '00', 'dongjuanjuan12@qq.com', '19888888888', '1', '', '166228ed2aa070468f91fda90592babd', '', '0', '0', '', NULL, 'admin', '2019-12-30 01:48:34', 'admin', '2019-12-30 01:55:40', NULL);
INSERT INTO `sys_user` VALUES (61, 109, 'shatianwei12', '沙天威12', '00', 'shatianwei12@qq.com', '19888888888', '0', '', '8b9b86f422f5b85b51661e1d7fe4187e', '', '0', '0', '', NULL, 'admin', '2019-12-30 01:48:34', 'admin', '2019-12-30 01:55:40', NULL);
INSERT INTO `sys_user` VALUES (62, 103, 'chenhe13', '陈贺13', '00', 'chenhe13@qq.com', '19888888888', '0', '', '2693d41b2564302f908b7d11f3c47ff7', '', '0', '0', '', NULL, 'admin', '2019-12-30 01:48:34', 'admin', '2019-12-30 01:55:40', NULL);
INSERT INTO `sys_user` VALUES (63, 103, 'guozhou13', '郭宙13', '00', 'guozhou13@qq.com', '19888888888', '0', '', '694965ee5f25d9da137dc1cd6e86e7be', '', '0', '0', '', NULL, 'admin', '2019-12-30 01:48:34', 'admin', '2019-12-30 01:55:40', NULL);
INSERT INTO `sys_user` VALUES (64, 103, 'qinjia13', '秦佳13', '00', 'qinjia13@qq.com', '19888888888', '1', '', 'e208932abe773c995a111ace43afdc3d', '', '0', '0', '', NULL, 'admin', '2019-12-30 01:48:34', 'admin', '2019-12-30 01:55:40', NULL);
INSERT INTO `sys_user` VALUES (65, 103, 'dongjuanjuan13', '董娟娟13', '00', 'dongjuanjuan13@qq.com', '19888888888', '1', '', '2bb838b2fac940eebf74038588297729', '', '0', '0', '', NULL, 'admin', '2019-12-30 01:48:35', 'admin', '2019-12-30 01:55:40', NULL);
INSERT INTO `sys_user` VALUES (66, 103, 'shatianwei13', '沙天威13', '00', 'shatianwei13@qq.com', '19888888888', '0', '', '5378356d72e4f446cc71744ea3fdfefa', '', '0', '0', '', NULL, 'admin', '2019-12-30 01:48:35', 'admin', '2019-12-30 01:55:40', NULL);
INSERT INTO `sys_user` VALUES (67, 104, 'chenhe14', '陈贺14', '00', 'chenhe14@qq.com', '19888888888', '0', '', 'd8455ac0678d3973db9dbb79c3dca5ce', '', '0', '0', '', NULL, 'admin', '2019-12-30 01:48:35', 'admin', '2019-12-30 01:55:40', NULL);
INSERT INTO `sys_user` VALUES (68, 104, 'guozhou14', '郭宙14', '00', 'guozhou14@qq.com', '19888888888', '0', '', 'd2fded0c724d761937bc1bee565875e7', '', '0', '0', '', NULL, 'admin', '2019-12-30 01:48:35', 'admin', '2019-12-30 01:55:40', NULL);
INSERT INTO `sys_user` VALUES (69, 104, 'qinjia14', '秦佳14', '00', 'qinjia14@qq.com', '19888888888', '1', '', '931193a5939b142e44031a341b9754a9', '', '0', '0', '', NULL, 'admin', '2019-12-30 01:48:35', 'admin', '2019-12-30 01:55:40', NULL);
INSERT INTO `sys_user` VALUES (70, 104, 'dongjuanjuan14', '董娟娟14', '00', 'dongjuanjuan14@qq.com', '19888888888', '1', '', '38b40d2d00a4d0ecdeecdd5a215f50fe', '', '0', '0', '', NULL, 'admin', '2019-12-30 01:48:35', 'admin', '2019-12-30 01:55:40', NULL);
INSERT INTO `sys_user` VALUES (71, 104, 'shatianwei14', '沙天威14', '00', 'shatianwei14@qq.com', '19888888888', '0', '', 'bc20179a7d2f6ad07e3effe83fda65ad', '', '0', '0', '', NULL, 'admin', '2019-12-30 01:48:35', 'admin', '2019-12-30 01:55:40', NULL);
INSERT INTO `sys_user` VALUES (72, 201, 'chenhe15', '陈贺15', '00', 'chenhe15@qq.com', '19888888888', '0', '', 'dd1da119310be9acde8b6e423128a7e9', '', '0', '0', '', NULL, 'admin', '2019-12-30 01:48:35', 'admin', '2019-12-30 01:55:40', NULL);
INSERT INTO `sys_user` VALUES (73, 201, 'guozhou15', '郭宙15', '00', 'guozhou15@qq.com', '19888888888', '0', '', '541399a5519fbb8830d1e032ee60e988', '', '0', '0', '', NULL, 'admin', '2019-12-30 01:48:35', 'admin', '2019-12-30 01:55:40', NULL);
INSERT INTO `sys_user` VALUES (74, 201, 'qinjia15', '秦佳15', '00', 'qinjia15@qq.com', '19888888888', '1', '', '2452053f010accaa98de1f0021116fe6', '', '0', '0', '', NULL, 'admin', '2019-12-30 01:48:35', 'admin', '2019-12-30 01:55:40', NULL);
INSERT INTO `sys_user` VALUES (75, 201, 'dongjuanjuan15', '董娟娟15', '00', 'dongjuanjuan15@qq.com', '19888888888', '1', '', '249f4a4da462c4e6a4fc4dbe3d63706b', '', '0', '0', '', NULL, 'admin', '2019-12-30 01:48:35', 'admin', '2019-12-30 01:55:40', NULL);
INSERT INTO `sys_user` VALUES (76, 201, 'shatianwei15', '沙天威15', '00', 'shatianwei15@qq.com', '19888888888', '0', '', 'f37e7d293a372be9fbc993b7b8626620', '', '0', '0', '', NULL, 'admin', '2019-12-30 01:48:35', 'admin', '2019-12-30 01:55:40', NULL);
INSERT INTO `sys_user` VALUES (77, 202, 'chenhe16', '陈贺16', '00', 'chenhe16@qq.com', '19888888888', '0', '', 'ee2e2c3ef4a448e7b9b10295e8dfd704', '', '0', '0', '', NULL, 'admin', '2019-12-30 01:48:35', 'admin', '2019-12-30 01:55:40', NULL);
INSERT INTO `sys_user` VALUES (78, 202, 'guozhou16', '郭宙16', '00', 'guozhou16@qq.com', '19888888888', '0', '', '0ed1f842ff7c37da593245f1d538aba1', '', '0', '0', '', NULL, 'admin', '2019-12-30 01:48:35', 'admin', '2019-12-30 01:55:40', NULL);
INSERT INTO `sys_user` VALUES (79, 202, 'qinjia16', '秦佳16', '00', 'qinjia16@qq.com', '19888888888', '1', '', '1d69547c93746bc30ddd4aaa16af3e67', '', '0', '0', '', NULL, 'admin', '2019-12-30 01:48:35', 'admin', '2019-12-30 01:55:40', NULL);
INSERT INTO `sys_user` VALUES (80, 202, 'dongjuanjuan16', '董娟娟16', '00', 'dongjuanjuan16@qq.com', '19888888888', '1', '', '681265a13e5ab7d6f66c9c59c6487279', '', '0', '0', '', NULL, 'admin', '2019-12-30 01:48:35', 'admin', '2019-12-30 01:55:40', NULL);
INSERT INTO `sys_user` VALUES (81, 202, 'shatianwei16', '沙天威16', '00', 'shatianwei16@qq.com', '19888888888', '0', '', 'd2f34d1e26a1dc4adbd4584bf4f4fad1', '', '0', '0', '', NULL, 'admin', '2019-12-30 01:48:35', 'admin', '2019-12-30 01:55:40', NULL);
INSERT INTO `sys_user` VALUES (82, 108, 'chenhe17', '陈贺17', '00', 'chenhe17@qq.com', '19888888888', '0', '', '2f45c364ee2aa581c973f257612cfe80', '', '0', '0', '', NULL, 'admin', '2019-12-30 01:48:35', 'admin', '2019-12-30 01:55:40', NULL);
INSERT INTO `sys_user` VALUES (83, 108, 'guozhou17', '郭宙17', '00', 'guozhou17@qq.com', '19888888888', '0', '', '525e36cea5c59b37d6cc2fa481770b54', '', '0', '0', '', NULL, 'admin', '2019-12-30 01:48:35', 'admin', '2019-12-30 01:55:40', NULL);
INSERT INTO `sys_user` VALUES (84, 108, 'qinjia17', '秦佳17', '00', 'qinjia17@qq.com', '19888888888', '1', '', '2ed9f278f59efaf2b21f36735b24b398', '', '0', '0', '', NULL, 'admin', '2019-12-30 01:48:35', 'admin', '2019-12-30 01:55:40', NULL);
INSERT INTO `sys_user` VALUES (85, 108, 'dongjuanjuan17', '董娟娟17', '00', 'dongjuanjuan17@qq.com', '19888888888', '1', '', 'a5d212889d2417c170d54f888765fe23', '', '0', '0', '', NULL, 'admin', '2019-12-30 01:48:35', 'admin', '2019-12-30 01:55:40', NULL);
INSERT INTO `sys_user` VALUES (86, 108, 'shatianwei17', '沙天威17', '00', 'shatianwei17@qq.com', '19888888888', '0', '', '71ad4c9812bd8044047f593076fdcf76', '', '0', '0', '', NULL, 'admin', '2019-12-30 01:48:35', 'admin', '2019-12-30 01:55:40', NULL);
INSERT INTO `sys_user` VALUES (87, 109, 'chenhe18', '陈贺18', '00', 'chenhe18@qq.com', '19888888888', '0', '', '00ed0cba7031a1c43102c4e44f30345f', '', '0', '0', '', NULL, 'admin', '2019-12-30 01:48:35', 'admin', '2019-12-30 01:55:40', NULL);
INSERT INTO `sys_user` VALUES (88, 109, 'guozhou18', '郭宙18', '00', 'guozhou18@qq.com', '19888888888', '0', '', 'aa789b9dc6f4c7ff80796ddc0b74ca82', '', '0', '0', '', NULL, 'admin', '2019-12-30 01:48:35', 'admin', '2019-12-30 01:55:40', NULL);
INSERT INTO `sys_user` VALUES (89, 109, 'qinjia18', '秦佳18', '00', 'qinjia18@qq.com', '19888888888', '1', '', 'c676e4e164e07e20739dae4a2c412661', '', '0', '0', '', NULL, 'admin', '2019-12-30 01:48:35', 'admin', '2019-12-30 01:55:40', NULL);
INSERT INTO `sys_user` VALUES (90, 109, 'dongjuanjuan18', '董娟娟18', '00', 'dongjuanjuan18@qq.com', '19888888888', '1', '', '5788f02826a53106b0a557ff5a315e63', '', '0', '0', '', NULL, 'admin', '2019-12-30 01:48:35', 'admin', '2019-12-30 01:55:40', NULL);
INSERT INTO `sys_user` VALUES (91, 109, 'shatianwei18', '沙天威18', '00', 'shatianwei18@qq.com', '19888888888', '0', '', 'fb69e4c74bf49d03a8e51412f8f9e524', '', '0', '0', '', NULL, 'admin', '2019-12-30 01:48:35', 'admin', '2019-12-30 01:55:40', NULL);
INSERT INTO `sys_user` VALUES (92, 103, 'chenhe19', '陈贺19', '00', 'chenhe19@qq.com', '19888888888', '0', '', '7223a4bcdaa22d0a59e021c896996e64', '', '0', '0', '', NULL, 'admin', '2019-12-30 01:48:35', 'admin', '2019-12-30 01:55:40', NULL);
INSERT INTO `sys_user` VALUES (93, 103, 'guozhou19', '郭宙19', '00', 'guozhou19@qq.com', '19888888888', '0', '', '5bda6621389e245c1224ac3d85b4a6af', '', '0', '0', '', NULL, 'admin', '2019-12-30 01:48:35', 'admin', '2019-12-30 01:55:40', NULL);
INSERT INTO `sys_user` VALUES (94, 103, 'qinjia19', '秦佳19', '00', 'qinjia19@qq.com', '19888888888', '1', '', '296dc7521013d5289f4e3a8014ce0ab3', '', '0', '0', '', NULL, 'admin', '2019-12-30 01:48:35', 'admin', '2019-12-30 01:55:40', NULL);
INSERT INTO `sys_user` VALUES (95, 103, 'dongjuanjuan19', '董娟娟19', '00', 'dongjuanjuan19@qq.com', '19888888888', '1', '', 'b2b1422620ff16432e91bae8b843819d', '', '0', '0', '', NULL, 'admin', '2019-12-30 01:48:35', 'admin', '2019-12-30 01:55:40', NULL);
INSERT INTO `sys_user` VALUES (96, 103, 'shatianwei19', '沙天威19', '00', 'shatianwei19@qq.com', '19888888888', '0', '', '1682856889d686e6f13b7275f2a8d367', '', '0', '0', '', NULL, 'admin', '2019-12-30 01:48:35', 'admin', '2019-12-30 01:55:40', NULL);
INSERT INTO `sys_user` VALUES (97, 104, 'chenhe20', '陈贺20', '00', 'chenhe20@qq.com', '19888888888', '0', '', '0e0535343a3188d1a551c7aaaa0e75ed', '', '0', '0', '', NULL, 'admin', '2019-12-30 01:48:35', 'admin', '2019-12-30 01:55:40', NULL);
INSERT INTO `sys_user` VALUES (98, 104, 'guozhou20', '郭宙20', '00', 'guozhou20@qq.com', '19888888888', '0', '', '277970559aa960402f742188e015eb6d', '', '0', '0', '', NULL, 'admin', '2019-12-30 01:48:35', 'admin', '2019-12-30 01:55:40', NULL);
INSERT INTO `sys_user` VALUES (99, 104, 'qinjia20', '秦佳20', '00', 'qinjia20@qq.com', '19888888888', '1', '', '6797e8014e68ea7090397e62e7adaf17', '', '0', '0', '', NULL, 'admin', '2019-12-30 01:48:35', 'admin', '2019-12-30 01:55:40', NULL);
INSERT INTO `sys_user` VALUES (100, 104, 'dongjuanjuan20', '董娟娟20', '00', 'dongjuanjuan20@qq.com', '19888888888', '1', '', '6c8ce6ee88758f9beaacfc201b706568', '', '0', '0', '', NULL, 'admin', '2019-12-30 01:48:35', 'admin', '2019-12-30 01:55:40', NULL);
INSERT INTO `sys_user` VALUES (101, 104, 'shatianwei20', '沙天威20', '00', 'shatianwei20@qq.com', '19888888888', '0', '', 'ad945d18139f1abaf8fa0d560793b9a1', '', '0', '0', '', NULL, 'admin', '2019-12-30 01:48:35', 'admin', '2019-12-30 01:55:40', NULL);
INSERT INTO `sys_user` VALUES (102, 201, 'chenhe21', '陈贺21', '00', 'chenhe21@qq.com', '19888888888', '0', '', '844dee732e7a515d630fa1b9eec5b455', '', '0', '0', '', NULL, 'admin', '2019-12-30 01:48:35', 'admin', '2019-12-30 01:55:40', NULL);
INSERT INTO `sys_user` VALUES (103, 201, 'guozhou21', '郭宙21', '00', 'guozhou21@qq.com', '19888888888', '0', '', 'f167eb26535ad07d6316af1b71078f7d', '', '0', '0', '', NULL, 'admin', '2019-12-30 01:48:35', 'admin', '2019-12-30 01:55:40', NULL);
INSERT INTO `sys_user` VALUES (104, 201, 'qinjia21', '秦佳21', '00', 'qinjia21@qq.com', '19888888888', '1', '', '8f8895554f2ba4c11c78f988b3e32ac1', '', '0', '0', '', NULL, 'admin', '2019-12-30 01:48:35', 'admin', '2019-12-30 01:55:40', NULL);
INSERT INTO `sys_user` VALUES (105, 201, 'dongjuanjuan21', '董娟娟21', '00', 'dongjuanjuan21@qq.com', '19888888888', '1', '', '9670f5a41c2d9aa3a25a7829c40f8c57', '', '0', '0', '', NULL, 'admin', '2019-12-30 01:48:35', 'admin', '2019-12-30 01:55:40', NULL);
INSERT INTO `sys_user` VALUES (106, 201, 'shatianwei21', '沙天威21', '00', 'shatianwei21@qq.com', '19888888888', '0', '', '1aa79412537ce94292b1f2b14a7af1a5', '', '0', '0', '', NULL, 'admin', '2019-12-30 01:48:35', 'admin', '2019-12-30 01:55:40', NULL);
INSERT INTO `sys_user` VALUES (107, 202, 'chenhe22', '陈贺22', '00', 'chenhe22@qq.com', '19888888888', '0', '', 'e935461516a9e7733a5c62b937f741cc', '', '0', '0', '', NULL, 'admin', '2019-12-30 01:48:35', 'admin', '2019-12-30 01:55:40', NULL);
INSERT INTO `sys_user` VALUES (108, 202, 'guozhou22', '郭宙22', '00', 'guozhou22@qq.com', '19888888888', '0', '', '7e5061eb683f5d32008aaf6408146146', '', '0', '0', '', NULL, 'admin', '2019-12-30 01:48:35', 'admin', '2019-12-30 01:55:40', NULL);
INSERT INTO `sys_user` VALUES (109, 202, 'qinjia22', '秦佳22', '00', 'qinjia22@qq.com', '19888888888', '1', '', 'f34adaa6426676872fac7a818d876b94', '', '0', '0', '', NULL, 'admin', '2019-12-30 01:48:35', 'admin', '2019-12-30 01:55:40', NULL);
INSERT INTO `sys_user` VALUES (110, 202, 'dongjuanjuan22', '董娟娟22', '00', 'dongjuanjuan22@qq.com', '19888888888', '1', '', '1161c7b74e4f7b96fbde24098fb0b641', '', '0', '0', '', NULL, 'admin', '2019-12-30 01:48:35', 'admin', '2019-12-30 01:55:40', NULL);
INSERT INTO `sys_user` VALUES (111, 202, 'shatianwei22', '沙天威22', '00', 'shatianwei22@qq.com', '19888888888', '0', '', '2dcbecbe39ffca87471bf28cf010f926', '', '0', '0', '', NULL, 'admin', '2019-12-30 01:48:35', 'admin', '2019-12-30 01:55:40', NULL);
INSERT INTO `sys_user` VALUES (112, 108, 'chenhe23', '陈贺23', '00', 'chenhe23@qq.com', '19888888888', '0', '', 'd5523a06342b86eacb1d374a719c2aad', '', '0', '0', '', NULL, 'admin', '2019-12-30 01:48:35', 'admin', '2019-12-30 01:55:40', NULL);
INSERT INTO `sys_user` VALUES (113, 108, 'guozhou23', '郭宙23', '00', 'guozhou23@qq.com', '19888888888', '0', '', 'f9c2412e6f0a93f7ee14b616ced70b0c', '', '0', '0', '', NULL, 'admin', '2019-12-30 01:48:35', 'admin', '2019-12-30 01:55:40', NULL);
INSERT INTO `sys_user` VALUES (114, 108, 'qinjia23', '秦佳23', '00', 'qinjia23@qq.com', '19888888888', '1', '', '1ed736f915b242a085cdc1004271d5dc', '', '0', '0', '', NULL, 'admin', '2019-12-30 01:48:35', 'admin', '2019-12-30 01:55:40', NULL);
INSERT INTO `sys_user` VALUES (115, 108, 'dongjuanjuan23', '董娟娟23', '00', 'dongjuanjuan23@qq.com', '19888888888', '1', '', '02c03597b73f2df688251cf71f8ee254', '', '0', '0', '', NULL, 'admin', '2019-12-30 01:48:35', 'admin', '2019-12-30 01:55:40', NULL);
INSERT INTO `sys_user` VALUES (116, 108, 'shatianwei23', '沙天威23', '00', 'shatianwei23@qq.com', '19888888888', '0', '', 'aadc997b5ee9750669f2ca48201be7e0', '', '0', '0', '', NULL, 'admin', '2019-12-30 01:48:35', 'admin', '2019-12-30 01:55:40', NULL);
INSERT INTO `sys_user` VALUES (117, 109, 'chenhe24', '陈贺24', '00', 'chenhe24@qq.com', '19888888888', '0', '', '731f582322b71d727ade457d10d812b4', '', '0', '0', '', NULL, 'admin', '2019-12-30 01:48:35', 'admin', '2019-12-30 01:55:40', NULL);
INSERT INTO `sys_user` VALUES (118, 109, 'guozhou24', '郭宙24', '00', 'guozhou24@qq.com', '19888888888', '0', '', '0a6d63b9f6109568b26396b8da4f54ab', '', '0', '0', '', NULL, 'admin', '2019-12-30 01:48:35', 'admin', '2019-12-30 01:55:40', NULL);
INSERT INTO `sys_user` VALUES (119, 109, 'qinjia24', '秦佳24', '00', 'qinjia24@qq.com', '19888888888', '1', '', '9208b5b7e3721d5df252d1403cd33e0b', '', '0', '0', '', NULL, 'admin', '2019-12-30 01:48:35', 'admin', '2019-12-30 01:55:41', NULL);
INSERT INTO `sys_user` VALUES (120, 109, 'dongjuanjuan24', '董娟娟24', '00', 'dongjuanjuan24@qq.com', '19888888888', '1', '', '23369f60e02fc1f64d014828c8dab039', '', '0', '0', '', NULL, 'admin', '2019-12-30 01:48:35', 'admin', '2019-12-30 01:55:41', NULL);
INSERT INTO `sys_user` VALUES (121, 109, 'shatianwei24', '沙天威24', '00', 'shatianwei24@qq.com', '19888888888', '0', '', '9c9760ce8f783be8d7c13bb009a7991c', '', '0', '0', '', NULL, 'admin', '2019-12-30 01:48:35', 'admin', '2019-12-30 01:55:41', NULL);

-- ----------------------------
-- Table structure for sys_user_online
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_online`;
CREATE TABLE `sys_user_online`  (
  `sessionId` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '用户会话id',
  `login_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '登录账号',
  `dept_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '部门名称',
  `ipaddr` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '登录IP地址',
  `login_location` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '登录地点',
  `browser` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '浏览器类型',
  `os` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '操作系统',
  `status` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '在线状态on_line在线off_line离线',
  `start_timestamp` datetime(0) NULL DEFAULT NULL COMMENT 'session创建时间',
  `last_access_time` datetime(0) NULL DEFAULT NULL COMMENT 'session最后访问时间',
  `expire_time` int(5) NULL DEFAULT 0 COMMENT '超时时间，单位为分钟',
  PRIMARY KEY (`sessionId`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '在线用户记录' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for sys_user_post
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_post`;
CREATE TABLE `sys_user_post`  (
  `user_id` bigint(20) NOT NULL COMMENT '用户ID',
  `post_id` bigint(20) NOT NULL COMMENT '岗位ID',
  PRIMARY KEY (`user_id`, `post_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '用户与岗位关联表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_user_post
-- ----------------------------
INSERT INTO `sys_user_post` VALUES (1, 1);
INSERT INTO `sys_user_post` VALUES (2, 2);

-- ----------------------------
-- Table structure for sys_user_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_role`;
CREATE TABLE `sys_user_role`  (
  `user_id` bigint(20) NOT NULL COMMENT '用户ID',
  `role_id` bigint(20) NOT NULL COMMENT '角色ID',
  PRIMARY KEY (`user_id`, `role_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '用户和角色关联表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_user_role
-- ----------------------------
INSERT INTO `sys_user_role` VALUES (1, 1);
INSERT INTO `sys_user_role` VALUES (2, 101);
INSERT INTO `sys_user_role` VALUES (3, 101);
INSERT INTO `sys_user_role` VALUES (4, 101);
INSERT INTO `sys_user_role` VALUES (5, 101);
INSERT INTO `sys_user_role` VALUES (6, 101);
INSERT INTO `sys_user_role` VALUES (7, 100);
INSERT INTO `sys_user_role` VALUES (8, 100);
INSERT INTO `sys_user_role` VALUES (9, 100);
INSERT INTO `sys_user_role` VALUES (10, 100);
INSERT INTO `sys_user_role` VALUES (11, 100);
INSERT INTO `sys_user_role` VALUES (12, 2);
INSERT INTO `sys_user_role` VALUES (13, 2);
INSERT INTO `sys_user_role` VALUES (14, 2);
INSERT INTO `sys_user_role` VALUES (15, 2);
INSERT INTO `sys_user_role` VALUES (16, 2);
INSERT INTO `sys_user_role` VALUES (17, 2);
INSERT INTO `sys_user_role` VALUES (18, 2);
INSERT INTO `sys_user_role` VALUES (19, 2);
INSERT INTO `sys_user_role` VALUES (20, 2);
INSERT INTO `sys_user_role` VALUES (21, 2);
INSERT INTO `sys_user_role` VALUES (22, 2);
INSERT INTO `sys_user_role` VALUES (23, 2);
INSERT INTO `sys_user_role` VALUES (24, 2);
INSERT INTO `sys_user_role` VALUES (25, 2);
INSERT INTO `sys_user_role` VALUES (26, 2);
INSERT INTO `sys_user_role` VALUES (27, 2);
INSERT INTO `sys_user_role` VALUES (28, 2);
INSERT INTO `sys_user_role` VALUES (29, 2);
INSERT INTO `sys_user_role` VALUES (30, 2);
INSERT INTO `sys_user_role` VALUES (31, 2);
INSERT INTO `sys_user_role` VALUES (32, 2);
INSERT INTO `sys_user_role` VALUES (33, 2);
INSERT INTO `sys_user_role` VALUES (34, 2);
INSERT INTO `sys_user_role` VALUES (35, 2);
INSERT INTO `sys_user_role` VALUES (36, 2);
INSERT INTO `sys_user_role` VALUES (37, 2);
INSERT INTO `sys_user_role` VALUES (38, 2);
INSERT INTO `sys_user_role` VALUES (39, 2);
INSERT INTO `sys_user_role` VALUES (40, 2);
INSERT INTO `sys_user_role` VALUES (41, 2);
INSERT INTO `sys_user_role` VALUES (42, 2);
INSERT INTO `sys_user_role` VALUES (43, 2);
INSERT INTO `sys_user_role` VALUES (44, 2);
INSERT INTO `sys_user_role` VALUES (45, 2);
INSERT INTO `sys_user_role` VALUES (46, 2);
INSERT INTO `sys_user_role` VALUES (47, 2);
INSERT INTO `sys_user_role` VALUES (48, 2);
INSERT INTO `sys_user_role` VALUES (49, 2);
INSERT INTO `sys_user_role` VALUES (50, 2);
INSERT INTO `sys_user_role` VALUES (51, 2);
INSERT INTO `sys_user_role` VALUES (62, 2);
INSERT INTO `sys_user_role` VALUES (63, 2);
INSERT INTO `sys_user_role` VALUES (64, 2);
INSERT INTO `sys_user_role` VALUES (65, 2);
INSERT INTO `sys_user_role` VALUES (66, 2);
INSERT INTO `sys_user_role` VALUES (67, 2);
INSERT INTO `sys_user_role` VALUES (68, 2);
INSERT INTO `sys_user_role` VALUES (69, 2);
INSERT INTO `sys_user_role` VALUES (70, 2);
INSERT INTO `sys_user_role` VALUES (71, 2);
INSERT INTO `sys_user_role` VALUES (72, 2);
INSERT INTO `sys_user_role` VALUES (73, 2);
INSERT INTO `sys_user_role` VALUES (74, 2);
INSERT INTO `sys_user_role` VALUES (75, 2);
INSERT INTO `sys_user_role` VALUES (76, 2);
INSERT INTO `sys_user_role` VALUES (77, 2);
INSERT INTO `sys_user_role` VALUES (78, 2);
INSERT INTO `sys_user_role` VALUES (79, 2);
INSERT INTO `sys_user_role` VALUES (80, 2);
INSERT INTO `sys_user_role` VALUES (81, 2);
INSERT INTO `sys_user_role` VALUES (82, 2);
INSERT INTO `sys_user_role` VALUES (83, 2);
INSERT INTO `sys_user_role` VALUES (84, 2);
INSERT INTO `sys_user_role` VALUES (85, 2);
INSERT INTO `sys_user_role` VALUES (86, 2);
INSERT INTO `sys_user_role` VALUES (87, 2);
INSERT INTO `sys_user_role` VALUES (88, 2);
INSERT INTO `sys_user_role` VALUES (89, 2);
INSERT INTO `sys_user_role` VALUES (90, 2);
INSERT INTO `sys_user_role` VALUES (91, 2);
INSERT INTO `sys_user_role` VALUES (92, 2);
INSERT INTO `sys_user_role` VALUES (93, 2);
INSERT INTO `sys_user_role` VALUES (94, 2);
INSERT INTO `sys_user_role` VALUES (95, 2);
INSERT INTO `sys_user_role` VALUES (96, 2);
INSERT INTO `sys_user_role` VALUES (112, 2);
INSERT INTO `sys_user_role` VALUES (113, 2);
INSERT INTO `sys_user_role` VALUES (114, 2);
INSERT INTO `sys_user_role` VALUES (115, 2);
INSERT INTO `sys_user_role` VALUES (116, 2);
INSERT INTO `sys_user_role` VALUES (117, 2);
INSERT INTO `sys_user_role` VALUES (118, 2);
INSERT INTO `sys_user_role` VALUES (119, 2);
INSERT INTO `sys_user_role` VALUES (120, 2);
INSERT INTO `sys_user_role` VALUES (121, 2);

-- ----------------------------
-- Table structure for useraccount
-- ----------------------------
DROP TABLE IF EXISTS `useraccount`;
CREATE TABLE `useraccount`  (
  `UserID` int(4) NOT NULL COMMENT '用户编号(主键)',
  `Name` char(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '用户姓名',
  `UserNum` int(11) NULL DEFAULT NULL COMMENT '账号',
  `Password` char(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '登录密码',
  `Department` char(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '所属部门',
  `Tel` char(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '手机号',
  `Email` char(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '邮箱',
  `Sex` char(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '性别',
  `Position` char(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '职位',
  `Address` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '住址',
  `Flag` int(4) NULL DEFAULT NULL COMMENT '在线状态',
  `Identificat` char(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '身份证号',
  `Entry_Time` date NULL DEFAULT NULL COMMENT '入职时间',
  `Remark` char(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`UserID`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '系统用户信息表\n' ROW_FORMAT = Dynamic;

SET FOREIGN_KEY_CHECKS = 1;
